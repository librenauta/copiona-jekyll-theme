---
layout: post
title: 'notas de un librenautadmin en gnu/linux'
image:
  path: public/ddfd3cfa-59e0-4713-ae35-ebe7a55c77de/notas.png
category: proyectos
author: librenauta
tags:
  - administrar
uuid: ddfd3cfa-59e0-4713-ae35-ebe7a55c77de
order: 24
draft: false
last_modified_at: 2018-01-20 21:30:21.155018278 +00:00
---
# cositas que me ayudan en la terminal :D

# deploy sitios en copiona.com

1. asignar subdominio en [njal.la](https://njal.la/domains/copiona.com/)

2. editar `/etc/nginx/nginx.conf`

~~~bash
server {
        server_name $sitio;
        root /srv/$sitio;
        index index.html;
}
~~~

comprobar si esta ok la sintaxis del archivo de nginx `nginx -t`

3. certbot

para crear un nuevo cert al dominio

`sudo certbot --nginx`

seleccionar dominio de la lista con el numero

4. clonar repositirio con sitio en

`/srv/$sitio`

5. done :D

# youtube-dl | yt-dlp

1. descargar un tema,playlist o album desde bandcamp.com/youtube

`yt-dlp -x --audio-format mp3 "$url"``

[more](https://github.com/ytdl-org/youtube-dl/blob/master/README.md)

# grabar pantalla

1280x720 es mi resolución, cambiar a la tuya, requiere ffmpeg instalado.

`ffmpeg -f x11grab -s 1366x768 -r 25 -i $DISPLAY -f alsa -i default -c:v libx264 -b:v 400k -s 1280x720 grabacion.mp4`

# convertir cosas

1. pasar una pagina de pdf a png en buena calidad

`convert -density 300 2-bille.pdf -quality 100 2-bille.png`

2. convertir subtitulos
`ffmpeg -i subtitulo.srt subtitulo.vtt`

3. agrupar varios .png/.jpg en un pdf de acuerdo al nombre de los archivos (numerados mejor)
`convert *.png documento.pdf`

# borrar cosas

`ncdu` <3

# mi config de i3

aprendi bastante desde [acá](https://www.youtube.com/watch?v=Bw5sDLOvN20)

instalar dmenu :D

[conf](public/archivos/i3-config)

4. reemplazar cosas en documentos

`find . -type f -exec sed -i 's/copiona.com/mochila.copiona.com/g' {} \;`


# instalar linux fedora 36
```
                          .,,uod8B8bou,,.
              ..,uod8BBBBBBBBBBBBBBBBRPFT?l!i:.
         ,=m8BBBBBBBBBBBBBBBRPFT?!||||||||||||||
         !...:!TVBBBRPFT||||||||||!!^^""'   ||||
         !.......:!?|||||!!^^""'            ||||
         !.........||||                     ||||
         !.........||||       ~copiona      ||||
         !.........||||     by librenauta   ||||
         !.........||||                     ||||s
         !.........||||                     ||||
         !.........||||           <3        ||||
         `.........||||                    ,||||
          .;.......||||               _.-!!|||||
   .,uodWBBBBb.....||||       _.-!!|||||||||!:'
!YBBBBBBBBBBBBBBb..!|||:..-!!|||||||!iof68BBBBBb....
!..YBBBBBBBBBBBBBBb!!||||||||!iof68BBBBBBRPFT?!::   `.
!....YBBBBBBBBBBBBBBbaaitf68BBBBBBRPFT?!:::::::::     `.
!......YBBBBBBBBBBBBBBBBBBBRPFT?!::::::;:!^"`;:::       `.  
!........YBBBBBBBBBBRPFT?!::::::::::cert^''...::::::;         iBBbo.
`..........YBRPFT?!::::::::::::::::::::::::;iof68bo.      WBBBBbo.
  `..........:::::::::::::::::::::::;iof688888888888b.     `YBBBP^'
    `........::::::::::::::::;iof688888888888888888888b.     `
      `......:::::::::;iof688888888888888888888888888888b.
        `....:::;iof688888888888888888888888888888888899fT!  
          `..::!8888888888888888888888888888888899fT|!^"'   
            `' !!988888888888888888888888899fT|!^"'
                `!!8888888888888888899fT|!^"'
                  `!988888888899fT|!^"'
                    `!9899fT|!^"'
                      `!^"'

```

sudo dnf install update

comandos luego de instalar OS

## gestor de ventanas:


sudo dnf install i3blocks
cp .config/i3blocks en el nuevo.config/i3blocks

sudo dnf install i3-gaps

sudo dnf install feh (background)

sudo dnf install gimp

sudo dnf install arandr (gestor de monitores)

## ruby

sudo dnf install ruby ruby-devel

para instalar timetrap

https://github.com/librenauta/timetrap

sudo dnf install sqlite-devel

gem install timetrap

## shell

https://gitlab.com/perrotuerto/sexy-bash-prompt
https://github.com/twolfson/sexy-bash-prompt

## telegram

https://www.linuxcapable.com/how-to-install-telegram-on-fedora-35/

eliminar barra nombre a las terminales de gnome-terminal
https://askubuntu.com/questions/1230157/how-to-remove-title-bar-from-terminal-on-the-new-ubuntu-20-04

```gsettings set org.gnome.Terminal.Legacy.Settings headerbar "@mb false"```

## git

```
git config user.email librenauta@riseup.net

git config user.name librenauta
```

```sudo dnf install git-lfs```

```sudo dnf install tig```


install xinput


## touchpad
tap to click
https://major.io/2021/07/18/tray-icons-in-i3/

## montar disco cifrado

`udisksctl unlock -b /dev/sdb3`
`udisksctl mount -b /dev/mapper/fedora-home` 
//para montar el home del disco, fedora-root para montar el root

```sudo dnf install thunderbird```

```sudo dnf install vlc```


## decifrar llaves pgp

https://www.gnupg.org/gph/es/manual/x129.html


## setear backkground para inicio de session [fuente](https://www.debugpoint.com/change-login-background-fedora/)

```sudo dnf copr enable zirix/gdm-wallpaper```

```sudo dnf install gdm-wallpaper```

```sudo set-gdm-wallpaper /your/image/path```

## utils video

```sudo dnf install ffmpeg```

## editorial p2p

```sudo wget https://gitlab.com/snippets/1917490/raw -P /usr/local/bin && sudo mv /usr/local/bin/raw /usr/local/bin/export-pdf && sudo chmod +755 /usr/local/bin/export-pdf```



`sudo dnf install pandoc`
`sudo dnf -y install texlive-pdfbook2)`
`sudo dnf install texlive-scheme-full`

## download youtube-dl
`sudo dnf install youtube-dl`

## transmission p2p client
`sudo dnf install transmission`

## enviar cambios a copiona.com

`rsync -av _site/ root@copiona.com:/srv/copiona.com`

## hacer andar scanner epson stylus CX4500 en fedora

para saber si tenemos el scanner detectado en el usb
`sudo lsusb`

ir a : [epson](https://download.ebz.epson.net/dsc/search/01/search/searchModule)
descargar el paquete desde [acá](http://support.epson.net/linux/en/iscan_c.php?version=2.30.4)

o desde este [backup](public/ddfd3cfa-59e0-4713-ae35-ebe7a55c77de/scanner/iscan-bundle-2.30.4.x64.rpm.tar.gz)

para ejecutar el programa

`iscan`

# recorrer linea por linea de un archivo y ecjecutar una accion en bash

`while IFS= read -r line; do cp $line /tmp/ ; done < txt.md `