---
layout: post
title: 'Collage para Telegrafica RedMundialCasera'
category: proyectos
tags:
- diseño
- colash
author: librenauta
order: 1
draft: false
uuid: 6afb8ec6-05fb-4b2a-ade2-e7dd9ca73cb5
last_modified_at: 2018-07-20 12:30:21.155018278 +00:00
---
# /Obediencia Tecnológica

_Tele_gráfica fue un proyecto autogestivo de taller [Caput by gustako](https://gustako.wordpress.com/)
me convocaron a participar con un collage :3
![obediencia](public/proyectos/telegrafica/telegrafica.png)
1. [obediencia tecnológica para descargar en .svg](public/proyectos/telegrafica/telegrafica.svg)

# 2020 - COVID-19
> este collage está en la portada del dossier [desobediencia.partidopirata.com.ar](https://desobediencia.partidopirata.com.ar)
