---
layout: post
title: 'TAPA "Queremos redes libres"'
tags:
  - redeslibres
  - internet
category: proyectos
author: librenauta
uuid: e7d09676-c322-4c96-9218-9aeef2d5ab42
order: 4
draft: false
last_modified_at: 2020-02-10 12:30:21.155018278 +00:00
---
# Queremos redes libres, edición mx del zine hecho por el [PIP](https://partidopirata.com.ar)

y nuevamente editado para la campaña [\<Salvemos internet>](https://salvemosinternet.tk/) con aportes de diferentes territorios de América latina y el caribe.


![queremos-redes-libres](public/proyectos/queremos-redes-libres-ed-mx/cover.png)
diseño de tapa by @librenauta
1. [descargar el txt en epub](public/proyectos/queremos-redes-libres-ed-mx/redes-libres.epub)
2. [descargar el txt en pdf](public/proyectos/queremos-redes-libres-ed-mx/redes-libres.pdf)
3. [descargar la portada en svg](public/proyectos/queremos-redes-libres-ed-mx/cover.svg)
4. [descargar portada y contratapa en pdf](public/proyectos/queremos-redes-libres-ed-mx/redes-libres_forros.pdf)

## Por otras telecomunicaciones posibles

> Conoce sobre la lucha para salvar internet y la búsqueda de redes autónomas y comunitarias en diversas regiones de América Latina y el Caribe.
