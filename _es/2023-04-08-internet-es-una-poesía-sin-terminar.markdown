---
title: Risografía - internet es una poesía sin terminar
author: Librenauta
description: Fanzine de internet es una poesía sin terminar impreso con la tecnica de risografía
image:
  path: public/6af6ba20-de51-4247-a233-2ac562552cf4/cover.jpg
  description:
category: proyectos
tags:
- fanzine
- risograph
- barcelona
draft: false
order:
picture_multiple: public/6af6ba20-de51-4247-a233-2ac562552cf4/images
layout: post
uuid: 6af6ba20-de51-4247-a233-2ac562552cf4
last_modified_at: 2023-01-23 23:20:21.155018278 +00:00
liquid: false
---

Viajé a barcelona y unos días antes pude contactarme con [dotheprint](https://dotheprint.es/) un pequeño estudio de impresión de risografía para hacer algunas copias locales. La verdad no sabía muy bien como sería el resultado de la impresión asi que me vi unos tutoriales de como generar cada capa en youtube color por color y mande a imprimir un fanzine que diseñe en viaje llamado: _internet es una poesía sin terminar_ 


> Un poco engloba cuál es la internet que quiero y por qué :F

![internet-es-una-poesía-sin-terminar](public/6af6ba20-de51-4247-a233-2ac562552cf4/internet-es-una-poesía-sin-terminar.png)

---

[acá](public/6af6ba20-de51-4247-a233-2ac562552cf4/internet_es_una_poesia_sin_terminar_v2_colores.pdf) se puede puede bajar el pdf para leerlo online e irpimirlo [formato A3]

[acá](public/6af6ba20-de51-4247-a233-2ac562552cf4/internet_es_una_poesia_sin_terminar_v2_negro.pdf) una version en formato magenta y negro para poder imprimir en láser [formato A3]

[acá](public/6af6ba20-de51-4247-a233-2ac562552cf4/internet-es-una-poesía-sin-terminar-riso-layer.zip) se pueden bajar un .zip con los archivos separados por color para imprimir en risografía.



Acá una muestra de las capas, el diseño esta hecho en inkscape en 2 colores + opacidades y luego procesado en gimp separando cada color en una máscara y exportandolo :D

Es importante saber que la máquina de risografía sólo lee valores en escala de grises. por eso las placas siguientes están en negro, el color lo aplica cuando imprime cada tambor. ∩｀-´)⊃━( . °°. )  