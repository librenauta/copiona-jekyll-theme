---
title: Quemar un libro 7 [ Verano Dibujado ]
description: Verano dibujado es una publicación de Proyectos Dibujados
title_page: ~/Quemar un libro a la luz de un scanner [ Verano Dibujado ]
author: librenauta
image:
  path: public/cf2cfa45-e5be-441c-8847-368816fd2ff0/cover.jpg
  description: página de verano dibujado
permalink:
category: scann
tags:
- risografía
- book
- córdoba
- verano
draft: false
order:
layout: post
uuid: cf2cfa45-e5be-441c-8847-368816fd2ff0
liquid: false
last_modified_at: 2023-01-16 15:20:21.155018278 +00:00
---

# Verano dibujado - 50 ediciones

Cuando estuve en la [mani](https://www.manifestivalgrafico.ar/) en córdoba, me traje esta maravilla en riso, en sync con el clima. pleno verano. 

aquí el [scann para descarga <3](public/cf2cfa45-e5be-441c-8847-368816fd2ff0/verano-dibujado.pdf) para mirar de cerquita, imprimir, copiar y compartir.
