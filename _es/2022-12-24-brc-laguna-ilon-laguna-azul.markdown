---
title: BRC - Laguna Ilon + Laguna Azul
author: Librenauta
description: log de imágenes de una travesía en brc
image:
  path: public/7695c93e-71d7-44f2-9a1e-a75d64fd8ed6/01.jpg
  description:
picture_multiple: public/7695c93e-71d7-44f2-9a1e-a75d64fd8ed6/images
category: proyectos
enable_gpx_traces: true
gpx: public/7695c93e-71d7-44f2-9a1e-a75d64fd8ed6/2022-12-24_15-17_Sat.gpx
tags:
- brc
- naturaleza
- sur
draft: false
order:
layout: post
uuid: 7695c93e-71d7-44f2-9a1e-a75d64fd8ed6
last_modified_at: 2022-12-24 22:20:21.155018278 +00:00
liquid: false
---

# BRC - Laguna Ilon + Laguna Azul
