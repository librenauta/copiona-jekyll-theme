---
layout: post
title: 'Asimov'
tags:
  - ASIMOV
  - 3dprint
  - Juego
  - diseño industrial
category: proyectos
author: librenauta
uuid: e3db9d53-3f53-43f5-a499-e716435a4363
order: 15
draft: false
last_modified_at: 2020-08-03 22:30:21.155018278 +00:00
---

# /Asimov/

### Juego Modular Colaborativo

Asimov es un juego de ensamble colaborativo para niñes y no tan niñes, un conjunto de módulos espaciales que permiten construir una estación no tripulada en una ventana hogareña.
El objetivo es poder utilizar todas las piezas y todos los recursos no verbales para dejar flotando uno de los tantos modelos listados en las tarjetas "ASI".
Este juego de construcción requiere de 2 o más participantes necesariamente, ya que necesitarás un compañere que suspenda un módulo espacial del otro lado y así dejar flotando el primero para luego ir construyendo toda la estación

![img-1](public/proyectos/asimov/asimov-1.jpg)

![img-1](public/proyectos/asimov/asimov-2.jpg)

<iframe src="https://player.vimeo.com/video/444058128" width="100%" height="420" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/444058128">ASIMOV/ | MODULAR GAME</a> from <a href="https://vimeo.com/librenauta">librenauta</a></p>

### Ventanas y Retroalimentación

La experiencia visual de asimov está basada conceptualmente en los caleidoscopios, en busca de un recurso para lograr una retroalimentación de imágenes. Esta mezcla logra generar una experiencia que se combina entre luz, combinación de colores, destreza, prueba y error <3 <3

En el sitio podés ver más información y descargar los modelos 3d en .stl para imprimir.

[asimov.copiona.com](https://asimov.copiona.com)
