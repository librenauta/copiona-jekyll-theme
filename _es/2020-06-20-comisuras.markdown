---
layout: post
title: 'Comisuras'
tags:
  - comiditas
  - recetas
  - vegan
category: proyectos
author: librenauta & cian
uuid: 3e574f7a-3ed5-4f71-9115-8b7951fac41d
order: 13
draft: false
last_modified_at: 2020-06-20 22:30:21.155018278 +00:00
---

# Comisuras, recetas para disfrutar~

>Comisuras fue nombrado por un **autocorrect**, el día que quise escribir "comiditas" en un mensaje de txt.

[Comisuras](https://comisuras.copiona.com) es un backup de recetas que voy haciendo con @soycian de vez en cuando para docuentar qué comemos. También para compartir las recetas y los remix de las recetas que alguna vez nos compartieron, amigues, xadres, abuelas e internet.

![img-1](public/proyectos/comisuras/comisuras.png)

Las recetas son uno de los mejores ejemplos de cómo la cultura se transforma, de cómo cada une le pone su "toque", gusto , experiencia en la creación de una comida. las recetas son libres, y como libres las compartimos con ustedes, como alguien nos las compartió a nosotzs.

> visitar [--> Comisuras](https://comisuras.copiona.com)
