---
layout: post
title: 'Bankrupt!'
tags:
  - bankrupt
  - música
  - online
category: proyectos
author: librenauta
uuid: b500b797-074e-4422-9fa1-2d6c5e800256
order: 23
draft: false
last_modified_at: 2020-12-23 22:30:21.155018278 +00:00
---



### _bankrupt es un portal de música online <3, sin publicidad, sin registro y gratis_

El objetivo de esta web es ser un reproductor de música, básicamente con tracks y albums que se encuentran accesibles y de descarga fácil en internet/p2p.

la idea es generar el sitio y que pueda ser replicado por la que quiera, clonando el repositorio y agregando su música.

y por qué haces esto librenauta?, porque tenemos acceso a la cultura con internet y como actualmente está todo monetizado en servicios extranjeros que extraen los datos de las personas con sus app/spyware (spoti), mi aporte es liberar esos bits que no generan NINGUNA perdida [(KH000 // Kopimashin)](https://konsthack.se/portfolio/kh000-kopimashin/) y [(this)](https://torrentfreak.com/pirate-bay-founder-builds-the-ultimate-piracy-machine-151219/) en términos monetarios, si no que multiplican el poder de transformación. pasen, pongan play y disfruten de lo que es de todzs.

![lovelike](public/proyectos/bankrupt/lovelike.png)

> hoy es [phoenixmania](https://phoenixmania.github.io) pero el rework será el siguiente.



## Aquí unos mockups.
![bankrupt-1](public/proyectos/bankrupt/bankrupt-1.png)

![bankrupt-1](public/proyectos/bankrupt/bankrupt-2.png)

![bankrupt-1](public/proyectos/bankrupt/bankrupt-3.png)
