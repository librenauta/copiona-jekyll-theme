---
title: El internet no es un archivo es un performance
author: Doreen A. Ríos
description: El internet no es un archivo es un performance
proyect: f804d881-5340-465a-a445-0ae0e4853bde
image:
  path: editorial/el-internet-no-es-un-archivo-es-un-performance/el-internet-no-es-un-archivo-es-un-performance-cover.png
  description: shanzhai-byung-chul-han-copia.
pdf_read: editorial/el-internet-no-es-un-archivo-es-un-performance/el-internet-no-es-un-archivo-es-un-performance.pdf
pdf_cover: editorial/el-internet-no-es-un-archivo-es-un-performance/el-internet-no-es-un-archivo-es-un-performance-cover.pdf
pdf_imposition: editorial/el-internet-no-es-un-archivo-es-un-performance/el-internet-no-es-un-archivo-es-un-performance_imposition.pdf
category: editorial
export_pdf: 'export-pdf --title="El internet no es un archivo es un performance" --author="Doreen A. Rios" --press="pixel2pixel" --template="a6template.tex" --no-day Teclas presionadas en un dia de trabajo cognitivo.md'
tags:
- internet
- archivo
- netart
- libro
draft:
order:
layout: book
uuid: 9db959e9-265e-4ef8-bc4b-0b59827ed666
liquid: false
---

# El internet no es un archivo, es un performance

> Por Doreen A. Ríos [^0]

>_texto originalmente comisionado y publicado por **NMenos1**_[^1]

[^0]: https://doreenrios.com

[^1]: https://www.nmenos1.xyz/public/texto/web

Habitar internet es habitar varias paradojas simultáneas que se deslizan entre
la hipervisibilidad y la hipervigilancia; entre el acceso masificado a la
información y las cámaras de eco, propiciadas por nuestros algoritmos
personalizados. Y, por supuesto, entre la inminente vulnerabilidad de los
formatos digitales y la idea de que lo que vive en internet estará ahí por
siempre.

Es claro que la temprana promesa del internet, de generar una aldea global donde
las subjetividades personales se dislocan para conectar con otrxs, ha quedado muy
atrás en este paseo por la red de redes, y a esto se suma un gesto peculiar donde
el mismo paso del tiempo parece multiplicar su velocidad en cada _click_, que trae
consigo una nueva ola de imposiciones, restricciones, fragmentaciones y, de vez en
cuando, oportunidades de reconfiguración. En otras palabras, como bien dice Michael
Connor, ha sido una década extraña para haber servido como porrista profesional de
la cultura de internet.

Pero ¿cómo llegamos hasta acá? La respuesta a esta pregunta, así como las fibras
que sostienen a las comunidades en internet, se teje y desteje dependiendo de quién
la responda, pero para conectarles con mi terruño de píxeles, iniciaré donde, para
esta porrista profesional de la cultura de internet, comenzó todo: un cibercafé.


Por ahí del 2003 yo tenía 11 años y pasaba mucho tiempo en el cibercafé más
cercano a la casa de mi abuela, el aún inmortal Cibercafé Y2K. Allí pasaba
varias horas descargando canciones del difunto Ares, quemando CDs, jugando
minijuegos en línea y, por supuesto, chateando con extrañxs de la WWW. En esas
épocas me parecía muy emocionante entrar a foros de diversos temas e
intercambiar ideas con personas desconocidas. Aquí, en algún foro sobre
historias paranormales, _creepypastas_, aliens y la sexta dimensión, me encontré
con JODI, pues algunx usuarix de estos lares decidió que este era el espacio ideal
para discutir esta _metapieza_ del net art temprano. Recuerdo que la primera
publicación sobre el tema la discutía como un hallazgo curseado de las aguas profundas
del internet, pues dentro de este sitio web, con una cantidad anormal de doble us,
imperaba un aire de rareza que levantaba teorías de conspiración acerca de sus
creadores y su posible función.

Recuerdo haber escrito ese URL en un cuaderno que usaba, precisamente, para compilar
mis páginas favoritas, contando y volviendo a contar cuántas doble us le componían
y, por supuesto, también recuerdo haber vuelto muchas (muchísimas) veces a **wwwwwwwww.jodi.org**
con la firme intención de entender qué habitaba ahí. Después de varias discusiones,
otrx usuarix de este foro cerró la conversación con una publicación muy incisiva
que, palabras más, palabras menos, decía: "no hay nada de paranormal en este sitio
web y tampoco hay nada qué entenderle. Es una pieza de arte".

![](1.png "JODI")

> JODI es un colectivo conformado por Joan Heemskerk y Dirk Paesmans, clave en la etapa temprana del Net.Art.

Esa sentencia, que apagó el interés de muchxs, fue el detonante de aquello que encaminó
mis intereses y búsquedas personales hasta el día de hoy, pues, para mí, esa serendipia,
ese encuentro del tercer tipo, era una puerta que nunca había tocado y que abría
paso a una conversación más profunda acerca del arte. Una conversación que no iba
de nombrar, definir y etiquetar lo que es, sino de lo que puede ser.

A casi 20 años de ese momento, ahora, nos encontramos habitando un internet completamente
distinto. Pasamos de la utopía de la libertad de expresión y la colectividad radical
a la censura automatizada y a la polarización sin debate. Pero también pasamos a
la expansión de la accesibilidad y diversificación de contenidos apoyada por el auge
de los teléfonos inteligentes y su hiperconexión casi ubicua. Con esto en mente,
es interesante pausar en este planteamiento y reflexionar sobre las metáforas que
nos han servido para entender lo que es el internet:

> **macro repositorio / archivo / mente colectiva / aldea global / herramienta militar / la nube / red informática / ciberespacio / metaverso / red de redes / tejido global / micelio informático**


De todas las anteriores me gustaría retomar una de las que me hace más ruido: **archivo**.
Para argumentar lo anterior me gustaría tomar un par de ideas de _La arqueología del saber_(1969)
de Foucault[^2] y de Mal de archivo: una impresión freudiana (1996) de Derrida[^3],

[^2]: Foucault, M. (2004), The Archaeology of Knowledge, New York: Pantheon Books.

[^3]: Derrida, J. (1996), Archive Fever: A Freudian Impression, Chicago, IL: University of Chicago Press.

pues la forma en la que se aborda la idea de archivo en la contemporaneidad está firmemente atada a estas
exploraciones filosóficas. Según Foucault, el archivo es "primero la ley de lo que se puede
decir" (1969). Es decir, para él el concepto de archivo es el de un _dispositivo discursivo_.
Por otro lado, para Derrida el archivo adquiere una dimensión material y aparece dentro de
la autoridad política en donde, a través de un viaje a sus configuraciones lingüísticas,
el filósofo nos ofrece una reflexión acerca de los orígenes de la palabra archivo. Aquí
explora cómo _arkhé_ significa tanto comienzo como mandamiento. El _arkheion_ era
originalmente el almacén donde se guardaban los documentos necesarios para la gestión
de las _polis_ griegas por parte de los arcontes. En virtud de su función, los arcontes
conservaban la autoridad hermenéutica sobre los documentos, es decir, podían dictar la ley
según su interpretación del material de archivo (1996).

Más adelante, y ya más vinculadxs con la teoría de medios, investigadorxs como
Featherstone[^4] (2000, 2006) y Lynch[^5] encontraron en el archivo una especie
de concepto paradigmático para interpretar la producción y el consumo de los
medios de comunicación masivos.

[^4]: Featherstone, M. (2000), 'Archiving cultures', British Journal of Sociology,
51:1, pp. 161-84.

[^5]: Lynch, M. (1999), 'Archives in formation: Privileged spaces, popular archives

Lo que ambos subrayan es un proceso de aparente democratización de los archivos:
lo que antes era dominio exclusivo de los estados-nación, se pone ahora a disposición
de lxs internautas, que pueden construir sus propios archivos cotidianos. Por otro
lado, Manovich, teórico de los nuevos medios, ve en la base de datos una de las principales
formas simbólicas de los medios digitales contemporáneos. Frente a esto, Manovich (1999, 2002)[^6]

sostiene que un sitio web es una base de datos en la medida en que no
sigue una narrativa lineal, se construye socialmente y es efímera (2002: 194-212).

[^6]: Manovich, L. (1999), 'Database as symbolic form', Convergence, 5:2, pp. 80-99.

Si a estas tendencias académicas se le suma una mirada sobre las prácticas digitales
y los medios de comunicación, es posible detectar una convergencia en torno a la
figura del archivo y su relevancia política tanto para el poder constituido y el
poder corporativo como para lxs sujetos colectivos. Bajo esta lógica, es fácil ver
que el verdadero negocio de las empresas de la Web 2.0 nunca ha sido la velocidad
a la que pueden comunicarse, sino la cantidad de datos que pueden almacenar, analizar
y poner a la venta Fuchs[^7]. Es decir, aquí el archivo se convierte en la institución
clave en la que se apoyan para secuestrar los bienes comunes digitales. Sin embargo,
del mismo modo que el capital encierra a dichos bienes comunes en y a través del
archivo, muchxs artistas, activistas y movimientos sociales buscan instituir sus
propias lógicas de distribución abierta a través de una gestualidad performática
de la web.

[^7]: Fuchs, C. (2011), 'New media, web 2.0 and surveillance', Sociology Compass,
5:2, pp. 134-47.

Desde proyectos de acceso a la información como **Sci-Hub** y **Monoskop**, pasando
por los pantanos de **Pirate Bay** y las redes de intercambio **peer-2-peer**, y
hasta piezas de envenenamiento de datos como **Go Rando**, **Instagram Demetricator**,
**Twitter Demetricator** y **Facebook Demetricator** de Ben Grosser[^8]
, podemos notar un transitar por la activación y desactivación de aquello que, desde un inicio, se
asume como efímero. En otras palabras, estas propuestas que aparecen, desaparecen
y reaparecen, pueden ser etiquetadas como gestos performáticos. Si pensamos, por
ejemplo, en las gestualidades performáticas de los años 70's, como aquellas planteadas
por Ana Mendieta y su serie _Siluetas_ o Shigeko Kubota y sus exploraciones de videoperformance
con piezas como _Video Girls and Video Songs for Navajo Sky_, mismas que exploran
al cuerpo como materia prima pero también la posibilidad de desprenderse de lo objetual,
podemos trazar una línea que conecta fuertemente con la idea del performance como
acto de resistencia. Aquí, lxs artistas, en lugar de haber desmaterializado o inmaterializado
su obra, han cambiado su paleta de materiales físicos a materiales virtuales y esto
les permitió jugar con la idea de la activación/desactivación táctica de sus piezas.
Es precisamente esta intersección entre cuerpo, performance, virtualidad y táctica
la que conecta la idea del archivo (digital) dinámico que tiene la capacidad de hacerse
aparecer y desaparecer.

[^8]: https://bengrosser.com/

![](2.png "foto_cuerpo_flores")

> Yagul de la serie Siluetas (1973-1977) de Ana Mendieta .

En cierta medida, mi línea de argumentación va en contra de los postulados de Hamilton[^9]
, quien sostenía que los medios de comunicación alternativos (es decir, aquellos
fuera de la hegemonía) deberían caracterizarse por la desprofesionalización,
la descapitalización y la desinstitucionalización, apuntando a una especie de
desprendimiento de los sistemas per se . Ya que, aunque estoy de acuerdo en que
los bienes comunes de los archivos digitales deben existir y gestionarse al margen
de las relaciones capitalistas, lo que se necesita no es la desinstitucionalización,
sino la reinstitucionalización y la reprofesionalización - en otras palabras, la
destrucción total del sistema hegemónico sumado a la creación de valores nuevos.

[^9]:  Hamilton, J. F. (2000), 'Alternative media: Conceptual difficulties,
critical possibilities', Journal of Communication Inquiry, 24:4, pp. 357-78.

Esta práctica de coleccionar, de acceder y de construir archivos (en y desde internet)
debería ser autorreflexiva y politizada. En ese sentido, es útil plantearnos una
posibilidad de desaceleración a través del acto performático. El performance entendido
como una acción que sucede en un espacio y tiempo específicos, y no como la
actuación entendida desde encarnar falsedad. El performance como acto simbólico,
que reflexiona sobre el aquí y el ahora y que ocurre en una dimensión efímera y
flexible, adquiere importancia mientras sucede y, aunque no se reserven fragmentos
del mismo en una versión archivada, es capaz de vivir a través de quienes lo presenciaron.

Por lo anterior, se vuelve interesante traer y fusionar la idea de la profanación
propuesta por Agamben,[^10] en donde sostiene que "profanar" es lo contrario de "consagrar",
con el sutil simbolismo de las ventanas _pop-up_, para entrever un camino que nos
conecta de regreso con JODI. Pues, si bien el sitio web que integra todo su cuerpo
de obra es en sí mismo una pieza expansiva hecha de varias micropiezas, también es
un performance no lineal que escapa de las formas de recolección de archivo impuestas
por los valores del capitalismo digital. Pues, aunque JODI apuesta por la reintegración
y rearmado de la pieza en cuestión, también se vuelve ininteligible para el discurso
hegemónico. Es en este metaespacio digital, que vive en **wwwwwwwww.jodi.org**, donde
también hay cabida para politizar la tecnología, la representación y la memoria.

[^10]: Agamben, G. (2007), Profanations, New York: Zone Books.

Me gusta pensar que, entonces, el internet -esa red que somos todxs, la que construimos
a partir de nuestras trincheras y compartimos con otrxs- es un performance, pues
está pensado para mediar y expandirse a partir de lxs demás. Si lo observamos como
un archivo, entonces hablamos exclusivamente de ese internet secuestrado por las
macro-corporaciones que nos imponen sus objetivos y censuras y, como porrista profesional
de la cultura de internet, me considero responsable de formular otras formas de intercambios
posibles.

Considero que los bienes comunes del internet, que producimos y compartimos, deben
pensarse como opuestos y siempre comprometidos con una crítica exhaustiva y decidida
de la "totalidad" (Foster 2004: 5). Igualmente, pienso que deben contribuir y estar
integrados a la constitución de un modo de producción poscapitalista más allá de
la tiranía del intercambio monetario. Y, finalmente, creo que también deben ser sensibles
con todas las entidades no humanas y construirse tomando en cuenta una posibilidad
de descompresión y desacumulación, pues no podemos obviar las devastadoras repercusiones
de las tecnologías digitales y los servicios en la nube en términos de consumo de
energía y agua, así como de producción de residuos Murdock[^11]. En otras palabras:
habitar el internet desde el performance y la búsqueda de lo efímero, en lugar de
habitarlo como una bodega de archivos muertos sobre archivos muertos sobre archivos
muertos.

[^11]: Murdock, G. (2018), 'Media materialties: For a moral economy of machines',
Journal of Communication, 68:2, pp. 359-68.



> _**Bibliografía**_



1. _Berardi, F. (Bifo) (2009), The Soul at Work: From Alienation to Autonomy, LosAngeles, CA: Semiotext(e)._

2. _d'Urbano, Paolo (2020), '(An/)archival commons: Digital media and contemporary social movements', Journal of Alternative & Community Media, 5:1, pp. 27-43, doi: https://doi.org/10.1386/joacm_00073_1_

3. _Garcia, D. and Lovink, G. (1997), 'The ABC of tactical media', Nettime, 16 May, https://www.nettime.org/Lists-Archives/nettime-l-9705msg00096.html. Accesado el 25 de agosto de 2021._

4. _P2P Foundation (2015), Commons Transition: Policy Proposals for an Open Knowledge Society, Amsterdam: P2P Foundation._

5. _Terranova, T. (2004), Network Culture: Politics for the Information Age, London: Pluto Press._
