---
title: Migrar bots a mastodon
author: Librenauta
description: Migración
proyect: f804d881-5340-465a-a445-0ae0e4853bde
image:
  path: public/a9061c6b-b499-4c4c-9386-8b4a87703108/cover.png
  description: "<3"
category: proyectos
tags:
- bots
- migración
- mastodon
draft: false
order:
layout: post
uuid: a9061c6b-b499-4c4c-9386-8b4a87703108
last_modified_at: 2023-02-06 20:20:21.155018278 +00:00
liquid: false
---

# Migrar bots de tw a mastodon

El 7.02.2023 la API[^1] de tw se [cierrá](https://twitter.com/TwitterDev/status/1621026986784337922), musk (el dueño de tw) cerrará el acceso de forma gratuita, esto constituye la muerte muchísimos bots que mejoraban mi experiencia en esa plataforma. definitivamente era una de las pocas cosas que me acercaban a tw. todos los bots creados con [cheapbotsdonequick.com/](https://cheapbotsdonequick.com/) morirán a partir de mañana. tanto como los crossposters como [crossposter.masto.donte.com.br/](https://crossposter.masto.donte.com.br/) y las [paginas](https://smallworld.kiwi/) para encontrar gente cerca en tw.

Por suerte para cada decision individual de una empresa hay una propuesta colectiva


* primero hay que generar una cuenta en [botsin.space](https://botsin.space) la instancia dedicada a bots en mastodon.

* para levantar nuestros bots ahora logearemos con nuestra cuenta de mastodon en [cheapbotstootsweet.com](https://cheapbotstootsweet.com/) 

basicamente [v21](https://twitter.com/v21) hizo el mismo proyecto pero para que funcione con la API de mastodon. 

![cheapbot](public/a9061c6b-b499-4c4c-9386-8b4a87703108/01.png)

*  copiar exactamente el mismo .json que pegamos en cheapbotdonequick en cheapbotstootsweet <3 


[^1]: una API es una interfaz de programación de aplicaciones que permite a desarrolladoras interactuar con plataformas de acuerdo a protocolos definidos. más info [acá](https://es.wikipedia.org/wiki/Interfaz_de_programaci%C3%B3n_de_aplicaciones)




