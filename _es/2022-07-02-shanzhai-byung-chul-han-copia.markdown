---
title: Shanzhai - Copia
author: Byung Chul-Han
description: Crítica Cultural
proyect: f804d881-5340-465a-a445-0ae0e4853bde
image:
  path: editorial/shanzhai-byung-chul-han/shanzhai-copia-cover.png
  description: shanzhai-byung-chul-han-copia.
pdf_read: editorial/shanzhai-byung-chul-han/shanzhai-copia.pdf
pdf_cover: editorial/shanzhai-byung-chul-han/shanzhai-copia-cover.pdf
pdf_imposition: editorial/shanzhai-byung-chul-han/shanzhai-copia_imposition.pdf
category: editorial
export_pdf: 'export-pdf --title="SHanzhai" --author="Byung Chul-Han" --press="pixel2pixel" --template="a6template.tex" --no-day shanzhai-byung-chul-han-copia.md'
tags:
- Shanzhai
- Copia
- Libro
draft:
order:
layout: book
uuid: f296d22b-8711-4d79-9445-4e261f2f3edd
liquid: false
---
# BYUNG-CHUL HAN SHANZHAI

## El arte de la falsificación y la deconstrucción en China

_traducción: Paula Kuffer_

### FUZHI: COPIA "[複製]"

Cuando se supo que los guerreros chinos de terracota que se expusieron en el Museo
de Etnologia de Hamburgo en 2007 eran una copia, se clausuró la exposición. El director
del centro, que se erigió en defensor de la verdad, declaró en ese entonces: “Hemos
llegado a la conclusión de que no tenemos otro remedio que cancelar la exposición
para mantener la reputación del museo”. Incluso se ofrecieron a devolver el dinero
de la entrada a todos los visitantes que habian acudido. Desde el comienzo de la
excavación arqueológica se llevó a cabo, de manera paralela, la fabricación de las
réplicas de los guerreros de terracota. Junto a la excavación se abrió un taller
de copias. Pero en ningin caso confeccionaban “falsificaciones”. Para hablar con
propiedad, habría que decir que los chinos estaban intentando _retomar_ la producción,
que en ningún momento fue una creación. Los propios originales formaban parte de
una producción en serie con módulos, en realidad adornos móviles, que podía proseguirse
siempre que estuviera a disposición la tócnica de fabricación.

Los chinos tienen dos conceptos para designar la copia. El término _fangzhipin_ [仿製品]
se refiere a las recreaciones en las que es evidente la diferencia respecto del
original. No se trata de modelos o copias, que pudieran comprarse, por ejemplo,
en la tienda de un museo.
El segundo concepto para la copia se denomina _fuzhipin_ [複製品]. En este caso se
trata de una reproducción exacta del original, la cual, para los chinos, tiene el
mismo valor que el original. No conlleva, bajo ningún concepto, una connotación negativa.
La discrepancia en cuanto a la interpretación de la copia a menudo ha generado
malentendidos y controversias entre China y los museos occidentales. Los chinos
a menudo mandan copias en vez de originales, puesto que estan convencidos de que
en lo esencial no son distintos. Se tomaron el rechazo por parte del museo
occidental como una ofensa.

A pesar de que la globalización genera muchas sorpresas y causa grandes disgustos
al Lejano Oriente, podría liberar las energías deconstructivas. Para el espectador
occidental, la concepción de la identidad del Lejano Oriente resulta muy irritante.
El Santuario de Ise, el lugar sagrado más importante del Japón sintoísta, que cada
año congrega a millones de japoneses, tiene 1300 años de antigüe- dad. Pero en realidad,
este complejo de templos se reconstruye completamente cada 20 años. Esta práctica
religiosa resulta tan ajena a los historiadores del arte occidental que finalmente,
tras controvertidos debates, la +++UNESCO+++ eliminó el templo de Shinto de la lista del
patrimonio cultural de la humanidad. Según los expertos de la +++UNESCO+++, el Santuario
de Ise no tiene más de 20 años de antigüedad. En este caso, ¿qué es una copia y qué
es un original? Aquí se invierte absolutamente la relación entre original y copia.
O más bien desaparece por completo la distinción. En lugar de la diferencia entre
original y copia se impone la diferencia entre viejo y nuevo. Se podría decir que
la copia es más original que el original o que está más cerca del original que el
original, puesto que cuanto más antiguo es un edificio más alejado está de su estado
primigenio. En cierto modo, una reproducción volvería a su "estado original", puesto
que no está ligada a un sujeto artístico.
No solo se renueva el edificio, sino que también se sustituyen todos los tesoros
del templo. En el templo siempre hay dos
conjuntos idénticos de tesoros. Nunca se plantea la pregunta por el original y la
copia. Se trata de dos copias que a la vez son dos originales. Después de fabricar
un nuevo conjunto, el antiguo se destruye. Los objetos combustibles se queman y los
metálicos se entierran. Sin embargo, después de la última renovación, los tesoros
no se destruyeron sino que se llevaron a un museo. Se salvaron gracias a su creciente
valor expositivo. Aun así, la destrucción de estos forma parte de su propio valor
cultural, que está desapareciendo cada vez más en favor de su valor museístico.

En Occidente, cuando se restauran los monumentos, a menudo las marcas antiguas se enfatizan
a propósito. Las piezas originales se tratan como si fueran reliquias. El Lejano
Oriente es ajeno a este culto del original. Desarrolló una técnica de mantenimiento
muy distinta, que sería más efectiva que la conservación o la restauración. Consiste
en la reconstrucción constante. Esta técnica anula por completo la diferencia entre
original y réplica. Toma la naturaleza como ejemplo. Se podría decir que los originales
se preservan mediante las copias. El organismo también se renueva a partir de un
cambio ininterrumpido de células. Al cabo de un tiempo, queda renovado. Las células
antiguas se sustituyen por nuevo material celular. En este caso no se plantea la
pregunta por el original. Lo viejo muere y se reemplaza por lo nuevo. La identidad
y la novedad no son excluyentes. En una cultura en la que la reproducción constante
se presenta como una técnica de conservación y mantenimiento, las imitaciones nunca
pueden considerarse meras copias.

![](editorial/shanzhai-byung-chul-han/1.png "shanzhai")

![](editorial/shanzhai-byung-chul-han/2.png "shanzhai")

La catedral de Friburgo está cubierta de andamios gran parte del año. Está hecha
de arenisca, un material muy blando y poroso que no resiste la lluvia ni el viento.
Al cabo de un tiempo, se desmigaja. De ahí que la catedral esté sometida a estudios
constantes para detectar los desperfectos y poder cambiar las piedras erosionadas.
En el taller de la catedral se exponen copias de las figuras de arenisca deterioradas.
Se intenta conservar las piedras de la Edad Media a toda costa. Pero en algún momento
se ven obligados a retirarlas y sustituirlas por piedras nuevas. En lo esencial,
se trata de la misma práctica de los japoneses. Se acaba colocando una réplica, aunque
solo sea al cabo de un largo período. El resultado, a fin de cuentas, es el mismo.
Con el paso del tiempo, tendremos ante nosotros una reproducción. Aunque creamos
que nos encontramos ante un original. ¿Pero qué tendrá de original la catedral cuando
la última piedra antigua quede sustituida por una nueva?

El original es algo imaginario. Es posible hacer una reproducción exacta de la catedral de Friburgo, es decir, un
_fuzhipin_, en uno de los muchos parques temáticos chinos. ¿Sería una copia o un original?
¿Qué lo convertiría en una mera copia? ¿Qué hace que la catedral de Friburgo sea
considerada un original? Desde la perspectiva material, el _fuzhipin_ no se diferencia
en nada del original que ya no conserva ninguna pieza inicial. En todo caso, la catedral
de Friburgo se distinguiría de su _fuzhipin_ en el parque temático chino por el emplazamiento
y el valor cultural que deriva del culto religioso que se ofrece. Pero si se omitiera
su valor cultural en favor de su valor expositivo, no se diferenciaría en nada de
su doble.
En el terreno del arte, en el mundo occidental, la idea de un original
inviolable aparece en un momento histórico concreto. En el siglo +++XVII+++, todavia se
relacionaban de un modo muy distinto al actual con las obras de arte de la antigüedad
encontradas en las excavaciones. Estas no se restauraban según el original. Más bien
se intervenía en ellas explícitamente y se transformaba su apariencia. Por ejemplo,
Gian Lorenzo Bernini (1598-1680) así lo hizo con la conocida estatua de _Ares Ludovisi_
(que a su vez era una copia del original griego) añadiendo un puño a la espada. En
la época de Bernini, el Coliseo servía de cantera de mármol. Se derribaron los muros
y se usaron para levantar nuevos edificios. La conservación de monumentos en sentido
moderno empieza con la museificación del pasado, que sustituye el _valor cultual_ por
el _expositivo_. Resulta interesante que vaya de la mano de la aparición del turismo.
El llamado "Grand Tour", que surge en el Renacimiento y alcanza su punto álgido en
el siglo +++XVIII+++, puede considerarse como la primera etapa del turismo moderno. En
virtud de los turistas, aumenta el valor expositivo de los edificios y las obras
de arte antiguas, que son ofrecidas como atracciones. Precisamente en el siglo en
que tiene lugar este incipiente turismo se adoptan las primeras medidas para la conservación
de las construcciones antiguas. Se considera una necesidad conservar estos edificios.
La embrionaria industrialización aumenta la exigencia de conservación y museificación
del pasado. La aparición de la historia del arte y de la arqueología descubre el
_valor de conocimiento_ de los edificios y las obras de arte antiguas, rechazando cualquier
intervención modificadora.
Para la cultura del Lejano Oriente no existe una fijación
previa y primordial. Seguramente, esta actitud espiritual explica que los asiáticos
tengan menos escrúpulos que los europeos en relación con los clones. El investigador
Hwang Woo-Suk, que en 2004 acaparó la atención mundial con su tentativa de clonación,
era budista. Encontró un gran apoyo entre los adeptos del budismo, mientras que los
cristianos se aferran a la prohibición de la clonación humana. Hwang legitimó su
intento de clonación en base a su filiación religiosa: "Soy budista, y no tengo ningún
problema filosófico con la clonación. Y como ustedes saben, el fundamento del budismo
es que la vida se recicla a través de la reencarnación. En cierto modo creo que la
clonación terapéutica reinaugura el círculo de la vida". [^1]

[^1]: Byung-Chul Han, "Das Klonen und der Feme Osten" [La clonación y
el Lejano Oriente], en _Lettre International_, nº 64, 2004, págs. 108-109.

 En el caso del templo de Ise, la técnica de conservación
también consiste en permitir que el ciclo de vida vuelva a comenzar una y otra vez,
no en que la vida se enfrente a la muerte, sino en que se mantenga por _medio de la
muerte_. Este sistema de conservación incorpora la propia muerte. En un ciclo infinito
de vida no existe nada único, originario, singular o definitivo. Solo hay repeticiones
y reproducciones. En la concepción budista de la vida como ciclo infinito, en lugar
de la creación aparece la des-creación. La iteración y no la creación, la recurrencia
y no la revolución, los módulos y no los arquetipos definen la técnica de producción
china.
Es sabido que los guerreros de terracota están hechos con módulos o más bien
adornos móviles. La producción con módulos no es compatible con la idea de original,
puesto que desde el comienzo se usan adornos móviles. La producción modular no se
guía por la idea de originalidad o de carácter único, sino por la de _reproductibilidad_.
Su propósito no es producir un objeto singular y original, sino la producción en
masa, que permite variaciones y modulaciones. _Modula_ lo idéntico y de este modo genera
diferencias. La producción modular modula y varía. Así se da lugar a una gran variedad.
Elimina la singularidad para aumentar la eficiencia de la reproducción. No es una
casualidad que la imprenta se inventara en China. También la pintura china se vale
de la técnica modular. El tratado de pintura chino más famoso,
_El jardín de la semilla de mostaza_, contiene una serie casi infinita de adornos
móviles, a partir de los cuales se puede armar y componer un cuadro.

![](editorial/shanzhai-byung-chul-han/3.png "shanzhai")

Este modo de producción modular plantea una nueva concepción de la creatividad. La
combinación y la variación ganan peso. En este sentido, la técnica cultural china
se comporta como la naturaleza: "Los artistas chinos (...) nunca pierden de vista
el hecho de que producir obras en grandes cantidades también implica creatividad.
Ellos confían en que, como en la naturaleza, siempre habrá algo entre diez mil cosas
que cambiará".[^2]

[^2]: Lothar Ledderose, _Ten Thousand Things. Module and Mass Production in
Chinese Art_, Princeton, Princeton University Press, 2000, pág. 7.

El arte chino no mantiene una relación mimética con la naturaleza,
sino funcional. No se trata de retratar la naturaleza del modo más realista posible,
sino de operar de la misma manera que la naturaleza. La naturaleza también genera
variaciones nuevas sucesivamente, por lo visto sin intervención de "genio" alguno:
"Pintores como Zheng Xie se esfuerzan por emular la naturaleza en dos aspectos. Producen
grandes cantidades, casi ilimitadas, de obras y son capaces de ello mediante sistemas
de módulos de composición, diseños y pinceladas. Pero también invisten a cada una
de sus obras en su única e inimitable figura, como lo hace la naturaleza con su prodigiosa
invención de las formas. Una vida dedicada a entrenar las sensibilidades estéticas,
permite al artista aproximarse al poder de la naturaleza".[^3]

[^3]: lbíd., pág. 213.
