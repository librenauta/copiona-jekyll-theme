---
title: Feminismo Glitch - Manifiesto - capítulo 2 y 3
author: Legacy Russell
description: Feminismo Glitch - Manifiesto
proyect: f804d881-5340-465a-a445-0ae0e4853bde
image:
  path: editorial/feminismo-glitch-legacy-russell/feminismo-glitch-legacy-russell-capitulo-2-3-cover.png
  description: shanzhai-byung-chul-han-copia.
pdf_read: editorial/feminismo-glitch-legacy-russell/feminismo-glitch-legacy-russell-capitulo-2-3.pdf
pdf_cover: editorial/feminismo-glitch-legacy-russell/feminismo-glitch-legacy-russell-capitulo-0-1-cover.pdf
pdf_imposition: editorial/feminismo-glitch-legacy-russell/feminismo-glitch-legacy-russell-capitulo-2-3_imposition.pdf
category: editorial
export_pdf: 'export-pdf --title="Feminismo Glitch - introduccion y capitulo 2 y 3" --author="Legacy Russell" --press="pixel2pixel" --template="a6template.tex" --no-day feminismo-glitch-legacy-russell-capitulo-2-3.md'
tags:
- feminismo
- glitch
- libro
- manifiesto
draft:
order:
layout: book
uuid: d0910868-56fa-48dc-9dfe-dd39d4645529
liquid: false
---

# Feminismo Glitch de Legacy Russell

> _Traducción by Valeria Mussio_

## 02 — EL GLITCH ES CÓSMICO

> Cuando todxs seamos polvo de estrellas, vamos a decir

> que los medios distorsionan

> la percepción del público

> de los cuerpos cósmicos

> …No soy opaco. Soy tan relevante que estoy desapareciendo.

> _Anaïs Duplan,_

> _“¿En una escala del 1 al 10, qué tan “amorosx” te sentís?”_

![3-1](editorial/feminismo-glitch-legacy-russell/3-1.png)

*********

Existen muchas formas de pensar el cuerpo. Cuando el poeta, artista y curador Anaïs
Duplan habla de los “cuerpos cósmicos”, aporta un giro único. Esta nueva corporalidad
cósmica provee un punto de vista emocionante para acercarnos al cuerpo como arquitectura.
Cuando consideramos el glitch como una herramienta, es útil pensar también cómo esta
podría ayudarnos a entender mejor al cuerpo como idea.

El cuerpo es una idea cósmica, es decir, “inconcebiblemente vasta”. Aunque la evidencia
de la vida humana tendiendo hacia el Antropoceno abarca 2.6 millones de años y más,
recién estamos empezando a rascar la superficie de lo que es el cuerpo, lo que puede
hacer, cómo se ve su futuro.

_Cuerpo_: es una palabra que construye mundos, llena de potencial y, como _glitch_, llena
de movimiento. _Dar cuerpo_ (bodied), usada como verbo, es definida por el Diccionario
de Oxford como “darle forma material a algo abstracto”. Tanto en el sustantivo como
en el verbo, usamos la palabra _cuerpo_ para darle forma a la abstracción, para identificar
un todo amalgamado.

Todxs comenzamos en la abstracción: cuerpos sin género pero con sexo que, a medida
que se desarrollan, toman una forma con género ya sea a través de la performance
o de acuerdo a construcciones que se proyectan socialmente. Para desmaterializar
— para volver a abstraer — el cuerpo y trascender sus limitaciones, necesitamos hacer
espacio para otras realidades.

El internet es “un cuarto propio”[^1]. El crítico de arte Gene McHugh, en su ensayo
“El contexto de lo digital: una breve investigación sobre las relaciones online”,
observa: “Para muchas personas que alcanzaron la mayoría de edad como individuos
y seres sexuales online, el internet no es una esquina esotérica de la cultura, donde
la gente se escapa de la realidad y juega a ser otrx. [El internet] _es_ la realidad.”[^2]
Por lo tanto, el término “nativo digital” ha sido aplicado a una generación que no
recuerda una vida que no esté entrelazada con el internet.

[^1]: Virginia Woolf, A Room of One’s Own, New York: Harcourt, Brace and Company,
1929.

[^2]: Gene McHugh, “The Context of the Digital: A Brief Inquiry into Online Relationships,”
in You Are Here: Art After the Internet, edited by Omar Kholeif, Manchester, UK:
Cornerhouse Publications, 2017, p. 31.

McHugh resalta “Playground” (2013), la obra para-dos-personas de la videoartista
y performer Ann Hirsch, como ejemplo de estas negociaciones entre el juego, la realidad
y la sexualidad en el internet. La obra de Hirsch explora su relación online como
pre-adolescente y nativa digital con un hombre mayor, en un momento en que el mundo
offline no le proveía el suficiente estímulo para su exploración libre de lo emocional,
lo sexual y lo intelectual. Si bien complicada en su dinámica, la relación online
propiciada entre los dos abrió nuevos caminos que enriquecieron el entendimiento
de Hirsch acerca de su propio cuerpo y su política.

La aplicación de la dicotomía “online-vs-IRL” en la discusión del juego con el sexo
y el género online tiene muchísimas fallas. Tales limitaciones están vinculadas con
un constructo de “la vida real” que no expande nuevos mundos, sino que los obstaculiza
de forma violenta. IRL flaquea en su sesgado preconcepto de que las construcciones
de las identidades online son latentes, enclosetadas y orientadas hacia la fantasía
(es decir, no son reales), y no explícitas, estimulantes en todo su potencial, y
muy capaces de vivir más allá del ciberespacio. Al contrario, el término AFK logra
desestimar la fetichización de “la vida real”, ayudándonos a ver que, debido a que
las realidades en lo digital se replican offline y vice versa, nuestros gestos, nuestras
exploraciones y nuestras acciones online pueden informar e incluso profundizar nuestra
existencia offline o AFK. Esto es poderoso.

De este modo, el feminismo glitch le da peso a los “yoes” que creamos a partir del
material del internet. El feminismo glitch hace lugar para crear otras realidades,
donde sea que cada unx se encuentre a sí mismx. Como parte de este proceso, un individuo
no solo se ve inspiradx a explorar su rango online, sino que también puede ser llevadx
a encarnar de una forma bastante literal lo digital como estética, borroneando aún
más la división entre cuerpo y máquina. La aplicación creativa AFK de la estética
vernácula y maquínica en la forma física presenta un giro performático único.

La performance de lx artista boychild ejemplifica esto, encarnando lo que la artista
y escritora James Bridle llamó “la Nueva Estética” en su fusión de lo virtual y de
lo físico a través de su práctica creativa. Término acuñado por Bridle en el 2012,
la Nueva Estética es definida como “una forma de ver que…revela un desdibujamiento
entre “lo real” y “lo digital”…entre el humano y la máquina.”[^3] boychild performatea
roboticamente, muchas veces desnudx, un característico drag y lip-sync inspirado
en la danza Butoh con una luz brillante que emana de la boca pintada de lx artista.
Esta obra apunta a la historia de la cibernética y al amanecer del internet, mientras
evoca de forma simultánea la sensibilidad de la vida nocturna queer.

[^3]: James Bridle, “#Sxaesthetic,” blog entry, Book Two, March 15, 2012, booktwo.org.

En una conversación con lx artista y co-colaboradorx Wu Tsang, boychild explica,
“La vida nocturna es importante para mi trabajo porque crea un espacio para que yo
exista; nada contextualiza mi performance de la forma en que lo hacen estos lugares.
Es mi mundo, mi existencia en el under. Además, existo en un mundo que viene después
del internet…pasé mi adolescencia encontrando cosas ahí. El under existe en el internet
para mí.”[^4] boychild traza una conexión entre el “under” de los espacios de la
vida nocturna, esos lugares que permiten la exploración y experimentación de nuevas
identidades, y el internet como patio de juegos, sirviendo un propósito similar.
Esto subraya el rol del internet-como-cabaret, donde la performance avant-garde,
como el trabajo de boychild, comienza con y se apropia de la cultura digital. Significativamente,
podemos definir a los años 90 como un momento caracterizado por el auge de la cultura
digital y el ciberfeminismo, y un incremento simultáneo del borramiento sistemático
de espacios que traían la noche queer en las ciudades internacionales más importantes.
Las performances de boychild generan preguntas acerca del cuerpo-como-máquina y cómo
los afectos no binaries pueden ser negociados y expresados -computados, incluso-
a través del material maquínico. boychild observa sobre esta práctica que “es como
el cuerpo físico convirtiéndose en un cyborg…es como el glitch; hay una cosa repetitiva
que sucede. Se mueve lentamente, pero también es rápido.”[^5] A través de esto giro
cyborgiano, lx artista intencionalmente encarna el error, una suerte de convulsión
del sistema que se sirve de la máquina en una resistencia AFK.

[^4]: Hili Perlson, “Truth in Gender: Wu Tsang and boychild on the Question of Queerness,”
SLEEK, October 29, 2014, sleek-mag.com.

[^5]: Rachel Small, “boychild,” Interview Magazine, December 10, 2014, interviewmagazine.com.

El pasaje de los cuerpos glitcheados desde el under del internet hacia un escenario
AFK activa la producción de una nueva cultura visual, una suerte de patois biónica
en la que lxs nativxs digitales son fluidxs. Suspendidxs entre lo online y lo offline,
atravesando eternamente este loop, lxs nativxs digitales embebidos en una realidad
a la que le dio forma la Nueva Estética continúan desprovistos de una tierra natal.
No hay manera de volver al concepto de “lo real”, ya que la práctica digital y la
cultura visual que surgió de ella reformuló para siempre el cómo leemos, percibimos
y procesamos todo lo que sucede AFK. Por lo tanto, esta diáspora digital es un componente
importante del glitch, porque significan que los cuerpos en esta era de la cultura
visual no tienen un destino único, sino que asumen una naturaleza distribuida, ocupando
de forma fluida muchos seres, muchos lugares, todos a la vez.

El escritor, poeta, filósofo y crítico Édouard Glissant define la diáspora como “el
pasaje de la unidad a la multiplicidad”, explorando estas “partida(s)” dentro del
dominio de unx mismx como plausibles solo cuando “unx acepta no ser un único ser
e intenta ser la mayor cantidad de seres a la misma vez.”[^6] El feminismo glitch
re-aplica la “aceptación de no ser un único ser”, haciendo atractivo el rango cósmico
dentro del que una dispersión colectiva y personal hacia la inmensidad se convierte
en una abstracción consentida.

[^6]: (Manthia Diawara, “Conversation with Édouard Glissant aboard the Queen Mary
II,” August 2009, disponible en liverpool.ac.uk/media/livacuk/csis 2/blackatlantic/research/Diawara_text_defined.pdf.)


Lo que la teórica Lisa Nakamura llama “turismo” en _Cibertipos: raza, etnia e identidad en el internet_, descripto como “el proceso por el que los miembros de un grupo se prueban “para ver cómo les quedan” los descriptores generalmente aplicados a personas de otro [grupo]”[^7] continúa siendo una limitación al cómo procesamos el rol de lo digital en tanto se relaciona con nuestra identidad. La noción de Nakamura de la identidad en el Internet como algo mayormente turístico funciona dentro de la falacia del dualismo digital. Apostando por una transformación cósmica, el feminismo glitch concibe estos actos de experimentación como caminos hacia el florecimiento del propio ser. Quizás, aunque haya comenzado inicialmente bajo el anonimato cuasi sin cara de las plataformas online, la oportunidad de experimentar y probarse diferentes “yoes” empodera el aprovechamiento de una identidad pública más integrada con una potencia radical.

[^7]: Lisa Nakamura, Cybertypes: Race, Ethnicity, and Identity on the Internet, New
York: Routledge, 2002, 8.

Pienso en CL[^8], una joven feminista que produce zines como parte de su práctica
creativa, que en una conversación me compartió que el uso temprano de plataformas
online como LiveJournal y, años después, Twitter, la animó a tantear el terreno y
probarse a sí misma dentro de un espacio público jugando con el lenguaje, el humor
y la representación, y así ver cómo todo eso era recibido por lxs demás. En un principio
vio una oportunidad para “esconder la raza por un rato” y “simplemente ser” en estas
plataformas; la vasta “falta de cara” del espacio digital le proveía una neutralidad
que impulsó su confianza, y empezó a notar que su feroz ingenio, sus políticas feministas
y su forma de ver el mundo eran bienvenidas por el público online. CL señala que
fue a través del internet que aceptó su identidad como “una chica Negra inteligente”,
una percepción de sí misma que encontró su origen primero online, y que luego fue
llevada AFK con un propósito individual mucho más grande, con el apoyo de su comunidad
y con un entendimiento holístico.

El “yo” glitcheado siempre se está moviendo. El viaje diaspórico desde lo online
hasta lo offline es una forma de partenogénesis, de reproducirse a unx mismx sin
fertilización -astillando, fusionando, emergiendo. Esta es la rúbrica para una tecnología
política encarnada que es orgullosamente queer, y que crea un espacio para nuevos
cuerpos y seres cósmicos.

[^8]: Iniciales utilizadas para preservar su anonimato.

## 03 — EL GLITCH TIRA SHADE

El ascenso meteórico hacia el éxito y el reconocimiento cultural de la auto-definida
“ciborg” y artista Juliana Huxtable en los últimos años es importante y oportuno.
Dentro de los reinos del arte, la música, la literatura y la moda, ella busca romper
la rigidez de los sistemas binarios. Criada en College Station, Texas, Huxtable nació
intersexual y fue asignada con el género masculino. En los 90, en el momento en que
el Internet y la mitología de su utopía estaban en alza, Huxtable se identificaba
como hombre, usando el nombre de Julian Letton.

En el ambiente cristiano y conservador de Texas, reclamar el derecho a una identidad
trans parecía inimaginable. Pero cuando ella dejó su hogar para asistir al Bard College,
una universidad pequeña de artes liberales en el estado de Nueva York, entró en un
periodo que marcó un florecimiento en su sentido del “yo” del que habla abiertamente:
“Tenía el cerebro completamente lavado por la porquería del Cinturón Bíblico (…)
pero el internet se convirtió en una especie de soledad. Me dio una sensación de
control y libertad que no tenía en mi día a día, porque iba por la vida sintiéndome
odiada, avergonzada, atrapada y sin poder. Me sentía muy suicida.”[^9]

[^9]: Antwaun Sargent, “Artist Juliana Huxtable’s Bold, Defiant Vision,” Vice, March
25, 2015, vice.com.

A medida que su práctica artística se expandía, la participación de Huxtable en diversas
plataformas digitales –salas de chat, blogs, redes sociales y mucho más- aumentó
la visibilidad tanto de su trabajo visual como escrito, creando la oportunidad para
su circulación tanto dentro como más allá del mundo del arte contemporáneo. A la
misma vez, imágenes de la propia Huxtable circularon de forma mimética. Un GIF viaja
online y se hace viral, mostrando emociones a través del loop eterno del afecto digital,
citando la reacción de Huxtable a la pregunta “¿Cuál es el shade más zarpado que
tiraste en tu vida?” a lo que ella responde “Existir en este mundo.”

El Nuevo Museo Trienal del 2015 en Nueva York llevó el poder de la presencia creativa
de Huxtable a nuevas alturas. El cuerpo desnudo de Huxtable reposando fue el sujeto
de _Juliana_, la escultura de plástico impresa en 3D de Frank Benson. La estatua de
Benson es un homenaje a Huxtable y una “respuesta post-Internet a la (…) escultura
griega _Hermafrodita Durmiente_ (…)como la obra de arte antigua, la pose desnuda de
Huxtable revela partes corporales de ambos sexos”.[^10] Benson hace contemporánea
su versión de este clásico, con Huxtable inclinada sobre un brazo, el otro extendido
con un gesto de manos como un “mudra” yóguico, y su figura pintada con un color verde
metálico.

[^10]: Ibid.

En el espacio de la galería, la escultura que Benson hizo de Huxtable estaba posicionada
de forma adyacente a cuatro impresiones con inyección de tinta de la propia Huxtable.
Estas incluían dos auto-retratos y dos poemas — ambos titulados “Sin título (Poder
Casual)”- como parte de la serie de su serie del 2015 “Tops universales para todxs
lxs santxs auto-canonizadxs de la conversión[^11]”. El título de la serie presta
atención a la celebración de la transformación, del convertirse, significando un
viaje cósmico hacia cánones nuevos y más inclusivos, y, por extensión, nuevos y más
inclusivos “yoes”. Los auto-retratos, respectivamente titulados “Sin título en la
rabia (Cataclismo Nibiru)” (2015) y “Sin título (Carne en destrucción)” (2015), muestran
a la artista como un avatar de la Nación Nuwaubiana, pintada en un retrato de color
violeta neón, y en el otro de verde alienígena. Los poemas de la artista que acompañan
las impresiones de los retratos vagan por el pasado, el presente y el futuro, repletos
de meditaciones tecnicolores sobre una amplia gama de temas: el cambio climático,
COINTELPRO, las reparaciones para la comunidad Negra, la santidad. En estos textos,
Huxtable provoca a Octavia Butler, Angela Davis, Aaliyah, y al “surrealismo de barrio”
de Hype Williams, quien dirigió muchos de los videos de las estrellas Negras de pop
y R&B de los ’90.

[^11]: Nota de traducción: la palabra “becoming” (convertirse, transformarse) como
adjetivo también quiere decir “apropiado, favorecedor”. Son dos sentidos que me parece
bueno tener en cuenta en estos casos.

En una conversación con la artista Lorraine O’Grady, Huxtable reflexiona sobre la
experiencia de mostrar su propio trabajo- y su cuerpo, a través de la escultura de
Benson- en el Trienial:

Tuve una creciente sensación de ansiedad (…) La performance ofrecía una manera poderosa
de lidiar con las preguntas acerca de la auto-supresión o de la presencia, tentando
a la audiencia con la idea de que yo hacía una performance para habilitar su consumo
de mi imagen o de mi cuerpo — y luego rechazar eso completamente. El texto, el video
y todos esos medios se transformaron en modos de abstraer la presencia o abstraerme
a mí misma del presente. Y por eso ahora la performance se siente como una forma
de lidiar con las consecuencias de un momento cultural.[^12]

[^12]: “Introducing: Lorraine O’Grady and Juliana Huxtable, Part 1,” Museum of Contemporary
Art, Spring 2019, moca.org.

El ejercicio de Huxtable para “abstraer la presencia o abstraerse a sí misma” como
un modo de performatividad –entre lo online y lo AFK- intersecta con las ambiciones
cósmicas del feminismo glitch de abstraer el cuerpo como medio para llegar más allá
de sus limitaciones convencionales. En su celebridad, Huxtable ejercita regularmente
una “visibilidad necesaria”, eligiendo hacer visible su cuerpo cósmico a través de
una continua documentación de sí misma online, mayormente a través de Instagram.[^13]
Ella explica, “el Internet, y específicamente las redes sociales, se convirtieron
en una forma esencial para explorar inclinaciones que, de otra forma, no hubieran
tenido salida.”[^14]

[^13]: Para restituir un término acuñado por la curadora Taylor LeMelle en el contexto
de “Tecnologías de ahora: la Negrura en Internet”, un programa organizado por Legacy
Russell que tuvo lugar en el Instituto de Arte Contemporáneo, acompañadx de Rizvana
Bradley, Taylor LeMelle y Derica Sheilds. Este programa fue presentado durante la
exhibición “Wandering/WILDING: la Negrura en el Internet”, organizada por Legacy
Russell en la Galería IMT, en Londres, presentada en colaboración del Instituto de
Arte Contemporáneo, Londres, 2016.

[^14]: Petra Collins, “Petra Collins selects Juliana Huxtable,” Dazed Digital, July
8, 2014, dazeddigital.com

Para Huxtable, como para muchxs otrxs que usan el espacio online como un lugar para
re-presentarse y re-performar sus identidades de género, el “Internet representa
(…) una “herramienta” para la organización feminista global (…) y la oportunidad
de ser protagonista (…) en la revolución propia”. También es un ““lugar seguro” (…)
una forma de, no solo sobrevivir, sino resistir los regímenes represivos de sexo
y género”[^15] y la normatividad antagónica del mainstream.

[^15]: Jesse Daniels, “Rethinking Cyberfeminism(s): Race, Gender, and Embodiment,”
Women, Science and Technology: A Reader in Feminist Science Studies, New York: Routledge,
2001, p. 365.


Huxtable es en sí misma un glitch particularmente poderoso. Con su sola presencia Huxtable “tira shade”: ella personifica las problemáticas del binarismo y el potencial liberador de la codificación del género, asumiendo el propio rango posible. Tales cuerpos cósmicos glitchean, activando la producción de nuevas imágenes que “crean (…) un futuro como práctica de supervivencia”.[^16] El glitch es la llamada y la respuesta a la declaración de Huxtable de su propio ser, el “shade” de “existir en el mundo”, permaneciendo como la forma más “zarpada” de rechazo.

[^16]: Tina M. Campt, Listening to Images, Durham, NC: Duke University Press, 2017,
p. 114.

En un paisaje distópico global que no hace lugar para ningunx de nosotrxs, que no
nos ofrece ningún santuario, el puro acto de vivir –sobrevivir- en la cara de la
hegemonía genérica y racial se convierte en algo especialmente político. Elegimos
seguir vivxs, contra todo pronóstico, porque nuestras vidas importan. Elegimos apoyarnos
lxs unxs a lxs otrxs en el vivir, ya que el acto de seguir vivxs es una forma de
construir mundos. Estos mundos son nuestros para crear, reclamar, comenzar. Viajamos
fuera del camino, lejos de la demanda de ser meramente “un único ser”. Nos encriptamos
para contener multitudes contra la corriente de un código cultural que preconiza
la singularidad del binarismo.

Glitchear es un gerundio, una acción continua. Es un activismo que se despliega con
una extravagancia sin límites.[^17] Sin embargo, bajo la corriente de este camino
hay una tensión irrefutable: el cuerpo glitcheado es, de acuerdo a la diseñadora
UX, programadora y fundadora del colectivo @Afrofutures_UK, Florence Okoye, “simultáneamente
observado, vigilado, etiquetado y controlado, a la vez que es invisible para las
estructuras ideáticas, creativas y productivas del complejo tecno-industrial.”[^18]

[^17]: El cuerpo glitcheado es un cuerpo que desafía las jerarquías y estratos de
la lógica, es orgullosamente absurdo y por lo tanto sinsentido. Pienso acá en “Fifty-eight
Indices on the Body” Indice 27, del filósofo Jean-Luc Nancy, donde reflexiona: “Los
cuerpos producen sentido más allá del sentido. Son una extravagancia del sentido.”
En Jean-Luc Nancy, Corpus, traducido por Richard Rand, New York: Fordham University
Press, 2008, p. 153.

[^18]: Florence Okoye, “Decolonising Bots: Revelation and Revolution through the
Glitch,” Het Nieuwe Instituut, October 27, 2017, botclub.hetnieuweinstituut.nl.

Nos ven y no nos ven, somos visibles e invisibles. Siendo a la vez el error y la
corrección de la “esclavitud maquínica” de la mente heterosexual, el glitch revela
y oculta simbióticamente. Por lo tanto, la acción política del feminismo glitch es
el llamado a colectivizar en red, amplificando nuestras exploraciones del género
como medios para deconstruirlo, “reestructurando las posibilidades de acción.”[^19]

[^19]: Ibid.

En el trabajo de la artista y drag queen residente en Londres, Victoria Sin, podemos
ver habitada esta restructuración. Asignada como femenina al nacer, Sin se identifica
como no binarix y queer, un cuerpo que amplifica el género en su performance del
mismo, tanto online (a través de Instagram) como AFK. En el escenario –ya sea en
el mundo o envueltx dentro de los seductores tejidos de lo digital- Sin juega con
las trampas[^20] del género. Sus personajes drag se mantienen altamente femeninos;
estos diferentes yoes realizan una performance que enfatiza la producción sociocultural
de la feminidad exagerada como un tropo del género, un ritual y un ejercicio.

[^20]: Nota de la traductora: “trappings” significa tanto rasgos como trampas. Me
quedé con trampas.

Sin se pone el género como una prótesis. Un homenaje a la historia expansiva de la
performance femenina/masculina del drag y del _genderfucking_.[^21] El vestuario de
Sin está repleto de encastres de senos y nalgas, una peluca voluptuosa, un maquillaje
pintado con una vivaz maestría, y un amplio y brillante vestido. La estética de Sin
es un cocktail evocativo e hiptonizador, que teje con sátira y experticia el estilo
sensorial del cabaret, el rumor del burlesque, el glamour del Hollywood vintage —
todo con un toque de Jessica Rabbit.

[^21]: Nota de la traductora: algo así como “joder con el género”.

![3-2](editorial/feminismo-glitch-legacy-russell/3-2.png)
