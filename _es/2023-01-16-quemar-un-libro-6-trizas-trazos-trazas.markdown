---
title: Quemar un libro 6 [ Trizas Trazos Trazas ]
description: Trizas Trazos Trazas by Victoria Burgos, Gaby Peral, Bárbara Stark and Ángeles Torino
title_page: ~/Quemar un libro a la luz de un scanner [ Trizas Trazos Trazas ]
author: librenauta
image:
  path: public/f7580462-d3b9-474f-b001-1b797b906df3/cover.jpg
  description: Trizas Trazos Trazas by Victoria Burgos, Gaby Peral, Bárbara Stark and Ángeles Torino
permalink:
category: scann
tags:
- risografía
- book
- córdoba
draft: false
order:
layout: post
uuid: f7580462-d3b9-474f-b001-1b797b906df3
liquid: false
last_modified_at: 2023-01-16 15:20:21.155018278 +00:00
---

# Trizas Trazos Trazas by Victoria Burgos, Gaby Peral, Bárbara Stark and Ángeles Torino

Cuando estuve en la [mani](https://www.manifestivalgrafico.ar/) en córdoba previamente charlamos con [victoria](https://www.instagram.com/victoriabch/) y nos encontramos para hacer un _trueque_ **hermoso**. Se llevó algunas cosas y me dejó esta joyita en risografía licenciada en CC 4.0 internacional. Lamentablemente no se pueden hacer obras derivadas de este material por la licencia pero si se puede compartir.

aquí el [scann para descarga <3](public/f7580462-d3b9-474f-b001-1b797b906df3/trizas-trazos-trazas.pdf) para mirar de cerquita y copiar
