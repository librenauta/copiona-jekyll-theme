---
layout: post
title: 'Cubo, luz y acción!'
image:
  path: public/1782ec21-dc4e-4f5a-a89b-cdcc623eb958/4.jpg
category: proyectos
author: librenauta
tags:
  - cubo
  - colores
  - luz
uuid: 1782ec21-dc4e-4f5a-a89b-cdcc623eb958
order: 23
draft: false
last_modified_at: 2022-07-19 21:30:21.155018278 +00:00
---
# cubo luzzz

Compré [este](https://www.aliexpress.com/item/1005003685795972.html?spm=a2g0o.order_detail.0.0.1e9bf19cEITJYw) cubo con muy poca esperanza, y resulta que es de vidrio de verdad y con algunas cosas locas :D

tiene 3 caras opuestas de colores: cian, magenta y amarillo [ CMY ] y cuando lo pones en determinada posicion podes generar [ RGB ] red, green and blue :D

> un día a las 10am en mi casa

![](public/1782ec21-dc4e-4f5a-a89b-cdcc623eb958/1.jpg)
![](public/1782ec21-dc4e-4f5a-a89b-cdcc623eb958/2.jpg)
![](public/1782ec21-dc4e-4f5a-a89b-cdcc623eb958/3.jpg)

_divertirse post 30_
