---
title: Una flor en 2D
author: Librenauta
description: como una figurita de flor
proyect: f804d881-5340-465a-a445-0ae0e4853bde
image:
  path: public/a080cb37-6ef6-42a2-90bb-7ebb637a5d29/cover.jpg
  description: "<3"
category: proyectos
tags:
- una flor
- recolección
- flora
draft: false
order:
layout: post
uuid: a080cb37-6ef6-42a2-90bb-7ebb637a5d29
last_modified_at: 2023-01-18 20:20:21.155018278 +00:00
liquid: false
---

# ~ recolectar

Si secás flores en algún cuaderno, como son vegetales después las podés pegar con plasticola al papel <3

* por ahí tardan un poquito en cargar porque las escanie en 600dpi :D

![flores](public/a080cb37-6ef6-42a2-90bb-7ebb637a5d29/una-flor-en-2d-1.jpg)
![flores](public/a080cb37-6ef6-42a2-90bb-7ebb637a5d29/una-flor-en-2d-2.jpg)
![flores](public/a080cb37-6ef6-42a2-90bb-7ebb637a5d29/una-flor-en-2d-3.jpg)

