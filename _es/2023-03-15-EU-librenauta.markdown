---
title: EU-librenauta
author: Librenauta
description: roadmap de librenauta viajando al viejo mundo
proyect: f804d881-5340-465a-a445-0ae0e4853bde
image:
  path: public/a2088214-d68d-443e-a260-621bfe646c6d/eze.png
  description: "dither de una foto del techo de ezeiza"
picture_multiple: public/a2088214-d68d-443e-a260-621bfe646c6d/images
category: proyectos
tags:
- viaje
- eu
draft: false
order:
layout: post
uuid: a2088214-d68d-443e-a260-621bfe646c6d
last_modified_at: 2023-01-20 23:20:21.155018278 +00:00
liquid: false
---

# Roadtrip

> tiempo: [16.03.2023] al [30.04.2023]

* 15.03.2023 Buenos Aires > Madrid ~ aeropuerto

* 16.03.2023 Madrid > Valencia en tren renfe. El primer lugar que voy a visitar es Carcaixent.

* 22.03.2023 Valencia > Barcelona en tren renfe. me cruzo con sofie & milton en su casa. y después un piso en el centro de bcn

* 03.04.2023 Barcelona > Berlin en avión ~ tren a Berlin Friedrichstraße station para llegar caminando al hostel

* 10.04.2023 Berlin > Rotterdam ~ tren hasta la rotterdam central station

* 16.04.2023 Rotterdam > Bruselas ~ tren de rotterdam central station a Brusells-Mid

* 16.04.2023 Bruselas > Londres ~  salgo para londres en el Eurostar, llego a St Pancras International y luego hostel

* 22.04.2023 Londres > Valencia ~ avión 

* 29.04.2023 Valencia > Madrid ~ tren para la estación

* 29.04.2023 Madrid > Argentina


### Cuaderno de viaje

perdón por mi letra. se que tengo que mejorarla. pero lo que no tengo de caligrafía lo tengo de recolectora.
 
estan escaneadas _muy_ increible para poder usar las flores en los colashes que necesites.

todas las flores son libres.