---
layout: post
title: 'EDSL tapas'
tags:
  - EDSL
  - software libre
  - teoría
  - cultura libre
category: proyectos
author: librenauta
uuid: 12920754-a34b-4ff3-8cf9-0b9e8e4e2c6c
order: 11
draft: false
last_modified_at: 2020-05-22 22:30:21.155018278 +00:00
---
# EDSL tapas

### Diseño de tapas para 5 artículos que me parecía importante poder tener en formato fanzine de la revista de teoría sobre software libre y cultura libre [en defensa del software libre](https://endefensadelsl.org)

Las imágenes de fondo son del particular [himalayev](https://himalayev.tumblr.com/) me sirvieron de soporte y contraste para poder diseñar algo que conviva con las hermosas creaciones con pinceladas pixelares en proccesing del hima. gracias por compartirlas.

## Pensamiento crudo
#### Aaron Swartz
![pensamiento](public/proyectos/pensamiento-crudo/pensamiento-crudo.png)
1. [link a img de mejor resolución](public/proyectos/pensamiento-crudo/pensamiento-crudo-hd.png)
2. [link a svg](public/proyectos/pensamiento-crudo/pensamiento-crudo.svg)
3. [link a pensamiento crudo](https://utopia.partidopirata.com.ar/zines/pensamiento_crudo-imposed.pdf)
4. [link al pdf en imposición del manifiesto by edsl](https://endefensadelsl.org/guerrilla_del_acceso_abierto.html)
4. [bkp en copiona](public/proyectos/pensamiento-crudo/guerrilla_del_acceso_abierto-imposed.pdf)

## En nosotras confiamos: acuñando alternativas al capitalismo
#### Jerome Roos
![en-nosotras](public/proyectos/en-nosotras-confiamos/en-nosotras-confiamos.png)
1. [link a img de mejor resolución](public/proyectos/en-nosotras-confiamos/en-nosotras-confiamos-hd.png)
2. [link a svg](public/proyectos/en-nosotras-confiamos/en-nosotras-confiamos.svg)
3. [link a en nosotras confiamos imposición](https://endefensadelsl.org/moneylab-imposed.pdf)
4. [bkp en copiona](public/proyectos/en-nosotras-confiamos/en-nosotras-confiamos-imposicion.pdf)

## Las hackers no pueden solucionar la vigilancia
#### Dmytri Kleiner
![Las-hackers](public/proyectos/las-hackers-no-pueden-solucionar-la-vigilancia/las-hackers-no-pueden-solucionar-la-vigilancia.png)
1. [link a img de mejor resolución](public/proyectos/las-hackers-no-pueden-solucionar-la-vigilancia/las-hackers-no-pueden-solucionar-la-vigilancia-hd.png)
2. [link a svg](public/proyectos/las-hackers-no-pueden-solucionar-la-vigilancia/las-hackers-no-pueden-solucionar-la-vigilancia-hd.png)
3. [link a al txt en imposición](https://endefensadelsl.org/las_hackers_no_pueden_solucionar_la_vigilancia-imposed.pdf)
4. [bkp en copiona](public/proyectos/las-hackers-no-pueden-solucionar-la-vigilancia/las-hackers-no-pueden-solucionar-la-vigilancia-imposicion.pdf)

## Hacklabs y Hackerspaces: rastreando dos genealogías
#### Maxigas
![hacklabs](public/proyectos/hacklabs/hacklabs.png)
1. [link a img de mejor resolución](public/proyectos/hacklabs/hacklabs-hd.png)
2. [link a svg](public/proyectos/hacklabs/hacklabs-hd.png)
3. [link al txt en imposición](https://endefensadelsl.org/hacklabs-y-hackerspaces-imposed.pdf)
4. [bkp en copiona](public/proyectos/hacklabs/hacklabs-imposicion.pdf)

## Usuaria turing
#### Olia Lialina
![hacklabs](public/proyectos/usuaria-turing/usuaria-turing.png)
1. [link a img de mejor resolución](public/proyectos/usuaria-turing/usuaria-turing-hd.png)
2. [link a svg](public/proyectos/usuaria-turing/usuaria-turing.svg)
3. [link al txt en imposición](https://endefensadelsl.org/usuaria_turing_completa-imposed.pdf)
4. [bkp en copiona](public/proyectos/usuaria-turing/usuaria-turing-imposicion.pdf)
