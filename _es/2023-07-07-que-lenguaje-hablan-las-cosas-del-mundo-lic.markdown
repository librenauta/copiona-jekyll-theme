---
title: Qué lenguaje hablan las cosas del mundo [LIC]
author: MAQ && Librenauta
description: LiC como parte de muestra en espacio galería Ramos generales
image:
  path: public/171f23c3-3011-4083-8006-75aacb220c7a/cover.jpg
  description:
category: proyectos
tags:
- arte-electronico
- audio
- ascii
draft: false
order:
picture_multiple: public/171f23c3-3011-4083-8006-75aacb220c7a/images
layout: post
uuid: 171f23c3-3011-4083-8006-75aacb220c7a
last_modified_at: 2023-07-07 13:20:21.155018278 +00:00
liquid: false
---

Después de cruzar ideas con [MAQ](https://maq-arena.com.ar/vistatrabajo.html) en barcelona, quedamos en contacto para poder pensar algo en conjunto ya que veíamos que tenemos algunas perspectivas similares sobre cómo leer cosas del mundo~.

MAQ tiene una visión transversal del sonido y la performance. Queríamos hacer algo que pueda inundar el espacio de transformación: sonido y gráfica


Gracias a la invitación de [Andrés Belfanti](http://andresbelfanti.com/) a exponer en [Ramos Generales](https://www.instagram.com/rgenerales) pudimos ponernos a pensar algo para la muestra: ¿Qué lenguajes hablan las cosas del mundo?

Acá un registro de lo que hicimos con varias imagenes y videos de [Aina](https://instagram.com/arania.tvroom) : 

<figure class="w-100">
  <video src="public/171f23c3-3011-4083-8006-75aacb220c7a/lic-0.mp4" autoplay="autoplay" loop="loop" controls="controls"  >
  </video>
</figure>

---

<figure class="w-100">
  <video src="public/171f23c3-3011-4083-8006-75aacb220c7a/lic-3.mp4" autoplay="autoplay" loop="loop" controls="controls"  >
  </video>
</figure>

<figure class="w-100">
  <video src="public/171f23c3-3011-4083-8006-75aacb220c7a/lic-1.mp4" autoplay="autoplay" loop="loop" muted="muted" controls="controls"  >
  </video>
</figure>

<figure class="w-100">
  <video src="public/171f23c3-3011-4083-8006-75aacb220c7a/lic-4.mp4" autoplay="autoplay" loop="loop" controls="controls"  >
  </video>
</figure>

<figure class="w-100">
  <video src="public/171f23c3-3011-4083-8006-75aacb220c7a/lic-2.mp4" autoplay="autoplay" loop="loop" muted="muted" controls="controls"  >
  </video>
</figure>
