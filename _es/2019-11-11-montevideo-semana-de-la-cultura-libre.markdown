---
title: Montevideo - Semana de la cultura libre
author: Librenauta
description: Semana de la cultura libre 2019
image:
  path: public/50fa33ca-3372-4471-b474-83cab9f5ddc0/cover.jpg
  description:
picture_multiple: public/50fa33ca-3372-4471-b474-83cab9f5ddc0/images
category: proyectos
enable_gpx_traces: true
gpx: public/50fa33ca-3372-4471-b474-83cab9f5ddc0/TTT-2019-11-18.gpx
tags:
- cultura libre
- uruguay
- montevideo
draft: false
order:
layout: post
uuid: 50fa33ca-3372-4471-b474-83cab9f5ddc0
last_modified_at: 2019-11-11 12:20:21.155018278 +00:00
liquid: false
---

# Semana de la cultura libre en Montevideo

presente la bibliobox en [aquí](https://festival.creativecommons.uy/cultura-libre-de-copyright/)
