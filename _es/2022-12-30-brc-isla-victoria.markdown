---
title: BRC - isla victoria
author: Librenauta
description: log de imágenes de una travesía en brc
image:
  path: public/109ef8c4-1e86-43a1-98f2-0b834269c923/isla-victoria-1.jpg
  description:
category: proyectos
enable_gpx_traces: true
gpx: public/109ef8c4-1e86-43a1-98f2-0b834269c923/2022-12-30_19-28_Fri.gpx
tags:
- brc
- naturaleza
- sur
- isla
draft: false
order:
picture_multiple: public/109ef8c4-1e86-43a1-98f2-0b834269c923/images
layout: post
uuid: 109ef8c4-1e86-43a1-98f2-0b834269c923
last_modified_at: 2022-12-30 22:20:21.155018278 +00:00
liquid: false
---

# BRC - isla victoria
