---
layout: editorial
title: 'Editorial p2p'
image:
  path: editorial/editorial.jpg
proyects:
- 7f329843-a8aa-4eac-b1c5-f4f3fdb5ab8b
- 5275e5b9-5377-4881-84cf-6f9b183d829c
- 8900e5ce-9be2-4f14-98c8-de713f181765
- 1b31b24c-69d2-4cf7-a92e-f2037cc0d9d1
- 8f1db3ff-b2ef-42b6-8197-20b7728c0996
- f296d22b-8711-4d79-9445-4e261f2f3edd
- db811759-96d7-49e9-b084-79420e0b63e4
- 35f397c8-dfbf-4383-8911-286d14d62762
- 9db959e9-265e-4ef8-bc4b-0b59827ed666
- 7ffca44a-9823-4e44-8df8-16eb38d63c2f
- 58000ba3-dfa1-4d06-98d6-f10a8a711d3d
- d0910868-56fa-48dc-9dfe-dd39d4645529
- c77c09c0-3c2c-4922-b94a-7158fb837702
- a97959fa-348c-4810-a8ac-a9c58f5d3cde
author: librenauta
uuid: f804d881-5340-465a-a445-0ae0e4853bde
tags:
- editorial
- script
- automatización
- p2p
order: 18
draft: false
last_modified_at: 2020-08-15 22:30:21.155018278 +00:00
---

p2p Pequeña editorial autogestiva creada con software libre y edición de pequeñas tiradas

[acá podés ver un tutorial paso a paso de como instalar y publicar textos](/editoria-latex-pandoc)

Estas pequeñas ediciones de textos son posibles gracias al inmenso apoyo y herramientas de [utopia pirata](https://utopia.partidopirata.com.ar) y [perrotuerto](https://perrotuerto.blog) con su script [export-pdf](https://gitlab.com/snippets/1917490/raw)
