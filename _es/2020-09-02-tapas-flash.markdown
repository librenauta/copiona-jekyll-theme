---
layout: post
title: 'Tapas Flash'
category: proyectos
tags:
  - diseño
  - glitch
  - tapas
  - visual poetry
author: librenauta
uuid: cd2efe2b-030b-4be5-abf9-00e1e7eee9b0
order: 20
draft: false
last_modified_at: 2020-09-02 22:30:21.155018278 +00:00
---
# Tapas Flash para editorial flash #

 > Tapas flash es un mini proyecto de editorial flash para diseñar tapas y asignar el tiempo que me llevó hacerlas en la contratapa, buscando partes de otras imágenes, y haciendo colash vectorial <3


#### Visual Poetry #3
Me imprimi unas tapas flash que me diseñe para un nuevo cuaderno: filmina | adhesivo flujo | contac chroma | opalina 120gr #exp #rmx

![visual](public/proyectos/visual-poetry/visual-poetry-cover.png)

![visual](public/proyectos/visual-poetry/visual-poetry-cover-1.jpg)
![visual](public/proyectos/visual-poetry/visual-poetry-cover-2.jpg)
![visual](public/proyectos/visual-poetry/visual-poetry-cover-3.jpg)
![visual](public/proyectos/visual-poetry/visual-poetry-cover-4.jpg)

1. [link a pdf](public/proyectos/visual-poetry/visual-poetry-cover.pdf)

#### Chaos poetry <3 #2

![visual](public/proyectos/visual-poetry/chaos-poetry-2021.png)

Pueden descargar e imprimirse sus propias tapas <3 (están en formato A4)

1. [link a svg](public/proyectos/visual-poetry/chaos-poetry-2021.svg)
2. [link a pdf](public/proyectos/visual-poetry/chaos-poetry-2021.pdf)

Esta tapa fue diseñada para el calendario del [2021] que me compartió [jine](https://www.nervousdata.com/) via [mastodon](https://sonomu.club/@jine/105697515019410041)
[link en otros idiomas](https://nervousdata.com/kalender.html)
[* PDF páginas individuales ES](https://www.nervousdata.com/files/1semana1pagina_2021_es.pdf)
[* PDF para imprimir (imposición) ES](https://www.nervousdata.com/files/1semana1pagina_2021_es_print.pdf)

#### Visual Poetry <3 #1
![visual](public/proyectos/visual-poetry/visual-poetry.jpg)

![visual](public/proyectos/visual-poetry/visual-poetry-2.jpg)

Pueden descargar e imprimirse sus propias tapas <3 (están en A4)

1. [link a svg](public/proyectos/visual-poetry/visual-poetry.svg)
2. [link a pdf](public/proyectos/visual-poetry/visual-poetry.pdf)

_un glitch scan_
![un glitch](public/proyectos/visual-poetry/editorial-flash-glitch.jpg)
