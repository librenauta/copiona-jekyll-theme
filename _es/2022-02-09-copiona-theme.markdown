---
title: copiona-theme
font_family_sans_serif: Fira Code
headings_font_family: Fira Code
enable_rounded: false
enable_shadows: false
body_bg: "#ffffff"
body_color: "#000000"
primary: "#751fa7"
secondary: "#ff48b0"
link_color: "#751fa7"
link_hover_color: "#000000"
h1_font_size: 2.5
h2_font_size: 2.0
h3_font_size: 1.75
h4_font_size: 1.5
h5_font_size: 1.25
h6_font_size: 1.0
mark_bg: "#ff5555"
navbar_light_color: "#ff5555"
navbar_light_hover_color: "#6c757d"
navbar_light_active_color: "#212529"
order: 6
draft: false
layout: theme
uuid: d8e33d0c-6957-4ef8-875f-b28f10a8359a
liquid: false
usuaries:
- 27
last_modified_at: 2022-02-11 18:16:01.287387361 +00:00
---
