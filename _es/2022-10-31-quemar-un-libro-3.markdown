---
title: Quemar un libro 3 [ Nuestros refugios a medio armar - Valeria Mussio ]
description: libro escaneado
title_page: ~/Quemar un libro a la luz de un scanner [ Nuestros refugios a medio armar - Valeria Mussio ]
author: librenauta
image:
permalink:
category: scann
tags:
- scanner
- book
- libros
- quemar
- poesía
draft:
order:
layout: post
uuid: 8022daa0-b6c2-4c26-bb10-8e24b9d547bc
liquid: false
last_modified_at: 2022-10-31 17:20:21.155018278 +00:00
---

## Nuestros refugios a medio armar - Valeria Mussio

valeria es un glitch de internet, hace epubs con gifs. así la conocí yo. ahora leo sus poemas
y pienso que al final se puede ser todo esto: contener un corazón de 8bits y también contemplar la caída de un cometa brillando en nuestros ojos con esa imagen de un futuro que no fue.

![scann-1](public/8022daa0-b6c2-4c26-bb10-8e24b9d547bc/nuestros-refugios-a-medio-armar.png)

![scann-2](public/8022daa0-b6c2-4c26-bb10-8e24b9d547bc/nuestros-refugios-a-medio-armar_laika_1.png)

![scann-2](public/8022daa0-b6c2-4c26-bb10-8e24b9d547bc/nuestros-refugios-a-medio-armar_laika_2.png)
ps psss [acá](pdf/nuestros-refugios-a-medio-armar-valeria-mussio.pdf) tienen para descargar Nuestros refugios a medio armar.

_quemar en una luz para no perder el acceso_
