---
layout: post
title: 'zinenautas'
image:
  path: public/proyectos/zinenauta/cover.jpg
  description: flyer
tags:
  - webzine
  - internet
category: proyectos
author: librenauta
uuid: f4da7832-ac37-4311-a6e4-21082aa41c9c
order: 7
draft: false
last_modified_at: 2020-05-09 22:30:21.155018278 +00:00
---
## Un web/paper zine

## zinenauta

>Zinenauta, web | físico zine de autodefensa digital y cultura libre && internet's

Zinenauta es una exploración visual en proceso constante, aquí podrás ver nuestra relación con las tecnologías junto con una experiencia de cómo bajar a papel una parte de internet. Dentro de este fanzine encontrarás contenido de +culturalibre +code +telegráfica +privacidad +anonimato +collagedigital.

Zi-ne-nau-ta será una publicación de 10 ejemplares, la primera serie de #5 invitan a reflexionar sobre la distopía de las telecomunicaciones y cómo vivímos dentro de internet en el contexto de una sociedad de control, por otra parte los siguientes 5 números explorarán todas las aristas que creemos valiosas en la red desde que la conocemos, soy muy fan de internet. no puedo solo ver el vaso medio lleno hay cosas increíbles.

escribir y publicar esto con herramientas creadas por comunidades libres es increíble.

ver online en: --> [zinenauta.copiona.com](https://zinenauta.copiona.com)

![zinenautas](public/proyectos/zinenauta/zinenauta.jpg)

## Los primeros #5

[zinenauta #1](https://zinenauta.copiona.com)
![img](public/proyectos/zinenauta/z1.png)

[zinenauta #2](https://zinenauta.copiona.com/Z2.html)
![img](public/proyectos/zinenauta/z2.png)

[zinenauta #3](https://zinenauta.copiona.com/Z3.html)
![img](public/proyectos/zinenauta/z3.png)

[zinenauta #4](https://zinenauta.copiona.com/Z4.html)
![img](public/proyectos/zinenauta/z4.png)

[zinenauta #5](https://zinenauta.copiona.com/Z5.html)
![img](public/proyectos/zinenauta/z5.png)

[zinenauta #6](https://zinenauta.copiona.com/Z6.html)
![img](public/proyectos/zinenauta/z6.png)

[zinenauta #7](https://zinenauta.copiona.com/Z7.html)
![img](public/proyectos/zinenauta/z7.png)

podés comprarlos en papel en escribiendome [aquí](https://t.me/librenauta) o [aquí](mailto:librenauta@riseup.net) <3
