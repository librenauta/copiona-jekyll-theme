---
title: Quemar un libro 8 [ Un jardín by Cian ]
description: Un jardin, libro de poesia visual y escrita de @soycian
title_page: ~/Quemar un libro a la luz de un scanner [ Un jardín by Cian  ]
author: librenauta
image:
  path: public/406c3ff2-dacf-4e5e-9d97-5dfcd03e4f41/cover.jpg
  description: la tapa contiene una figura que se asemeja a una figura humana pero mezclado con naturaleza
permalink:
category: scann
tags:
- papercut
- soycian
- la plata
- primavera
draft: false
order:
layout: post
uuid: 406c3ff2-dacf-4e5e-9d97-5dfcd03e4f41
liquid: false
last_modified_at: 2023-03-15 21:25:21.155018278 +00:00
---

# Un Jardín by [@soycian](https://instagram.com/soycian) 

Es un pequeño pero muy meticuloso fanzine: permite jugar, mirar a través de las ventanitas y conocer a cian desde el ángulo perfecto: la sensibilidad. 

aquí el [scann para descarga <3](public/406c3ff2-dacf-4e5e-9d97-5dfcd03e4f41/un-jardin.pdf) para mirar de cerquita, imprimir, copiar y compartir.
