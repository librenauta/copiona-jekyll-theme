---
title: Shanzhai - Original
author: Byung Chul-Han
description: Crítica Cultural
proyect: f804d881-5340-465a-a445-0ae0e4853bde
image:
  path: editorial/shanzhai-byung-chul-han/shanzhai-original-cover.png
  description: shanzhai-byung-chul-han-copia.
pdf_read: editorial/shanzhai-byung-chul-han/shanzhai-original.pdf
pdf_cover: editorial/shanzhai-byung-chul-han/shanzhai-original-cover.pdf
pdf_imposition: editorial/shanzhai-byung-chul-han/shanzhai-original_imposition.pdf
category: editorial
export_pdf: 'export-pdf --title="Shanzhai" --author="Byung Chul-Han" --press="pixel2pixel" --template="a6template.tex" --no-day shanzhai-original.md'
tags:
- Shanzhai
- Original
- Libro
draft:
order:
layout: book
uuid: db811759-96d7-49e9-b084-79420e0b63e4
liquid: false
---
# BYUNG-CHUL HAN SHANZHAI

## El arte de la falsificación y la deconstrucción en China

ZHENJI: ORIGINAL

En una carta a Wilhelm Fliess del 6 de diciembre de 1896,
Freud escribe: "Sabes que trabajo con el supuesto de que nuestro
mecanismo psíquico se ha generado por superposición de
capas porque de tiempo en tiempo el material existente de
huellas mnémicas experimenta un reordenamiento según nuevas
concernencias, una inscripción. Lo esencialmente nuevo
en mi teoría es entonces la tesis de que la memoria no existe
de manera simple sino múltiple, registrada en diferentes variedades
de signos". [^1]

[^1]: Sigrnund Freud, _Cartas a Fliess (1887-1904)_, Buenos Aires, Arnorrortu
Editores, 1986, pág. 218.

Así pues, las imágenes del recuerdo no
son reflejos invariables de la vivencia. Más bien son producto
de una construcción compleja del aparato psíquico. Están so
metidas a una transformación incesante. Las constelaciones
y relaciones nuevas modifican su aspecto constantemente. El
aparato psíquico sigue un complejo movimiento temporal, por
medio del cual lo posterior también constituye a lo anterior.

En él se entremezclan pasado, presente y futuro. La concepción
de Freud de la transcripción cuestiona toda teoría de la
reproducción, que presupone que las escenas vividas quedan
grabadas invariablemente en la memoria y pueden ser recuperadas
 en su misma forma después de mucho tiempo. Los
recuerdos no son copias que se mantienen iguales a sí mismas,
sino huellas que se cruzan y se superponen.
En chino clásico, el original se denomina zhenji [真機]
Literalmente significa "huella verdadera". Se trata de una
huella singular, puesto que no tiene lugar en una trayectoria
teleológica. Y en su interior no está habitada por ninguna
promesa. Tampoco está relacionada con nada enigmático ni
kerigmático. Además, no se condensa hasta adoptar una
presencia unívoca y uniforme. Más bien deconstruye toda idea
de un original que encarna una presencia y una identidad
invariables e inconfundibles que descansan en sí mismas.[^2]

[^2]: También Derrida denomina _"différance"_ a la "huella", que escapa a
toda marca en la presencia y la identidad ( Jacques Derrida, _Márgenes de la filosofía,_
Madrid, Cátedra, 2006). Su concepto de la huella carece de
cualquier dimensión teleológica o teológica. En eso también se diferencia
de la figura heideggeriana de la "huella", "una promesa casi inaudible",
"una liberación hacia lo abierto", "a veces oscura y desorientadora, a veces
como un relámpago de súbita intuición" (Martin Heidegger, De camino
al habla, Barcelona, Ediciones del Serbal, 1987, pág. 124).

El proceso y la diferencia le confieren una fuerza centrífuga
deconstructiva. No da lugar a ninguna obra de arte acabada,
cerrada en sí misma, que pudiera adoptar una forma definitiva
y escapar a toda transformación. Su diferencia respecto
de sí misma no le permite alcanzar el reposo que le podría
proporcionar una forma definitiva. En este sentido, siempre
se aparta de sí misma. La concepción china del original como
huella (ji, [機] ) remite a la estructura freudiana de la "huella
mnémica", que está sometida a un reordenamiento y transcripción
constantes. La idea del original chino no se entiende como una
creación única sino como un proceso infinito, no apunta a la identidad
definitiva sino a la transformación incesante. El cambio, sin embargo,
no tiene lugar en el alma de una subjetividad artística. La huella se
borra en favor de un proceso que no admite una fijación esencialista.

El Lejano Oriente no conoce ninguna dimensión prede-
constructiva como la del original, el origen o la identidad.
En realidad, el pensamiento del Lejano Oriente _comienza con_
la deconstrucción. El ser, en tanto concepto fundamental
del pensamiento occidental, es igual a sí mismo, no permite
ninguna reproducción más allá de sí mismo. La prohibición
platónica de la mímesis surge de esta concepción del ser. La
belleza o el bien, según Platón, permanecen inmutables y
solo idénticos a sí mismos. Son "uniformes" (monoeides). De
este modo, no queda lugar para ninguna desviación. Según
esta interpretación del ser, toda reproducción tiene algo de
demoníaco, que destruye la identidad y la pureza originarias.
La idea platónica ya bosqueja la reflexión sobre el original.
Toda reproducción conlleva una _falta de ser_. En cambio,
la figura fundamental del pensamiento chino no es el ser
uniforme y único, sino el _proceso_ poliforme y heterogéneo.

Una obra de arte china nunca permanece idéntica a sí misma.
Cuanto más venerada, más cambia su aspecto. Los expertos
y los coleccionistas escriben sobre ella. Se inscriben en la obra
por medio de marcas y sellos. De esta manera, se van superponiendo
inscripciones, de igual manera que las huellas mnémicas en
el aparato psíquico. La propia obra está en transformación
constante, sometida a una transcripción incesante. Esta
no descansa en sí misma. Más bien fluye. Se opone a la presencia.
La obra se vacía convirtiéndose en un lugar que genera y
comunica inscripciones.[^3]

Cuanto más famosa es una obra, más
inscripciones muestra. Se presenta como un palimpsesto.

[^3]: La transformación constante del original no solo está ligada a la historia
de su recepción, sino también a otros factores: "A lo largo del tiempo, la
obra se modifica cambiando de formato debido a los nuevos montajes, los recortes
por el mal estado del material, los motivos estéticos o comerciales,
los retoques o la incorporación posterior de firmas. En los casos más radicales,
ante un cuadro chino podría aplicarse La metáfora del barco que regresa
a su puerto de partida después de generaciones, habiendo cambiado por el
camino todas sus piezas tras infinitud de reparaciones. ¿Se trata del mismo
barco? La tripulación es otra, los habitantes de la ciudad de origen son otros,
y no se conserva ningún plano que pueda certificar que al menos se mantiene
la forma original del barco después de todos los recambios de las piezas"
(Christian Unverzagt, _Der Wandlungsleib des Dong Yuan. Die Geschichte eines_
malerischen Œuvres [El cuerpo cambiante del Dong Yuang. Historia de una
obra pictórica], Stuttgart, Franz Steiner Verlag, 2007, pág. 184).

![](editorial/shanzhai-byung-chul-han/shanzhai-original-1.png "shanzhai")

La transformación no afecta únicamente a algunas
piezas concretas de un artista, sino a toda su obra. Su
corpus se modifica de modo constante. Aumenta y disminuye.
Se agregan imágenes que aparecen de pronto, y desaparecen
otras que en un pasado se le habían atribuido. Como la
obra del conocido maestro Dong Yuan, que tiene un aspecto
distinto durante la dinastía Ming que durante la dinastía Song.
Las falsificaciones o las reproducciones también definen la
imagen de un maestro. Se lleva a cabo una inversión temporal.
Lo posterior o sucesivo determina el origen. De este modo lo
deconstruye. La obra es un gran espacio vacío o en construcción
que siempre se está llenando de nuevos contenidos, de nuevas
imágenes. En este sentido, podríamos decir: cuanto
más grande es un maestro, más vacía está su obra. Es un
significante sin identidad, que se llena constantemente
de nuevos significados. El origen se muestra como una
construcción posterior.[^4]

[^4]: Tampoco La pretensión de autenticidad, ajena a los chinos,
es capaz de fijar inequívocamente la obra de un maestro. Según
el catálogo establecido por Wilhelm Valentiner (1921), la obra de
Rembrandt está compuesta por 711 pinturas. Bredius (1935) contabilizó
630. Y treinta años después (1968) Horst Gerson solo reconoció 420
631. como auténticas. El corpus del Rembrandt Research Project, que
excluye las obras de sus colaboradores, habla de 300 obras.
EL meticuloso análisis de estilo de los entendidos o expertos
tampoco está libre de arbitrariedad.

Tampoco Adorno concibe la obra de arte como una for-
ma estática, fija e invariable, sino como algo vivo espiritualmente,
que es capaz de cambiar: "Lo que entretanto
ha cambiado en Wagner no es meramente su efecto, sino
la obra misma, en sí. ( ... )Las obras de arte en cuanto algo
espiritual no son nada en sí definitivo. Forman un campo
de tensiones de todas las voluntades y fuerzas posibles, de
las tendencias internas y de lo que se les opone, de los logros
y de los necesarios fracasos. Una y otra vez, de ellas se
desprenden, surgen objetivamente nuevos estratos; otros
se hacen indiferentes y se extinguen. La verdadera relación
con una obra de arte no es tanto que esta, como se dice, se
adecue a una nueva situación, como que en la obra misma
se descifre aquello a lo que históricamente se reacciona
de manera distinta".[^5]

[^5]: Theodor W. Adorno, "Actualidad de Wagner", en Escritos musicales
I-III, Madrid, Akal, 2006, pág. 556.

En este fragmento la obra de arte se
presenta como un ser vivo que crece, que muda de piel y
se transforma. El cambio no se produce en la "situación"
externa, sino en el interior del ser, que está en la base de
la obra. Adorno se distancia claramente de la transformación
de lo idéntico, que se debe a una situación. Según
Adorno, la obra de arte sería un cuerpo cambiante, que no
está sometido al cambio, sino que él mismo cambia. La riqueza
interior y la profundidad interior de la obra pasarían
por su vitalidad y su capacidad de transformación. Se distingue
 por su abundancia inagotable y su profundidad sin
fondo. La animan hasta convertirla en un organismo vivo.
Su riqueza se despliega independientemente de la situación.
Por su parte, la obra de arte china está vacía y es llana.
No tiene alma ni verdad. El vacío des-sustancializador
se abre a las inscripciones y las transcripciones. La obra de
un maestro chino también tiene capacidad de transformarse
porque en sí misma está vacía. Aquello que impulsa el
cambio hacia adelante no es la interioridad del ser, sino la
exterioridad de la transmisión o de la situación.

No solo el estilo de una obra cambia constantemente,
sino también su tema. Cada época tiene una idea distinta
sobre este. Así pues, puede suceder que los verdaderos originales
de un maestro se alejen de su obra, mientras que
las falsificaciones, que responden al gusto de la época, sean
tomadas en cuenta y se revelen importantes históricamente.
En este caso, las falsificaciones tienen más valor histórico
artístico que el original verdadero. Son más originales que
el original. Las preferencias estéticas de una época, el gusto
predominante de un período influye en la obra de un maestro.
Los cuadros con temáticas que no son contemporáneas
caen en el olvido, mientras que aumentan las obras de temas
populares. Por ejemplo, si una época está muy marcada por
las tradiciones, de pronto en la obra de Dong Yuan se multiplican
los cuadros con imágenes de motivos tradicionales.
La transformación silenciosa de su obra responde a las distintas
necesidades a lo largo del tiempo. En la época de la
dinastía Ming, en que los comerciantes hacían de mecenas y
desempeñaban un papel importante para el arte, de repente,
en los cuadros de Dong Yuan, apareció un motivo nuevo: los
vendedores.[^6]

[^6]: Ibíd., pág. 128.

Las falsificaciones y las recreaciones trabajan
permanentemente en favor de esta transformación.

En la antigua práctica artística china, el aprendizaje
se lleva a cabo copiando.[^7]

[^7]: Wen Fong, "The Problem of Forgeries in Chinese Painting" [El problema
de la falsificación en la pintura china], en Artibus Asiae, Vol. 25, 1962,
pág. 100: "El hecho es que la larga tradición de aprendizaje del arte de la
pintura a través de la copia hizo que en China cada pintor fuera un potencial
falsificador, y es bien sabido que algunos de los más grandes pintores
chinos y expertos eran, o se decía que eran, maestros 'falsificadores'.
Según Chao Hsi-ku (principios del siglo xvrn), Mi Fu tenía el hábito de sacar
ventaja de su preeminencia como experto sustituyendo importantes obras
maestras, que le eran entregadas para 'autentificación', por copias exactas".

La copia también funciona como una señal de respeto para con el maestro.
La gente estudia, elogia y venera una obra copiándola. La copia es una
alabanza. Se trata, en realidad, de una práctica que
tampoco resulta desconocida para el arte europeo. La copia
de Manet del cuadro de Gauguin parece una declaración
de amor. Las imitaciones de Van Gogh de las estampas
japonesas de Hiroshige expresan admiración. Es sabido que
Cézanne iba a menudo al Louvre para copiar a los maestros
antiguos. Ya Delacroix lamentaba que se abandonara el
ejercicio de la copia como fuente esencial e inagotable de
conocimiento. El culto a la originalidad deja en un segundo
plano esta práctica esencial para el proceso creativo.
En realidad, la creación no es un _acontecimiento_ repentino,
 sino un proceso dilatado, que exige un diálogo intenso
con _lo que ya ha sido_ para _extraer_ algo de ello. El constructo
del original borra lo que ha sido, aquello anterior de lo
cual _se extrae_ algo.

En China, no era ninguna tontería para la carrera de un
pintor colocar una falsificación de un antiguo maestro en la
colección de un experto reputado. Si un pintor lograba crear
la falsificación de un maestro, ganaba un gran reconocimiento,
puesto que era una manera de demostrar sus capacidades.
Para el experto que autentificaba la falsificación, esta tenía el
mismo valor que la obra del maestro. A Zhang Daqian, el pintor
chino más famoso del siglo xx, le llegó la fama cuando un
respetado coleccionista confundió una de sus falsificaciones
de un maestro antiguo con el original. En lo relativo al conocimiento,
los falsificadores y los expertos no se diferencian
en lo esencial. Entre ellos se genera una competencia, un
"duelo por el conocimiento",[^8]

[^8]: Christian Unverzagt, Der Wandlungsleib des Dong Yuan, op. cit., pág. 199.

en el que está en juego quién tiene un conocimiento más íntimo del arte del maestro.

![](editorial/shanzhai-byung-chul-han/shanzhai-original-2.png "shanzhai")

![](editorial/shanzhai-byung-chul-han/shanzhai-original-3.png "shanzhai")

![](editorial/shanzhai-byung-chul-han/shanzhai-original-4.png "shanzhai")

![](editorial/shanzhai-byung-chul-han/shanzhai-original-5.png "shanzhai")

![](editorial/shanzhai-byung-chul-han/shanzhai-original-6.png "shanzhai")

Cuando un falsificador toma prestado un cuadro de una
colección y al devolverlo no entrega el original sino una copia,
no estamos ante un engaño, sino ante un acto de justicia.[^9]

[^9]: Wen Fong, _The Problem of Forgeries, op. cit.,_ pág. 99: "Debe ser señalado
que el arte de la falsificación en China nunca cargó con las oscuras connotaciones
que tiene en occidente. Como el objetivo del estudio del arte siempre
ha sido cultivar la estética o el puro placer, antes que el conocimiento científico,
la adquisición de una obra maestra genuina -y por la misma razón, la habilidad
de crear una perfecta falsificación- era una cuestión de virtuosismo y
orgullo. El problema legal o ético de una 'honesta transacción de negocios' no
tiene nada que ver. Es más, era precisamente por muy buenos motivos éticos
y prácticos, que el propietario de una falsificación era usualmente protegido
de que se sepa la verdad. La verdad científica no tenía ninguna relevancia
inmediata en la apreciación del arte. Si alguien era lo suficientemente crédulo
tanto para comprar como para disfrutar falsificaciones, ¿por qué arruinar las
ilusiones de ese pobre hombre?".



La regla del juego reza así: cada uno tiene el cuadro que
se merece. No es la capacidad adquisitiva sino el conocimiento
lo que legitima la posesión de un cuadro. Se trata de una
insólita práctica de la China antigua a la que la especulación
contemporánea en el mundo del arte vendría a poner fin.

En la película Fraude de Orson Welles, Elmyr de Hory,
mientras falsifica un cuadro de Matisse ante la cámara, dice
sobre este: "Muchos de estos cuadros son débiles. El trazo
de Matisse no es muy seguro, me parece. Dibuja fragmento
a fragmento dudando mucho. Y siempre añade algo, una y
otra vez. Sus líneas no fluyen, no se muestran tan dúctiles y
certeras como las mías. Debo dudar para que se parezca más
a Matisse". Elmyr pinta mal a propósito, para que su falsificación
se parezca más al original. De este modo, invierte el
comportamiento convencional entre el maestro y el falsificador:
el falsificador pinta mejor que el maestro. También podría
decirse que si Elmyr quisiera hacer una copia de Matisse
más original que el original lo lograría, porque sus habilidades
plasmarían la intención del artista mejor que este.
Cuando el falsificador más famoso de Vermeer, Han van Meegeren,
presentó en París su recreación libre de _La cena de Emaús,_
todos los expertos de Vermeer, que se creían a
sí mismos infalibles, declararon que el cuadro era verdadero.
Tampoco los análisis técnicos pudieron revelar que se
trataba de una falsificación.[^10]

[^10]: Frank Amau, _Kunst der Fiilscher - Fiilscher der Kunst_ [El arte de los
falsificadores, falsificadores de arte], Düsseldorf, Econ, 1964, pág. 258: "El
octogenario decano entre los historiadores del arte de Holanda, el doctor
Abraham Bredius, una autoridad en el terreno de los maestros holandeses,
inspeccionó el cuadro y declaró que era una obra de Vermeer. Por precaución,
también se hicieron las 'cuatro pruebas de la verdad', que por esa
época se tenían por infalibles: l. La resistencia de los colores ante el alcohol
y otros disolventes. 2. Los restos de albayalde en las partes blancas.
3. Radioscopia del fondo. 4. Estudio con microscopio y análisis espectral
de los colorantes más importantes. No pudieron constatar nada que negara
la autenticidad del cuadro".

El 18 de septiembre de 1938, el cuadro se presentó en sociedad.
La crítica estaba entusiasmada.
Van Meegeren hizo la falsificación a conciencia.
Estudió viejos documentos para poder restituir los pigmentos
originales. Cual alquimista, experimentó con óleos y
disolventes. Buscó cuadros viejos del siglo XVII en las tiendas
de antigüedades para conseguir lienzos originales, a los
que les quitó los colores y les volvió a dar la primera capa.

![](editorial/shanzhai-byung-chul-han/shanzhai-original-7.png "shanzhai")

![](editorial/shanzhai-byung-chul-han/shanzhai-original-8.png "shanzhai")

Después de la Segunda Guerra Mundial, cuando examinaron
la colección de pintura del mariscal del Reich
Hermann Goring, descubrieron un Vermeer desconocido
hasta entonces, _Cristo y la adúltera._ Al investigar a los
holandeses que habían vendido cuadros de Vermeer a
los nazis, detuvieron y encarcelaron a Han van Meegeren.
En ese momento, nadie le creyó cuando afirmó que
se trataba de una falsificación. Así fue que pintó bajo
observación su último Vermeer, _El joven Cristo en el Templo._
Se cuenta que durante el juicio declaró: "Ayer este
cuadro valía millones. Los expertos y los amantes del
arte llegaban desde todos los rincones del mundo para
admirarlo. Hoy no vale nada, y nadie cruzaría la calle ni
para verlo gratis. Pero el cuadro es el mismo. ¿Qué es lo
que cambió?".

![](editorial/shanzhai-byung-chul-han/shanzhai-original-9.png "shanzhai")

![](editorial/shanzhai-byung-chul-han/shanzhai-original-10.png "shanzhai")

Mientras que el hijo de Van Meegeren todavía afirmaba
en 1951 que muchas de las admiradas obras maestras
que se exhibían en las salas de las grandes galerías
parisinas eran falsificaciones de su padre, Jean Decoen
publicó el escrito _Retour à la vérité_ [Retorno a la verdad],
en el que intentaba defender la autenticidad de _La cena de Emaús._
La idea de original está estrechamente
entrelazada con la de verdad. La verdad es una técnica
cultural, que atenta contra el cambio por medio de
la _exclusión_ y la _trascendencia._ Los chinos aplican otra
técnica cultural, que opera con la _inclusión_ y la _inmanencia._
Solo en el terreno de esta última es posible relacionarse con
las copias y las reproducciones de manera libre y productiva.

Si Elmyr y Van Meegeren hubieran nacido durante el
Renacimiento, no cabe duda de que habrían gozado de
más reconocimiento. Al menos no los habrían perseguido
legalmente. Todavía era embrionaria la idea de una subjetividad
artística genial. La obra aún se imponía sobre el
artista. Tan solo importaba la capacidad artística que uno
pudiera demostrar falsificando obras de los grandes maestros,
que idealmente no debían distinguirse de estas. Si un
falsificador pintaba tan bien como un maestro, entonces
él mismo era a su vez un maestro y no un falsificador. Es
bien sabido que Miguel Ángel era un falsificador genial.
En cierto modo, fue uno de los últimos chinos del Renacimiento.
Como algunos de los pintores chinos, hizo copias
perfectas de los cuadros que había tomado prestados y
sustituyó los originales en vez de devolverlos.[^11]

[^11]: Las ideas de genio y de original se configuran con Leonardo da Vinci.
Este eleva al pintor a genio creador y proclama la superioridad de la pintura
frente al resto de las artes, ante la imposibilidad de hacer una copia
exacta de un cuadro. Sobre la pintura, escribe: "Entre las ciencias inimitables
está en primer lugar la pintura. Ella no se enseña a quien no tiene
don natural, al contrario de las matemáticas, en las que el discípulo recibe
tanto cuanto el maestro le enseña; ni se copia como las letras, en las que
tanto vale la copia como el original; ni se modela como en la escultura, en
la que el objeto modelado equivale al original; y en cuanto a la fecundidad
de la obra, esta no produce infinitos hijos como ocurre con los libros impresos.
Solo ella conserva su nobleza, solo ella honra a su autor, y queda
preciosa y única sin parir hijos iguales a ella" (Leonardo da Vinci, Tratado
de la pintura, Madrid, Akal, 2004).

En el año 1956, en el Museo Cernuschi de París, dedicado al arte
asiático, se celebró una exposición de las
grandes obras del arte chino. No tardó en hacerse manifiesto
que en realidad los cuadros eran falsificaciones. La
cuestión es que el falsificador era nada más y nada menos
que el pintor más importante del siglo xx, Zhang Daqian,
cuyas obras se estaban exponiendo a la vez en el Museo
de Arte Moderno. Era considerado el Picasso chino. Ese
mismo año, tuvo lugar un encuentro entre él y Picasso,
que fue interpretado como la cumbre de la confluencia
entre el arte oriental y occidental. Cuando se dio a conocer
que las obras maestras antiguas eran falsificaciones
suyas, el mundo occidental vio en él un burdo mentiroso.
Pero para Zhang Daqian se trataba de cualquier
cosa salvo falsificaciones. La mayoría de estos cuadros
antiguos no eran, bajo ningún aspecto, falsificaciones,
sino reproducciones de pinturas desaparecidas que solo
se habían transmitido literariamente.

En China, los propios coleccionistas a menudo eran
pintores. Como en el caso de Zhang Daqian, que era un
apasionado coleccionista. Poseía más de cuatro mil cuadros.
Su colección no era un archivo muerto, sino una
antología de maestros antiguos, un lugar vivo de comunicación
y transformación. Él mismo era un cuerpo cambiante, un
artista de la metamorfosis. No le costaba lo más
mínimo ponerse en el papel de maestros antiguos y crear,
en cierto modo, originales. "El genio de Zhang probablemente
garantiza que algunas de sus falsificaciones permanecerán
sin ser detectadas por un largo tiempo. Creando
pinturas 'antiguas' que correspondían a las descripciones
verbales inscriptas en catálogos de pinturas perdidas,
Zhang pudo pintar falsificaciones que los coleccionistas
ansiaban 'descubrir'. En algunas obras, transformaba las
imágenes de maneras totalmente inesperadas; reordenando
una composición de la dinastía Ming corno si fuera una
pintura de la dinastía Song."[^12]

[^12]: Fu Shen, Jan Stuart, _Challenging the Past. The Paintings of Chang Daichien_ [Desafiando al pasado. Las pinturas de Chang Dai-chien], Washington,
University of Washington Press, 1991, pág. 37.

En este sentido, sus cuadros son originales, puesto que
siguen la "verdadera huella" de los maestros antiguos,
engrosando y transformando su obra a posteriori. Únicamente
el énfasis en la idea de un original irrepetible, intocable
y excepcional los degrada convirtiéndolos en meras falsificaciones.

Esta práctica singular de la _creación continuada_ solo es concebible en
una cultura que no esté atravesada por las interrupciones
revolucionarias y las discontinuidades, por el ser y la
esencia, sino por la continuidad y las transformaciones
silenciosas, el proceso y el cambio.
