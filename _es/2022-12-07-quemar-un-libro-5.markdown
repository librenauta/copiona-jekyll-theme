---
title: Quemar un libro 5 [ La Femme Squelette ]
description: La Femme Squelette by clarissa pinkola estés & My Atlegrim
title_page: ~/Quemar un libro a la luz de un scanner [ Mil Sonidos - Roberto Paci Daló y Emanuele Quinz ]
author: librenauta
image:
  path: public/5bd49843-356d-4183-8afd-02a55c8e4600/la-femme-squelette.jpg
  description: La Femme Squelette by clarissa pinkola estés & My Atlegrim
permalink:
category: scann
tags:
- scanner
- book
- La Femme Squelette
draft:
order:
layout: post
uuid: 5bd49843-356d-4183-8afd-02a55c8e4600
liquid: false
last_modified_at: 2022-12-07 15:20:21.155018278 +00:00
---

# La Femme Squelette by clarissa pinkola estés & My Atlegrim

es un libro de ilustración bellisimo impreso en risografía. tiene una tapa con colores fluo radiantes hecha en serigrafía (estimo que a máquina). vale la pena cada página.

Gracias sofiti por este prestamo, gracias marjo por tu lectura en francés y traducción en tiempo real :D

![la-femme-squelette](public/5bd49843-356d-4183-8afd-02a55c8e4600/la-femme-squelette-1.jpg)

![la-femme-squelette](public/5bd49843-356d-4183-8afd-02a55c8e4600/la-femme-squelette-2.jpg)

está escaneado con 600dpi para poder capturar la trama de la risografía donde aparezca.

[Acá para descargar el libro y mirarlo en zoom](public/5bd49843-356d-4183-8afd-02a55c8e4600/la-femme-squelette.pdf)

el texto es de un libro de Estés llamado "Mujeres que corren con los lobos"

[Acá para ver el texto en francés](https://www.didierbressan-psychotherapie.com/blog/interpretation-des-contes-de-fee/la-femme-squelette.html)


## La Mujer Esqueleto:

Había hecho algo que su padre no aprobaba, aunque ya nadie recordaba lo que era. Pero su padre la había arrastrado al acantilado y la había arrojado al mar. Allí los peces se comieron su carne y le arrancaron los ojos. Mientras yacía bajo la superficie del mar, su esqueleto daba vueltas y más vueltas en medio de las corrientes.
Un día vino un pescador a pescar, bueno, en realidad, antes venían muchos pescadores a esta bahía. Pero aquel pescador se había alejado mucho del lugar donde vivía y no sabía que los pescadores de la zona procuraban no acercarse por allí, pues decían que en la cala había fantasmas.

El anzuelo del pescador se hundió en el agua y quedó prendido nada menos que en los huesos de la caja torácica de la Mujer Esqueleto. El pescador Pensó: “¡He pescado uno muy gordo! ¡Uno de los más gordos!” Ya estaba calculando mentalmente cuántas personas podrían alimentarse con aquel pez tan grande, cuánto tiempo les duraría y cuánto tiempo él se podría ver libre de la ardua tarea de cazar. Mientras luchaba denodadamente con el enorme peso que colgaba del anzuelo, el mar se convirtió en una agitada espuma que hacía balancear y estremecer el kayak, pues la que se encontraba debajo estaba tratando de desengancharse. Pero, cuanto más se esforzaba, más se enredaba con el sedal. A pesar de su resistencia, fue inexorablemente arrastrada hacia arriba, remolcada por los huesos de sus propias costillas.

El cazador, que se había vuelto de espaldas para recoger la red, no vio cómo su calva cabeza surgía de entre las olas, no vio las minúsculas criaturas de coral brillando en las órbitas de su cráneo ni los crustáceos adheridos a sus viejos dientes de marfil. Cuando el pescador se volvió de nuevo con la red, todo el cuerpo de la mujer había aflorado a la superficie y estaba colgando del extremo del kayak, prendido por uno de sus largos dientes frontales.

“¡Ay!”, gritó el hombre mientras el corazón le caía hasta las rodillas, sus ojos se hundían aterrorizados en la parte posterior de la cabeza y las orejas se le encendían de rojo. “¡Ay!”, volvió a gritar, golpeándola con el remo para desengancharla de la proa y remando como un desesperado rumbo a la orilla. Como no se daba cuenta de que la mujer estaba enredada en el sedal, se pegó un susto tremendo al verla de nuevo, pues parecía que ésta se hubiera puesto de puntillas sobre el agua y lo estuviera persiguiendo. Por mucho que zigzagueara con el kayak, ella no se apartaba de su espalda, su aliento se propagaba sobre la superficie del agua en nubes de vapor y sus brazos se agitaban como si quisieran agarrarlo y hundirlo en las profundidades.

“¡Aaaaayy!”, gritó el hombre con voz quejumbrosa mientras se acercaba a la orilla. Saltó del kayak con la caña de pescar y salió corriendo, pero el cadáver de la Mujer Esqueleto, tan blanco como el coral, lo siguió sobre su espalda, todavía prendido en el sedal. El hombre corrió sobre las rocas y ella lo siguió. Corrió sobre la tundra helada y ella lo siguió. Corrió sobre la carne puesta a secar y la hizo pedazos con sus botas de piel de foca.

La mujer lo seguía por todas partes e incluso había agarrado un poco de pescado helado mientras él la arrastraba en pos de sí. Y ahora estaba empezando a comérselo, pues llevaba muchísimo tiempo sin llevarse nada a la boca. Al final, el hombre llegó a su casa de hielo, se introdujo en el túnel y avanzó gateando hacia el interior. Sollozando y jadeando permaneció tendido en la oscuridad mientras el corazón le latía en el pecho como un gigantesco tambor. Por fin estaba a salvo, sí, a salvo gracias a los dioses, gracias al Cuervo, sí, y a la misericordiosa Sedna, estaba… a salvo… por fin.

Pero, cuando encendió su lámpara de aceite de ballena, la vio allí acurrucada en un rincón sobre el suelo de nieve de su casa, con un talón sobre el hombro, una rodilla en el interior de la caja torácica y un pie sobre el codo. Más tarde el hombre no pudo explicar lo que ocurrió, quizá la luz de la lámpara suavizó las facciones de la mujer o, a lo mejor, fue porque él era un hombre solitario. El caso es que se sintió invadido por una cierta compasión y lentamente alargó sus mugrientas manos y, hablando con dulzura como hubiera podido hablarle una madre a su hijo, empezó a desengancharla del sedal en el que estaba enredada.

“Bueno, bueno”. Primero le desenredó los dedos de los pies y después los tobillos. Siguió trabajando hasta bien entrada la noche hasta que, al final, cubrió a la Mujer Esqueleto con unas pieles para que entrara en calor y le colocó los huesos en orden tal como hubieran tenido que estar los de un ser humano.

Buscó su pedernal en el dobladillo de sus pantalones de cuero y utilizó unos cuantos cabellos suyos para encender un poco más de fuego. De vez en cuando la miraba mientras untaba con aceite la valiosa madera de su caña de pescar y enrollaba el sedal de tripa. Y ella, envuelta en las pieles, no se atrevía a decir ni una sola palabra, pues temía que aquel cazador la sacara de allí, la arrojara a las rocas de abajo y le rompiera todos los huesos en pedazos.

El hombre sintió que le entraba sueño, se deslizó bajo las pieles de dormir y enseguida empezó a soñar. A veces, cuando los seres humanos duermen, se les escapa una lágrima de los ojos. No sabemos qué clase de sueño lo provoca, pero sabemos que tiene que ser un sueño triste o nostálgico. Y eso fue lo que le ocurrió al hombre.

La Mujer Esqueleto vio el brillo de la lágrima bajo el resplandor del fuego y, de repente, le entró mucha sed. Se acercó a rastras al hombre dormido entre un crujir de huesos y acercó la boca a la lágrima. La solitaria lágrima fue como un río y ella bebió, bebió y bebió hasta que consiguió saciar su sed de muchos años.

Después, mientras permanecía tendida al lado del hombre, introdujo la mano en el interior del hombre dormido y le sacó el corazón, el que palpitaba tan fuerte como un tambor. Se incorporó y empezó a golpearlo por ambos lados: ¡Pom, Pom!…. ¡Pom, Pom!

Mientras lo golpeaba, se puso a cantar “¡Carne, carne, carne! ¡Carne, carne, carne! “. Y, cuanto más cantaba, tanto más se le llenaba el cuerpo de carne. Pidió cantando que le saliera el cabello y unos buenos ojos y unas rollizas manos. Pidió cantando la hendidura de la entrepierna, y unos pechos lo bastante largos como para envolver y dar calor y todas las cosas que necesita una mujer.

Y, cuando terminó, pidió cantando que desapareciera la ropa del hombre dormido y se deslizó a su lado en la cama, piel contra piel. Devolvió el gran tambor, el corazón, a su cuerpo y así fue como ambos se despertaron, abrazados el uno al otro, enredados el uno en el otro después de pasar la noche juntos, pero ahora de otra manera, de una manera buena y perdurable. La gente que no recuerda la razón de su mala suerte dice que la mujer y el pescador se fueron, y a partir de entonces las criaturas que ella había conocido durante su vida bajo el agua, se encargaron de proporcionarles siempre el alimento. La gente dice que es verdad y que eso es todo lo que se sabe.


extraido de aquí: http://literatura-con-estrogenos.blogspot.com/2014/01/el-cuento-de-la-mujer-esqueleto.html
