---
title: Rayos Solares Barrocos
author: Mark Fisher
description: Crítica Cultural
proyect: f804d881-5340-465a-a445-0ae0e4853bde
image:
  path: editorial/rayos-solares-barrocos/rayos-solares-barrocos-cover.png
  description: rayos-solares-barrocos.
pdf_read: editorial/rayos-solares-barrocos/rayos-solares-barrocos.pdf
pdf_cover: editorial/rayos-solares-barrocos/rayos-solares-barrocos-cover.pdf
pdf_imposition: editorial/rayos-solares-barrocos/rayos-solares-barrocos_imposition.pdf
category: editorial
export_pdf: 'export-pdf --title="Rayos Solares Barrocos" --author="Mark Fisher" --press="pixel2pixel" --template="a6template.tex" --no-day rayos-solares-barrocos-mark-fisher.md'
tags:
- Mark Fisher
- Rayos Solares Barrocos
- Libro
draft:
order:
layout: book
uuid: 1b31b24c-69d2-4cf7-a92e-f2037cc0d9d1
liquid: false
---

Hacia fines de la década de 1980 y comienzos de la de 1990, la privatización psiquica que es hoy una característica notable de la vida contemporánea británica entró en una nueva fase. La huelga de mineros en los ochenta representó la derrota de una forma de vida colectiva. La privatización de las industrias nacionales, la venta de las viviendas sociales y la proliferación de la tecnología de consumo y de las plataformas del nuevo entretenimiento (como la televisión saltelital, que por entonces se encontraba recién en sus comienzos) allanaron el camino para la retirada, y la denigración, del espacio público. En la medida en que los hogares estuvieron más conectados, el espacio exterior comenzó a ser abandonado, patologizado y cercado.

  En este contexto debemos ubicar el ataque del gobierno Tory a las raves en los noventa. La infame acta de la Justicia Criminal y Orden Público de 1994 estuvo dirigida tanto a las ocupaciones, el saboteo de la tierra y el acampe no-autorizado como las raves. En ese momento, el Acta parecía arbitraria, draconiana y absurda. La regulación de las fiestas, bajo una legislación que dependía de un término ridículamente vago como "ritmos repetitivos", parecía un exeso. Sin embargo, el Acta mostró una vez más que el autoritarismo siempre ha sido complemento del énfasis puesto por el neoliberalismo oficial en la libertad individual. El evento fundacional del neoliberalismo fue el salvaje aplastamiento de la administración democrática socialista del Salvador Allende en Chile. A lo largo de la década de 1980, el gobierno de Thatcher desplegó medidas autoritarias contra la población urbana negra y de clase trabajadora organizada. ¿Pero por qué ir ahora contra los ravers? Puede ser que perturben la paz rural, pero la mayoría no está comprometida con ningún tipo de disenso o rebelión sistemáticos.

  La campaña contra las raves puede haber sido draconiana, pero no fue absurda ni arbitraria. Muy al contrario, el ataque a las raves fue parte de un proceso sistemático, que había comenzado con el nacimiento mismo del capitalismo. Los objetivos de este proceso fueron esencialmente: exorcismo cultural, purificación comercial e individualización forzosa.

## EXORCISMO CULTURAL

  El exorcismo fue dirigido contra lo que Herbert Marcuse llamó "el espectro de un mundo que podría ser libre" [^1]

[^1]: (Herbert Marcuse, Eros y civilización, Madrid, Serpa, 1983)

un espectro que la cultura musical, especialmente en sus modos colectivo y extático, siempre ha convocado. La misión histórica de la burguesía británica fue la eliminación total de ese espectro, algo que estuvo tan cerca de lograr a comienzos del siglo XXI como ninguna otra cultura antes lo había estado. La asociación de las raves con la campiña inglesa las volvió especialmente problemáticas. Como Michael Perelman muestra en _The Invention of Capitalism: Classical Political Economy and The Secret History of Primitive Accumulation_ (La invención del capitalismo: la economía política clásica y historia secreta de la acumulación originaria), el surgimiento del capitalismo no hubiera sido posible sin los cercamentos de la campiña. "Si bien su estándar de vida no era particularmente pródigo, los pueblos precapitalistas del norte de Europa, como la mayoría de los pueblos tradicionales, disfrutaban de mucho tiempo libre (...) La gente común tenía innumerables feriados que interrumpían el tiempo de trabajo"[^2]

[^2]: Michael Perelman, _The Invention of Capitalism: Classical Political Economy and The Secret History of Primitive Accumulation_, Durham, Duke University Press, 2000.

Al menos un tercio del año estaba dedicado al ocio. Para que el capitalismo pudiera ser dominante, esta cultura del ocio -y todas las expectativas y hábitos que la acompañaban- tenía que ser eliminada. Este proceso implicó la brutal destrucción de la capacidades del campesinado de autoabastecerse. Además del desahucio violento, la burguesía tambíen propagó un funesto culto al trabajo, que ensalzaba el valor no dedicado a la acumulación de capital, considerándolo un despilfarro moralmente degenerado.

  Los extáticos festivales de la cultura rave resucitaron el uso del tiempo y la tierra que la burguesía había prohibido y buscado sepultar. Sin embargo, a pesar de todo lo que evocaban a aquellos viejos ritmos festivos, las raves no eran evidentemente un tipo de revival arcaico. Eran un espectro del postcapitalismo más que del precapitalismo. La cultura rave surgió de la síntesis de las nuevas drogas, la tecnología y la cultura musical. El +++MDMA+++ y la psicodelia electrónica basada en las consolas Akai generaron una conciencia que no tenía razones para aceptar que un trabajo aburrido era inevitable. La misma tecnología que facilitó el despilfarro y la futiliadd de la dominación capitalista podía ser usada para eliminar el trabajo hastioso, para dar a las personas estándares de vida mucho mejores que los del campesinado precapitalista, liberando simultáneamente incluso más tiempo para el ocio del que podían disfrutar aquellos campesinos. De por sí, la cultura rave estaba en sintonía con esos impulsos insonscientes que, como Marcuse señaló, no podían aceptar el "desmembramiento temporal del placer [...] su distribución en pequeñas dosis separadas".[^3]

[^3]:Herbert Marcuse, Eros y civilizacion, op. cit.

¿Por qué tendrían las raves que terminar? ¿Por qué deberían existir las miserables mañanas de lunes?

## PURIFICACIÓN COMERCIAL

  Las raves también evocaban los espacios  intersticiales _-entre el comercio y el festival-_ que causaron preocupación en la primera burguesía. En los siglos XVII y XVIII, mientras luchaban por imponer su hegemonía, la burguesía estaba sumamente ejercitada en el status problemático de la feria. La "contaminación" ilegítima del "puro" comercio por los excesos carnavalescos y las festividades colectivas preocuparon a los escritores e ideólogos burgueses. El problema que enfrentaron, sin embargo, fue que la actividad comercial estaba siempre contaminada con elementos festivos. No existía comercio "puro", libre de la energía colectiva. Una esfera comercial de esas características tendría que ser producida, y para ello era tan necesario contener e incorporar ideológicamente al "mercado" como domesticar la feria. Como Peter Stallybrass y Allon White señalaron en _The Politics and poetics of transgression_[las políticas poéticas de la transgreción], "la feria , como el mercado, no es algo puro que encuentre afuera. La feria se encuentra en la encrucijada, situada en la intersección de fuerzas económicas y culturales, productos y vajeros, mercancías y comercio"[^4]

[^4]:Peter Stallybrass y Allon White, _The Politics and poetics of transgression_, Ithaca, Cornell University Press, 1986)

El concepto de "la economía", tal como lo entendemos en la actualidad, tuvo que ser inventado, y para ello se requirió estabilizar la inquietante e irresoluta figura de la feria "Cuando la burguesía trabajó para producir la economía como un dominio separado, tuvo que romper su íntima y variada interconexión con el calendario festivo; del mismo modo, trabajó _conceptualmente_ para reformar la feria o como un evento de intercambio racional y comercial o como un ámbito de placer popular". Una división tal fue necesaria para que la burguesía pudiera realizar una distinción clara y definitiva entre el esfuerzo moralizante y el ocio decadente; se trató además de un rechazo del "desmembramiento temporal del placer". Por lo tanto, "si bien las clases burguesas frecuentemente tuvieron miedo a la amenaza de la subversión política y la licencia moral, quizás se hayan escandalizado más aún por la profunda confusión conceptual que implicaba al feria, con su mezcla de trabajo y placer, intercambio y juego"[^5]

[^5]:Ibid.

La feria siempre llevó consigo las marcas del "espectro de un mundo que podría ser libre", que amenazaba con sacar al comercio de su asociación con el trabajo duro y acumulación de capital que la burguesía trataba de imponerle. Por eso "el carnaval, el circo, el gitano y el lumpenproletariado jugaron un rol simbólico en la cultura burguesa totalmente desproporcionado en relación con su importancia social real".[^6]

[^6]:Ibíd.

El carnaval, el gitano y el lumpenproletariado evocaban formas de vida -y formas de comercio- que eran incompatibles  con el trabajo solitario del sujeto burgués aislado y con el mundo que este proyectaba. Es por eso que no podían ser tolerados. Si otras formas de vida eran posibles, entonces -contra las formulaciones más famosas de la señora Thatcher- había alternativas, después de todo.


## INDIVIDUALIZACIÓN FORZOSA

  La modernidad capitalista fue en este sentido conformada por el proceso siempre incompleto de eliminación de la colectividad festiva. Es posible, dice Foucault en _Vigilar y castigar_ leer las impresiones de esas colectividades en las propias formas que asumirían las instituciones disciplinarias como la fábrica, la escuela y el hospital. "por detrás de los dispositivos disciplinatorios, se lee la obsesión de los 'contagios', de la peste, de las revueltas de los crímenes de la vagancia, de las deserciones, de los individuos que aparencen y desaparecen, viven y mueren en el desorden"[^7]

[^7]:Michel Foucault, _Vigilar y castigar,_ Buenos Aires, Siglo XXI, 2002.

En esa memoria , que también es una ficción, una hiperstinción,[^8]

[^8]:"hiperstincion" es un cocepto desarrollado por Nick Land y la Cybernetic Culture Research Unit (_CCRU_) y se refiere a una idea performativa que provoca su propia realidad, una ficción que crea el futuro que predice. ver Aceleracionismo. Estrategias para una transición hacia el postcapitalismo, Buenos Aires, Caja Negra, 2017- _N. del T._

la plaga y la festividad se fusionan: ambas son imaginadas como espacios de los que los limites entre los cuerpos colapsan, en los que los rostros y las identidades se desvanecen. "Ha habido en torno a la peste toda una ficción literaria dela fiesta: las leyes suspendidas, los interdictos levantados, el fenesí del tiempo que pasa, los cuerpos que se mezclan sin respeto, los individuos que se desenmascaran, que abandonan su identidad estatutaria y figura bajo la cual se los reconocía, dejando aparecer una verdad totalmente distinta." La solución es una individualización impuesta, lo inverso al carnaval: "No la fiesta colectiva, sino las particiones estrictas; no las leyes transgredidas, sino la penetración del reglamento hasta los más finos detalles de la existencia y por el intermedio de una jerarquía completa que garantiza el funcionamiento capilar del poder; no las máscaras que se ponen y se quitan, sino la asignación a cada cual su 'verdadero' nombre, de su 'verdadero' lugar, de su 'verdadero' cuerpo y de la 'verdadera' enfermedad".

  Realismo Capitalista que se enraizó en Reino Unido en la década de 1990 buscó completar este proyecto de individualización forzosa. Toda huella remanente de colectividad debía ser extripada a partir de entonces. Esas marcas se encontraban no solo en las raves, los campamentos nómades y las fiestas libres, sino también en las tribunas de los estadios y en la cultura del fútbol en general. algunos de cuyos elementos, de todos modos, se estaban fusionando con la cultura rave. La tragedia de Hillsbourgh,[^9]

[^9]:Se conoce como "Tragedia de Hillsborough" el suceso ocurrido el sábado 15 de abril de 1989 en el estadio de Hillsborough, en Shefield, Inglaterra, en el que fallecieron 96 personas aplastadas contra las vallas del estadio a causa de una avalancha humana. El suceso tuvo lugar durante el partido de fútbol entre Liverpool y el Nottingham Forest, de las semifinales de la Copa de Inglaterra.

 en 1989, fue el equivalente para el fútbol británico de la doctrina del shock analizada por naomi Klein. La tragedia -causada por la maliciosa incompetencia de la "policía de Thatcher", la infame fuerza del condado de Yorkshire del Oeste- permitió una agresiva ocupación corporativa del fútbol ingles.

  Las tribunas populares fueron cerradas y se le asignó un asiento individual a cada espectador. De un solo golpe toda una forma de vida colectiva había sido clausurada. La modernización de los estadios de fútbol en Inglaterra estaba sumamente atrasada; pero esta fue la versión neoliberal de la "modernización", que equivalía a la hipermercantilización, la individualización y la corporativización. La multitud fue descompuesta en consumidores solitarios; y el cambio de identidad de la primera division inglesa, que pasó a llamarse Premier League, y la venta de los derechos televisivos a Sky fueron presagios de la incontrolable desolación existencial que se abatiría sobre Inglaterra en el siglo XXI. La soledad conectada, propia de la adicción a los _smartphone_, es el reverso depresivo de la festividad hedónica del MDMA. La sociabilidad es supervisada por múltiples plataformas corporativas insertas unas dentro de otras. Nos trasnformamos en nuestros perfiles, trabajando las veinticuatro horas, los siete días de la semana, para el capitalismo de la comunicación.

  La individualización forzosa no fue por supuesto inmediatamente exitosa. El Acta de Justicia Criminal provocó nuevas formas de rebelión carnavalesca, siendo el caso más notable del colectivo Reclaim the Streets [Recupera las calles]. Las imágenes de las autopistas bloqueadas por las Ravers parece hoy pertenecer a una época histórica que queda seductoramente en el pasado -en algún sentido tan alejadas como la contracultura de los sesenta-, pero las olas de nueva organización política que ha atravezado Grecia, España, Escocia e incluso ahora Inglaterra (con el ascenso de Jeremy Corbin) nos recuerda que el proyecto de individualización forzosa nunca puede ser completo. En todo momento, la colectividad puede ser redescubierta y reinventada. El "espectro de un mundo que podría ser libre" siempre tiene que ser reprimido, ya que puede revitalizarse en cualquier festividad que "dure demasiado", en cualquier ámbito laboral u ocupación universitaria que se niegue a la "necesidad" del trabajo monótono, en cualquier grupo en el que se afirme la conciencia que rechaza la "inevitabilidad" del individualismo competitivo. La enorme extensión e intensidad de la maquinaria que fue necesaria para clausurar las raves es un testimonio de esto. El individualismo tiene que ser impuesto, vigilado, obligado. Toda la inventiva del capital -hoy aturdida y conspicuativamente exhausta- está dedicada a  esta compulsión.

  "De vez en cuando", escribe Frederic Jameson en _valencias de la dialéctica_ como en un globo ocular enfermo en el que se perciben perturbadores flashes de luz, o como en esos rayos solares barrocos en los que los rayos de otro mundo de pronto interrumpen el nuestro, se nos recuerda que la utopía existe y que otros sistemas, otros espacios, todavía son posibles"[^10]

[^10]:Fredric Jameson, Valencias de la dialéctica, Buenos Aires, Eterna Cadencia, 2013.
 El imaginario psicodélico parece especialmente apropiado para el "flash energético" de las raves, que hoy parecen un recuerdo que se escurre desde una mente que no es la nuestra. Aunque de hecho los recuerdos provienen de lo que nosotros fuimos alguna vez: una conciencia grupal que espera en el futuro virtual y no solo en el pasado real. Así que quizá sea mejor ver las otras posibilidades que estas radiaciones barrocas iluminan no como una utopía distante, sino como un carnaval sumamente próximo, en un espectro que nos persigue incluso -especialmente- en los más miserables espacios des-socializados.
