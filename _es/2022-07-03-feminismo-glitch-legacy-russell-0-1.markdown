---
title: Feminismo Glitch - Manifiesto - intro y capítulo 1
author: Legacy Russell
description: Feminismo Glitch - Manifiesto
proyect: f804d881-5340-465a-a445-0ae0e4853bde
image:
  path: editorial/feminismo-glitch-legacy-russell/feminismo-glitch-legacy-russell-capitulo-0-1-cover.png
  description: shanzhai-byung-chul-han-copia.
pdf_read: editorial/feminismo-glitch-legacy-russell/feminismo-glitch-legacy-russell-capitulo-0-1.pdf
pdf_cover: editorial/feminismo-glitch-legacy-russell/feminismo-glitch-legacy-russell-capitulo-0-1-cover.pdf
pdf_imposition: editorial/feminismo-glitch-legacy-russell/feminismo-glitch-legacy-russell-capitulo-0-1_imposition.pdf
category: editorial
export_pdf: 'export-pdf --title="Feminismo Glitch - introduccion y capitulo 1" --author="Legacy Russell" --press="pixel2pixel" --template="a6template.tex" --no-day feminismo-glitch-legacy-russell-capitulo-0-1.md'
tags:
- feminismo
- glitch
- libro
- manifiesto
draft:
order:
layout: book
uuid: 58000ba3-dfa1-4d06-98d6-f10a8a711d3d
liquid: false
---

# Feminismo Glitch de Legacy Russell

> _Traducción by Valeria Mussio_

## Introducción

De pre-adolescente me conecté como LuvPunk12 y pasé los siguientes años vagando en
las autopistas de la maquinaria espectral, ocupando salas de chat y construyendo
fantasías con gifs de GeoCities. Creciendo en Saint Mark’s Place en el medio de East
Village, aprendí a construir y performar mi ser-genérico gracias a lxs chicxs punks
que conocí en las escalinatas de mi casa, desde drag queens que se subían al escenario
en Singy Lulu’s y dominaron cada año Wigstock Tompkins Square Park, hasta de la cultura
Boricua, todo lo que era, en esa época, parte de la base del East Village y el Bajo
Lado Este.

LuvPunk12 se convirtió en el amalgama simbólico de toda esta corriente. Elegí ese
nombre cuando vi LUV PUNK! en un sticker de una manzana de caramelo con forma de
corazón en una cabina de teléfono, afuera de mi edificio. Tenía doce años. Lo saqué
y lo puse en mi carpeta, usándolo como una insignia de orgullo. Se convirtió en un
recuerdo enraizado del hogar mientras transicionaba dentro y fuera de espacios más
allá de East Village, que con frecuencia se sentían alienantes para mí.

LuvPunk12 como el picaporte de una sala de chat fue el nacimiento de una performance,
una exploración de un ser-futuro. Yo era un cuerpo joven: Negrx, percibida mujer,
femenina, queer. No había pausas, no había indulto; el mundo a mi alrededor nunca
me dejó olvidar esas etiquetas. Pero online podía ser quien yo quisiera ser. Y así
mis doce años se volvieron dieciséis, se volvieron veinte, se volvieron setenta.
Envejecí. Morí. A través de la narración y del cambio de forma, resucité. Reclamé
mi rango. Online encontré el pavoneo genérico de la ascendencia, el drag sediento
de la aspiración. Mi “feminidad” se transformó, empecé a explorar “hombre”, a expandir
“mujer”. Jugué con dinámicas de poder, intercambiando con extrañxs sin cara, empoderadx
a través de la creación de seres nuevxs, deslizándome dentro y fuera de pieles digitales,
celebrando en los nuevos rituales del cyber-sexo. En salas de chat me vestí con diferentes
corpo-realidades mientras la rueda arcoiris de la muerte se amortiguaba en la estática
y divagante jam de la conexión dial-up de AOL.

Esos dulces tonos del dial-up eran pavlovianos: me hacían salivar por la anticipación
de las palabras que descansaban detrás de las campanas. Era una nativa digital empujando
a través de esos paisajes cibernéticos con una conciencia incipiente, un poder ejercido
tímidamente. No era lo suficientemente privilegiada como para ser un cyborg por completo,
pero estaba llegando a eso, seguramente a mi manera.

Y no estaba sola.

Away from the keyboard (lejos del teclado — AFK), inmersx en una East Village que
se gentrificaba rápidamente, caras, pieles, identidades como la mía y de las comunidades
mixtas en las que surgí desaparecían lentamente. Me estaba transformando en unx extrañx
en mi propio territorio, una reminiscencia de un capítulo pasado de Nueva York. Familias
creativas de color como la mía que habían construido el vibrante paisaje del centro
de Nueva York estaban siendo expulsadas por los precios del barrio. De repente lxs
que vivían al lado eran cada vez más blancxs, ascendentemente móviles, y visiblemente
incómodos por mi presencia o la de mi familia. La “vieja guardia” se enfrentaba a
una generación de niñxs con fideicomiso. Lxs recién llegadxs estaban intrigadxs por
la mitología de la East Village como bastión cultural, y, sin embargo, mostraron
poco interés en invertir en la pelea necesaria para proteger su legado.

Más allá de mi puerta, mi feminidad queer se encontró a sí misma, también, en un
pasaje vulnerable entre los canales de la heteronormatividad de la escuela secundaria.
Mi cuerpo pre-púber estaba exhausto de las costumbres sociales, cansado de que le
digan que ocupe menos espacio, de ser visto pero no escuchado, borrado sistemáticamente,
quitado con edición, ignorado. Lo único que quería hacer era moverme. Pero en la
luz del día me sentía atrapadx, siempre desplazándome con dificultad bajo el peso
de la incesante observación blanca y heteronormativa.

Bajo este tipo de vigilancia, la inocencia real y el juego de la niñez se tornan,
de pronto, inviables. En vez de eso, busqué oportunidades para sumergirme en la potencia
del rechazo. Comencé a poner resistencia contra la violencia de esta visibilidad
no consentida, a tomar el control de los ojos que se posaban sobre mí y de cómo interpretaban
mi cuerpo. Era claro para mí, mientras me paraba en una intersección volátil, que
el binarismo era un tipo de ficción. Incluso para un cuerpo joven, Negro y queer,
una doble-conciencia DuBoisiana se astillaba más allá, la “doble” se conviertía en
“triple” conciencia amplificada y expandida por el “tercer ojo” del género.

Mirando a través de estos velos de raza y género pero nunca siendo yo completamente
vistx, con puntos de referencia limitados acerca del mundo que estaba más allá, estaba
distanciadx de cualquier espejo preciso. Para mi cuerpo, entonces, la subversión
vino a través del remix digital, buscando lugares de experimentación donde pudiera
explorar mi verdadero yo, abiertx y listx para ser leídx por aquellxs que hablaran
mi idioma. Online, busqué convertirme en unx fugitivx del mainstream, reacix a aceptar
su limitada definición de los cuerpos como el mío. Lo que el mundo AFK ofrecía no
era suficiente. Quería -demandaba- mucho más.

La construcción binaria del género es, y siempre ha sido, precaria. Agresivamente
contingente, es una invención inmaterial que en su viralidad tóxica ha infectado
nuestras narrativas sociales y culturales. Para existir dentro de un sistema binario
unx debe asumir que nuestros seres son imposibles de cambiar, que la forma en la
que somos leídxs en este mundo debe ser algo que elijan lxs demás, y no algo que
podamos definir -y elegir-nosotrxs mismxs. Estar en la intersección de la percepción-femenina,
queer y Negra es encontrarse a unx mismx en un vértice integral. Cada uno de esos
componentes es una tecnología clave en sí y para sí mismo. Juntos y por separado,
“femenino”, “queer”, “Negra” como una estrategia de supervivencia demanda la creación
de maquinarias individuales que innoven, construyan, resistan. Con la movilidad física
generalmente restringida, las personas que se identifican como femeninas, las personas
queer, las personas Negras, inventan maneras para crear un espacio a partir de la
ruptura. Ahí, en esa disrupción, con nuestra congregación colectiva en ese camino
que constantemente te hace tropezar, y en esa encrucijada llena de cables que son
el género, la raza y la sexualidad, unx encuentra el poder del glitch.

Un glitch es un error, una equivocación, una falla en el funcionamiento. Dentro de
la tecnocultura, un glitch es parte de la ansiedad maquínica, un indicador de que
algo salió mal. Esta tecnología incorporada de la ansiedad, para alertar que algo
salió mal, se desborda naturalmente cuando encontramos glitches en escenarios AFK:
el motor de un auto que deja de funcionar; quedarse atrapadxs en el ascensor; un
apagón en toda la ciudad.

Sin embargo, estos son micro-ejemplos en una trama mucho más amplia de las cosas.
Si nos alejamos un poco, considerando los sistemas mucho más grandes y complicados
que han sido usados para dar forma a la máquina de la sociedad y la cultura, el género
es inmediatamente identificable como un engranaje central dentro de esta rueda. El
género ha sido utilizado como un arma contra su propia gente. La idea del “cuerpo”
trae consigo este arma: el género circunscribe al cuerpo, lo “protege” para evitar
que pierda sus límites, que reclame su vasta infinidad, que se dé cuenta de su verdadero
potencial.

Usamos el cuerpo para dar forma material a una idea que no tiene forma, un ensamblaje
que es abstracto. El concepto de cuerpo es hogar de discursos sociales, políticos
y culturales, que cambian dependiendo de dónde está situado el cuerpo y cómo es leído.
Cuando le ponemos género a un cuerpo estamos asumiendo cuál es su función, su condición
sociopolítica, su fijación. Cuando el cuerpo es determinado como un individuo masculino
o femenino, performa un género como si tuviera un sistema de puntaje, guiado por
una serie de reglas y requerimientos que validan y verifican la humanidad de ese
individuo. Un cuerpo que resiste la aplicación de pronombres, o que se mantiene indescifrable
dentro de la asignación binaria es un cuerpo que rechaza alcanzar un puntaje. Esta
no-performance es un glitch. El glitch es una forma de rechazo.

Dentro del feminismo glitch, el glitch es celebrado como un vehículo de rechazo,
una estrategia de no-performance. El glitch apunta a hacer abstracto de nuevo aquello
que ha sido forzado dentro de un material incómodo y mal definido: el cuerpo. En
el feminismo glitch, tomamos la noción de glitch-como-error desde su génesis en el
reino de lo máquinico y lo digital, y nos preguntamos cómo puede ser re-aplicado
para comunicar la manera en la que vemos el mundo AFK, dándole forma a nuestra posible
participación en él para crear agenciamientos por y para nosotrxs. Utilizando el
internet como material creativo, el feminismo glitch mira primero a través de los
ojos de artistas que, en su trabajo e investigación, ofrecen soluciones para este
problemático material que es el cuerpo. El proceso para convertirse en tensiones
de superficies materiales, impulsándonos a preguntar: _¿Quién define el material
del cuerpo? ¿Quién le da valor — y ¿por qué?_

Estas preguntas son difíciles e incómodas, requieren que nos enfrentemos al cuerpo
como un marco estratégico que generalmente se utiliza para fines particulares. Sin
embargo, en esta misma línea de cuestionamiento, el feminismo glitch se mantiene
como una mediación del deseo por todos esos cuerpos como el mío que continúan alcanzando
la mayoría de edad de noche, en el Internet. El glitch reconoce que los cuerpos-con-género
están muy lejos de un absoluto y son más bien un imaginario, manufacturado y vuelto
bien de consumo por el capital. El glitch es una oración activista, un llamado a
la acción, mientras trabajamos por un fracaso fantástico, liberándonos del entendimiento
del género como algo estacionario.

Mientras continuamos navegando hacia un concepto más vasto y abstracto del género,
debe ser dicho que, paradójicamente, por momentos realmente se siente como si todo
lo que tuviéramos fueran estos cuerpos en los que estamos enclosetados y en los que
se nos asigna un género, entre otras cosas. Bajo el sol del capitalismo, son pocas
las cosas que verdaderamente poseemos [más allá de nuestro cuerpo], e incluso así,
muchas veces somos sujetos de complicadas coreografías dictadas por los intrincados,
burocráticos y rizomáticos sistemas de las instituciones. La brutalidad de este estado
precario es particularamente evidente a través de la expectativa constante de que
nosotres como cuerpos reafirmemos una performance de género que encaje dentro del
binarismo, en pos de cumplir con las prescripciones del día a día. Como escribe el
politólogo y antropólogo James C. Scott, “La legibilidad se transforma en una condición
para la manipulación.”[^1] Estas agresiones, marcadas como neutrales en su banalidad,
son, de hecho, violentas. Cotidianos en su naturaleza, nos encontramos a nosotrxs
mismxs defendiéndonos de los avances del género binario, mientras se cuela a través
de las cosas básicas de la vida moderna: al abrir una cuenta de banco, al tramitar
un pasaporte, cuando tenemos que ir al baño.

[^1]: Jame C. Scott, _Seeing Like a State: How Certain Schemes to Improve the Human
Condition Have failed_, New Haven, CT: Yale University Press, 1999, p.183

Entonces, ¿qué quiere decir desmantelar el género? Semejante programa es un proyecto
de desarme; demanda el final de nuestra relación con la práctica social del cuerpo
tal como la conocemos. En su novela de 1956, Giovannis’s Room, el protagonista del
escritor y activista James Baldwin, David, musita oscuramente: “No importa, es solo
el cuerpo, [y] se va a terminar pronto.” A través de la aplicación del glitch, ghosteamos
a nuestro cuerpo asignado con un género y nos aceleramos hacia su final. Las infinitas
posibilidades que se presentan como consecuencia de esto nos permiten explorarnos:
podemos des-identificar y a través de la des-identificación, podemos crear nuestras
propias reglas en la lucha contra el problema del cuerpo.

El feminismo glitch nos pide que observemos la sociedad profundamente deficiente
en la que actualmente participamos y estamos implicadxs, una sociedad que demanda
incesantemente que tomemos decisiones basadas en un género binario conceptual que
nos limita como individuxs. El feminismo glitch nos urge a considerar _lo que está
en el medio_ como un componente central de supervivencia -ni masculino ni femenino,
ni hombre ni mujer, más bien un espectro a través del que podemos empoderarnos para
elegir y definirnos a nosotrxs mismxs por nosotrxs mismxs. Así, el glitch crea una
fisura dentro de la que nuevas posibilidad de ser y transformarse se manifiestan.
Esta falla del funcionamiento en los confines de una sociedad que nos falla es un
rechazo necesario y señalado. El feminismo glitch disiente, resiste contra el capitalismo.

Como feministas glitch, esta es nuestra política: rechazamos ser talladxs de acuerdo
a la línea hegemónica del cuerpo binario. Este fracaso calculado impulsa a la violenta
maquinaria socio-cultural a hipar, suspirar, disparar, amortiguar. Queremos un nuevo
contexto y, para este contexto, queremos una nueva piel. El mundo digital provee
un espacio potencial donde esto puede funcionar. A través de lo digital construimos
nuevos mundos y nos atrevemos a modificar el nuestro. A través de lo digital, el
cuerpo que “glitchea” encuentra su génesis. Aceptar el glitch es entonces una acción
participativa que desafía el status quo. Crea un hogar para aquellxs que atraviesan
los complejos canales de la diáspora de género. El glitch es para aquellxs seres
que se sumergieron felizmente en lo-del-medio, aquellxs que migraron lejos de su
lugar asignado como origen de género. La continua presencia del glitch genera un
espacio abierto y protegido en el que innovar y experimentar. El feminismo glitch
demanda una ocupación de lo digital como medio para construir un mundo nuevo. Nos
permite aprovechar la oportunidad para generar nuevas ideas y recursos para la (r)evolución
en curso de los cuerpos, que inevitablemente puede moverse y transformarse mucho
más rápido que las costumbres AFK o las sociedad que las producen y bajo las que
estamos obligadxs a operar offline.

Con el temprano avatar de LuvPunk12, me encapuché con la piel de lo digital, haciendo
política a través de mi joven juego con el género, viajando sin pasaporte, ocupando
espacio, amplificando mi negrura queer. Esta experiencia de motín maquínico fue fundacional
para mí, y me dio el coraje para dejar ir la ambivalencia que viene con el miedo
a convencerme de ideas fósiles, inherente a los disturbios de la adolescencia. Encontré
familia y fe en el futuro con estas intervenciones, dándole forma a mis visiones
de un ser que podría realmente empoderarse al definirse a sí mismx, un porvenir que
el decoro social regularmente desaconseja para un cuerpo negro y queer.

La escritora feminista y activista Simone de Beauvoir es famosa por afirmar “No se
nace mujer: se llega a serlo.” El glitch plantea: no se nace cuerpo, se llega a serlo.
A través del artificio de una Shangri-La simple y digital -un mundo online en el
que podamos, finalmente, ser liberadxs de las costumbres del género, así como lo
soñaron las primeras cyberfeministas- ahora perforada, el Internet sigue siendo una
embarcación a través de la que algo que se está transformando puede realizarse a
sí mismx. El glitch es un pasaje a través del que el cuerpo se desplaza hacia su
liberación, una rasgadura en el tejido de lo digital.

Este libro es para aquellos que estén en camino a convertirse en sus avatares, aquellxs
que continúan jugando, experimentando y construyendo a través del internet como medio
para fortalecer el loop entre lo online y lo AFK. Este libro va a nombrar y celebrar
artistas que hacen que la crítica del cuerpo sea central en sus prácticas, y comparten
la dura pelea por espacios creados en este viaje en búsqueda de refugio, seguridad
y porvenir. Para citar al poeta, crítico y teórico Fred Moten, “Lo normativo es el
efecto secundario, es una respuesta a lo irregular.”

Como feministas glitch, inyectamos nuestras irregularidades dentro de los sistemas
como errata, activando una arquitectura nueva a través de estos malfuncionamientos,
buscando y celebrando los deslices del género en nuestro raro y salvaje deambular.
Con este propósito en mente, este libro está estructurado en doce secciones, y cada
una de ellas intenta proponer un efecto secundario alternativo, permitiendo que nos
acompañemos a través de los lentes de prácticas y políticas nuevas, para descubrir
nuevas formas en las que la vida no solo imita, sino que comienza con el arte. Cada
una de las doce secciones comienza con una declaración, una pared blanca contra la
que arrojar al feminismo glitch en su desliz, en su costado y en su manifiesto. Este
texto va a aplicar el concepto del glitch en una investigación, y celebración, de
artistas y de su trabajo, que nos ayuden a imaginar nuevas posibilidades acerca de
qué es lo que puede hacer un cuerpo, y cómo esto puede funcionar contra la norma.
Comenzando online, viajaremos a través del loop online-AFK, viendo cómo el feminismo
glitch puede ser usado en todo el mundo, inspiradxs por practicantes que, en su rebelión
contra el cuerpo binario, nos guían a través de mundos desobedientes, hacia nuevos
contextos y nuevas visiones de futuros fantásticos.


## 01 — EL GLITCH RECHAZA

Consideren la pieza _NOPE_ (_un manifiesto_) de lx artista E. Jane, publicada en
el año 2016. Comienzo acá, con las palabras de _NOPE_, porque dentro de ellas se
encuentra el rechazo fundacional requerido para “glitchear.” _Glitchear_ es aceptar
el malfuncionamiento, y aceptar el malfuncionamiento es, en sí, una expresión que
comienza con el “no”. Así, _NOPE_ de E. Jane nos ayuda a dar los primeros pasos.


![2-1](editorial/feminismo-glitch-legacy-russell/2-1.png)

E. Jane escribe:

**NOPE**

**(un manifiesto)**

“No soy una artista de la identidad solo porque soy unx artista Negra con múltiples
“yo”.

No me enfrento con las nociones de género y representación en mi arte. Me enfrento
con la seguridad y el porvenir. Ya estamos mucho más allá de preguntarnos si deberíamos
estar en la habitación. Estamos en la habitación. Y también estamos muriendo a un
paso acelerado y necesitamos un futuro sostenible.

Necesitamos más gente, necesitamos mejores ambientes, necesitamos escondites, necesitamos
demandas utópicas, necesitamos una cultura que nos ame.

No estoy preguntándome quién soy. Soy una mujer Negra y expansiva en mi Negrura y
en mi ser-queer, así como mi Negrura y mi ser-queer son ya, desde siempre, expansivas.
Nada de esto es simplemente “identidad y representación” por fuera de la mirada colonial.
Estoy por fuera de ella en la tierra del NOPE.”

Antes de hablar sobre qué es el glitch y qué es lo que puede hacer, meditemos alrededor
de la idea de un “[yo] con múltiples yos”, y reconozcamos que la construcción del
propio “yo”, creativo o de cualquier tipo, es compleja. E. Jane, al nombrar y clamar
por “múltiples yos”, resiste contra una lectura plana de los cuerpos históricamente
“otrerizados” [^2]- cuerpos interseccionales que han viajado sin descanso, gloriosamente,
a través de espacios estrechos. Estos son los “yos” que, como dijo la escritora y
activista Audre Lorde en su poema de 1978 “Letanía de la supervivencia”, “viven en
la orilla” y “no se suponía que sobrevivieran.”

[^2]: Un intento de traducir “othered”, en referencia a los cuerpos que son vueltos
“el otro”.

Asir “los múltiples yos” es, por lo tanto, un acto inherentemente feminista: la multiplicidad
es una libertad. Dentro de su práctica creativa, E. Jane explora la libertad encontrada
en la multiplicidad, estirando su rango a través de dos “yos”: E. Jane y su avatar
“alter-ego”, Mhysa. Mhysa es una autoproclamada “popstar por el underground de la
cyber-resistancia”, que atravesó algunos de los primeros trabajos que E. Jane presentó
a través del ahora difunto “hub cultural multimedia” y “motor creativo” NewHive.

La pieza de NewHive de E.Jane, “MhysaxEmbaci-Freakinme” (2016) en colaboración con
Mhysa en un palpitante campo de peonias, labios brillantes, y cuerpos que se movían
siempre un poquito fuera de ritmo en el drag digital de un collage sincopado de sonidos
e imaginería. Estos dos “yos” comenzaron como entidades relativamente distintas,
con Mhysa “permitiendo [que E. Jane] sea una parte de [sí mismas que] las instituciones
blancas trataron de asfixiar”, sirviendo como un alter-ego que se grabó a si mismx
y compartió fragmentos de su propia transformación floreciente en Instagram, Twitter
y Facebook. Luego, en 2017, Mhysa lanzó un LP con once canciones oportunamente titulado
_Fantasii_, marcando el momento en que “el deslizamiento entre IRL y URL” se profundizó
mientras Mhysa interpretaba canciones y sets AFK, entrando al mundo de E. Jane y
perforando la división cuidadosamente construida entre la identidad online y la identidad
off-line.

El viaje de E. Jane hacia Mhysa, primero como avatar y luego como una extensión AFK
de sí mismx, está caracterizado por la búsqueda de un lugar para itinerar, y por
la búsqueda del propio abanico de posibilidades. Pienso en el poema de 1892 de Walt
Whitman, “Canto a mí mismo”:

**¿Me contradigo?**

**Muy bien, entonces, me contradigo,**

**(Soy gigante, contengo multitudes)**

Whitman, un hombre blanco, fue considerado radicalmente queer para su época. En estas
líneas suyas captura perfectamente el problema del patriarcado y de la blancura.
Whitman es un agente estrechamente ligado con el status quo social y cultural, pero
su “contener multitudes” es un ejercicio del derecho a ser borroso, móvil, abstracto.
El patriarcado ejerce su dominio social ocupando espacio como si fuera su derecho
de nacimiento; cuando el patriarcado entra en contacto con la blancura, deja muy
poco lugar para todo lo demás. El espacio no es solo reclamado por aquellos que ejercen
la “mirada primaria” de la que habla E. Jane, sino que está hecho para ellos: el
espacio para convertirse en un “yo” libre y con múltiples posibilidades, y la complejidad
agencial que esto provee está garantizada y protegida para los “yos” normativos y
los cuerpos que ocupan.

Lo que E. Jane protege ferozmente -el “yo” expansivo-, Whitman lo usa sin miedo,
totalmente despreocupado por la posibilidad de que su privilegio le sea arrebatado.
Con más de ciento veinte años de diferencia, ambos hablan con lx otrx a través del
vacío, pero miran hacia mundos muy diferentes. Cuando consideramos la identidad y
el lenguaje que usualmente se usa para hablar de ella (por ejemplo, “lo hegemónico”
y lo que está “en los márgenes”), no es una gran sorpresa el hecho de que, bajo el
patriarcado blanco, los cuerpos –“yos”- que no pueden ser definidos con claridad
por la “mirada primitiva”, sean expulsados del centro. Ahí, un cuerpo Negro, femenino
y queer es aplanado, esencializado en una dimensión singular, se le da muy poco espacio
para ocupar y todavía menos territorio para explorar. Como figuras planas y sombrías
paradas en los márgenes, se nos arranca el derecho a sentir, a transformarnos, a
expresar el abanico de posibilidades del “yo”.

La historia de este tipo de achatamiento u “otrerización” es uno que tiene raíces
profundas dentro de la dolorosa narrativa de la raza, el género y la sexualidad en
Estados Unidos, pero también persiste de forma consistente a través de la historia
mundial de la guerra. Donde el imperialismo puso su mano, donde continúa el neocolonialismo,
la fuerza del achatamiento puede ser encontrada. Si uno puede renderizar otro cuerpo
para volverlo irreconocible y sin cara, si uno puede determinar a otrx como sub-humano,
se vuelve mucho más fácil para un grupo establecer una posición de supremacía sobre
otro.

La violencia es un componente central de la supremacía, y, como tal, uno de los agentes
más importantes del patriarcado. Donde vemos la limitación del derecho de un cuerpo
a “variar”, ya sea en un nivel individual o estatal, vemos dominación.

E. Jane no está siendo hiperbólicx cuando escribe que estamos “muriendo a un paso
acelerado”. Arrastradxs hacia los márgenes, nos encontramos como gente queer, gente
de color, gente que se identifica como mujer, lxs más vulnerables en las condiciones
climáticas del mundo, yendo desde el cambio climático hasta el capitalismo de la
plantación. Así, imaginar la forma que podría tomar un futuro sostenible, encontrar
“escondites” seguros sumados a las técnicas que nos proveen espacios propios, es
urgente.

El Glitch recorre los bordes y atraviesa los límites, aquellos que ocupamos e impulsamos
en nuestro camino por definirnos a nosotrxs mismxs. El Glitch se trata de reclamar
nuestro derecho a la complejidad, a la variedad, dentro y más allá de los márgenes
proverbiales. E. Jane tiene razón: _realmente_ “necesitamos escondites, demandas
utópicas, una cultura que nos ame.”

La arquitectura imaginativa de la utopía sigue presente en el feminismo glitch. Nos
da un hogar y una esperanza. En 2009, el académico y teórico queer José Esteban Muñoz
escribió en su libro _Cruising Utopa_, “Lo queer es aquello que nos deja sentir que
este mundo no es suficiente, que de hecho algo falta.”[^3] En este “algo que falta”
está el deseo, un anhelo por un mundo mejor, un rechazo del aquí y ahora. Muñoz observa
que “hemos sido expulsadxs del ritmo del tiempo heterosexual, y hemos construido
mundos en nuestras propias configuraciones espaciales y temporales”.[^4] Como rechazo
del “tiempo heterosexual”, y, por extensión, del modelo eurocéntrico de tiempo y
espacio, E. Jane postula un NOPE que no se conforma con un mundo o un sistema social
que nos falla.

[^3]: José Esteban Muñoz, Cruising Utopia: The Then and There of Queer Futurity,
New York: NYU Press, 2009, p.1.

[^4]: Ibid., p. 182.

El romance oblicuo del internet como utopía, contra esta realidad como telón de fondo,
no debe ser desechado como ingenuo. Hoy en día, embeber el material digital con fantasía
no es un retro-acto de mitologización; continúa como un mecanismo de supervivencia.
Usar el internet para jugar, performar, y explorar, todavía tiene potencial. Darnos
a nosotrxs mismxs este espacio para experimentar quizá pueda acercarnos a la proyección
de un “futuro sostenible”.

Esto es cierto tanto online como AFK. Toda tecnología refleja la sociedad que la
produce, incluyendo sus estructuras de poder y sus prejuicios. Esto es real hasta
en el nivel del algoritmo. El mito anticuado, no obstante, que equipara lo digital
a lo radical continúa demostrándose falso. Las instituciones culturales normativas
y el constructo social de normas taxonómicas -género, raza, y clase- que se crean
dentro de ellas rápidamente marginalizan la diferencia. Paradójicamente, la propia
naturaleza de esas diferencias excita, y son catalogadas como “salvajes.” Sin embargo,
lo salvaje solo se permite mientras esté adecuadamente mantenido, creciendo solo
dentro del espacio que se le asigna. Así como las instituciones físicas carecen de
inteligencia y conciencia, también lo hacen las instituciones de lo digital -Facebook,
Twitter, Instagram, Snapchat, Tiktok. Estas son las instituciones que están (re)definiendo
el futuro de la cultura visual; también están, sin duda alguna, profundamente llenas
de fallas.

En la primavera del 2018, en medio del #MeToo, apareció una publicidad de Snapchat
que le preguntaba a sus espectadores si preferirían “cachetear a Rihanna” o “pegarle
una piña Chris Brown”, lo que resultó en un backlash de indignación por el uso del
abuso doméstico sufrido por Rihanna en el 2009, a manos de su entonces pareja, el
cantante Chris Brown. Individuos de alto perfil como el rapero Joe Budden y la mediática
Chelsea Clinton le pusieron voz a su apoyo a Rihanna, y al horror generalizado con
respecto a la desagradable publicidad en Twitter. Rihanna misma fue a Instagram,
la plataforma rival de Snapchat, para responderle a Snapchat escribiendo: “Gastas
dinero animando algo que intencionalmente va a lastimar a las víctimas de violencia
doméstica y haces chistes con eso.” En los días siguientes, las acciones de Snapchat
bajaron 800 millones de dólares. Rihanna ejercitó su propio rechazo, su propia no-performance
alejándose del “público” de Snapchat, una intervención en la que alzó el puño en
solidaridad con lxs sobrevivientes del abuso doméstico.

La paradoja que supone usar plataformas que groseramente cooptan, sensacionalizan
y capitalizan a los cuerpos de color, los cuerpos que se identifican como femeninos
y los cuerpos queers (y también nuestro dolor) como medios para avanzar sobre diálogos
culturales y políticos urgentes acerca de nuestra lucha (sumado a nuestra alegría
y nuestros caminos) es algo que continúa imposible de ser ignorado. En estas líneas
fallidas surgen preguntas acerca del consentimiento -el tuyo, el mío, el de nosotrxs-
mientras continuamos “adhiriendo”, “alimentándonos” (a nuestros cuerpos como son
representados o performados online) dentro de estos canales. Para citar a la poeta
Nikki Giovanni: “No es esto contra revolucionario?”[^5]

[^5]: Pensando acá en el poema de Nikki Giovanni “Seduction / Kidnap Poem” (1975).

Quizás si. Sin embargo, si asumimos que la declaración de 1984 de Audre Lorde, “las
armas del amo nunca podrán desmantelar la casa del amo” sigue siendo cierta, entonces
quizá lo que estas instituciones -tanto online como offline- requieren no es un desmantelamiento,
sino un amotinamiento en forma de ocupación estratégica. El glitch nos desafía a
que pensemos cómo podemos “penetrar…romper…punzar…arrancar” lo material de la institución,
y, por extensión, de su cuerpo.[^6] Así, hackear el “código” del género, volver borrosos
los binarismos, se convierte en nuestro principal objetivo, un catalizador revolucionario.
Los cuerpos glitcheados -aquellos que no se alinean dentro del canon de la heteronormatividad
blanca y cisgénero- suponen una amenaza al orden social. Vastos y con un amplio rango
de posibilidades, no pueden ser programados.

[^6]: Jean-Luc Nancy, “Fifty-Eight Indices on the Body,” in Jean-Luc Nancy, Corpus,
translated by Richard Rand, New York: Fordham University Press, 2008.

Los cuerpos glitcheados no son considerados en el proceso de programación de nuevas
tecnologías creativas. En 2015, el algoritmo de reconocimiento de imágenes de Google
confundió a sus usuarios Negrxs con gorilas. La “acción inmediata” de la compañía
en respuesta a esto fue “evitar que Google Photos vuelva a catalogar ninguna imagen
como gorila, chimpancé o mono -incluso aunque las fotos fueran de estos primates.
Varios años después, en 2018, la app de Google Arts & Culture, con su opción de “museum
doppelgänger” que permitía a lxs usuarixs encontrar una obra de arte que contenga
figuras o caras que se parecieran a la de elle, provocó asociaciones problemáticas,
ya que el algortimo identificaba los parecidos de acuerdo a atributos racializados
o que esencializaban las etnias. Para muchxs de nosotrxs, estas “herramientas” no
han hecho mucho más que ampliar la gama del sesgo racial. Estas tecnologías subrayan
el arco dominante de la blancura en el proceso histórico de creación de imágenes
en el arte, y la diseminación de esas imágenes en un mercado que presenta sesgos
muy profundos por sí mismo. Además, resaltan la desigualdad estructural inherente
a la creación de estas herramientas en sí, con tales algoritmos creados por y para
la blancura, haciendo eco del discriminatorio y violento canon histórico del arte.

Online, lidiamos con múltiples preguntas acerca del uso, la participación y la visibilidad.
Nunca antes en nuestra historia existió semejante oportunidad para producir, y acceder,
a tan distintos tipos de públicos. En 1995, el poeta y activista Essex Hemphill reflexionó:
“Estoy ante el umbral del ciberespacio y me pregunto, ¿puede ser posible que tampoco
sea bienvenido acá? ¿Me permitirán construir una realidad virtual que me empodere?
¿Pueden los hombres invisibles ver sus propios reflejos?”[^7]

[^7]: Essex Hemphill, “On the Shores of Cyberspace,” lecture given at “Black Nations/Queer
Nations?” conferencia, City University of New York, 1995.

Hoy las preguntas de Hemphill perduran, volviéndose incluso más complicadas, por
el hecho de que el “público” del internet no es singular ni cohesivo, sino divergente
y fractal. Además, el “espacio” del ciberespacio que Hemphill menciona ha demostrado
no ser una utopía universalmente compartida. En vez de eso, es un espacio con muchos
mundos, y dentro de estos mundos, entendimientos vastamente disímiles sobre cómo
se vería la utopía o en qué podría transformarse -y para quiénes. El internet es
un edificio institucional inmersivo, uno que refleja y rodea. No tiene una entrada
fija: está en todos lados, alrededor nuestro. Por lo tanto, la noción de Hemphill
del “umbral” resulta anticuada hoy en día.

Esta búsqueda por “nuestros propios reflejos” -reconocernos a nosotrxs mismxs dentro
de la materia digital y del “espejo negro” eléctrico que la contiene — está inextricablemente
ligada con la búsqueda por el auto-reconocimiento lejos de la pantalla también. Los
cuerpos “otrerizados” son renderizados invisibles porque no pueden ser leídos por
el mainstream normativo, y, por lo tanto, no pueden ser categorizados. Como tales,
son borrados o mal clasificados dentro y fuera de un algoritmo de designación. Quizás,
entonces, esta “tierra del NOPE” de la que habla E. Jane en su manifiesto es la misma
utopía que reclama Hemphill, la tierra sagrada donde nuestros avatares digitales
y nuestros “yos” AFK pueden quedar suspendidos para siempre en un beso eterno. Una
tierra donde no estamos esperando ser bienvenidxs por aquellas fuerzas que nos esencializan
o rechazan, sino donde creamos una seguridad para _nosotrxs mismxs_ ritualizando
la celebración de _nosotrxs mismxs_.

Con esto, lo digital se transforma en el catalizador para realizar las variaciones
del propio dominio del “yo”. Con cada unx de nosotrxs, “hombres invisibles”, seguimos
siendo responsables de manifestar nuestros propios reflejos, y a través del Internet
actual, podemos encontrar maneras para sostener estos espejos en frente del otrx.
Así, nos empoderamos a través de la tarea liberadora de tomar el imaginario digital
como oportunidad, un sitio para construir-en, y, a su vez, el material para construir-con.

El Glitch se manifiesta con tal variación, generando rupturas entre lo _reconocido_
y lo _reconocible_, y amplificándose dentro de estas rupturas, extendiéndolas para
convertirlas en valles fantásticos llenos de posibilidades. Es acá donde nos abrimos
a la oportunidad de reconocer y realizarnos a nosotrxs mismxs, “reflejándonos” para
realmente _vernos_ lxs unxs con lxs otrxs mientras nos movemos y modificamos. La
filósofa y teórica del género Judith Butler observa en su libro _Excitable Speech:
A Politics of the Performative_, “Unx “existe” no solo por la virtud de ser reconocidx,
sino…por ser reconocible.”[^8] Nos delineamos a nosotrxs mismxs a través de nuestra
capacidad para ser reconocibles; nos volvemos cuerpos al reconocernos, y, mirando
hacia afuera, al reconocer aspectos de nosotrxs mismxs en lxs demás.

[^8]: Judith Butler, Excitable Speech: A Politics of the Performative, New York:
Routledge, 1997, p. 5.)

A través de las reflexiones de Hemphill sobre los “reflejos” en el ciberespacio,
se hace evidente la falta del mismo dentro de un medio social más amplio, con la
prevalencia aún limitada de tales “reflejos”, tanto online como offline. Siempre
vamos a tener problemas para poder reconocernos a nosotrxs mismxs si seguimos volviendo
a lo normativo como punto de referencia central. En una conversación entre la escritora
Kate Bornstein y lx artista trans, activista y productorx Zackary Drucker, Bornstein
observó que “Cuando el género es binario, es un campo de batalla. Cuando te deshacés
de lo binario, el género se transforma en un parque de juegos.”[^9]

[^9]: Kate Bornstein in Conversation with Zackary Drucker, “Gender Is a Playground,”
Aperture 229, Winter 2017, p. 29.

La etimología de _glitch_ tiene sus raíces más profundas en el vocablo Yiddish _glensh_
(deslizarse, planear, resbalarse) o el alemán _glitcschen_ (resbalarse). _Glitch_
es, por lo tanto, una palabra activa, una que implica movimiento y cambio desde el
principio; este movimiento dispara el error.

La palabra _glitch_ como la usamos y entendemos actualmente fue popularizada por
primera vez en los años 60s, parte de los escombros culturales del programa espacial
de los Estados Unidos en proceso de “burguerización.” En 1962, el astronauta John
Glenn usó la palabra en su libro _Into Orbit_: “Otro término que adoptamos para describir
algunos de nuestros problemas fue “glitch”. Literalmente, un glitch…es un cambio
tan repentino en el voltaje que ningún fusible podría protegernos contra él”.[^10]
La palabra resurgió algunos años después, en 1965, con el _St. Petersbug_ Times reportando
que “un glitch había alterado la memoria de la computadora dentro de la nave espacial
estadounidense Gemini 6”; luego, aparece de nuevo en las páginas del _Time Magazine_:
“Los glitches -la palabra de un hombre del espacio para referirse a los disturbios
irritantes”.[^11] Más tarde, en 1971, los “glitches” aparecen en un artículo del
_Miami News_ sobre la falla de funcionamiento del Apollo 14, cuando un glitch casi
estropea un alunizaje.

[^10]: Emily Siner, “What’s A ‘Glitch,’ Anyway?: A Brief Linguistic History,” NPR,
October 24, 2013, npr.org.

[^11]: Ibid.

Atravesando estos orígenes, podemos llegar a la conclusión de que el glitch es un
modo de no-performance: una “falla en la performance”, un rechazo directo, un “nope”
por derecho propio, ejectutado con experticia por una máquina. Esta falla en la performance
revela una tecnología resistiendo contra las pesadas cargas del funcionamiento. A
través de estos movimientos, la tecnología se torna, en efecto, resbaladiza: vemos
evidencia de esto en páginas que no responden, que nos presentan el binarismo fatal
de elegir entre “matar” o “esperar”, la colorida rueda de la muerte, la iconografía
de la “Mac triste”, una pantalla congelada -todos indicativos de un error fatal del
sistema.

Aquí yace una paradoja: el glitch se mueve, pero también el glitch bloquea. Incita
el movimiento y a la vez crea un obstáculo. El glitch impulsa y a la vez previene.
Con esto, el glitch se transforma en un catalizador, abriendo nuevos caminos, permitiéndonos
tomar nuevas direcciones. En Internet exploramos nuevos públicos, nos relacionamos
con nuevas audiencias, y, sobre todo, nos _glitschen_ (deslizamos) entre nuevas concepciones
de cuerpos y “yos”. Por lo tanto, el glitch es algo que se extiende más allá de las
mecánicas tecnológicas más literales: nos ayuda a celebrar la falla como fuerza generativa,
una nueva forma de conquistar el mundo.

En el año 2011, el teórico Nathan Jurgenson presentó su crítica del “dualismo digital”,
identificando y problematizando la separación entre el autodominio online y la “vida
real.” Jurgenson argumenta que el término _IRL_ (“In Real Life — En la Vida Real”)
es una falsedad anticuada, que implica que dos “yos” (es decir, un “yo” online versus
un “yo” offline) operan aislados uno del otro, y por lo tanto infiere que cualquier
actividad online carece de autenticidad y está divorciada de la identidad offline
del usuario. Por lo tanto, Jurgenson propone el uso de AFK[^12] en lugar de IRL,
ya que AFK significa más una progresión continua del ser, una que no termina cundo
el usuario se aleja de la computadora, sino que continúa por fuera y se introduce
en la sociedad que se encuentra lejos del teclado.

[^12]: Nota de la traductora: “Away From Keyboard — Lejos del teclado”

El glitch atraviesa este loop, moviéndose más allá de la pantalla y permeando cada
esquina de nuestras vidas. Nos muestra que la experimentación online no nos aleja
de nuestros “yos” AFK, y tampoco evita que cultivemos comunidades significativas
y colaborativas más allá de nuestras pantallas. En cambio, propone el opuesto polar:
la producción de estos “yos”, las pieles digitales que desarrollamos y usamos online,
nos ayudan a entender quiénes somos con mayores matices. Así, usamos el glitch como
vehículo para re-pensar nuestros seres físicos. En efecto, el cuerpo es en sí mismo
una arquitectura que es activada y luego compartida como un meme para mejorar la
lógica social y cultural. Históricamente, el feminismo fue construido sobre esta
base estancada, primero pronunciándose por la paridad pero, paradójicamente, no siempre
para todos los cuerpos, o sin ningún tipo de objetivos anti-sexistas, anti-racistas,
anti-clasistas, homofóbicos, transfóbicos o capacitistas. Como movimiento, el lenguaje
del feminismo -y, más contemporáneo, el “estilo de vida feminista”- ha sido en gran
parte co-dependiente de la existencia del binarismo de género, trabajando por el
cambio solo dentro de un orden social existente.[^13] Esto es lo que hace que el
discurso alrededor del feminismo sea tan complicado y confuso.

[^13]: bell hooks, Feminism Is for Everybody: Passionate Politics, Boston: South
End Press, 2000.

La legendaria construcción de la figura de “cyborg” en el texto de la teórica feminista
Donna Haraway, “A Cyborg Manifesto” (1984) -sobre el que tantas discusiones sobre
tecno y ciberfeminismo fueron construidas- complica mucho más nuestro entendimiento
sobre el cuerpo. El cyborg de Haraway discute y se aleja activamente del léxico de
lo humano, una clasificación por la que los cuerpos históricamente “otrerizados”
(por ejemplo, la gente de color, la gente queer) lucharon largamente, intentando
ser integradxs en ella. En retrospección, es un 20/20: Haraway, en el 2004, volvió
a leer su manifiesto, y notó que “Un cuerpo cyborg no es inocente…somos responsables
por las máquinas…La raza, el género y el capital requieren una teoría cyborg de los
“todos” y de las partes.”[^14]

[^14]: Donna J. Haraway, The Haraway Reader, New York: Routledge, 2004, p. 38.

En 1994, la teórica cultural Sadie Plant acuñó el término “ciberfeminismo”. Como
proyecto histórico y como política continua, el ciberfeminismo sigue siendo unx compañerx
filosóficx de este discurso sobre el glitch: ve el espacio online como un medio para
la construcción de mundos, desafiando la normatividad patriarcal del “mainstream
offline”. Sin embargo, la temprana historia del ciberfeminismo fue muy similar a
los comienzos del feminismo AFK, en su problemática re-aplicación de las políticas
feministas de la primera y de la segunda ola, dentro de las que, en ese punto, ya
era una tercera ola feminista en plena marcha.

Las primeras ciberfeministas hicieron eco de la retórica de la primera ola del feminismo
AFK, al ser fóbicas de las alianzas transnacionales. La cara pública del ciberfeminismo
fue regularmente promovida y fetichizada como la de las mujeres blancas -Sadie Plant,
Faith Wilding, N. Katherine Hayles, Linda Dement, para nombrar algunas pioneras-
y encontró su mayor apoyo en el reino de la academia de las escuelas de arte. Esta
realidad demarcó el espacio digital como blanco y occidental, trazando una ecuación:
_mujeres blancas = produciendo teoría blanca = produciendo un ciberespacio blanco_.

Este paisaje ciberfeminista blanco marginalizó a la gente queer, a la gente trans,
y a la gente de color, que apuntaban a descolonizar el espacio digital a través de
su producción en canales y redes similares. Algunas excepciones como Old Boy’s Network,
SubROSA, o la VNX Matrix hicieron un gran impacto al ofrecer un discurso alternativo
que reconocía periféricamente el racismo de la mano del sexismo, pero la hipervisibilidad
de las caras y las voces blancas en toda la cibercultura feminista demostraba la
continua exclusión, incluso en este entorno nuevo y “utópico.”

A pesar de esto, aquellos primeros días del ciberfeminismo establecieron una base
importante en la introducción de lo tecnológico, lo digital, e incluso lo cibernético
como el imaginario computacional en el feminismo mainstream. Con el ciberfeminismo,
lxs feministas ahora podían establecer redes, teorizar, y criticar online, trascendiendo
(ya sea temporalmente, ya sea simbólicamente) el sexo, el género, la geografía. Con
esto también vino la conciencia fundacional acerca de cómo el poder opera como agente
del capitalismo dentro del edificio del espacio online, estimulado por constructores
tecnológicos que le dieron forma a la manera en la que lxs usuarixs experimentamos
los mundos digitales y sus políticas.

El feminismo es una institución por derecho propio. En sus raíces encontramos un
legado de exclusión de las mujeres Negras desde su momento fundacional, un movimiento
que por mucho tiempo se hizo a sí mismo exclusivo para las mujeres blancas de clase
media. En la base del primer feminismo y de la defensa feminista, la supremacía racial
servía a las mujeres blancas tanto como a sus contrapartes masculinas, junto a las
feministas reformistas -esto es, un feminsmo que operaba dentro del orden social
establecido más que resistir contra él- volviéndose una suerte de movilidad de clase.
Esto subraya la realidad de que la categoría “mujer” como una asignación de género
que indica, si no otra cosa, un derecho a la humanidad, no siempre incluyó en ella
a la gente de color.

La “hermandad” feminista por el propósito de incrementar el rango blanco y para ampliar
la movilidad económica, social y cultural, es un ejercicio en servicio de la supremacía
-_solo para las mujeres blancas_. Este es el lado feo del movimiento: uno en el que
reconocemos que, si bien el feminismo es un desafío al poder, no todxs estuvieron
siempre en la misma página acerca de para quién es ese poder y cómo debería usarse
en pos del progreso. _¿Progreso para quiénes?_ Por eso, la pregunta de Sojourner
Truth, una abolicionista estadounidense, activista por los derechos de las mujeres
y esclava liberada, “¿No soy yo una mujer?”, preguntada en 1851, continua resonando
dolorosamente incluso hoy, surgiendo en la siempre-urgente realidad de quien es traídx
dentro de la definición de lo que [se supone] que es ser mujer, y, por extensión,
de quién es realmente reconocidx como alguien completamente humano.

Mientras vadeamos nuestro camino a través del feminismo contemporáneo y las negociaciones
de poder representadas por #BlackLivesMatter, #MeToo, o la tradición de la Marcha
del Día de la Mujer, debemos reconocer que estos movimientos están definidos e incentivados
por la tecnología, presagios de una prometedora y potencialmente más inclusiva “cuarta
ola” desenvolviéndose en el horizonte. Pero todavía permanecen los peligrosos vestigios
de la historia de la primera y la segunda ola. La escritora, activista y feminista
bell hooks puede haber declarado que “el feminismo es para todxs”, pero lo que nos
queda para llegar a eso es un todavía largo y sinuoso camino por delante.

Donde el _glitch_ se encuentra con el _feminismo_ en un discurso que problematiza
la construcción del cuerpo, es importante señalar la construcción histórica del género
que se intersecta con una construcción histórica de la raza. El cuerpo también es
una herramienta social y cultural. Por esto, el derecho a definir lo que un cuerpo
es, sumado al de quién tiene control sobre estas cosas llamadas “cuerpos”, nunca
fue ejercido equitativamente. En el paisaje contemporáneo, en el que el término “interseccional”
es empleado de forma tan ligera, es importante reconocer el trabajo de la negrura,
en particular con respecto al proyecto feminista.

La pregunta urgente de Soujourner Truth puede también iluminar al cuerpo queer dentro
del espectro de la identificación. En un entorno contemporáneo, la línea de investigación
de Truth pide el reconocimiento de humanidad y de un futuro que celebre los cuerpos
de color, cuerpos que se identifican como femeninos, cuerpos que aceptan el entremedio
y el más allá, todo como resistencia activa, un borramiento estratégico del binarismo.
No podemos olvidarnos de esto: siempre fue, y continua siendo, la presencia de la
negrura la que ayudó a establecer el primer precedente de la noción de interseccionalidad
dentro del feminismo. _Interseccionalidad_ como término fue acuñado en 1989 por la
teórica y activista Kimberlé Crenshaw para hablar de las realidad de la negrura y
del ser-mujer como parte de una experiencia vivida, ninguna de las dos excluyentes
de la otra, más bien promoviendo el trabajo desde ambos lados. La contribución perdurable
de Crenshaw refuerza las bases para el pensamiento primero que llevó a hacer espacios
para la multiplicidad a través de los “yos” dentro de un contexto social y cultural
más amplio, uno que resuena incluso hoy tanto online como AFK.

Como observa la artista y ciberfeminista Cornelia Sollfrank: “El ciberfeminismo no
se expresa a sí mismo desde un punto de vista único e individual, sino a través de
las diferencias y los espacios entremedio”.[^15] Es en este espacio intermedio que
nosotrxs, como feministas glitch, encontramos nuestras posibilidades, nuestros múltiples
y variados “yos”. Por lo tanto, el trabajo de la negrura en la expansión del feminismo
-y, por extensión, del ciberfeminismo- permanece como un esencial precursor de las
políticas glitch, creando un nuevo espacio para redefinir las caras del movimiento,
amplificando la visibilidad de cuerpos históricamente “otrerizados”.

[^15]: (Cornelia Sollfrank, “The Truth about Cyberfeminism,” available at
obn.org/reading_room/writings/html/truth.html.)

Podemos encontrar ejemplos de esto en textos como la trilogía de 1980 _Xenogenesis_,
de la escritora Octavia Butler, que galvaniza la noción del porvenir del tercer sexo,
desafiando el género binario. O en la discusión de Audre Lorde con respecto a lo
erótico como poder en su ensayo de 1978, “Uses of the Erotic: The Erotic as Power”,
que nos insta a descubrir todo nuestro rango, a través de una conexión con nosotrxs
mismxs que nos trae disfrute. Estas contribuciones no salieron del ciberfeminismo,
pero sí lo transformaron, expandieron y liberaron. Tal alquimia vuelve ilimitada
a la capacidad de movilización del glitch.

Entonces revisitemos, ocupemos, y descolonicemos las palabras de Whitman para proclamar
nuestro rechazo:

**¿Me contradigo?**

**Muy bien, entonces, me contradigo**

**(Soy gigante, contengo multitudes)**
