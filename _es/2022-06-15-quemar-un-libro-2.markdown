---
title: quemar un libro 2 [Kurt Schwitters Merz]
description: libro escaneado
title_page: ~/Quemar un libro a la luz de un scanner (Kurt Schwitters Merz)
author: librenauta
image:
permalink:
category: scann
tags:
- scanner
- book
- libros
- quemar
- merz
draft:
order:
layout: post
uuid: d3b8c1c9-db85-4b77-b020-6a13c6815d21
liquid: false
last_modified_at: 2022-06-15 22:20:21.155018278 +00:00
---

## Kurt Schwitters Merz scan chaos dadá

En esta oportunidad tuve la casualidad de encontrar a [pierina](https://kiterea.com) en plena mudanza y entre guardar libro y encintar cajas me tope con MERZ. me dijo:

 > - _si querés llevalo_

 > - oki

 Asique lo quemé en el scanner de casa para poder armar una versión digital de esta edición preciosa de Buchwald Editorial.

 ¿Qué hay de DADA en el aire?
~~~
 D      A

        D       A
~~~

![scann-1](public/0b28fefe-661f-4389-abf4703fbcf21f0d/1.jpg)

![scann-2](public/0b28fefe-661f-4389-abf4703fbcf21f0d/2.jpg)

ps psss [acá](pdf/Kurt_Schwitters-Merz.pdf) tienen para descargar Merz - Kurt Schwitters.

_quemar en una luz para no perder el acceso_
