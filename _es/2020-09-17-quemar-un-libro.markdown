---
layout: post
title: 'Quemar un libro a la luz del scanner'
tags:
  - scan
  - libros
  - scantailor
  - ocrmypdf
category: scann
author: librenauta
uuid: 73b655bc-e9dd-407a-af5c-8e71776a5d8b
order: 21
draft: false
last_modified_at: 2020-09-17 22:30:21.155018278 +00:00
---

# /libros escane_dos

### _Scanner house for fun and piracy_

Este es mi primer libro escaneado desde 0, si bien ya había editado otros o pasado ocr a algunos pdf de por ahí, nunca había maquetado uno.

![Scanner-1](public/proyectos/quemar-un-libro/scanner-1.png)

![Scanner-1](public/proyectos/quemar-un-libro/scanner-2.png)

![Scanner-1](public/proyectos/quemar-un-libro/scanner-3.png)

+ Usé un scanner Hp Scanjet N8420 (que me encontré en la calle, frente a una oficina cuando compran cosas nuevas y tiran las viejas porque son viejas) y solamente le enchufe el cable usb en casa + el plug 220v para probar si funciona y anda perfecto.

+ luego de escanear página por página, utilice el software [ScanTailor](https://scantailor.org/) que permite hacer ajustes increíbles con lo escaneado:
1. permite alinear cada página por si no salieron escaneadas en 0°
2. autodetecta si es una página doble y la corta en items separados
3. autodetecta la caja y margenes o podés hacerlo manual (me funcionó mejor manual)
4. limpia errores de escaneo y genera un mejor contraste (en este libro no lo utilicé porque tenía imágenes y me las oscurecia mucho y no encontré una forma rápida (que debe existir) de no procesar esas imágenes y que le pase el contrast a todo el resto del texto )

En una carpeta "out" scantailor te copia el resutado de su proceso y todas las imágenes estan en formato .tif

 luego como las cajas que hice a mano era un poco diferentes (por lo tanto el output de las imágenes de scantailor no eran todas iguals) con el siguiente comando las ajuste a  todas a 1000px de ancho
 `mogrify -resize 1000 *.tif`

con mogrify es posible cambiarle el formato a las imágenes, yo las pasé a png para que pesen menos.

`mogrify -format jpg *.tif`

con [imagemagick](https://imagemagick.org/index.php) instalado use convert para crear un pdf con todas las imágenes
`convert *.jpg shanzhai.pdf`

instale ocrmypdf para poder generar una capa con los caractéres y poder seleccionar el texto
`sudo dnf/apt install ocrmypdf`
y luego ejecuté
`ocrmypdf ./shanzhai.pdf ./shanzhai-ocr.pdf`

* Descarga -> [Shanzhai byung-chul han : El arte de la falsificación y la deconstrucción en china](pdf/shanzhai-ocr.pdf)

* Magnet -> [magnet de shanzhai](magnet:?xt=urn:btih:810bead2a6bd76705919d7c083d9b61dbd50e6a6&dn=shanzhai)

* Descarga 2 -> [Shanzhai byung-chul han  El arte de la falsificación y la deconstrucción en china by Marcelo B](pdf/Byung-Chul-Han-Shanzhai-El-arte-de-la-falsificación-y-la-deconstruccion-en-China-Caja-Negra-Edit-2017-86-págs-Con-OCR-600.pdf)

Gracias por compartir tu escaneo !
