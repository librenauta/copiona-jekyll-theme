---
title: Mirar de cerca
author: Librenauta
description: imágenes de flores
image:
  path: public/77bc6b3a-24a4-4cce-8938-120a23ce7b0d/cover.jpg
  description:
category: proyectos
tags:
- naturaleza
- flores
- canal
draft: false
order:
picture_multiple: public/77bc6b3a-24a4-4cce-8938-120a23ce7b0d/images
layout: post
uuid: 77bc6b3a-24a4-4cce-8938-120a23ce7b0d
last_modified_at: 2023-01-07 16:20:21.155018278 +00:00
liquid: false
---

# Mirar de cerca  

es un canal de telegram donde compartimos fotos de flores en primerisimo primer plano, lo inicie hace un tiempo y distintas personas de diferentes latitudes suman fotos, [acá](https://telegram.com/mirar-de-cerca) podés ver como crece.

creo que lo inicié por un recuerdo hermoso: cuando existía [phhhoto.com](https://news.phhhoto.com/) que fue la red social que creó los ahora conocidos como "boomerang", yo seguía [una muchacha](https://news.phhhoto.com/post/121162999525/shades-of-summer-created-by-emilyblincoe-on) que sólo subía fotos de sus manos con flores, creo que estuve enamorado de sus manos pero me dí cuenta años mas tarde.
