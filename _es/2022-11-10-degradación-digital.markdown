---
title: degradación digital de un otoño artificial
author: Librenauta
description: patrones generados con image2ascii desde escaneos de hojas
proyect: f804d881-5340-465a-a445-0ae0e4853bde
image:
  path: public/f698e707-4941-4733-b390-03d7c9d81904/cover.jpg
  description: flyer
category: proyectos
tags:
- pattern
- degradación
- ascii art
draft: false
order:
layout: post
uuid: f698e707-4941-4733-b390-03d7c9d81904
liquid: false
---

# Degradación digital de un otoño artificial.


![degradación-digital-1](public/f698e707-4941-4733-b390-03d7c9d81904/degradación-digital-1.jpg)

## Memoria:

degradar con el software generalmente está vinculado a la compresión y pérdida de calidad. como bien dice hito steyerl en [En defensa de la imagen pobre](https://copiona.com/en-defensa-de-la-imagen-pobre/)



> "La imagen pobre es una copia en movimiento. Su calidad es mala, su resolución deficiente. Se deteriora conforme acelera. Es el fantasma de una imagen, una vista previa, una miniatura, una idea errante, una imagen itinerante distribuida de forma gratuita, exprimida a través de lentas conexiones digitales, comprimida, reproducida, extraída, remezclada, así como copiada y pegada en otros canales de distribución."



una imágen de mala calidad es una imagen pobre, despojada del esplendor de la definición, una lumpenproletaria de la sociedad de clases.  

pero a partir de esta máxima muy hermosa que escribe hito, estuve pensando en realizar un proceso de degradación digital sobre hojas escaneadas (después de pasar el otoño del 2021 mirando y juntando hojas degradadas de la huerta que habité). En la construcción conceptual, junto a un grupo hermoso en el taller de "línea de fuga" con [julieta marra, francis, julia, danna, delfi, manuel y tone ] jugando con la impresión con la ticketera parecía interesante imprimir las hojas escaneadas.

-

En la línea de la degradación no me interesaba usar el recurso de la pixelación, sino que parte del concepto era poder emular un poco esta fascinación por los patrones de construcción de hojas que tienen las plantas [1,1,2,3,5,8,12] :D.

-

Usando el histórico `ascii art` como inspiración y seleccionando una paleta de caracteres específica es posible hacer un acercamiento. degradar la imagen en síntesis, en pérdida de información, pero sin perder resolución. una degradación que intenta simular el proceso natural. imprimir el otoño con hardware, degradar las hojas con software.

ahora lo divertido, cómo se hace todo esto?

## receta:

paleta de caractéres:

```
PALETTE : ▓ ▒ ░ ─└ ┼ ┘├ ┐┌ ┤ │ | -
```

la gema que use es:

gema: https://rubygems.org/gems/image2ascii

repo: https://github.com/michaelkofron/image2ascii#image2ascii

dependencias:

```
$  sudo dnf install ImageMagick
$  sudo dnf install ImageMagick-devel
$  gem install image2ascii
$  gem install imagemagick
$  gem install rmagick
$  gem install raimbow
```

luego pasar imagen a caractéres ej:


`image2ascii $archivo.png -t 30 -g -x '▓ ▒ ░ ┐┌ ┤ │ | - . ~'`

con `-t` podés indicar el % de pantalla que querés que ocupe el resultado de la transformación

con `-g` utiliza escala de grises (por defecto es a color)

con `-x` podés indicar los caractéres específicos para usar.

## Nombres

impresora: 80
hoja$: h$.png
hoja en caractéres: h$-[1,2,3,4,5]

### imprimir desde terminal

`pl -d 80 $archivo`

parece que se pueden imprimir muchos archivos uno seguido del otro con

`lp -d 80 h1.png h2.png h3.png`

#### ejemplos que usé

`lp -d 80 h1.png h1-1.png h1-2.png h1-3.png h1-4.png h1-5.png `

`lp -d 80 h2.png h2-1.png h2-2.png h2-3.png h2-4.png h2-5.png `

`lp -d 80 h3.png h3-1.png h3-2.png h3-3.png h3-4.png h3-5.png `

`lp -d 80 h4.png h4-1.png h4-2.png h4-3.png h4-4.png h4-5.png `

`lp -d 80 h5.png h5-1.png h5-2.png h5-3.png h5-4.png h5-5.png `

## primer hoja h1

con lo siguiente lo que hacemos es la conversion de la imagen a caractéres , guardarla en un archivo h.temp.txt y luego enviarla a la impresora.

Es posible imprimir de esta forma directamente. lo que sucede es que la ticketera tiene un ancho específico (50mm) donde entran una cantidad especifica de caractéres, entonces la version de 80% contiene muchisimos más caractéres que la version de 3%. lo cual hace que se impriman sin forma. o sin la forma igual a la hoja original. entonces hay forma de escalar la cantidad de caractéres por pulgada a imprimir :D. por eso despues de `lp -d 80` esta la flag `-d cpi=68 -0 lpi=64` llegar a esto me llevo varios rollos de pruebas :v

### 80%
```
image2ascii h1.png -t 80  -x '▓ ▒ ░ ─└ ┼ ┘├ ┐┌ ┤ │ | -' > h-temp.txt &&  lp -d 80 -o cpi=68 -o lpi=64 h-temp.txt
```

### 50%

```
image2ascii h1.png -t 50  -x '▓ ▒ ░ ─└ ┼ ┘├ ┐┌ ┤ │ | -' > h-temp.txt &&  lp -d 80 -o cpi=48 -o lpi=22 h-temp.txt
```

### 30%

```
image2ascii h1.png -t 30  -x '▓ ▒ ░ ─└ ┼ ┘├ ┐┌ ┤ │ | -' > h-temp.txt &&  lp -d 80 -o cpi=27 -o lpi=13.5 h-temp.txt
```

### 20%

```
image2ascii h1.png -t 20  -x '▓ ▒ ░ ─└ ┼ ┘├ ┐┌ ┤ │ | -' > h-temp.txt &&  lp -d 80 -o cpi=18 -o lpi=8 h-temp.txt
```

### 10%s

```
image2ascii h1.png -t 10  -x '▓ ▒ ░ ─└ ┼ ┘├ ┐┌ ┤ │ | .' > h-temp.txt &&  lp -d 80 -o cpi=9 -o lpi=4 h-temp.txt
```

### 3% pantalla

```image2ascii h4.png -t 3  -x '▓ ▒ ░ ─└ ┼ ┘├ ┐┌ ┤ │ | .' > h-temp.txt &&  lp -d 80 -o cpi=3 -o lpi=1.5 h-temp.txt```

### resultado


```
--------- ┘├---------------------
--------|──└┌--------------------
------- ── ░─--------------------
------ ░──░▓┼ -------------------
-----| └└└─   └┐ ----------------
-----└─ └└─    └░┘|--------------
----├░─  ┼┼ └ ┼┼┼    ------------
---  ─└ ┼  ─ └┼    ┼ ┘ ----------
--│▒░─ ┼ ┼ ─  ┼┼├    ┼ ┌---------
-  ░─└  ┼┼    ┼┘├┘┘┘┘┘┼  |-------
- ▒  └└      ┼┘├├├┘┘  ┼ ─░ ------
-┼ ─  └ ┼ ┼  ─├┘┘├┘  ┼┼┼ └ ├-----
|▒ ─└  ┘┘┘┘ ├ ┘┘┘ ├┘ ┘  ┼  ░ ----
 ░└  ┼ ┼ ┘┘┘┘─├├├├├├ ┘├┼┼ ┼└  ---
┘ ┼┼┼  ┘┘├├┘├ ─ ├┐┘┘┐├├├ ├    |--
┌ └  ┼┘┘├├┐├ ┐ ├┐ ┘┘├├├  ├┘┼ ░├--
 ─  ┼   ├├├  ┘  ┐   ┘├├ ├┘┘┼   --
-|└  └  ┘ ┘┘  ┌ ┐  ┘┘┘  ├├┘  ─  -
---- ┘▒ ┼┘┘┘┘├   ├├├┘├┘├├├ ┼   ┼-
---|┘ ░─└─ └└┼┘└┼├┘├ ┘├├    ┘┼  -
---│ └  └└└ ┼ ┘  ┘┘┘┘├┘├  ┌├  └▒
-- └ └└└ ┼┼┼  ┼   ┼┼┼ ├┘├┘┘┘┼  ▒
--┤   └ └    └  ▒┼ ├┘┘  ├ ┼┼  └▒
--┤░─└└ └ ┼┼ └┼  ├┘┘┘┘ ├┘┘├ ┼┼└▒│
--| ─└└└ └┼┼┼    ┼  ┘ ┘┘┘ ┘└ └└ -
--   └└   ┼ ┼└└  ┘├┘├┘├┘┘   ┼   -
---├     └┼┼┼┼  ▓ ┘  ├┘┼┼┼ ┼  ┼--
---|░─   ┼ ┼┼ └ ▓┘┘┼├├┘   ┘ ─  --
----  ─ ┼┼┼┼ └└┼▓├ ┐┘ ┼ └ ┼└ │---
-----|┘░ ─  └──└  ┘├ ┼  ─└─ ┤----
-------┼└   ┼└└─▒┼    └└ ─░│-----
------- ─└┼ ┼   ░ ┼┼  ──░─|------
-------- ┼└┼┼ ─▒─┘┼ └─░░┌--------
----------  └ ─▓└┼┼ ░░├ ---------
-----------│└░░▓─┼─ ┌ -----------
------------  ░▓─└┐--------------
--------------| ▒|---------------
--------------┐▓└----------------
-------------|   ----------------
------------  ▒ -----------------
------------ ┼ ------------------
```

---

# un registro de cómo pasó esto en diferentes lugares

![flyer_osmiornica](public/f698e707-4941-4733-b390-03d7c9d81904/flyer_osmiornica.png)
![flyer_tranza](public/f698e707-4941-4733-b390-03d7c9d81904/flyer_tranza.png)
![flyer_fablabbrc](public/f698e707-4941-4733-b390-03d7c9d81904/flyer_fablabbrc.png)
![flyer_brc](public/f698e707-4941-4733-b390-03d7c9d81904/flyer_brc.png)


### proceso

![degradación-digital-1](public/f698e707-4941-4733-b390-03d7c9d81904/degradación-digital-1.jpg)
![degradación-digital-2](public/f698e707-4941-4733-b390-03d7c9d81904/degradación-digital-2.jpg)
![degradación-digital-3](public/f698e707-4941-4733-b390-03d7c9d81904/degradación-digital-3.jpg)
![degradación-digital-4](public/f698e707-4941-4733-b390-03d7c9d81904/degradación-digital-4.jpg)
![degradación-digital-5](public/f698e707-4941-4733-b390-03d7c9d81904/degradación-digital-5.jpg)
![degradación-digital-6](public/f698e707-4941-4733-b390-03d7c9d81904/degradación-digital-6.jpg)
![degradación-digital-7](public/f698e707-4941-4733-b390-03d7c9d81904/degradación-digital-7.jpg)
![degradación-digital-8](public/f698e707-4941-4733-b390-03d7c9d81904/degradación-digital-8.jpg)
![degradación-digital-9](public/f698e707-4941-4733-b390-03d7c9d81904/degradación-digital-9.jpg)

## muestra tranza

![degradación-digital-10](public/f698e707-4941-4733-b390-03d7c9d81904/degradación-digital-10.jpg)
![degradación-digital-11](public/f698e707-4941-4733-b390-03d7c9d81904/degradación-digital-11.jpg)
![degradación-digital-12](public/f698e707-4941-4733-b390-03d7c9d81904/degradación-digital-12.jpg)
![degradación-digital-13](public/f698e707-4941-4733-b390-03d7c9d81904/degradación-digital-13.jpg)
![degradación-digital-14](public/f698e707-4941-4733-b390-03d7c9d81904/degradación-digital-14.jpg)

## muestra brc

![degradación-digital-11](public/f698e707-4941-4733-b390-03d7c9d81904/degradación-digital-15.jpg)
![degradación-digital-12](public/f698e707-4941-4733-b390-03d7c9d81904/degradación-digital-16.jpg)
![degradación-digital-13](public/f698e707-4941-4733-b390-03d7c9d81904/degradación-digital-17.jpg)
![degradación-digital-14](public/f698e707-4941-4733-b390-03d7c9d81904/degradación-digital-18.jpg)

faltan más que @digicronos me tiene que pasar <3

lo que resolví luego es hacer una exportación en buena resolución (300dpi) de cada una de las imágenes convertidas desde el archivo en .svg a .png para poder llamarlas directamente al imprimir. acá van las pruebas

imágenes procesos de degradación:

## hoja 1

`lp -d 80 h1.png `

![h1](public/f698e707-4941-4733-b390-03d7c9d81904/h1.png)

`lp -d 80 h1-1.png `

![h1](public/f698e707-4941-4733-b390-03d7c9d81904/h1-1.jpg)

`lp -d 80 h1-1.png `

![h1](public/f698e707-4941-4733-b390-03d7c9d81904/h1-2.jpg)

`lp -d 80 h1-1.png `

![h1](public/f698e707-4941-4733-b390-03d7c9d81904/h1-3.jpg)

`lp -d 80 h1-1.png `

![h1](public/f698e707-4941-4733-b390-03d7c9d81904/h1-4.jpg)

`lp -d 80 h1-1.png `

![h1](public/f698e707-4941-4733-b390-03d7c9d81904/h1-5.jpg)

---

## hoja 2

`lp -d 80 h2.png `

![h2](public/f698e707-4941-4733-b390-03d7c9d81904/h2.jpg)

`lp -d 80 h2-1.png `

![h2](public/f698e707-4941-4733-b390-03d7c9d81904/h2-1.jpg)

`lp -d 80 h2-1.png `

![h2](public/f698e707-4941-4733-b390-03d7c9d81904/h2-2.jpg)

`lp -d 80 h2-1.png `

![h2](public/f698e707-4941-4733-b390-03d7c9d81904/h2-3.jpg)

`lp -d 80 h2-1.png `

![h2](public/f698e707-4941-4733-b390-03d7c9d81904/h2-4.jpg)

`lp -d 80 h2-1.png `

![h2](public/f698e707-4941-4733-b390-03d7c9d81904/h2-5.jpg)

## hoja 3

`lp -d 80 h3.png `

![h3](public/f698e707-4941-4733-b390-03d7c9d81904/h3.jpg)

`lp -d 80 h3-1.png `

![h3](public/f698e707-4941-4733-b390-03d7c9d81904/h3-1.jpg)

`lp -d 80 h3-1.png `

![h3](public/f698e707-4941-4733-b390-03d7c9d81904/h3-2.jpg)

`lp -d 80 h3-1.png `

![h3](public/f698e707-4941-4733-b390-03d7c9d81904/h3-3.jpg)

`lp -d 80 h3-1.png `

![h3](public/f698e707-4941-4733-b390-03d7c9d81904/h3-4.jpg)

`lp -d 80 h3-1.png `

![h3](public/f698e707-4941-4733-b390-03d7c9d81904/h3-5.jpg)

## hoja 4

`lp -d 80 h4.png `

![h4](public/f698e707-4941-4733-b390-03d7c9d81904/h4.jpg)

`lp -d 80 h4-1.png `

![h4](public/f698e707-4941-4733-b390-03d7c9d81904/h4-1.jpg)

`lp -d 80 h4-1.png `

![h4](public/f698e707-4941-4733-b390-03d7c9d81904/h4-2.jpg)

`lp -d 80 h4-1.png `

![h4](public/f698e707-4941-4733-b390-03d7c9d81904/h4-3.jpg)

`lp -d 80 h4-1.png `

![h4](public/f698e707-4941-4733-b390-03d7c9d81904/h4-4.jpg)

`lp -d 80 h4-1.png `

![h4](public/f698e707-4941-4733-b390-03d7c9d81904/h4-5.jpg)

## hoja 5

`lp -d 80 h5.png `

![h5](public/f698e707-4941-4733-b390-03d7c9d81904/h5.jpg)

`lp -d 80 h5-1.png `

![h5](public/f698e707-4941-4733-b390-03d7c9d81904/h5-1.jpg)

`lp -d 80 h5-1.png `

![h5](public/f698e707-4941-4733-b390-03d7c9d81904/h5-2.jpg)

`lp -d 80 h5-1.png `

![h5](public/f698e707-4941-4733-b390-03d7c9d81904/h5-3.jpg)

`lp -d 80 h5-1.png `

![h5](public/f698e707-4941-4733-b390-03d7c9d81904/h5-4.jpg)

`lp -d 80 h5-1.png `

![h5](public/f698e707-4941-4733-b390-03d7c9d81904/h5-5.jpg)


# update 18.01.2023

y qué pasa si uso estos [caracteres](https://twitter.com/SmoothUnicode/status/1612263040304689153):

```
╰╯╰╭╭╮╰╰╮╰╰╭╭╭║╮╯╮╭╭═╰╭╮╭╯╰╭╰╰╮╰╰╯╓╭╯╭╮╰┼╭╮╨┐╰╰╓╯╮╯╮╯╯╛╮├╭╮╮╪╰╞╦╯╭╭╒╭╢┼═╰╟╮╭╰╩┘╖╭╰╩╰╭╰╛╨└╧╮╰┘╮╭╭╣╮╜╛╮╟╜═╞╯╕└╵╔╯╷╓╧╵╔╖╧┼╫║║╨┼╧╨┬┬═╣╦╤╝╓╥├╙╠╫╠
```
