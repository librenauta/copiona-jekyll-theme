---
title: Digitalidad Disminuida
author: Librenauta
description: digitalidad disminuida cada vez más.
proyect: f804d881-5340-465a-a445-0ae0e4853bde
image:
  path: public/b26197ae-03cf-47ce-9c7b-094f89c6f6fb/digitalidad-disminuida-1.jpg
  description: digitalidad disminuida
category: proyectos
tags:
- digitalidad
- simulación
- art
draft: false
order:
layout: post
uuid: b26197ae-03cf-47ce-9c7b-094f89c6f6fb
last_modified_at: 2022-11-21 22:20:21.155018278 +00:00
liquid: false
---

# Digitalidad disminuida

[dd.copiona.com](https://dd.copiona.com) | tenemos un mundo y nos esforzamos por hacer la simulación de otro | fanzine brr-art para mañana  | filmina+web | retromedia ?

![digitalidad-disminuida-2](public/b26197ae-03cf-47ce-9c7b-094f89c6f6fb/digitalidad-disminuida-2.png)

![digitalidad-disminuida-3](public/b26197ae-03cf-47ce-9c7b-094f89c6f6fb/digitalidad-disminuida-3.png)

digitalidad disminuida es un fanzine que podés leer siempre y cuando imprimas una [filimina](public/b26197ae-03cf-47ce-9c7b-094f89c6f6fb/digitalidad-disminuida.pdf) o me la compres en algúna feria y la apoyes sobre tu monitor al entrar en [dd.copiona.com](https://dd.copiona.com).

Este fanzine se trata de poder agregar una capa adicional en el vector que sale de la digitalidad para poder entenderla mejor. Para adentro de la computadoras las capas de abstracción cada vez son más complejas. Estuve pensando que en vez de querer aumentar nuestra realidad con esas capas de abstracción, que además siempre terminan siendo funcionales a nuestra propia explotación (caso de cascos de realidad aumentada para entrar a la ofician desde tu homeoffice), podemos pensar que la digitalidad esta disminuida y que necesita de nosotras para poder funcionar. apoyar una filmina en un monitor, para aumentar la digitalidad. 2 capas para construir un mensaje. un futuro.

<figure class="w-100">
  <video src="public/b26197ae-03cf-47ce-9c7b-094f89c6f6fb/digitalidad-disminuida.mp4" autoplay="autoplay" loop="loop" controls="controls"  >
  </video>
</figure>
