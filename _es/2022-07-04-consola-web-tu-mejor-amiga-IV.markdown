---
layout: post
title: 'Consola web tu mejor amiga IV [cont.ar]'
image:
  path: public/f57cbf86-841a-466b-8bbf-f4a263c6657b/1.png
category: licuadora
author: librenauta
tags:
  - cultura libre
  - videos libres
  - descargar video
uuid: f57cbf86-841a-466b-8bbf-f4a263c6657b
order: 22
draft: false
last_modified_at: 2022-07-04 22:30:21.155018278 +00:00
---

### _Consola web for fun and piracy_

## bajando capitulos de series en cont.ar

1. Vamos a [crear una cuenta](https://www.cont.ar/) en cont.ar para poder reproducir videos

2.  por supusto nos encontramos con esto al pulsar [f12]
![]()

3. vamos a la serie que queremos bajar y le damos play al video, apretamos [f12] para poder inspeccionar la página. en la serie de filtros del inspector pulsamos primero RED y luego XHR, para buscar las peticiones que tienen la lista de .ts
4. las url de cont.ar tienen esta forma ´https://www.cont.ar/watch/e88869eb-db0e-4bc3-88f8-b73f14f73e93´ que nos servira para poder buscar el json que necesitamos.
5. al buscar el ´uuid: e88869eb-db0e-4bc3-88f8-b73f14f73e93' en XHR encontramos un json con información de la serie y bloque que dice streams esto:

´´´
{
	"data": {
		"id": "e88869eb-db0e-4bc3-88f8-b73f14f73e93",
		"name": "#PASIÓN",
		"vgi": 2366,
		"length": "1732",
		"hms": "28:52",
		"synopsis": "Los videojuegos son una industria sin techo a nivel mundial. Desarrolladores independientes generan nuevos juegos y propuestas y algunos proyectos viajan en nuestro colectivo de videojuegos.",
		"posterImage": "https://gob-artwork.obs.sa-argentina-1.myhuaweicloud.com/content/v/f55595ce0480bc64ce8456ebe92d74d4.jpg",
		"streams": [
			{
				"video_id": 17608,
				"type": "HLS",
				"upload_path": "17608/20220622/",
				"manifest_name": "stream.m3u8",
				"url": "https://arsat.cont.ar/vod-contar-002/17608/20220622/stream.m3u8",
				"created_at": "2022-06-22T12:47:16.000Z",
				"updated_at": "2022-06-22T12:47:16.000Z"
			},
	}
}

´´´

6. creamos una carpeta que se llame serie con ´mkdir series/´ y ´cd series´ para hacer un ´wget https://arsat.cont.ar/vod-contar-002/17608/20220622/stream.m3u8´

7. en ese archivo tendremos algo asi:

´´´
#EXTM3U
#EXT-X-INDEPENDENT-SEGMENTS

#EXT-X-MEDIA:TYPE=AUDIO,GROUP-ID="audio",NAME="Español",LANGUAGE="Español",URI="audio_1_128000_variant.m3u8"

#EXT-X-STREAM-INF:BANDWIDTH=6042036,AVERAGE-BANDWIDTH=5233747,CODECS="avc1.640032,mp4a.40.2",RESOLUTION=1920x1080,AUDIO="audio",FRAME-RATE=25.0
video_1080p_4800000_variant.m3u8
#EXT-X-STREAM-INF:BANDWIDTH=4231220,AVERAGE-BANDWIDTH=2768661,CODECS="avc1.64001F,mp4a.40.2",RESOLUTION=1280x720,AUDIO="audio",FRAME-RATE=25.0
video_720p_2400000_variant.m3u8
#EXT-X-STREAM-INF:BANDWIDTH=2330164,AVERAGE-BANDWIDTH=1467409,CODECS="avc1.4D401E,mp4a.40.2",RESOLUTION=852x480,AUDIO="audio",FRAME-RATE=25.0
video_480p_1200000_variant.m3u8
#EXT-X-STREAM-INF:BANDWIDTH=1548084,AVERAGE-BANDWIDTH=1045591,CODECS="avc1.4D401E,mp4a.40.2",RESOLUTION=640x360,AUDIO="audio",FRAME-RATE=25.0
video_360p_800000_variant.m3u8
#EXT-X-STREAM-INF:BANDWIDTH=711860,AVERAGE-BANDWIDTH=611813,CODECS="avc1.42C015,mp4a.40.2",RESOLUTION=426x240,AUDIO="audio",FRAME-RATE=25.0
video_240p_400000_variant.m3u8

´´´
donde hay una URI con la lista .m3u8 de .ts para el audio y un .m3u8 para la RESOLUTION que elijamos. por ejemplo la primera de ´RESOLUTION=1920x1080´

8.



GO PIRATE!

~librenauta
