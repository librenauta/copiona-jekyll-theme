---
title: Quemar un libro 4 [ Mil Sonidos - Roberto Paci Daló y Emanuele Quinz ]
description: libro escaneado
title_page: ~/Quemar un libro a la luz de un scanner [ Mil Sonidos - Roberto Paci Daló y Emanuele Quinz ]
author: librenauta + rapo
image:
permalink:
category: scann
tags:
- scanner
- book
- libros
- quemar
- mil mesetas
draft:
order:
layout: post
uuid: 928f8f2d-4988-41eb-bd40-2f1fdd663fc0
liquid: false
last_modified_at: 2022-06-15 22:20:21.155018278 +00:00
---

## Mil Sonidos - Roberto Paci Daló y Emanuele Quinz

Una vez con [rapo](https://rapofran.com.ar/) hicimos un taller de mil mesetas pero enmarcado en nomadología,
un capítulo vinculado a la musica + deleuze y guattari.

a rapo no le fue suficiente entonces se topa con este libro y me dice: hay que escanearlo. cae a casa con un
budin y mientras mateamos escaneamos en loop mil sonidos. girar las paginas 156 veces.


![scann-2](public/928f8f2d-4988-41eb-bd40-2f1fdd663fc0/mil-sonidos-roberto-paci-daló-emanuele-quinz-1.jpg)

![scann-2](public/928f8f2d-4988-41eb-bd40-2f1fdd663fc0/mil-sonidos-roberto-paci-daló-emanuele-quinz-2.jpg)

ps psss [acá](pdf/mil-sonidos-roberto-paci-daló-emanuele-quinz.pdf) tienen para descargar Mil Sonidos - Roberto Paci Daló y Emanuele Quinz

_quemar en una luz para no perder el acceso_
