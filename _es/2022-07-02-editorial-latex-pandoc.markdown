---
title: Editorial, latex y piratas
author: Librenauta && piratas
description: ¿cómo tener una pequeña editorial gracias a amigas de internet?
category: proyectos
tags:
- fanzine
- latex
- editorial
draft: false
order:
layout: post
uuid: bcf63cf1-29ac-4bea-8ec9-b30072d9e9e6
last_modified_at: 2022-07-02 23:20:21.155018278 +00:00
liquid: false
---


## Post de cómo utilizar e instalar export-pdf en GNU/linux

[script export-pdf del perrotuerto, hice un backup en mi gitlab para que no muera](https://gitlab.com/-/snippets/2189719/raw/main/export-pdf-li) antes era [este snippets pero murió](https://gitlab.com/snippets/1917490/raw)

se puede instalar asi: 

1. pc con linux (debian, fedora o ubuntu) (o crear una máquina virtual con linux)
2. instalar "dependencias"= programas que necesita utilizar export-pdf para funcionar.

serían estos:
  1. pandoc `sudo dnf install pandoc`
  2. pdfbook2 `sudo dnf -y install texlive-pdfbook2)``
  3. lualatex `sudo dnf -y install texlive-luatex`

(un snippets es un script que se puede instalar en la pc y luego llamarlo desde la terminal)


# Instalación:

`sudo wget https://gitlab.com/snippets/2189719/raw /usr/local/bin && sudo mv /usr/local/bin/raw /usr/local/bin/export-pdf && sudo chmod +755 /usr/local/bin/export-pdf`

(esto se pega en una terminal y se ejecuta para poder instalar export-pdf)

 desglose de la instalación para entender las partes:

- `sudo` = en una shell , sudo es el programa que da privilegios para instalar cosas de forma permanente en el sistema.

- `wget` = wget es un programita hermoso que descarga cualquier url que pegues en una shell.

- `/usr/local/bin` = es una ubicación común de directorio (lalmadas carpetas en window$)

- `/usr/local/bin/export-pdf` = es la ubicación de nuestro script

- `sudo chmod +755 /usr/local/bin/export-pdf` = cambiar los permisos del script export-pdf para poder ejecutarlo como usuarie común. eso lo hace "chmod +775" si no cambiamos este permiso, solo podremos ejecutar el script export-pdf como superusuarie.

luego de instalarlo, entrar a la carpeta donde está nuestro texto.md y ejecutar:

 `export-pdf --title="titulo de mi libro" --author="autora de mi libro" --press="nombre de la editorial" --template="nombre del tempalte.tex"  --get-template --no-day texto.md`

 enter y genera el pdf :F

 toda esa linea va junta en un solo reglón, entonces se ejecuta el programa export-pdf que instalamos al principio, luego hay "inputs" que se llaman "flags" que son las que tienen `--algo`, esos imput utiliza export-pdf para construir el documento, a demás de requerir un template.tex y el archivo texto.md (el contenido con marcado markdown)

 como utiliza markdown para generar el documento, tan solo hay que agregar `![imagen](/imagen-1.png) y ubicar la imagen en el mismo directorio donde esta el .md <3`.

 recién leyendo le script vi esto:

```JSON
  puts "\nArgumentos:"
  puts "  --title=\"text\"      Título del texto"
  puts "  --author=\"text\"     Autor del texto"
  puts "  --press=\"text\"      Editorial del texto"
  puts "  --template=\"path\"   Plantilla para el documento"
  puts "  --bib=\"path\"        Bibliografía para el documento"
  puts "  --ragged-right      Justificación en bandera"
  puts "  --just-tex          Solo genera los archivos TeX"
  puts "  --leave-h1          Respeta los h1 del documento"
  puts "  --numbered          Numeración en partes y capítulos"
  puts "  --get-template      Genera la plantilla por defecto"
  puts "  --en-date           Genera fechas en formato inglosajón"
  puts "  --no-day            No incluye el día en la fecha"
  puts "  --tit-head          Sustituye al autor por el título en la cornisa"
  puts "  --imposition        Genera un archivo con imposición"
  puts "  --geometry          Geometría del documento: ancho de página, alto de página, margen interior y margen exterior; por defecto ['5.5in', '8.5in', '.75in', '1.25in']"
  puts "  -h | --help         Muestra esta ayuda
```

esos son los imputs que acepta el script, osea las "flags" -- :D
 `--get-template`      Genera la plantilla por defecto
		       es una flag importante al principio, para generar el primer tempalte.tex, luego ese mismo se puede copiar y acomodar a formatos y lo que quieras

 `--imposition` es la flag para imposición, (osea imprimir y doblar a la mitad a modo de fanzine) [creo que esto es nuevo]

  `--geometry` hay una geometria por defecto, pero se puede buscar los formatos que estén disponibles en tu país y ponerlo en pulgadas, ese "in" es de inch= pulgadas
  Acá en arg, las hojas vienen bajo norma iram, A0, A1, A2, A3, A4 (la de impresión hogareña)


En esta web [tug.org/FontCatalogue](https://www.tug.org/FontCatalogue/) hay ejemplos de como cargar las tipografias que queremos. El .tex que genera export-pdf viene con esta sentencia en su inicio:
`\usepackage[osf]{Alegreya}`

de acuerdo a la tipografia que elijamos sera diferente como llamarla, ej.:

[esta tipografia que estoy usando](https://www.tug.org/FontCatalogue/crimsonproextralight/)
```
\usepackage[extralight]{CrimsonPro}
\usepackage[T1]{fontenc}
%% The font package uses mweights.sty which has som issues with the
%% \normalfont command. The following two lines fixes this issue.
\let\oldnormalfont\normalfont
\def\normalfont{\oldnormalfont\mdseries}
\normalfont
```
en latex se comenta de esta forma %% :]

agregando  `\usepackage{CJKutf8}` importas CJKutf8 para poder visualizar caracteres en chino, japones y koreano.[fuente](https://es.overleaf.com/learn/latex/Chinese)
