---
title: En defensa de la imagen pobre
author: Hito Steyerl
description: En defensa de la imagen pobre
proyect: f804d881-5340-465a-a445-0ae0e4853bde
image:
  path: editorial/en-defensa-de-la-imagen-pobre/en-defensa-de-la-imagen-pobre.png
  description: la-pradera.
pdf_read: editorial/en-defensa-de-la-imagen-pobre/en-defensa-de-la-imagen-pobre.pdf
pdf_cover: editorial/en-defensa-de-la-imagen-pobre/en-defensa-de-la-imagen-pobre-cover.pdf
pdf_imposition: editorial/en-defensa-de-la-imagen-pobre/en-defensa-de-la-imagen-pobre_imposition.pdf
category: editorial
export_pdf: 'export-pdf --title="En Defensa de la Imagen Pobre" --author="Hito Steyerl" --press="RMXS" --template="a6template.tex" --imposition en-defensa-de-la-imagen-pobre.md'
tags:
  - imagen
  - tecnología
  - cine
  - fotografia
draft: false
order:
layout: book
uuid: a97959fa-348c-4810-a8ac-a9c58f5d3cde
liquid: false
---

# EN DEFENSA DE LA IMAGEN POBRE

## Hito Steyerl


 > Revista e-flux. Journal #10. Noviembre 2009. Traducido por Diego Maureira y editado por [Departamento de Estudio de los Medios.](https://www.dem.cl/traduccion/25) con ajustes de @librenauta e imágenes del post original


La imagen pobre es una copia en movimiento. Su calidad es mala, su resolución deficiente. Se deteriora conforme acelera. Es el fantasma de una imagen, una vista previa, una miniatura, una idea errante, una imagen itinerante distribuida de forma gratuita, exprimida a través de lentas conexiones digitales, comprimida, reproducida, extraída, remezclada, así como copiada y pegada en otros canales de distribución.

Una imagen pobre es un desgaste o una extracción; un AVI o un JPEG, una lumpen proletaria en la sociedad de clases de las apariencias, clasificada y valorada de acuerdo a su resolución. La imagen pobre ha sido subida, descargada, compartida, formateada y reeditada. Esta transforma la calidad en accesibilidad, el valor exhibitivo en valor de culto, las películas en clips, la contemplación en distracción. La imagen es liberada de las catacumbas de los cines y los archivos y arrojada a la incertidumbre digital, a expensas de su propia sustancia. La imagen pobre tiende a la abstracción: es una idea visual en su propio devenir.

La imagen pobre es una bastarda ilícita de quinta generación de una imagen original. Su genealogía es dudosa. Sus nombres de archivo están deliberadamente mal escritos. A menudo desafía el patrimonio, la cultura nacional o incluso los derechos de autorx. Es transmitida como un señuelo, una carnada, un índice o como un recuerdo de su propio origen visual. Se burla de las promesas de la tecnología digital. No solo es degradada al punto de ser una mancha errática, sino que incluso pone en duda si realmente podría ser llamada imagen. Solo la tecnología digital podría producir tan deteriorado tipo de imagen en primer lugar.

Las imágenes pobres son las Condenadas de la Pantalla contemporáneas, los escombros de la producción audiovisual, la basura que queda en las costas de las economías digitales. Ellas testifican la dislocación, la transferencia y el desplazamiento violento de las imágenes, su aceleración y circulación dentro del círculo vicioso del capitalismo audiovisual. Las imágenes pobres son arrastradas por el mundo como mercancías o efigies de éstas, como regalos o como recompensas. Propagan placer o amenazas de muerte, teorías conspirativas o contrabando, resistencia o paralización. Las imágenes pobres muestran lo extraño, lo obvio y lo increíble, mientras seamos aún capaces de descifrarlas.

![](editorial/en-defensa-de-la-imagen-pobre/1.png)

> Shoveling pirated DVDs in Taiyuan, Shanxi province, China, April 20, 2008. From here.

### Bajas resoluciones

En una película de Woody Allen, el personaje principal está fuera de foco.[^1]
[^1]: _Deconstructing Harry,_ dirigida por Woody Allen (1997).

No es un problema técnico sino una suerte de enfermedad que la ha dado: su imagen es consistentemente borrosa. Debido a que el personaje de Allen es un actor, esto se transforma en un problema mayor: es incapaz de encontrar trabajo. Su falta de definición se transforma en un problema material. Estar enfocado se identifica como una posición de clase, una posición de facilidad y privilegio, mientras que estar fuera de foco reduce su valor como imagen.

Sin embargo, la jerarquía contemporánea de las imágenes no solo está basada en la nitidez, sino también –y primeramente– en la resolución. Basta con mirar cualquier tienda electrónica y este sistema, descrito por Harun Farocki en una notable entrevista del 2007, se vuelve inmediatamente evidente.[^2]

[^2]: “Wer Gemälde wirklich sehen will, geht ja schließlich auch ins Museum”, _Frankfurter Allgemeine Zeitung,_ 14 de Junio, 2007. Conversación entre Harun Farocki y Alexander Horwath.

En la sociedad de clases de las imágenes, el cine toma el papel de las tiendas más importantes de la cadena. En las principales tiendas, los productos de lujo son comercializados en un entorno exclusivo. Los derivados más accesibles de las mismas imágenes circulan como DVDs, en televisión o en línea, como imágenes pobres.

Obviamente, una imagen de alta resolución luce más brillante e impresionante, más mimética y mágica, más aterradora y seductora, que una imagen pobre. Es más rica, por decirlo de algún modo. Ahora, incluso los formatos de consumo se están adaptando cada vez más al gusto de las cineastas y estetas, quienes insistieron en el 35 mm como garantía prístina de visualidad. La insistencia sobre la película análoga como único medio de importancia visual resonó en los discursos sobre cine, casi sin prestar atención a su inflexión ideológica. Nunca importó que aquellas economías de lujo de producción cinematográfica estuvieran (y aún estén) firmemente ancladas a sistemas de cultura nacional, a estudios de producción capitalistas, al culto de genios masculinos y la versión original, y que por tanto a menudo sean conservadoras en su estructura misma. La resolución fue fetichizada como si su ausencia significara la castración del autor. El culto de la película estándar dominó incluso la producción cinematográfica independiente. La imagen rica estableció su propio conjunto de jerarquías, con nuevas tecnologías ofreciendo más y más posibilidades para degradarla creativamente.

![](editorial/en-defensa-de-la-imagen-pobre/2.png)

> Nine 35mm film frames from Stan Brakhage’s Existence is Song, 1987.

### Resurrección (como Imagen Pobre)

Pero insistir en las imágenes ricas tuvo también consecuencias más serias. Un ponencista en una reciente conferencia de ensayos sobre cine rechazó mostrar clips de una pieza de Humphrey Jennings porque no había ninguna proyección cinematográfica apropiada disponible. Aunque había disponible un reproductor de DVD estándar y un proyector de video, el público tuvo que imaginar cómo eran esas imágenes.

En este caso la invisibilidad de la imagen fue más o menos voluntaria y basada en premisas estéticas. Pero tiene un equivalente mucho más general basado en las consecuencias de las políticas neoliberales. Hace veinte o incluso treinta años, la reestructuración neoliberal de la producción medial o mediática comenzó a oscurecer lentamente los imaginarios no comerciales, al punto que el cine experimental y ensayístico se volvió casi invisible. A medida que se volvió prohibitivamente costoso mantener aquellas obras circulando en los cines, estas fueron consideradas a su vez demasiado marginales para ser transmitidas por televisión. De este modo desaparecieron lentamente no solo de los cines, sino también de la esfera pública. Los ensayos videográficos y las películas experimentales se mantuvieron desconocidas para la mayoría, salvo algunas raras proyecciones en museos o clubes de cine metropolitanos, presentadas en su resolución original, antes de desaparecer nuevamente en la oscuridad del archivo.

Por supuesto, este desarrollo estuvo conectado a la radicalización neoliberal del concepto de cultura como mercancía, la comercialización del cine, su dispersión en multicines y la marginación de las producciones de cine independiente. También estuvo relacionado con la reestructuración de la industria global medial y el establecimiento de monopolios de audiovisualidad en ciertos países o territorios. De esta manera, el material visual de resistencia o no conformista desapareció de lo visible hacia un espacio subterráneo de archivos y colecciones alternativas, mantenido vivo solo por una red de organizaciones o individuos comprometidos, que hacían circular entre ellos copias de VHS de contrabando. Las fuentes de estos eran extremadamente extrañas: las cintas se movían de mano en mano, dependientes del boca-a-boca, dentro de círculos de amigas y colegas. Con la posibilidad de transmitir videos en línea, esta condición sufrió un drástico cambio. Un número creciente de material extraño comenzó a reaparecer en plataformas de acceso público, algunas de ellas cuidadosamente curadas (Ubuweb) y otras solo como un montón de material (Youtube).

Actualmente, hay al menos veinte torrents de ensayos cinematográficos de Chris Marker disponibles en línea. Si quieres una retrospectiva, puedes tenerla. Pero la economía de las imágenes pobres es más que solo descargas: puedes guardar los archivos, verlos de nuevo, incluso reeditarlos o mejorarlos si piensas que es necesario. Y los resultados circulan. Archivos AVI borrosos de obras maestras casi olvidadas son intercambiados en plataformas P2P semi-secretas. Videos clandestinos de celular, tomados de forma ilegal en museos, son transmitidos en Youtube. Copias de DVDs tomadas por artistas son intercambiadas [^3].

[^3]:  El excelente texto de Sven Lütticken “Viewing Copies: On the Mobility of Moving Images”, en _e-flux journal,_ no. 8 (Mayo de 2009), llamó mi atención sobre este aspecto de las imágenes pobres.

Muchos trabajos de cine de vanguardia, ensayístico y no comercial han sido resucitados como imágenes pobres. Les gusto o no.

![](editorial/en-defensa-de-la-imagen-pobre/3.png)

### Privatización y piratería

Que versiones raras de cine militante, experimental y clásico, así como el videoarte reaparezcan como imagen pobre, es significativo en otro nivel. Su situación revela mucho más que el contenido o la apariencia de las propias imágenes: revela también las condiciones de su marginación, la constelación de fuerzas sociales que las conduce a su circulación en línea como imágenes pobres.[^4]

[^4]:  Agradezco a Kodwo Eshun por señalar este punto.

Las imágenes pobres son pobres porque no tienen asignado ningún valor dentro de la sociedad de clases de las imágenes. Su estatus como ilícitas o degradadas les concede libertad sobre esos criterios. Su falta de resolución da cuenta de su apropiación y desplazamiento.[^5]

[^5]: Por supuesto, en algunos casos las imágenes de baja resolución también aparecen en los entornos de los principales medios de comunicación (principalmente noticias). Donde son asociados con la urgencia, la inmediatez y la catástrofe –y son extremadamente valiosas–. Ver Hito Steyerl, “Documentary Uncertainty,” _A Prior_ 15 (2007).

Obviamente, esta condición no solo está relacionada con la reestructuración neoliberal de la producción medial y la tecnología digital; también tiene que ver con la reestructuración postsocialista y postcolonialista de los Estados nacionales, su cultura y sus archivos. Mientras algunos Estados nación son desmantelados o desarticulados, nuevas culturas y tradiciones son inventadas y nuevas historias son creadas. Obviamente, esto también afecta los archivos de películas. En muchos casos, todo un patrimonio de copias de películas se queda sin su marco de apoyo de la cultura nacional. Como observé una vez en el caso de un museo de cine en Sarajevo, el archivo nacional podía encontrar su siguiente vida en la forma de una tienda de videos para arrendar.[^6]

[^6]: Hito Steyerl, “Politics of the Archive: Translations in Film”, _Transversal_ (Marzo de 2008).

Las copias piratas se filtran de tales archivos debido a la privatización desorganizada. Por otro lado, incluso la Biblioteca Británica vende sus contenidos en línea a precios astronómicos.

Como ha señalado Kodwo Eshun, las imágenes pobres circulan en parte en el vacío dejado por las organizaciones estatales de cine, a las cuales les resulta demasiado difícil operar como archivos de 16/35 mm o mantener cualquier tipo de infraestructura de distribución en la era contemporánea.[^7]

[^7]: De la correspondencia con el autor vía e-mail.

Desde esta perspectiva, las imágenes pobres revelan el declive o la degradación del ensayo cinematográfico, o incluso cualquier cine experimental o no comercial, que en muchos lugares fue posible porque la producción cultural era considerada una tarea del Estado. La privatización de la producción medial se hizo gradualmente más importante que la producción controlada/patrocinada por el Estado. Pero, por otro lado, la privatización desenfrenada del contenido intelectual, junto con la mercantilización y el comercio en línea, permite a la vez la piratería y la apropiación; esto incrementa la circulación de imágenes pobres.



![](editorial/en-defensa-de-la-imagen-pobre/4.png)

![](editorial/en-defensa-de-la-imagen-pobre/5.png)

> Chris Marker’s virtual home on Second Life, May 29, 2009.

### Cine imperfecto

La emergencia de las imágenes pobres recuerda un manifiesto clásico del Tercer Cine, Por un cine imperfecto, de Julio García Espinosa, escrito en Cuba a finales de los 60.[^8]

[^8]: Julio García Espinosa, “For an Imperfect Cinema,” Traducción: Julianne Burton. _Jump Cut_, no. 20 (1979): 24–26.

Espinosa defiende un cine imperfecto porque, en sus palabras, «el cine perfecto –técnica y artísticamente pulido– es la mayoría de las veces un cine reaccionario». El cine imperfecto se esfuerza por superar la división del trabajo dentro de la sociedad de clases. Une el arte con la vida y la ciencia, borroneando la distinción entre productor y consumidor, autor y espectador. Insiste sobre su propia imperfección, es popular pero no consumista, comprometido sin volverse burocrático.

En su manifiesto, Espinosa también reflexiona sobre la promesa de los nuevos medios. Predice de manera explícita que el desarrollo de la tecnología del video amenazaría la posición elitista de los cineastas tradicionales y permitiría un tipo de film de las masas: un arte de la gente. Al igual que la economía de las imágenes pobres, el cine imperfecto disminuye la distinción entre autor y espectadores, y une arte y vida. Sobre todo su visualidad está resueltamente comprometida: borrosa, casera y llena de artilugios.

De alguna manera, la economía de las imágenes pobres se corresponde con la descripción del cine imperfecto, mientras que la descripción del cine perfecto representa el concepto de cine como “tienda insignia” (flagship store). Pero el cine imperfecto real y contemporáneo es mucho más ambivalente y efectivo de lo que Espinosa ha anticipado. Por un lado, la economía de las imágenes pobres, con su posibilidad inmediata de distribución universal y sus éticas de la mezcla y la apropiación, permite la participación de un grupo más grande que nunca de productores. Pero esto no significa que estas oportunidades solo sean usadas para fines progresistas. Discursos de odio, spam y otro tipo de basura también lo hacen a través de la red digital. La comunicación digital se ha vuelto, a su vez, uno de los mercados más disputados. Una zona que durante mucho tiempo ha estado sujeta a una verdadera marcha de acumulación y a un intento masivo (y, hasta cierto punto exitoso) de privatización.

La red en la que circulan las imágenes pobres constituye así tanto una plataforma para un nuevo frágil interés común como un campo de batalla para el comercio y las agendas nacionales. Ellos cuentan con material artístico y experimental, pero también con una increíble cantidad de porno y paranoia. Mientras el territorio de las imágenes pobres permite acceder a imaginarios excluidos, estos también son permeados por las más avanzadas técnicas de mercantilización. Mientras permite a las usuarias una activa participación en la creación y distribución de contenido, también las integra en la producción. Las usuarias se convierten en editoras, críticas, traductoras y co-autoras de las imágenes pobres.

Las imágenes pobres son, en consecuencia, imágenes populares: imágenes que pueden ser hechas y vistas por muchos. Ellas expresan todas las contradicciones de las masas contemporáneas: su oportunismo, su narcisismo, su deseo de autonomía y creación, su incapacidad de enfocarse y de tomar decisiones, su constante disposición a la transgresión y, simultáneamente, a la sumisión.[^9]

[^9]:  Ver Paolo Virno, _A Grammar of the Multitude: For an Analysis of Contemporary Forms of Life_ (Cambridge, MA: MIT Press, 2004).

En general, las imágenes pobres presentan un retrato de la condición afectiva de las masas, su neurosis, su paranoia y sus miedos, tanto como su anhelo de intensidad, diversión y distracción. La condición de las imágenes no solo habla de incontables transferencias y reformateos, sino también de un sinnúmero de personas que se preocuparon de convertirlas una y otra vez, subtitularlas, re-editarlas o subirlas.

A la luz de esto, tal vez hay que redefinir el valor de la imagen, o más precisamente, crear una nueva perspectiva para ésta. Además de la resolución y el valor de intercambio, uno podría imaginar otra forma de valor determinado por la velocidad, intensidad y alcance. Las imágenes pobres son pobres porque están intensamente comprimidas y viajan a gran velocidad. Estas pierden materia pero ganan rapidez. Pero también expresan un estado de desmaterialización, compartida no solo con el legado del arte conceptual sino sobre todo con los modos contemporáneos de la producción semiótica[^10].

[^10]:  Ver Alex Alberro, _Conceptual Art and the Politics of Publicity_ (Cambridge, MA: MIT Press, 2003).

El cambio de la semiótica del capital, como describió Felix Guattari[^11]

[^11]: Ver Félix Guattari, “Capital as the Integral of Power Formations,” En _Soft Subversions_ (New York: Semiotext(e), 1996), 202.

, juega a favor de la creación y diseminación de paquetes de información comprimida y flexible que puede ser integrada en secuencias y combinaciones cada vez más nuevas[^12].

[^12]: Todos estos desarrollos son discutidos en detalle en un excelente texto de Simon Sheikh, “Objects of Study or Commodification of Knowledge? Remarks on Artistic Research”, _Art & Research 2,_ no. 2 (Primavera de 2009).


![](editorial/en-defensa-de-la-imagen-pobre/6.png)

> Thomas Ruff, jpeg rl104, 2007.


Este aplanamiento del contenido visual –el concepto en conversión de las imágenes– las ubica dentro de un giro informático general, dentro de economías del conocimiento que las arranca junto a sus leyendas fuera de contexto, en un torbellino de permanente desterritorialización capitalista[^13].

[^13]: Ver también Alan Sekula, “Reading an Archive: Photography between Labour and Capital”, en _Visual Culture: The Reader,_ ed. Stuart Hall and Jessica Evans (London/New York: Routledge 1999), 181–192.

La historia del arte conceptual describe la desmaterialización del arte objetual primero como un movimiento de resistencia contra el valor fetiche de la visibilidad. Sin embargo, la desmaterialización del arte objetual se adapta perfectamente a la semiótica del capital, y por tanto, al giro conceptual del capitalismo[^14].

[^14]: Ver Alberro, _Conceptual Art and the Politics of Publicity._

De cierta manera, la imagen pobre está sujeta a una tensión similar. Por un lado, opera contra el valor fetiche de la alta resolución. Por otro lado, termina siendo integrada dentro de la información que prospera en comprimidos espacios capitalistas de atención, basándose en impresión en lugar de inmersión, en intensidad en lugar de contemplación, en vistas previas en lugar de proyecciones.

![](editorial/en-defensa-de-la-imagen-pobre/7.png)

> Imagen cortesía de Goodbye Emmanuel

### Compañera, ¿cuál es tu vínculo visual hoy?

Pero, simultáneamente, ocurre una paradójica inversión. La circulación de las imágenes pobres crea un circuito que completa las ambiciones originales del cine militante, y (de algunos) cines ensayísticos y experimentales. Crea una economía de imágenes alternativas, un cine imperfecto que existe dentro, tanto como más allá y por debajo, de los medios comerciales de transmisión. En la era de los datos compartidos, incluso el contenido marginado vuelve a circular y reconecta audiencias dispersas  a nivel global.

La imagen pobre, por tanto, construye redes globales anónimas, así como crea una historia compartida. A medida que viaja construye alianzas, provoca traducciones adecuadas e incorrectas, y crea nuevos públicos y debates. Al perder su sustancia visual recobra algo de su impacto político y crea un nuevo aura en torno a éste. Este aura ya no está basado en la permanencia del original sino en la transitoriedad de la copia. Ya no está anclado a una esfera pública clásica, mediada y apoyada por el marco del Estado nación o las corporaciones, sino que flota en la superficie de grupos de datos dudosos y temporales.  Al alejarse de las bóvedas del cine, es impulsada hacia nuevas y efímeras pantallas unidas por los deseos de espectadoras dispersas.

La circulación de las imágenes pobres crea, como los llamó Dziga Vertov, “enlaces visuales”[^15].

[^15]: Dziga Vertov, “Kinopravda and Radiopravda”, en _Kino-Eye: The Writings of Dziga Vertov,_ ed. Annette Michelson (Berkeley: University of California Press, 1995), 52.

De acuerdo a Vertov, el “enlace visual” suponía unir a las trabajadoras del mundo unas con otras[^16]

[^16]: Vertov, “Kinopravda and Radiopravda,” 52.

. Vertov imaginó un tipo de lenguaje comunista, visual y adámico que podría no solo informar o entretener, sino también organizar a sus espectadores. En cierto sentido, su sueño se ha vuelto realidad, aunque en su mayoría bajo las reglas de un capitalismo global de la información, cuyas audiencias están ligadas casi en un sentido físico por el entusiasmo mutuo, la sintonía afectiva y la ansiedad.

Pero también existe la circulación y producción de imágenes pobres basadas en cámaras de celular, computadores domésticos y formas inusuales de distribución. Sus conexiones ópticas –edición colectiva, archivos compartidos o circuitos de distribución de base– revelan enlaces erráticos y coincidentes entre productoras de distintos lugares, las que constituyen simultáneamente audiencias dispersas.

La circulación de imágenes pobres alimenta las líneas de ensamblaje de los medios capitalistas, al igual que las economías audiovisuales alternativas. Además de la mucha confusión y estupefacción, también es posible que cree movimientos disruptivos de pensamiento y afecto. La circulación de las imágenes pobres, en consecuencia, inicia otro capítulo en la genealogía histórica de los circuitos de información no conformistas: los “enlaces visuales” de Vertov, las pedagogías obreras internacionalistas que Peter Weiss describió en La estética de la resistencia, los circuitos del Tercer Cine y Tricontinentalismo, de creación cinematográfica y pensamiento no alienados. Por tanto, la imagen pobre –ambivalente como su estatus– toma su lugar en la genealogía de folletos copiados en carbono, el cine agit-prop, revistas underground de video y otros materiales no conformistas, los que estéticamente solían usar materiales pobres. Además, actualiza muchas de las ideas históricamente asociadas con estos circuitos, entre otras las ideas de Vertov sobre enlace visual.

Imaginen alguien del pasado con una boina preguntándoles: Camarada, ¿cuál es tu vínculo visual hoy? Ustedes podrían responder: es este enlace con el presente.

### ¡Ahora!

La imagen pobre encarna la trascendencia de muchas ex obras maestras de cine y videoarte. Ha sido expulsada del paraíso protegido en el que el cine parece haber estado alguna vez[^17].


[^17]: Al menos desde la perspectiva del engaño nostálgico.

Después de ser expulsado de la protegida y generalmente proteccionista área de la cultura nacional, descartado de la circulación comercial, estos trabajos se han vuelto viajeros en una tierra de nadie digital, cambiando constantemente su formato y resolución, velocidad y medios, a veces perdiendo incluso nombres y créditos en el camino.

Hoy muchos de estos trabajos han vuelto, como imágenes pobres, lo admito. Se podría argumentar, por supuesto, que esto no es el producto real, pero entonces –por favor– muéstrenme cuál es el real.

La imagen pobre no tienen que ver con el producto real, el auténtico original. En su lugar, tiene que ver con sus propias condiciones reales de existencia: sobre la circulación en enjambres, la dispersión digital, las temporalidades flexibles y fracturadas. Se trata del desafío y de la apropiación, al igual que del conformismo y la explotación.

En resumen: es acerca de la realidad.
