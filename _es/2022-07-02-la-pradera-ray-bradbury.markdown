---
title: La pradera
author: Ray Bradbury
description: sci-fi
proyect: f804d881-5340-465a-a445-0ae0e4853bde
image:
  path: editorial/la-pradera-ray-bradbury/la-pradera-cover.png
  description: la-pradera.
pdf_read: editorial/la-pradera-ray-bradbury/la-pradera.pdf
pdf_cover: editorial/la-pradera-ray-bradbury/la-pradera-cover.pdf
pdf_imposition: editorial/la-pradera-ray-bradbury/la-pradera_imposition.pdf
category: editorial
export_pdf: 'export-pdf --title="La Pradera" --author="Ray Bradbury" --press="pixel2pixel" --template="a6template.tex" --no-day La-pradera.md'
tags:
- Ray Bradbury
- la pradera
- Libro
draft:
order:
layout: book
uuid: 5275e5b9-5377-4881-84cf-6f9b183d829c
liquid: false
---
George, me gustaría que mirases el cuarto de los niños.
-¿Qué pasa?
-No sé.
-¿Entonces?
-Sólo quiero que mires, nada más, o que llames a un psiquiatra.
-¿Qué puede hacer un psiquiatra en el cuarto de los niños?
-Lo sabes muy bien. La mujer se detuvo en medio de la cocina y observó la estufa que se cantaba a sí misma, preparando una cena para cuatro.
-Algo ha cambiado en el cuarto de los niños
-dijo.
-Bueno, vamos a ver.

Descendieron al vestíbulo de la casa de la Vida Feliz, la casa a prueba de ruidos que les había costado treinta mil dólares, la casa que los vestía, los alimentaba, los acunaba de noche, y jugaba y cantaba, y era buena con ellos. El ruido de los pasos hizo funcionar un oculto dispositivo y la luz se encendió en el cuarto de los juegos, aún antes que llegaran a él. De un modo similar, ante ellos, detrás, las luces fueron encendiéndose y apagándose automáticamente, suavemente, a lo largo del vestíbulo.

¿Y bien?-dijo George .Hadley.

La pareja se detuvo en el piso cubierto de hierbas. El cuarto de los niños medía doce metros de ancho, por doce de largo, por diez de alto. Les había costado tanto como el resto de la casa.

Pero nada es demasiado para los niños- comentaba George.

El cuarto, de muros desnudos y de dos dimensiones, estaba en silencio, desierto como el claro de una selva bajo la alta luz del sol. Alrededor de las figuras erguidas de George y Lydia Hadley, las paredes ronronearon, dulcemente, y dejaron ver unas claras lejanías, y apareció una pradera africana en tres dimensiones, una pradera completa con sus guijarros diminutos y sus briznas de paja. Y sobre George y Lydia, el cielo raso, se convirtió en un cielo muy azul, con un sol amarillo y ardiente. George Hadley sintió que unas gotas de sudor le corrían por la cara.
-Alejémonos de este sol – dijo-. Es demasiado real, quizá. Pero no veo nada malo.

De los odorófonos ocultos salió un viento oloroso que bañó a George y Lydia, de pie entre las hierbas tostadas por el sol. El olor de las plantas selváticas, el olor verde y fresco de los charcos ocultos, el olor intenso y acre de los animales, el olor del polvo como un rojo pimentón en el aire cálido... Y luego los sonidos: el golpear de los cascos de lejanos antílopes en el suelo de hierbas; las alas de los buitres, como papeles crujientes... Una sombra atravesó la luz del cielo. La sombra tembló sobre la cabeza erguida y sudorosa de George Hadley.

-¡Qué animales desagradables!-oyó que decía su mujer.

-Buitres.

-Mira, allá lejos están los leones. Van en busca de agua. Acaban de comer-dijo Lydia-. No sé qué

-Algún animal.-George Hadley abrió la mano para protegerse de la luz que le hería los ojos entornados-. Una cebra, o quizá la cría de una jirafa.
-¿Estás seguro?-dijo su mujer nerviosamente. George parecía divertido.

-No. Es un poco tarde para saberlo. Sólo quedan unos huesos, y los buitres alrededor.

-¿Oíste ese grito?-preguntó la mujer.

-No.

-Hace un instante.

-No, lo siento.

Los leones se acercaban. Y George Hadley volvió a admirar al genio mecánico que había concebido este cuarto. Un milagro de eficiencia, y a un precio ridículo. Todas las casas debían tener un cuarto semejante. Oh, a veces uno se asusta ante tanta precisión, uno se sorprende y se estremece; pero la mayor parte de los días ¡qué diversión para todos, no sólo para los hijos, sino también para uno mismo, cuando se desea hacer una rápida excursión a tierras extrañas, cuando se desea un cambio de aire! Pues bien, aquí estaba África. Y aquí estaban los leones ahora, a una media docena de pasos, tan reales, tan febril y asombrosamente reales, que la mano sentía, casi, la aspereza de la piel, y la boca se llenaba del olor a cortinas polvorientas de las tibias melenas. El color amarillo de las pieles era como el amarillo de un delicado tapiz de Francia, y ese amarillo se confundía con el amarillo de los pastos. En el mediodía silencioso se oía el sonido de los pulmones de fieltro de los leones, y de las fauces anhelantes y húmedas salía un olor de carne fresca. Los leones miraron a George y a Lydia con ojos terribles, verdes y amarillos.

-¡Cuidado!-gritó Lydia.

Los leones corrieron hacia ellos. Lydia dio un salto y corrió, George la siguió instintivamente. Afuera, en el vestíbulo, después de haber cerrado ruidosamente la puerta, George se rió y Lydia se echó a llorar, y los dos se miraron asombrados.

-¡George!

-¡Lydia! ¡Mi pobre y querida Lydia!

-¡Casi nos alcanzan!

-Paredes, Lydia; recuérdalo. Paredes de cristal. Eso son los leones. Oh, parecen reales, lo admito. África en casa. Pero es sólo una película suprasensible en tres dimensiones, y otra película detrás de los muros de cristal que registra las ondas mentales. Sólo odorófonos y altoparlantes, Lydia. Toma, aquí tienes mi pañuelo.

-Estoy asustada.-Lydia se acercó a su marido, se apretó contra él y exclamó-: ¿Has visto? ¿Has sentido ¡Es demasiado real!

-Escucha, Lydia ...

-Tienes que decirles a Wendy y Peter que no lean más sobre África.

-Por supuesto, por supuesto-le dijo George, y la acarició suavemente.

-¿Me lo prometes?

-Te lo prometo.

-Y cierra el cuarto unos días. Hasta que me tranquilice.

-Será difícil, a causa de Peter. Ya sabes. Cuando lo castigué hace un mes y cerré el cuarto unas horas, tuvo una pataleta. Y lo mismo Wendy. Viven para el cuarto.

-Hay que cerrarlo. No hay otro remedio.

-Muy bien.-George cerró con llave, desanimadamente-. Has trabajado mucho. Necesitas un descanso.

-No sé ... no sé-dijo Lydia, sonándose la nariz. Se sentó en una silla que en seguida empezó a hamacarse, consolándola-. No tengo, quizá, bastante trabajo. Me sobra tiempo y me pongo a pensar. ¿Por qué no cerramos la casa, sólo unos días, y nos vamos de vacaciones?

-Pero qué, ¿quieres freírme tú misma unos huevos? Lydia asintió con un movimiento de cabeza.

-Sí.

-¿Y remendarme los calcetines?

-Sí-dijo Lydia con los ojos húmedos, moviendo afirmativamente la cabeza.

-¿Y barrer la casa?

-Sí, sí. Oh, sí.

-Pero yo creía que habíamos comprado esta casa para no hacer nada.

-Eso es, exactamente. Nada es mío aquí. Esta casa es una esposa, una madre y una niñera. ¿Puedo competir con unos leones? ¿Puedo bañar a los niños con la misma rapidez y eficacia que la bañera automática? No puedo. Y no se trata sólo de mí. También de ti. Desde hace un tiempo estás terriblemente nervioso.

-Quizá fumo demasiado.

-Parece como si no supieras qué hacer cuando estás en casa. Fumas un poco más cada mañana, y bebes un poco más cada tarde, y necesitas más sedantes cada noche. Comienzas, tú también, a sentirte inútil.

-¿Te parece? George pensó un momento, tratando de ver dentro de sí mismo.

-¡Oh, George!-Lydia miró, por encima del hombro de su marido, la puerta del cuarto-. Esos leones no pueden salir de ahí, ¿no es cierto?
George miró y vio que la puerta se estremecía, como si algo la hubiese golpeado desde dentro.

-Claro que no-dijo George.

Comieron solos. Wendy y Peter estaban en un parque de diversiones de material plástico, en el otro extremo de la ciudad, y habían televisado para decir que llegarían tarde, que empezaran a comer. George Hadley contemplaba, pensativo, la mesa de donde surgían mecánicamente los platos de comida

-Olvidamos la salsa de tomate-dijo.

-Perdón-exclamó una vocecita en el interior de la mesa, y apareció la salsa.

Podríamos cerrar el cuarto unos pocos días, pensaba George. No les haría ningún daño. No era bueno abusar. Y era evidente que los niños habían abusado un poco de África. Ese sol. Aún lo sentía en el cuello como una garra caliente. Y los leones. Y el olor de la sangre. Era notable, de veras. Las paredes recogían las sensaciones telepáticas de los niños y creaban lo necesario para satisfacer todos los deseos. Los niños pensaban en leones y aparecían leones. Los niños pensaban en cebras, y aparecían cebras. En el sol, y había sol. En jirafas, y había jirafas. En la muerte, y había muerte.

Esto último. George masticó, sin saborearla, la carne que la mesa acababa de cortar. Pensaban en la muerte. Wendy y Peter eran muy jóvenes para pensar en la muerte. Oh, no. Nunca se es demasiado joven, de veras. Tan pronto como se sabe qué es la muerte, ya se la desea uno a alguien. A los dos años ya se mata a la gente con una pistola de aire comprimido. Pero esto... Esta pradera africana, interminable y tórrida... y esa muerte espantosa entre las fauces de un león. Una vez, y otra vez...

-¿A dónde vas?-preguntó Lydia.

George no contestó. Dejó, preocupado, que las luces se encendieran suavemente ante él, que se apagaran detrás, y se dirigió lentamente hacia el cuarto de los niños. Escuchó con el oído pegado a la puerta. A lo lejos rugió un león.

Hizo girar la llave y abrió la puerta. No había entrado aún, cuando oyó un grito lejano. Los leones rugieron otra vez. George entró en África. Cuántas veces en este último año se había encontrado, al abrir la puerta, en el país de las Maravillas con Alicia y su tortuga, o con Aladino y su lámpara maravillosa, o con Jack Cabeza de Calabaza en el país de Oz, o con el doctor Doolittle, o con una vaca que saltaba por encima de una luna verdaderamente real... con todas esas deliciosas invenciones imaginarias. Cuántas veces se había encontrado con Pegaso, que volaba entre las nubes del techo; cuántas veces había visto unos rojos surtidores de fuegos de artificio, o había oído el canto de los ángeles. Pero ahora... esta África amarilla y calurosa, este horno alimentado con crímenes. Quizá Lydia tenía razón. Quizá los niños necesitaban unas cortas vacaciones, alejarse un poco de esas fantasías excesivamente reales para criaturas de no más de diez años. Estaba bien ejercitar la mente con las acrobacias de la imaginación, pero ¿y si la mente excitada del niño se dedicaba a un único tema? Le pareció recordar que todo ese último mes había oído el rugir de los leones, y que el intenso olor de los animales había llegado hasta la puerta misma del despacho. Pero estaba tan ocupado que no había prestado atención. La figura solitaria de George Hadley se abrió paso entre los pastos salvajes. Los leones, inclinados sobre sus presas, alzaron la cabeza y miraron a George. La ilusión tenía una única falla: la puerta abierta y su mujer que cenaba abstraída más allá del vestíbulo oscuro, como dentro de un cuadro.

-Váyanse-les dijo a los leones.

Los leones no se fueron. George conocía muy bien el mecanismo del cuarto. Uno pensaba cualquier cosa, y los pensamientos aparecían en los muros.

-¡Vamos! ¡Aladino y su lámpara!-gritó. La pradera siguió allí; los leones siguieron allí.

-¡Vamos, cuarto! ¡He pedido a Aladino!
Nada cambió. Los leones de piel tostada gruñeron.

-¡Aladino!

George volvió a su cena.

-Ese cuarto idiota está estropeado-le dijo a su mujer-. No responde.

-O...

-¿O qué?

-O no puede responder-dijo Lydia-. Los chicos han pensado tantos días en África y los leones y las muertes que el cuarto se ha habituado.

-Podría ser.

-O Peter lo arregló para que siguiera así.

-¿Lo arregló?

-Pudo haberse metido en las máquinas y mover algo.

-Peter no sabe nada de mecánica.

-Es listo para su edad. Su coeficiente de inteligencia...

-Aun así...

-Hola, mamá. Hola, papá.

Los Hadley volvieron la cabeza. Wendy y Peter entraban en ese momento por la puerta principal, con las mejillas como caramelos de menta, los ojos como brillantes bolitas de ágata, y los trajes con el olor a ozono del helicóptero.

-Llegáis justo a tiempo para cenar.

-Comimos muchas salchichas y helados de frutilla-dijeron los niños tomándose de la mano-. Pero miraremos cómo coméis.

-Sí. Habladnos del cuarto de juegos-dijo George. Los niños lo observaron, parpadeando, y luego se miraron.

-¿El cuarto de juegos?

-África y todas esas cosas-dijo el padre fingiendo cierta jovialidad.

-No entiendo-dijo Peter.

-Tu madre y yo acabamos de hacer un viaje por África con una caña de pescar, Tom Swift y su león eléctrico.

-No hay África en el cuarto-dijo Peter simplemente.

-Oh, vamos, Peter. Yo sé por qué te lo digo.

-No me acuerdo de ninguna África-le dijo Peter a Wendy-. ¿Te acuerdas tú?

-No.

-Ve a ver y vuelve a contarnos.
 La niña obedeció.

-¡Wendy, ven aquí!-gritó George Hadley; pero Wendy ya se había ido.
 Las luces de la casa siguieron a la niña como una nube de luciérnagas. George recordó, un poco tarde, que después de su última inspección no había cerrado la puerta con llave.

-Wendy mirará y vendrá a contarnos.

-A mí no tiene nada que contarme. Yo lo he visto.

-Estoy seguro de que te engañas, papá.

-No, Peter. Ven conmigo.
 Pero Wendy ya estaba de vuelta.

-No es África-dijo sin aliento.

-Iremos a verlo-dijo George Hadley, y todos atravesaron el vestíbulo y entraron en el cuarto. Había allí un hermoso bosque verde, un hermoso río, una montaña de color violeta, y unas voces agudas que cantaban. El hada Rima, envuelta en el misterio de su belleza se escondía entre los árboles, con los largos cabellos cubiertos de mariposas, como ramilletes animados. La selva africana había desaparecido. Los leones habían desaparecido. Sólo Rima estaba allí, cantando una canción tan hermosa que hacía llorar. George Hadley miró la nueva escena.

-Vamos, a la cama-les dijo a los niños. Los niños abrieron la boca.

-Ya me oísteis-dijo George.

Los niños se metieron en el tubo neumático, y un viento se los llevó como hojas amarillas a los dormitorios. George Hadley atravesó el melodioso cañaveral. Se inclinó en el lugar donde habían estado los leones y alzó algo del suelo. Luego se volvió lentamente hacia su mujer.

-¿Qué es eso?-le preguntó Lydia.

-Una vieja valija mía-dijo George.

Se la mostró. La valija tenía aún el olor de los pastos calientes, y el olor de los leones. Sobre ella se veían algunas gotas de saliva, y a los lados, unas manchas de sangre. George Hadley cerró con dos vueltas de llave la puerta del cuarto. Había pasado la mitad de la noche y aún no se había dormido. Sabía que su mujer también estaba despierta.

-¿Crees que Wendy habrá cambiado el cuarto?-preguntó Lydia al fin.

-Por supuesto.

-¿Convirtió la pradera en un bosque y reemplazo a los leones por Rima?

-Sí.

-¿Por qué?

-No lo sé. Pero ese cuarto seguirá cerrado hasta que lo descubra.

-¿Cómo fue a parar allí tu valija?

-No sé nada-dijo George-. Sólo sé que estoy arrepentido de haberles comprado el cuarto. Si los niños son unos neuróticos, un cuarto semejante...

-Se supone que el cuarto les saca sus neurosis y tiene una influencia favorable.
 George miró fijamente el cielo raso.

-Comienzo a dudarlo.

-Hemos satisfecho todos sus gustos. ¿Es ésta nuestra recompensa? ¿Desobediencia, secreteos?

-¿Quién dijo alguna vez "Los niños son como las alfombras, hay que sacudirlos de cuando en cuando"? Nunca les levantamos la mano. Están insoportables. Tenemos que reconocerlo. Van y vienen a su antojo. Nos tratan como si nosotros fuéramos los chicos. Están echados a perder, y lo mismo nosotros.

-Se comportan de un modo raro desde hace unos meses, desde que les prohibiste tomar el cohete a Nueva York.

-Me parece que le pediré a David McCIean que venga mañana por la mañana para que vea esa África.

-Pero el cuarto ya no es África. Es el país de los árboles y Rima.

-Presiento que mañana será África de nuevo. Un momento después se oyeron dos gritos. Dos gritos. Dos personas que gritaban en el piso de abajo. Y luego el rugido de los leones.

-Wendy y Peter no están en sus dormitorios-dijo Lydia.
 George escuchó los latidos de su propio corazón.

-No – dijo-. Han entrado en el cuarto de juegos.

-Esos gritos... Me parecieron familiares.

-¿Sí?

-Horriblemente familiares.

Y aunque las camas trataron de acunarlos, George y Lydia no pudieron dormirse hasta después de una hora. Un olor a gatos llenaba el aire de la noche.

-¿Papá?

-Sí.-dijo Peter.

 Peter se miró los zapatos. Ya nunca miraba a su padre, ni a su madre.

-¿Vas a cerrar para siempre el cuarto de juegos?

-Eso depende.

-¿De qué?

-De ti y tu hermana. Si intercalaseis algunos otros países entre esas escenas de África. Oh... Suecia, por ejemplo, o Dinamarca, o China.

-Creía que podíamos elegir los juegos.

-Sí, pero dentro de ciertos límites.

-¿Qué tiene África de malo, papá?

-Ah, ahora admites que pensabais en África, ¿eh?

-No quiero que cierres el cuarto-dijo Peter fríamente-. Nunca.

-A propósito. Hemos pensado en cerrar la casa por un mes, más o menos. Llevar durante un tiempo una vida más libre y responsable.

-¡Eso sería horrible! ¿Tendré que atarme los cordones de los zapatos, en vez de dejar que me los ate la máquina atadora? ¿Y cepillarme yo mismo los dientes, y peinarme y bañarme yo solo?

-Será divertido cambiar durante un tiempo. ¿No te parece?

-No, será espantoso. No me gustó nada cuando el mes pasado te llevaste la máquina de pintar.

-Quiero que aprendas a pintar tú mismo, hijo mío.

-No quiero hacer nada. Sólo quiero mirar y escuchar y oler. ¿Para qué hacer otra cosa?

-Muy bien, vete a tu pradera.

-¿Vas a cerrar pronto la casa?

-Estamos pensándolo.

-¡Será mejor que no lo pienses más, papá!

-¡No permitiré que ningún hijo mío me amenace!

-Muy bien.

Y Peter se fue al cuarto de los niños.

-¿Llego a tiempo?-dijo David McCIean.

-¿Quieres comer algo?-le preguntó George Hadley.

-Gracias, ya he desayunado. ¿Qué pasa aquí?

-David, tú eres psiquiatra.

-Así lo espero.

-Bueno, quiero que examines el cuarto de los niños. Lo viste hace un año, cuando nos hiciste aquella visita. ¿Notaste entonces algo raro?

-No podría afirmarlo. Las violencias usuales, una ligera tendencia a la paranoia. Lo común. Todos los niños se creen perseguidos por sus padres. Pero, oh, realmente nada. George y David McCIean atravesaron el vestíbulo.

-Cerré con llave el cuarto-explicó George-y los niños se metieron en él durante la noche. Dejé que se quedaran y formaran las figuras. Para que tú pudieses verlas.
 Un grito terrible salió del cuarto.

-Ahí lo tienes-dijo George Hadley-. A ver qué te parece.
 Los hombres entraron sin llamar. Los gritos habían cesado. Los leones comían.

-Afuera un momento, chicos-dijo George-. No, no alteréis la combinación mental. Dejad las paredes así. Marchaos.
 Los chicos se fueron y los dos hombres observaron a los leones, que agrupados a lo lejos devoraban sus presas con gran satisfacción.

-Me gustaría saber qué comen-dijo George Hadley-. A veces casi lo reconozco. ¿Qué te parece si traigo unos buenos gemelos y ...?

David McClean se rió secamente.

-No-dijo, y se volvió para estudiar los cuatro muros-. ¿Cuánto tiempo lleva esto?

-Poco menos de un mes.-No me impresiona muy bien, de veras.

-Quiero hechos, no impresiones.-Mi querido George, un psiquiatra nunca ha visto un hecho en su vida. Sólo tiene impresiones, cosas vagas. Esto no me impresiona bien y te lo digo. Confía en mi intuición y en mi instinto. Tengo buen olfato. Y esto me huele muy mal... Te daré un buen consejo. Líbrate de este cuarto maldito y lleva a los niños a mi consultorio durante un año. Todos los días.

-¿Es tan grave?

-Temo que sí. Estos cuartos de juegos facilitan el estudio de la mente infantil, con las figuras que quedan en los muros. En este caso, sin embargo, en vez de actuar como una válvula de escape, el cuarto ha encauzado el pensamiento destructor de los niños.

-¿No advertiste nada anteriormente?

-Sólo noté que consentías demasiado a tus hijos. Y parece que ahora te opones a ellos de alguna manera. ¿De qué manera?

-No los dejé ir a Nueva York.

-¿Y qué más?

-Saqué algunas máquinas de la casa, y hace un mes los amenacé con cerrar este cuarto si no se ocupaban en alguna tarea doméstica. Llegué a cerrarlo unos días, para que viesen que hablaba en serio.

-¡Aja!

-¿Significa algo eso?

-Todo. Santa Claus se ha convertido en un verdugo. Los niños prefieren a Santa Claus. Permitiste que este cuarto y esta casa os reemplazaran, a ti y tu mujer, en el cariño de vuestros hijos. Este cuarto es ahora para ellos padre y madre a la vez, mucho más importante que sus verdaderos padres. Y ahora pretendes prohibirles la entrada. No es raro que haya odio aquí. Puedes sentir cómo baja del cielo. Siente ese sol, George, tienes que cambiar de vida. Has edificado la tuya, como tantos otros, alrededor de algunas comodidades mecánicas. Si algo le ocurriera a tu cocina, te morirías de hambre. No sabes ni cómo cascar un huevo. Pero no importa, arrancaremos el mal de raíz. Volveremos al principio. Nos llevará tiempo. Pero transformaremos a estos niños en menos de un año. Espera y verás.

-¿Pero cerrar la casa de pronto y para siempre no será demasiado para los niños?

-No pueden seguir así, eso es todo.

Los leones habían terminado su rojo festín y miraban a los hombres desde las orillas del claro.

-Ahora soy yo quien se siente perseguido-dijo McCIean-. Salgamos de aquí. Nunca me gustaron estos dichosos cuartos. Me ponen nervioso.

-Los leones parecen reales, ¿no es cierto?-dijo George Hadley-. Me imagino que es imposible...

-¿Qué?-Que se conviertan en verdaderos leones.

-No sé.

-Alguna falla en la maquinaria, algún cambio o algo parecido...

-No.

Los hombres fueron hacia la puerta.-Al cuarto no le va a gustar que lo paren, me parece.

-A nadie le gusta morir, ni siquiera a un cuarto.

-Me pregunto si me odiará porque quiero apagarlo.

-Se siente la paranoia en el aire-dijo David McCIean-. Se la puede seguir como una pista. Hola.-Se inclinó y alzó del suelo una bufanda manchada de sangre-. ¿Es tuya?

-No-dijo George Hadley con el rostro duro-. Es de Lydia.

Entraron juntos en la casilla de los fusibles y movieron el interruptor que mataba el cuarto.
Los dos niños tuvieron un ataque de nervios. Gritaron, patalearon y rompieron algunas cosas. Aullaron, sollozaron, maldijeron y saltaron sobre los muebles.

-¡No puedes hacerle eso a nuestro cuarto, no puedes!

-Vamos, niños.

Los niños se dejaron caer en un sofá, llorando.

-George-dijo Lydia Hadley-, enciéndeles el cuarto, aunque sólo sea un momento. No puedes ser tan rudo.

-No puedes ser tan cruel.

-Lydia, está parado y así seguirá. Hoy mismo terminamos con esta casa maldita. Cuanto más pienso en la confusión en que nos hemos metido, más me desagrada. Nos hemos pasado los días contemplándonos el ombligo, un ombligo mecánico y electrónico. ¡Dios mío, cómo necesitamos respirar un poco de aire sano!
Y George recorrió la casa apagando relojes parlantes, estufas, calentadores, lustradoras de zapatos, ataderas de zapatos, máquinas de lavar, frotar y masajear el cuerpo, y todos los aparatos que encontró en su camino. La casa se llenó de cadáveres. Parecía un silencioso cementerio mecánico.

-¡No lo dejes!-gemía Peter mirando el cielo raso, como si le hablase a la casa, al cuarto de juegos-. ¡No dejes que papá mate todo!-Se volvió hacia George-. ¡Te odio!

-No ganarás nada con tus insultos.

-¡Ojalá te mueras!

-Hemos estado realmente muertos, durante muchos años. Ahora vamos a vivir. En vez de ser manejados y masajeados, vamos a vivir.
Wendy seguía llorando y Peter se unió otra vez a ella.

-Sólo un rato, un ratito, sólo un ratito-lloraban los niños.

-Oh, George-dijo Lydia-, no puede hacerles daño.

-Bueno... bueno. Aunque sólo sea para que se callen. Un minuto, nada más, ¿oísteis? Y luego lo apagaremos para siempre.

-¡Papá, papá, papá!-cantaron los niños, sonriendo, con las caras húmedas.

-Y en seguida saldremos de vacaciones. David McCIean llegará dentro de medía hora, para ayudarnos en la mudanza y acompañarnos al aeropuerto. Bueno, voy a vestirme. Enciéndeles el cuarto un minuto, Lydia. Pero sólo un minuto, no lo olvides.
Y la madre y los dos niños se fueron charlando animadamente, mientras George se dejaba llevar por el tubo neumático hasta el primer piso, y comenzaba a vestirse con sus propias manos. Lydia volvió un minuto mis tarde.

-Me sentiré feliz cuando nos vayamos-suspiró la mujer.

-¿Los has dejado en el cuarto?

-Quería vestirme. ¡Oh, esa África horrorosa! ¿Por qué les gustará tanto?

-Bueno, dentro de cinco minutos partiremos para Iowa. Señor, ¿cómo nos hemos metido en esta casa? ¿Qué nos llevó a comprar toda esta pesadilla?

-El orgullo, el dinero, la ligereza.

-Será mejor que bajemos antes que los chicos vuelvan a entusiasmarse con sus condenados leones. En ese mismo instante se oyeron las voces infantiles.

-¡Papá, mamá! ¡Venid pronto! ¡Rápido! George y Lydia bajaron por el tubo neumático y corrieron hacia el vestíbulo. Los niños no estaban allí.

-¡Wendy! ¡Peter!
Entraron en el cuarto de juegos. En la selva sólo se veía a los leones, expectantes, con los ojos fijos en George y Lydia.

-¿Peter, Wendy? La puerta se cerró de golpe.

-¡Wendy, Peter! George Hadley y su mujer se volvieron y corrieron hacia la puerta.

-¡Abrí la puerta!-gritó George Hadley moviendo el pestillo-. ¡Pero han cerrado del otro lado! ¡Peter!-George golpeó la puerta-. ¡Abrí!

-Se oyó la voz de Peter, afuera, junto a la puerta.

-No permitan que paren el cuarto de juegos y la casa.
El señor George Hadley y su señora golpearon otra vez la puerta.

-Vamos, no sean ridículos, chicos. Es hora de irse.
El señor McCIean llegará en seguida y...
Y se oyeron entonces los ruidos. Los leones avanzaban por la hierba amarilla, entre las briznas secas, lanzando unos rugidos cavernosos. Los leones. El señor Hadley y su mujer se miraron. Luego se volvieron y observaron a los animales que se deslizaban lentamente hacia ellos, con las cabezas bajas y las colas duras. El señor y la señora Hadley gritaron. Y comprendieron entonces por qué aquellos otros gritos les habían parecido familiares.

-Bueno, aquí estoy-dijo David McCIean desde el umbral del cuarto de los niños-. Oh, hola-añadió, y miró fijamente a las dos criaturas. Wendy y Peter estaban sentados en el claro de la selva, comiendo una comida fría. Detrás de ellos se veían unos pozos de agua, y los pastos amarillos. Arriba brillaba el sol. David McCIean empezó a transpirar-. ¿Dónde están vuestros padres?
Los niños alzaron la cabeza y sonrieron.

-Oh, no van a tardar mucho.

-Muy bien, ya es hora de irse.

El señor McCIean miró a lo lejos y vio que los leones jugaban lanzándose zarpazos, y que luego volvían a comer, en silencio, bajo los árboles sombríos.
Se puso la mano sobre los ojos y observó atentamente a los leones.
Los leones terminaron de comer. Se acercaron al agua.
Una sombra pasó sobre el rostro sudoroso del señor McCIean. Muchas sombras pasaron.
Los buitres descendían desde el cielo luminoso.

-¿Una taza de té?-preguntó Wendy en medio del silencio.
