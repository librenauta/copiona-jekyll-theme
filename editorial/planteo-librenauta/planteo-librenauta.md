# Planteo

> by librenauta

La siguiente colección es un paseo del supersymmetry de @librenauta que un
día decidió mirar las flores, hojas y porciones de naturaleza que existen en su
barrio y algunos otros lugares. Mirar de más cerca, más y más cerca para ver si encontraba la
forma de parar el tiempo.

_dedicado a cian que me mostró el primer suspiro_

********

![](1.png "planteo_librenauta")

_y este mundo?_

********

![](1-30.png "planteo_librenauta")

_La belleza es esta cosa_

********


![](1-5.png "planteo_librenauta")

_miro esto y no puedo dejar de pensar en asteriscos_

********

![](1-15.png "planteo_librenauta")

_hola primavera, de hoy temprano_

********

![](1-11.png "planteo_librenauta")

_flores_

********

![](1-19.png "planteo_librenauta")

_flores_

********

![](1-34.png "planteo_librenauta")

_No les mostré pero ayer vi este alien_

********

![](1-10.png "planteo_librenauta")

_BOOM_

********

![](1-12.png "planteo_librenauta")

_Buen día bb_

********

![](1-4.png "planteo_librenauta")

_Muuuaa_

********
