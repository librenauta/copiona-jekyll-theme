
# La maquina es tu amo y señor

_por YANG, JENNY CHAN - LIZHI, XU - FEI, LI - XIAOQIO, ZHANG_

## Nota edición pirata por @_floresdefuego
 ajustes de edicion y estilo por valeria

## Nota a la edición francesa

El origen de esta recopilación surge de un texto publicado en el boletín _Dans le
mundo, una clase en lutte,_ en agosto de 2013, por el colectivo Échanges et Mouvement,[^1]

[^1]:_Échanges et Mouvement_ se presenta así en su propia web: «no somos un "grupo",
en el sentido que suele otorgarsele a esta palabra en los circulos izquierdistas,
sino una roja […]. Se constituyó en 1975 por militantes provenientes de diferentes
grupos», https://web.archive.org/web/20170321025415/http://mondialisme.org/spip.php?article204
(última consulta: junio 2020). Su proposito es informar y reflexionar colectivamente
sobre la lucha de clases en las actividades de las organizaciones de la izquierda
radical de todo el mundo. Con la finalidad, nació _Dans le monde, une classe en lutte_
que en su editorial del n.°0 apunta que «el proyecto de la publicación, difundida
gratuitamente, es dar a conocer, la más amplia e independiente posible de toda interpretación,
las luchas tal como se desarrollan en el mundo entero. Cualquiera puede contribuir,
necesita informaciónes sobre luchas [...] que podría conocer directamente
o por otros medios», https://web.archive.org/web/20200612040729/https://archivesautonomies.org/IMG/pdf/echanges/dlm/dlm-n00.pdf
(última consulta: junio 2020)(_N de la T_)

centrado en el «desembarco de China en la Union Europea». Desde hace años, China
es importante en el tipo de organizacion del trabajo y de la vida de las obreras que
úne lo mejor del tayorismo y de las novelas naturistas del siglo XIX. Y ello con
tal rigor que incluso ha llegado a causar el rechazo de las obreras de los países
del Este, donde Foxconn y sus socios han empezado a implantar sus fábricas.[^2]

[^2]: Véase las referencias a dicho panfleto en la p.117

Entre los análisis sobre los iSlaves[^3]

[^3]: El término «iSlave» fue utilizado por primera vez en 2010, como lema de una
campaña internacional lanzada por +++SACOM+++ (Students and Scholars Againts Corporate
Misbehavior), GoodElectronics, Bread for All, Berne Declaration, Feinheit y Greenpace
Suiza.(_N de la E_).

chinos de Foxconn[^4]

[^4]: Extractos de Pun Ngai _et al.: iSlaves. Ausbeauting and winderstand in chinas
Foxconn-Fabriken,_ Mandelbaum Verlag, Viena 2013. Pueden consultarse algunos elementos
bibliográficos sobre Pun Ngai en la p.117. un testimonio anónimo evocaba, con un
irreprochable sentido literario, la vida de un «esclavo electrónico» que describía
los efectos destructores del trabajo en cadena y que respondía a su «amo y señor»,
la máquina, mediante el sabotaje y el boicot. Viejas prácticas heredadas de las revueltas
contra la Revolucion industrial, teorizadas en francia durante los bellos años del
sindicalismo revolucionario de la +++CGT+++ que precedieron a la Gran Guerra.[^5]

[^5]: Véase Miguel Chueca (comp.): _Émile Pouget: L'action directe, et autres écrits
syndicalistes (1933-1910)_, agone, Marsella, 2010(en castellano: _La acción directa.
Las leyes canallas. El Sabotaje,_ Hiru, Hondarribia, 2012); en particular, la introducción
de Chueca:«Émile Pouget, du _Père peinard_ au meneur syndicaliste». En 1889, Pouget
publica el primer número del seminario anarquista de talante panfletario _Le Père
peinard_ —expresión que remite a un tipo de persona que vive sin sobresaltos una
vida tranquila—, en el que defiende la huelga general. Unos años antes, en 1879,
había formado un sindicato de empleados, y en 1883, participó en una manifestación
de parados, junto a Louise Michel, una de las principales figuras de la Comuna de
París. Precisamente, durante los hechos de la Comuna de París. Precisamente, durante
los hechos de la Comuna fue detenido y condenado a ocho años de prisión, por haber
intentado defender a Michel de una agresión policial.(_N. de la T._)

Una oleada se de suicidios durante el primer semestre del 2010, en la ciudad-fábrica
de Foxconn en Shenzhen, permitió una mayor proyección mediática al trabajo de las
+++ONG+++ chinas. Estas denunciaban las condiciones de las que se veían sometido
el ejército de esclavos de la electrónica, formado por cientos de miles de jóvenes
migrantes: el retrato de una superviviente, Tian Yu, obrera de Foxconn, sirve a la
socióloga y militante Jenny Chan como hilo conductor para analizar el recorrido de
estas trabajadoras desprovistas de futuro.[^6]

[^6]: Pueden consultarse algunos datos biográficos sobre Jenny Chan en la p. 118,
así como diferentes referencias sobre su trabajo y el de sus colegas en la p. 21
y 22.

Ni las redes instaladas en 2010 en los edificios donde son alojados los iSlaves,
ni las modificaciones cosméticas aplicadas desde entonces a sus condiciones de trabajo
han conseguido frenar el suicidio como respuesta a la unica vida que se les ofrece.
Entre ellos, el joven obrero Xu Lizhi recurrió a la poesía para explicar su día a
día, antes de poner fin a su vida en octubre de 2014.

Esta recopilación estaría limitada a ser una crítica —siempre sujeta a revisión—
de la organización social del trabajo o, incluso, a ser un vehículo de la indignación
—que no es el peor motor frente a la indiferencia general— provocada por la alienación
de las obreras, si no fuera porque el último texto arroja cierta luz sobre ese «otro
lado del mundo». Una luz necesaria sobre unos Silicon Valley que se han expandido
en los países ricos y las capitales mundiales, donde las clases medias se esperanzan
con la idea de construir un mundo mejor gracias al acceso a las maravillas tecnológicas
fruto de la revolución informática. _Thierry Discepolo_

## **«La máquina es tu amo y señor[^7]»**

[^7]: Extracto de la investigación sobre Foxconn de Pun Ngai, Huilin Lu, Yuhua Guo
y Yuan Shen: _Wo zai fushikang(Me at Foxconn),_ Pekín 2012. Existe una versión alemana:
_iSlave, Ausbeutuning and Widerstand in Chinas Foxconn-Fabriken,_ Mandelbaum Verlag,
Viena/Berlín, 2013, accesible en la red: https://web.archive.org/save/http://www.gongchao.org/de/islaves-buch/
(última consulta: junio 2020).

_Las cuotas de producción y los controles de calidad presionan a las trabajadoras y trabajadores tanto como el uso de la violencia verbal, algo que resultaba más obvio en las asambleas matinales. Primero se llamaba a todas y todos por su nombre; después el jefe de sección explicaba las tareas del día y señalaba problemas como la falta de limpieza, el desorden en los bancos de trabajo, las conversaciones durante las horas laborales y el trabajo mal ejecutado. Cada mañana teníamos que escuchar esta reprimenda.[...]_

_Los supervisiores reprimen a trabajadoras y trabajadores, las máquinas le restan
sentido y valor a la vida de las empleadas. El trabajo no exige la habilidad de pensar
por una misma, todos los días se repiten los mismos movimientos corporales, para
que las obreras insensibilicen gradualmente sus sentimientos y se vuelvan apáticas.
Su presente ya no está vinculado a sus pensamientos. Me percaté de cómo, durante
el trabajo, a menudo desconectaba; ya tenía interiorizados todos los movimientos
del trabajo cuando de repente, volvía en mí y no sabía si había procesado la última
pieza o no. Le tenía que preguntar a mi compañera.[...]_

_Las máquinas parecen criaturas extrañas que se tragan la materia prima, la digieren
en su interior y la escupen como un producto acabado. El proceso de automatización
de la producción simplifica las tareas de las trabajadoras y trabajadores, que ya
no tienen ningún tipo de función importante, sino que, más bien, sirven a las máquinas.
Hemos perdido el valor que nos corresponde como seres humanos y nos hemos convertido
en una extensión de las máquinas, su apéndice, sí, su esclavo. Muchas veces pensaba
que la máquina era mi señor y amo, cuyo cabello tenía que peinar como un esclavo.
No podía peinarlo demasiado rápido, ni tampoco demasiado lento. Lo tenía que peinar
de forma limpia y ordenada, no quebrar o romper ni uno solo de sus cabellos, y no
se me podía caer el peine. Si no lo hacía correctamente estaba perdido.[...]_

_Un día, una trabajadora me dijo que, en enero del mismo año, ni habían pagado las
horas extraordinarias y que, por lo tanto, los trabajadores habían dejado sus herramientas
de trabajo.[...] algunos habían tomado la iniciativa y se habían negado a realizar
horas extraordinarias ese día. Los otros empleados del taller inmediatamente se unieron
a ellos y. al final de uno de los turnos ordinarios, una gran parte de obreras y
obreros renunciaron a las horas extras y abandonaron el taller. Algunos de quienes habían
tomado la iniciativa dejaron la empresa o fueron trasladados a otras secciones._

_En los talleres podías observar, a menudo, cómo se buscaban oportunidades para hacerse
el tonto. Un día, mi compañero Ming se pasó a nuestro sector; somos buenos amigos,
pero todavía me pregunto por qué no tenía nada para hacer durante las horas de trabajo.
«Mi máquina se estropeó», me dijo. Le respondí: «Eso es genial». Estuvo un rato y
me susurró: «He estropeado la máquina a propósito. Solo tenía que utilizar el botón
de emergencia, y la máquina entonces paró. Coloqué de nuevo el interruptor en la
posición original de encendido, así que nadie sabe lo que ha pasado». Otros trabajadores
me dijeron que cuando hay mucha tarea o quieren tener un rato en paz, él mismo trata
como basura piezas estandarizadas, y las destruye para tener que volver a fabricarlas
de nuevo. De esa manera, reduce la cuota de producción prevista y desacelera el ritmo
de trabajo. También me comentó: «Mi compañero de turno de noche a descartado incluso
dos cajas de piezas estandarizadas»._

_Por supuesto, existe una forma simple y directa de resistencia, largarse; es decir,
simplemente irse. Una vez, recibí un mensaje de un trabajador después del turno de
trabajo: «¡Lo dejo! No es nada, simplemente no quiero alargar la tortura nocturna
por más tiempo» Había trabajado en Foxconn apenas 35 días._ _*Yang*_

## **«Crecimiento, tu nombre es sufrimiento[^8]»**

> _Tian Yu, obrera migrante en Foxconn_

Entre las empresas más importantes dentro de la cadena global de proveedores que
opera en china se encuentra Foxconn, la multinacional de la electrónica de capital
taiwanés. Foxconn es el nombre comercial de Hon Hai Precision Industry Company que
con una plantilla de 1.4 millones de trabajadoras, es la mayor empresa del sector
privado en China y uno de los principales contratistas del mundo.[^9]

[^8]: Artículo publicado en _The Asia-Pacific Journal, año 11 vol. 31, n.° 1, 6 de
agosto del 2013. Esta es una versión revisada de un artículo anterior publicado en
_New Technology, Worker and Employment,_ n.° 28, 18 de julio de 2013, pp. 84-99.

[^9]: _iSuppli, 18 de julio de 2013, https://onlinelibrary.wiley.com/doi/full/10.1111/ntwe.12007#ntwe12007-bib-0041; BBC, 20 de marzo de 2012. _

Gracias a su fabricación de productos para empresas, mercados y consumidores en Estados
Unidos, Europa y el resto de los lugares del mundo, dicha empresa representa en culmen
de la industria china orientada a la explotación. La amplia expansión de Foxconn
en territorio chino dio lugar a una mitología en torno a su éxito corporativo hasta
2010, cuando la muy publicitada avalancha de suicidios de trabajadoras y trabajadores,
tanto en sus fábricas como residencias, durante ese año, situó la atención del mundo
sobre el lado oscuro de su régimen de contratación. Una de las empleadas que intentó
suicidarse fue la joven de 17 años Tian Yu. Alrededor de las 8h del 17 de marzo de
2010, solo 37 días después de ser contratada por la empresa Yu se arrojó desde el
cuarto piso de la residencia de trabajadores Longhua —en Shenzhen— de Foxconn.[^10]

[^10]: South China Morning Post, 19 de julio de 2012.

Sobrevivió milagrosamente, pero sufrió tres fracturas en la espina dorsal, cuatro
fracturas de cadera y quedó paralizada de la cintura para abajo. El trabajo en la fábrica de Longhua era su primer empleo y, probablemente, será su último. Este informe
se estructura en torno al relato que ella misma hace tras su acción, de las circunstancias
que la rodearon, sus consecuencias y el desesperado intento de acabar con su propia
vida.

Su narración, examinada dentro del contexto más amplio de la avalancha de suicidios
en Foxconn, proporciona una oportunidad para comprender las consecuencias sobre las
personas de los regímenes laborales desarrollados por las cadenas de producción y
suministro globalizadas de las industrias explotadoras chinas.[^11]

[^11]: Véase Ching Kwan Lee: _Gender and the South China Miracle: Two Worlds of Factory
Women, University of California Press,_ Berkeley, 1988; Pun Ngai: _Made in China:
Women Factory Workers in a Global Workplace, Duck University Press_, Durham, 2005;
Nelson Lichtenstein: _The Retail Revolution: How Wal-Mart Created A Brave New World
of Business,_ Metropolitan Books, Nueva York, 2009; Edna Bonacich y Gary G. Hamilton,
Benjamin Senauer y Misha Petrov (eds.): _The Market Makers: How Retailers Are Reshaping
the Global Economy,_ Oxford University Press, Oxford, 2011, pp. 211-230; Anita Chan
(ed.): _Walmart in China_, Cornell University Press, Ithaca, Nueva York, 2011.

El contrapunto a la fascinación contemporánea por los productos de apple es un régimen
de gestión autocrática y de arduo trabajo, desarrollado en las cadenas de montaje
de unos nodos de producción cuyas contradicciones nos recuerdan a la persistente
relevancia del concepto de «fetichismo de la mercancía» de Marx.


A miles de kilómetros de las estanterías de las tienda de Apple, y habitualmente
aislada de la preocupación de los consumidores, yace la realidad de una dura labor
alienante que, en condiciones extremas, contribuye a tragedias individuales como la de Yu.

Durante el año 2010, dieciocho trabajadores de entre 17 y 25 años intentaron suicidarse
dentro de las instalaciones de Foxconn. Catorce de ellos murieron, mientras que cuatro
sobrevivieron con lesiones graves que les incapacitaron en diferentes grados.[^12]

[^12]: Jenny Chain: «iSlave», _New Internationalist,_ n.° 441, Oxford, 1 de abril
de 2011, pp. 1-3, https://web.archive.org/web/20211016042154/https://newint.org/features/2011/04/01/islave-foxconn-suicides-workers/.

Enfrentándose a la crítica pública, Foxconn se esforzó por minimizar el daño a su
reputación afirmando que la tasa de suicidio en sus instalaciones se encontraba por
debajo de la ratio nacional, situada en veintitrés personas por cada 100.000.[^13]

[^13]: Michael R. Phillips, Xianyun Li y Yanping Zhang: «Suicide Rates in china,
1995-1999», _The Lancet,_ n.° 359, Londres, 2002, pp. 835-840.

Liu Kun, el director de comunicaciones públicas de la corporación, enfatizó que Foxconn
tenía más de un millón de empleados solo en China, y que las razones para cometer
sucidio eran múltiples. «Dado su tamaño, la ratio de suicidios en Foxconn no está
necesariamente alejada de la ratio relativamente elevada de china.»[^14]

[^14]: The Guardian, 28 de mayo de 2010.

Pero ningún estudio científico que trazara una comparativa podría ignorar que los
suicidios fueron cometidos por personas jóvenes que trabajaban en la misma compañía,
la mayor parte de ellas en el pequeño distrito industrial de Shenzhen. Para que estas
comparativas fueran válidas, requerirían que los datos extraídos recogiesen de manera
aleatoria un distrito con un tamaño de población similar y grupos de edad similares.
Poco sentido tendría, por ejemplo, comparar los suicidios de los trabajadores con
el patrón establecido entre desfavorecidas mujeres rurales o los ancianos, que son
los grupos que acaparan la mayoría de casos de suicidio del país. El conjunto de
suicidios dentro de Foxconn representa un fenómeno que no tiene precedente alguno
en la historia industrial de China. Cabe esperar que es testimonio de Yu contribuirá
a la comprensión de esta oleada de suicidios y, en términos generales, ayudará al
necesario escrutinio que exigen tanto el contexto chino como el contexto global del
capital internacional, que amparan y vinculan a Foxconn, al conjunto de marcas de
productos de consumo electrónico y al propio Gobierno chino.[^15]

[^15]: Jenny Chan: «Foxconn: the Global Predator», _Global Dialogue_ (International
Sociological Association), vol. 1, n.°2, Londres, 2010, pp. 1-3, https://web.archive.org/web/20211016042244/http://globaldialogue.isa-sociology.org/foxconn-the-global-predator/ ;
Jenny Chan, Pun Ngai y Mark Selden: «The Politics of Global Production: Apple, Foxconn
and China's New Working Class», _New Technology, Work and Employment,_ n.° 28, Mánchester,
2013, pp. 100-115; Tim Pringle: «Reflections on Labor in China: From A Moment to
A Movement,» _the South Atlantic Quarterly,_ vol. 112, n.° 1, Durham, 2013, pp. 203-212.

Este informe intercala el testimonio de Yu con las prácticas en los talleres de Foxconn,
y concluye con un análisis del papel desempeñado por el gobierno local y la Federación
Nacional de Sindicatos de China (+++ACFTU+++, por sus siglas en inglés) —la organización
que aparentemente representa a las trabajadoras y trabajadores— y las respuestas
de dichos agentes a los suicidios.

### **El nacimiento de Grupos de Estudios Universitarios sobre Foxconn**

Después del verano del 2010, y tras la oleada de suicidios y las denuncias de abusos
cometidos por las corporaciones, miembros de las facultades y estudiantes de veinte
universidades de la China continental, Taiwán y Hong Kong constituyeron el University
Research Group of Foxconn (Grupo de estudios universitarios sobre Foxconn). Junto
con el Students and Scholars Against Corporate Misbehavior (+++SACOM+++),[^16]

[^16]: +++SACOM+++ (https://www.Sacom.hk), radicada en Hong Kong fue fundada en 2005
para poner en contacto y colaboración a estudiantes, académicos ya activistas, interesados
en monitorizar el comportamiento de las corporaciones y defender los derechos de
las trabajadoras y trabajadores. Desde 2006, +++SACOM+++, que funciona como un grupo
de denuncia transnacional, es miembro central de GoodElectronics, una red global
sobre derechos humanos y producción sostenible en la industria electrónica.

investigadores internacionales han realizado estudios independientes acerca de las
prácticas laborales y los sistemas de producción de Foxconn.[^17]

[^17]: Véase Jenny Chan y Pun Ngai: «Suicide As Protest for the New Generation of
Chinese Migrant Workers: Foxconn, Global Capital, and the State», _The asia-pacific
Journal,_ vol. 18, n.° 37, Durham, 2010, pp. 1-50, https://web.archive.org/web/20211016042328/https://apjjf.org/-Jenny-Chan/3408/article.html ; y, también en
el número especial de _The South Atlantic Quarterly,_ vol. 112, n.° 1, Durham, 2013,
concretamente, J.Chan y N.Pun: «The Spatial Politics of Labor in China: Life, Labor
and A New Generation of Migrant Workers», pp. 179-190 ; Tim Pringle: «Reflections
on Labor in China: From A Moment to A Movement», pp. 191-202, y Ho-fung Hung: «Labor
Politics under Three Stages of Chinese Capitalism», pp. 203-212.

Jenny Chan es una de las principales investigadoras de campo en Hong Kong. Ha entrevistado
a trabajadoras y trabajadores de Foxconn, gerentes, dirigentes gubernamentales, a
un abogado laborista afiliado al +++SACOM+++ y a activistas por los derechos de los
y las trabajadoras de Hong Kong, Shenzhen, —en las ciudades de Longhua y Guanlan
(Cantón)—, Chengdú (Sichuan) y de la municipalidad de Chongqing.

Además de las extensas entrevistas con Yu, la autora ha realizado otras cuarenta
y dos a trabajadores en activo y antiguos empleados, siempre fuera del lugar de trabajo,
donde no se veían sometidos a la vigilancia de los gerentes y a sus represalias.
Durante la labor de campo, los miembros del equipo de investigación se identificaban
como grupo, para proteger el anonimato tanto de los investigadores universitarios
individuales —especialmente de los estudiantes a tiempo completo residentes en la
china continental y que estaban realizando el doctorado y el posdoctorado— como de
los y las activistas laborales más significados, frente al acoso empresarial o la
censura de los gobiernos locales. Dicho anonimato fue esencial, dada la estrecha
alianza entre dirigentes de Foxconn y los miembros del Gobierno en la cuatro municipalidades
—Pekínm, Shanghái, Tianjin y Chongqing— y las quince provincias donde Foxconn ha
realizado importantes inversiones para levantar inmensas instalaciones dedicadas
a la producción, la investigación y el desarrollo.[^18]

[^18]: Jenny Chan, Pun Ngai y Mark Selden, 2013, _op. cit._

### **La historia de Tian Yu**

El primer encuentro con Yu tuvo lugar en julio de 2010 en el shenzhen Longhua People's
Hospital, donde se estaba recuperando de las lesiones derivadas de su intento de
suicidio. Conscientes del frágil estado de Yu, tanto físico como psíquico, los investigadores
temían que su presencia pudiese causar mayor dolor tanto a ella como a su familia.
Sin embargo, su madre, al lado de su cama, y la misma Yu al despertar agradecieron
la presencia. Los investigadores buscaban comprender la naturaleza de los «problemas
privados» que habían detonado su intento de suicidio, canalizar ese conocimiento
como una manera de impulsar la visibilización de los mismos y convertir esas condiciones
en un «problema público».[^19]

[^19]: Edward Webster, Rob Lambert y Andries Beziudenhout: _Grounding Globalization:
Labour in the age of Insecurity,_ Blackwell Publishing, Malden (Massachussets), 2008.

A lo largo de las siguientes semanas, a medida que Yu iba recuperando sus facultades
y establecía lazos de confianza con los investigadores, fue rememorando su entorno
familiar, las circunstancias que le habían conducido a estar empleada en Foxconn
y sus experiencias trabajando en la cadena de montaje y viviendo en la residencia
de la empresa. Durante las entrevistas, quedó claro que los problemas de Yu no eran
individuales o «psicológicos», sino que eran los mismos que afrontaban muchos de
los empleados de la corporación. El viaje personal hecho por Yu no es atípico. Migró
desde el campo para acabar contratada como trabajadora joven y parte de la creciente
mano de obra de la industria manufacturera orientada a la explotación.

> _Nací en una familia granjera en febrero del 1993, en un pueblo cerca de la ciudad de Laohekou, en la provincia de Hubei, en China central. Mi abuela me crió mientras mis padres ganaban dinero como trabajadores fabriles lejos de casa._

Yu pertenecía a la generación de «los niños abandonados» que surgió como consecuencia
de la primera oleada migratoria hacia las zonas urbanas que asoló las zonas rurales
chinas. Desde mediados de la década del 1980, el deterioro de la economía rural —debido
a las políticas de un Estado sustentado en el desarrollo urbano— , y las crecientes
presiones del mercado que siguieron a la adhesión de China a la Organización Mundial
del Comercio(+++OMC+++) en 2001, trajo consigo desafíos sin precedentes para el vasto
campesinado chino.

> _Como mucho, mi familia podía ganar unos 15.000 yuanes al año trabajando la tierra,[^20] Difícilmente suficiente para mantener a unas seis personas... Algunos años después, mis padres regresaron con el dinero justo como para renovar la casa._

[^20]: El equivalente a 1986 euros.

La reconstrucción del entorno rural como un proyecto multidimensional que integre
objetivos sociales y económicos se ha mostrado como un programa defectuoso y permanece
infradesarrollado.[^21]

[^21]: Mark Selden: Mark Selden: _The Political economy of Chinese Development,_
M.E.Sharpe, Armonk (New York), 1993; Alexander Day y Mathew A. Hale: «Guest Editors'
Introduction», _Chinese Sociology and anthropology, vol. 39, n° 4, Routledge, Londres,
2007, pp.3-9.

Pese a la eliminación de los impuestos a la agricultura a mediados de los 2000, esta
se ha mantenido estancada mientras la juventud marchaba en masa a las ciudades. Las
decisiones personales que llevan a abandonar el hogar están conformadas por preocupaciones
tanto sociales como económicas. Los jóvenes del campo expresan cada vez más sus deseos
de ampliar horizontes y de experimentar la vida moderna y de consumo urbano en megaciudades
como Shenzhen, en el extremo norte de Hong Kong.[^22]

[^22]: Alexandra Harney: _The China Price: The True Cost of Chinese Competitive Advantage,_
The Penguin Press, New York, 2008.

_La tecnología de Internet y las comunicaciones móviles han abierto para nosotros
una ventana al maravilloso y próspero estilo de vida urbano. Casi toda la gente joven
de mi edad, incluyendo mis amigos de la escuela, se han marchado fuera a trabajar,
y yo también estaba excitada por la perspectiva de conocer el mundo exterior. Tras
completar un curso en la escuela vocacional, decidí abandonar la provincia para buscar
nuevas oportunidades con el apoyo de mi familia._

Poco después del Festival de Primavera, a principios de febrero de 2010, el padre
de Yu le dio unos 500 yuanes (aproximadamente 63 euros) para apoyarla en su búsqueda
de empleo en la provincia de Cantón, y le proporcionó un teléfono móvil de segunda
mano para que pudiera llamarles. Le pidió que se mantuviese segura.

> _Mi primo me llevó hasta la estación de autobuses de larga distancia. Me estaba uniendo a muchos jóvenes rurales que dejan la tierra para encontrar trabajos en la ciudad. Era la primera vez en mi vida que estaba lejos de casa, del lugar en el que me era familiar el entorno, la comida y la gente. Al bajarme del autobús, mi primera impresión de la ciudad industrial fue que Shenzhen no se parecía en nada a lo que había visto en telelvisión._

## **La llegada a Shenzhen; la entrada a Foxconn**

En 1980, Shenzhen era la primera Zona Económica Especial[^23]

[^23]: Son zonas de excepcionalidad fiscal y económica en las que se queda suspendida
la legislación vigente en materia de regulación a favor de una mayor libertad de
mercado. Según el Banco Mundial, sus características elementales serían: «1) un entorno
aduanero especial con una administración aduanera eficiente y generalmente con acceso
a insumos importados libre de aranceles e impuestos; 2) es más fácil acceder a la
infraestructura (como renta de propiedades, armazón de las fábricas y servicios básico)
y esta es la más fiable que la disponible normalmente en el país; y 3) incluye una
gama de incentivos fiscales que contemplan la exoneración de impuestos corporativos y
reducciones, además de un entorno administrativo mejorado». Extraído de _Desarrollado
el capital exportador en América Central. Infraestructura para Desarrollar las Exportaciones:
Zonas Económicas Especiales, Innovación y Sistemas de calidad, +++VV-AA.+++ , Banco
Mundial, 2012, p. 3, https://web.archive.org/web/20210413132104/https://apjjf.org/-Jenny-Chan/3408/article.html

abierta al mercado exterior chino y a las inversiones externas. Desde 1988, Foxconn
ha construido numerosas fábricas en el sur de china, que es el corazón del desarrollo
productivo orientado a la exportación y un área en el que las regulaciones laborales
y ambientales son débiles y su puesta en práctica más aún.[^24]

[^24]: Phyllis Andors: «Women and Work in Shenzhen» _Bulletin of Concerned Asian
Scholars, vol.20, n.° 3, Alexandria (Virginia), 1998, pp, 22-41; Anita Chan:_China's
Workers under Assault: The Exploitation of Labor in A Globalizing Economy, M.E Sharpe,
Armonk (New York), 2001.

Foxconn se ha expandido y diversificado, aprovechando unas políticas fiscales favorables,
el bajo coste del suelo y las rebajas en el precio del agua para las empresas electrónicas
emergentes. El resultado ha sido la creación de las nuevas e inmensas reservas de
mano de obra y la dependencia de un flujo continuo de trabajadoras de las zonas rurales;
por su lado, las nuevas asalariadas han experimentado un proceso de reclutamiento
despersonalizado como el que describe Yu:

> _En el departamento de Contratación de Foxconn hice cola durante toda la mañana, rellené las solicitudes de empleo, les di mis huellas para el lector digital, escaneé mi tarjeta de identidad y me hice el test sanguíneo necesario para completar el examen médico. El 8 de febrero de 2010 ya estaba contratada como empleada sin categoría de la cadena de montaje. Foxconn me asignó el número de personal F9347140. Esa misma tarde recibí una breve introducción por parte de la empresa sobre el horario laboral y las normas y regulaciones en la fábrica de Foxconn en Guanlan. También me dieron un manual de bolsillo impreso a color: El Manual del Empleado de Foxconn._

El prefacio de este manual está redactado con un lenguaje alentador:

> Corre hacia tus sueños más preciados, persigue una vida magnífica. En Foxconn puedes expandir tu conocimiento y acumular experiencia. Tus sueños abarcan desde aquí hasta mañana.

El instructor encargado de la orientación de las empleadas suele contar historias
de emprendedores como los +++CEO+++ Steve Jobs y Bill Gates para inspirar a las nuevas
trabajadoras. Foxconn propaga este sueño de la prosperidad a través del trabajo,
la creencia de que el éxito es posible mediante el trabajo duro.

> _Después, yo y otros cientos de trabajadoras fuimos conducidas a la fábrica de Longhua, lo que nos llevó casi una hora de viaje en el autobús de la empresa. Al caer la tarde, la puesta de sol bañaba las instalaciones de Foxconn con un brillo dorado. Alas 17h miles y  miles de trabajadoras de Foxconn atravesaban las puertas de la fábrica._

Longhua es el buque insignia de las bases productoras de Foxconn; allí las trabajadoras
son asignadas a los turnos diurnos y nocturnos de las cadenas de montaje. Es un nodo
central en la red de producción mundial, en la cual el ensamblaje y envío a los consumidores
globales de productos acabados funciona ininterrumpidamente trescientos sesenta y
cinco días del año- Camiones de contenedores y toros mecánicos resuenan sin cesar,
sirviendo a una red de fábricas de iPhones, iPads y otros productos electrńicos que
producen como churros, Yu se convirtió en una de las cuatrocientas mil personas que
componen la poderosa mano de obra de Longhua. La mayor parte de son también jóvenes
migrantes rurales, en los últimos años de la adolescencia o recién cumplida la veintena.

## **Llegada a la fábrica**

Hay doce grupos laborales en la empresa Foxconn, que compiten en «velocidad, calidad,
servicios de ingeniería, eficacia y creación de valor añadido para maximizar beneficios»[^25]

[^25]: Foxconn Technology Group <2008 Corporate Social and Environmental REsponsibility
annual Report> 2009 p. 8, https://web.archive.org/web/20211016042518/https://www.tribuneindia.com/news/archive/features/gurugram-to-have-new-railway-terminal-prabhu-387098

entre ellos se encuentran los +++IDPBG+++ (Integrated Digital Product Business Group)
y los +++IDSBG+++ (Innovation Digital System Business Group), que trabajan para Apple
en exclusiva, produciendo componentes y realizando el ensamblaje final del producto.
Los otros grupos empresariales confeccionan productos para Microsoft, +++IBM+++,
Samsung, Amazon, +++HP+++, Dell, Sony y otras importantes firmas.

> _Mi puesto en la cadena de montaje estaba en la +++IDPBG+++. Llegué tarde a mi primer día de trabajo. La fábrica era demasiado grande y me perdí. Así que estuve muhco tiempo buscando el taller de +++IDPBG+++. El directorio de la fábrica muestra que hay diez zonas, listadas de la +++A+++ a la +++H+++, +++J+++ y después la +++L+++ y posteriormente están subdivididas en +++A+++1, +++A+++2, +++A+++3, +++L+++6, +++L+++7, +++J+++20, y así sucesivamente. Se necesita casi una hora para recorrer caminando la distancia entre la puerta principal y la puerta norte, y una hora mas desde la puerta oeste a hasticker: haz patria, roba un libro.sta la puerta este. No sabía qué era cada edificio y desconocía lo que significaban los acrónimos en inglés que podían verse escritos por todas partes como +++FIH+++ (Foxconn International Holdings) y el +++JIT+++ [just-in-time]._

Las horas de trabajo en Foxconn son conocidas por ser miserablemente largas, como
pronto descubrió Yu. Cada cadena de producción puede tener desde unas pocas docenas
de trabajadoras hasta más de un centenar:

> _Yo era la responsable de inspeccionar las pantallas de cristal para ver si estaban rayadas. Me levantaba a las 6:30hs, acudía a una reunión de trabajo matinal no remunerada a las 7:20hs, empezaba a trabajar a las 7:40hs, iba a comer a las 11hs y después habitualmente me saltaba la comida de la tarde para trabajar horas extras hasta las 19:40hs._

_Asistía a reuniones de trabajo obligatorias no remuneradas cada día. Avisaba de
mi llegada a los encargados de la cadena a la que estaba asignada entre 15 y 20 minutos
antes de que pasara la lista de asistencias. Los jefes y encargados nos aleccionaban
acerca de la necesidad de mantener alta productividad, alcanzar los objetivos de
la producción diarios y conservar la disciplina. No existía la posibilidad de decir
que no a las horas extra. Las pausas para ir al lavabo también estaban limitadas.
Tenía que pasar mi tarjeta identificativa como empleada por los lectores electrónicos
tanto al comienzo como al final de cada turno. Debía pedir permiso a los asistentes
de los encargados de la cadena de montaje para poder levantarme del asiento._

La cadena de montaje funciona veinticuatro horas diarias de manera ininterrumpida;
la zona de producción, el taller, siempre totalmente iluminada, era visible desde
lejos.

_En marzo, me cambiaron de turno y me pasaron al de noche. Tener que comprobar las
pantallas y etiquetas de los productos me producía un intenso dolor ocular. Trabajar
12 horas diarias sin tener más que un día libre, semana sí, semana no, no deja tiempo
libre para utilizar las instalaciones de la empresa, como la piscina, o para mirar
escaparates en busca de teléfonos inteligentes dentro de los distritos comerciales
del inmenso complejo industrial._

## **La política de las «8s»**

Foxconn ha adoptado un modelo productivo aparentemente basado en el taylorismo clásico. El proceso productivo está simplificado hasta tal punto que, para llevar a cabo la mayor parte de las tareas, las trabajadoras no necesitan ningún conocimiento específico ni formación alguna. Los técnicos del departamento de Ingeniería Industrial utilizan habitualmente cronómetros e instrumentos informatizados para controlar a las obreras. Si son capaces de alcanzar las cuotas, los objetivos se ven incrementados al máximo que sea posible. En la cadena de montaje de Iphone, otra trabajadora describía cómo sus tareas eran cronometradas para determinar los segundos utilizados:

 > _Cogía una placa base de la cinta de transporte, escaneaba el logo, la colocaba en una bolsa antiestática, le pegaba una etiqueta y la colocaba en la cinta. Realizaba cada una de estas tareas en dos segundos. Cada diez segundos llevaba a cabo cinco tareas.[^26]_

[^26]: Entrevista realizada el 15 de octubre del 2011.

A medida que aumentaba la producción, las trabajadoras se enfrentan a serios problemas
si son incapaces de completar los pedidos dentro de las franjas de tiempo especificadas.
En algunos departamentos en los que las obreras normalmente disponían de un receso
de diez minutos, a aquellos que no habían logrado alcanzar los objetivos por hora
no se les permitía descansar. Las trabajadoras novatas, como Yu, reciben reprimendas
por trabajar «demasiado lentamente» en la cadena, sin importar que estén esforzándose
duramente para mantener el ritmo del trabajo «estándar». Yu rememoraba los carteles
emplazados en las paredes de los talleres y en los tramos de escaleras:

 > _«Valora la eficiencia cada minuto, cada segundo», «Logra los objetivos o el sol dejará de salir», «El secreto está en el detalle»._

La política de las «8S» de Foxconn se basa en el método de las «5s», un método de gestión japonés para mejorar la eficacia del desarrollo organizativo: _Seiri_ (clasificación), _seiton_ (orden), _seiso_ (limpieza), _seiketsu_ (estandarización de los primeros procedimientos «3s»), _shitsuke_ (disciplina, es decir, mantenimiento de los esfuerzos de _seiri, seiton, seiso y seiketsu_). A estos se les añade: _Safety_ (prevención), _saving_ (ahorro) y _security_
 (seguridad). Dichos principios son aplicados con rigidez, como Yu pudo experimentar.

## **«Crecimiento, tu nombre es sufrimiento»**

 La postura en que se sientan las trabajadoras, o en la que trabajan cuando están de pie, está tan monitorizada como el trabajo en sí mismo:

> _Debía sentarme de una manera determinada y homogénea. Las banquetas deben estar en orden, y no pueden moverse más allá de la «raya de cebra» amarilla y negra marcada en el suelo._

La ingeniería industrial de Foxconn está diseñada para que hasta los más mínimos
movimientos de las obreras estén racionalizados, planificados y medidos al máximo.
Cada trabajadora de la cadena de montaje se especializa en una tarea específica y
realiza cada hora, cada día y durante meses, movimientos repetitivos a gran velocidad.
Este «avanzado» sistema de producción elimina sensación de frescura, logro o iniciativa
respecto al trabajo. Como reflexionaba Yu:

>_Me era difícil de creer que la tarea de comprobar las pantallas tuviera un fin; que pudiera llegar a cumplimentar jamás el trabajo asignado_

Una colección de citas adornaron los muros de la fábrica, y que son observadas como
si fuesen palabra divina, son ilustrativas de la filosofía laboral del +++CEO+++ de
Foxconn, Terry Gou,  reflejo de ese espíritu empresarial y de la filosofía del trabajo
sin descanso:

> _«Crecimiento, tu nombre es sufrimiento.»_
  _«Un entorno duro es algo bueno»_
  _«La ejecución es la integración de la velocidad, la precisión y la exactitud»_
   _Terry Gou_

Gou basa su modelo de gestión en su experiencia militar e insiste en la obediencia
absoluta que deben rendir desde la base hasta los dirigentes de la cadena de mando:
«Un ejército de mil es fácil de obtener, un general es difícil de encontrar». El sistema
jerárquico de gestión está organizado de manera piramidal y con una clara escala
de mando. Los directivos formulan las estrategias de desarrollo corporativas y determinan
los objetivos anuales de beneficios. Los mandos intermedios trazan los planes de
implementación y la delegación de responsabilidades y, en los talleres, las operadoras
de producción afrontan una intensa supervisión de múltiples estratos de encargados,
incluidos los asistentes de los responsables de la cadena, los jefes de equipo y
supervisores. Yu confirmó que, mientras se preparaban para empezar a trabajar en
la cinta de producción, los encargados de las cadenas de la planta exigían a las
trabajadoras que respondiesen a la pregunta: «¿Cómo estás?», gritando al unísono:
«¡Bien!, ¡muy bien!, ¡muy muy muy bien!».

Un largo día de trabajo en obligatorio silencio, aparte del ruido de las máquinas,
es la norma. Yu recordaba:

> _Las conversaciones, las charlas  2:30:00 amistosas entre las trabajadoras no son algo muy común ni siquiera durante el descanso; todo el mundo se apresuraba a hacer cola en el comedor y a comer rápidamente. La empresa prohíbe la conversación en los talleres. En el área de la fábrica, hay cámaras +++CCTV+++ para la vigilancia de las trabajadoras, colocadas prácticamente en cada rincón. Miles de empleados de seguridad están de servicio, patrullando cada edificio y alojamiento de Foxconn. Son habituales las Áreas Especiales de Seguridad. Para poder entrar en el taller de +++iDPBG+++, tenía que pasar por varias capas de puertas electrónicas y sistemas de inspección. El sistema de acceso y entrada es muy estricto. No se nos permitía llevar teléfonos móviles ni ningún tipo de objeto metálico al taller. Si había algún botón metálico al taller. Si había algún botón metálico en mi ropa, tenía que quitarlo o, si no, no se me permitía el acceso, o ellos (los encargados de seguridad) directamente te los arrancaban de la ropa._

Con el objetivo de mantener una estricta confidencialidad respecto a compradores
como Apple o Microsoft, Foxconn mantiene un auténtico ejército de agentes de seguridad
privada, que justifica alegando a la responsabilidad contractual que le obliga a
salvaguardar los derechos de propiedad intelectual de sus clientes, y que cualquier
tipo de filtración de «información empresarial» comportaría grandes pérdidas financieras
a la compañía. En este sentido, las multinacionales tecnológicas globales transmiten
una presión extrema a los talleres chinos. Los aparatos como ordenadores portátiles,
disquetes, memorias externas y soporte de grabación multimedia están estrictamente
prohibidos en Foxconn. Su sistema de gestión y su cultura corporativa están basados
y guiados por el castigo, pese a la retórica del departamento de Recursos Humanos
de «amor y cuidado mutuos».[^27]

[^27]: Kirten Lucas, Dongjing Kang y Zhou Li: «Workplace Dignity in A Total Institution:
Examining the Experiences of Foxconn's Migrant Workforce», _Journal of Business Ethics,_
vol. 114, n.° 1, Prince George (Britsh Columbia, 2013),pp. 91-106.

Chan, Wang y Hsing[^28]

[^28]: Anita Chan y Hong-zen Wang:«The Impact of the state on Workers' Conditions:
Comparing Taiwanese Factories in China and Vietnam», _Pacific Affairs,_ n.°77, Vancouver,
2004-2005,pp.4 y 629-646; Y.Hsing: _Making Capitalism in china: The Taiwan Connection,_
Oxford University Press, Nueva York, 1998.

han investigado el estilo de gestión de las fábricas chinas orientadas a la exportación.
Yu reiteraba:

  > _No cometí ningún error con los productos, pero el jefe de cadena me culpaba a mí de todos modos. Vi a una chica a la que obligaron a mantenerse de pie en posición de firmes durante horas por, supuestamente, haber cometido un error. Las humillaciones públicas se sucedieron repetidas veces durante el mes de trabajo._

Los responsables de la cadena, ellos mismos bajo una gran presión para poder cumplir
con sus propias normas, trataban duramente a las trabajadoras para lograr los objetivos.
El joven responsable de la cadena explicaba:

  > _Si escuchamos demasiado a nuestros superiores, entonces debemos maltratar a los trabajadores que están por debajo de nosotros. Si tenemos demasiado en cuenta los sentimientos de las obreras, es posible que no completemos nuestras tareas. Cuando hay mucho trabajo, es fácil enfadarse.[^29]_

[^29]: Entrevista realizada el 17 de octubre de 2011.

En una entrevista de grupo , varias mujeres jóvenes analizaban el castigo ritual
que debían soportar.[^30]

[^30]: Entrevista realizada el 30 de marzo de 2011.

Una de las participantes expresó de manera muy clara su experiencia colectiva:

> _Algunas veces, después de acabar el trabajo, todas nosotras —más de cien personas— nos vemos obligadas a quedarnos. Esto sucede siempre que una trabajadora es castigada. Una chica se ve obligada a mantenerse en posición de firmes y a leer en voz alta una declaración de autocrítica. Debe hablar suficientemente alto como para que se la escuche. Algún encargado de la cadena preguntará si la trabajadora que está en el otro extremo del taller escucha claramente cuál ha sido el error que la chica ha cometido. Muy a menudo ellas llegan a sentirse como si, literalmente, se les cayese la cara de vergüenza. Es muy humillante. Se les caen las lágrimas. Su voz se hace cada vez más y más inaudible... Entonces el responsable grita: «Si una trabajadora pierde aunque sea un minuto (al no poder mantener el ritmo de trabajo), ¿Cuánto tiempo perderán cien personas?»._

Yu trabajó en la cadena de producción durante más de un mes y nunca hizo amigo alguno.
«Corazón con corazón, Foxconn y yo crecemos juntos», puede leerse en un anuncio rojo
brillante, colocado sobre una de las cadenas de producción. Sugiere que las trabajadoras
y la compañía se identifican unos con otros, pero, bajo la imagen de «una cálida
familia con un corazón amoroso», la vida de las obreras se ve totalmente atomizada.
La profunda soledad de las vidas desplazadas en muchas jóvenes trabajadoras migrantes
ya ha sido señalada en otros estudio.[^31]

[^31]: Tamara Jacka: _Rural Women in Urban China: Gender, Migration and Social Change,_
M.E Sharpe, Armonk (Nueva York), 2006; Hairong Yan: _New Masters, New Servants: Migration,
Development, and Women Workers in China, Duke University Press, Durham, 2008.

La experiencia de Yu ilustra la dificultad de establecer relaciones sociales significativas
en una megafábrica en la que se individualiza y enfrenta a las obreras entre sí para
cumplir con las incesantes y excesivas demandas de producción.

La práctica de Foxconn de rotar turnos noche y día no solo interrumpe el descanso
de los trabajadores, sino que también debilita su capacidad para establecer redes
de apoyo social. Como los compañeros de habitación normalmente trabajan en turnos
diferentes, es difícil cubrir la necesidad de descansar o de socializarse. Al hablar
de sus compañeras de dormitorio Yu recordaba:

> _No éramos cercanas. Las reasignaciones aleatorias de habitación rompían las amistades, aumentando nuestro aislamiento. aunque en el mismo cuarto vivían ocho chicas, éramos extrañas entre nosotras. Algunas nos mudábamos allí cuando otras se iban a otro sitio. Ninguna de mis compañeras de dormitorio era Hubei._

El padre de Yu explicaba el significado de esta experiencia:

> _Cuando ella llegó por primera vez a Shenzhen, a veces no podía entender bien cuando las otras hablaban._


Aunque el mandarín es el idioma nacional en China, los dialectos locales se utilizan
mucho entre habitantes del mismo lugar, y normalmente no son comprensibles para las
personas que no pertenecen a dicha área.

> _En la escuela, por el contrario, mis compañeros de clase y yo ejercen a menudo tiempo para el relax y la diversión, celebramos cumpleaños y cantábamos canciones. En Foxconn, cuando me sentí sola, algunas veces me conecté al servicio de mensajería en red de +++QQ+++._[^32]

[^32]: +++QQ+++ es un popular programa gratuito de mensajería instantánea, gestionado
por Tencent Holdings. el dominio de Internet _qq.com_ aloja una comunidad en rojo
de cientos de millones de usuarios.

Pero aquellos que chatean por Internet están condenados a mantenerse igualmente a
distancia entre ellos:

> _La fábrica de Foxconn en Longhua, sin ninguno de mis buenos amigos o de mis familiares cerca, es un inmenso lugar lleno de extraños._

## **La acumulación de desesperación**

> _Tras haber trabajado un mes, cuando llegó el momento de distribuir los salarios, todo el mundo recibió su tarjeta salarial de débito, pero yo no. Le pregunté al responsable de la cadena qué había pasado. Me dijo que, pese a haber trabajado en Longhua, Mi tarjeta salarial estaba en otra fábrica de Foxconn en la ciudad de Guanlan._

Antes de ser enviado a las instalaciones de Longhua, Yu había sido entrevistada en
el Centro de Contratación de Foxconn de Guanlan, cuyo parlamento de Recursos Humanos
se quedó con su ficha personal y no transfirió los documentos, por lo que no se había
abierto la cuenta bancaria de débito. En consecuencia, no recibió su salario: «No
me quedó más remedio que tomar un autobús a Guanlan sola y por mi cuenta». la fábrica
de Guanlan, que empezó a producir en verano de 2007, empleaba a 130.000 personas.
En un complejo fabril nada familiar: «Fui al bloque +++C+++10, +++B+++1, +++B+++2,
piso a piso, edificio tras edificio, para preguntar por mi tarjeta de pago».

Tras un infructuoso día en busca de la oficina adecuada, con encargados y administradores
esquivando la responsabilidad, fue incapaz de encontrar información acerca de su
tarjeta de pago. Recuerda que «Fui por mí misma de oficina en oficina, pero nadie
me señalaba la dirección correcta. Todos se deshacían de mí, diciéndome que le preguntaría
a cualquier otra persona». Nunca recibí su pago por un mes de trabajo, unos 1400yuanes,[^33]

[^33]: El equivalente a 176 euros.

que consistía en un sueldo base de 900 yuanes más bonificaciones por las horas extras.

> _Para entonces estábamos a mediados de marzo de 2010 y, después de más de un mes Shenzhen, ya había gastado todo el dinero que mis padres me habían dado. ¿Dónde podría obtener un préstamo? En este momento de crisis, se me rompió el teléfono móvil. No podía ponerme en contacto con mi primo en shenzhen, mi único vínculo con el hogar y la familia. Y no logré encontrar a nadie que me ayudese._

Los efectos acumulados del trabajo infinito en la cadena de montaje, los turnos castigadores,
la dura disciplina de la fábrica, un dormitorio sin amigas y rechazo por parte de
los encargados y administradores, agravados por el fracaso de la empresa a la hora
de proporcionarle unos ingresos, y por la imposibilidad de contactar con amigos y
familiares, fueron las circunstancias inmediatas de su intento de suicidio. Su testimonio
revela cómo se vio superada: «Estaba tan desesperanzada que mi mente se quedó en
blanco».

a las 8 hs del 17 de marzo, en plena desesperación, Yu saltó desde el cuarto piso
de su edificio de dormitorios. Después de doce días en coma, se despertó para descubrir
que la mitad de su cuerpo se habia quedado paralizado. Ahora está confinada a una cama
o una silla de ruedas.

## **«No hubo compasión ni justicia»**

«No puedes elegir tu nacimiento, pero aquí puedes alcanzar tu destino. aqui solo
necesitas soñar y subirás como la espuma», puede leerse en el eslogan de contratación
de Foxconn. La mayor empresa industrial del planeta se desveló como la antítesis
del lugar de trabajo soñado por Yu. En octubre de 2010, ocho meses después de su
hospitalización bajo una creciente presión pública, la corporación finalmente desembolsó
un único «pago humanitario» de 180.000 yuanes[^34]

[^34]: El equivalente a 22.650 euros.

para «ayudar a la familia Tian a volver a casa». En efecto, la empresa dejó de proporcionarle
apoyo médico y, a cambio de su silencio acerca de la negligencia de sus encargados,
enviado a Yu ya sus padres a casa de nuevo. En palabras del padre de Yu, «fue como
si estuvieran comprando y vendiendo un objeto. No hubo compasión ni justicia»

De todas maneras, Yu no ha perdido su voluntad de «mantenerse firme». Al regresar
a su pueblo el 30 de noviembre de 2010, escribió al Grupo de Investigación Universitario:

> _Por varias razones, viví una tragedia que ha causado un gran sufrimiento a mis familiares. Durante mi recuperación en Shenzhen, muchos estudiantes y profesores universitarios compasivos, además de tíos y tías, me dieron ánimos, preocupación por mi situación y fueron de gran consuelo para mi espíritu herido. Eso me permitió ver la vida con esperanza en el futuro[^35]_

[^35]: Traducción del chino al inglés por parte de la autora.

Un comprensivo y motivador profesor local la conducción desde su casa hasta el hospital
de Wuhan, la capital de la provincia de Hubei, donde el tratamiento para su parálisis
le permitio cepillarse los dientes y lavarse y vestirse sola sin mucha dificultad[^36]

[^36]: Ai Xiaoming: «The Citizen Camera», entrevista realizada por Tieh-Chih Chang
y Ying Qian, _New Left Review,_ n° 72, Londres, noviembre-diciembre de 2011, pp.63-79.

La combinacion entre el tratamiento continuado y su determinacion personal le ha
permitido sentarse sola y lograr tener un movimiento limitado en el pie derecho.

Durante tres años, Yu terapia recibió en su hospital local, con acupuntura y masajes.
Al mismo tiempo, comenz a coser zapatillas de algodn de andar por casa para obtener
un pequeño ingreso que le ayudará a pagar los costes de su rehabilitación. Los internautas
chinos hicieron famosas sus coloridas zapatillas y el «renacido fénix Tian Yu»,[^37]

[^37]: _Shenzhen Daily,_ 22 de abril de 2011.

pero la dura realidad es que el pueblo esta a mas de cinco horas construidas desde
Wuhan, limitando seriamente la posibilidad de una eventual empresa familiar de artesanía.
Pesea esto, Yu mantiene su visión optimista.

> _Ya no puedo ser una obrera migrante, tampoco puedo trabajar en la fábrica. Durante mi tratamiento, pensé que después de que me dieran de alta en el hospital, independientemente de si pudiera mantenerme en pie o no, debía tener mis propios ingresos y ser capaz de cuidar de mí misma sin depender de mis padres y amigos, y ser una persona útil a la sociedad, no una carga inútil. Gracias por su preocupación. Os deseo unos estudios y trabajos exitosos._

La desesperación de Yu es emblemática y un reflejo de la miríada de problemas laborales
en «los talleres electrónicos del planeta». Mientras Yu se sometía a un intensivo
tratamiento médico en el hospital de Shenzhen, más de una docena de jóvenes trabajadores
de Foxconn vieron aplastados sus sueños del mismo modo que ella. ¿cómo respondió
Foxconn a la explosión de suicidios en el trabajo y al escrutinio crítico de los
medios de masa?

En una entrevista en un medio de masas, el +++CEO+++ de Foxconn, Terry Gou, dijo
los «problemas emocionales» de las obreras chinas, a la vez que se sintió compelido
a asumir la responsabilidad corporativa:

> _Si un obrero en taiwán comete suicidio debido a problemas emocionales, su empleador no se verá señalado como culpable por ello, pero a nosotras se nos cuestiona nuestra tarea en China porque las trabajadoras viven y duermen en nuestras residencias_[^38]

[^38]: _The Straits Times, 11 de junio de 2011.

La empresa actuó velozmente frente a la ola de suicidios y, para controlar el daño
a su imagen, anunció planes de mejora de las condiciones de trabajo. Comenzaron a
exigir a todos los solicitantes de empleo que completen una prueba con treinta y seis
preguntas, aplicando un enfoque culpabilizador de las víctimas. Aquellos con «escasa
capacidad para manejar los problemas personales» y los «espíritus frágiles» había
sido el origen de los problemas. Foxconn dejó intactas las estructuras subyacentes
de gestión de las relaciones laborales: Las presiones para ir más rápido y los niveles
ilegales de horas extras obligatorias o la humillacion de los trabajadores en los
talleres. Quedaron intactos todos los aspectos de régimen fabril que son claves para
comprender las profundas frustraciones de las jovenes obreras y las continuadas crisis
laboral en la empresa.

## **El pliego «antisuicidio»**

En mayo de 2010, el director de Recursos Humanos de Foxconn intentó hacer que los
trabajadores firmaron un pliego «antisuicidio», que contenían una cláusula de exención
de responsabilidades para la empresa:

> _En caso de lesión o muerte sucedida por causas por las cuales Foxconn no puede ser considerado responsable, acepto, por la presente, que se resuelva el caso según los procedimientos legales y reguladores de la empresa. Ni yo ni mis familiares buscaremos obtener ningún tipo de compensación extra más allá de la fijada por la ley, impidiendo así que se arruine la reputación de la compañía y mejorando su funcionamiento estable._[^39]

[^39]: China Central TV, 28 de mayo de 2010.

La «carta de aceptación» del no-suicidio no solo buscaba limitar la responsabilidad
de Foxconn, sino asegurar también que, ante futuros suicidios, la responsabilidad
recaeera en la trabajadora como individuo. Debido a la intensa critica por parte
de las empleadas y amplias capas de la sociedad, Foxconn excluyó este requisito administrativo.

El mismo mes de denegación de responsabilidad, la oleada de suicidios logró su
máximo más espantoso cuando siete jóvenes trabajadores migrantes intentaron suicidarse,
con el resultado de seis muertes. como medida de emergencia, Foxconn colocó redes
de seguridad alrededor de los tejados, en ambos lados de los pasillos, y todas las
las ventanas fueron cubiertas con alambre y férreamente cerradas. Eran «medidas correctivas»
adoptadas para evitar que las trabajadoras salten hacia su propia muerte. Ahora
hay instaladas redes antisuicidio en los dos principales complejos fabriles de Foxconn
en Longhuaa y en Guanlan —donde hay alojados más de cincuenta mil trabajadores en
un entorno densamente poblado en el cual solo habitan trabajadores de la empresa—,[^40]

[^40]: Foxconn Technology Group, 11 de octubre del 2010.

_Me siento realmente constreñida en Foxconn desde los suicidios. Ahora por todas
partes hay redes de seguridad, las han colocado por todos lados, y realmente te provocan
una sensación de constricción, estoy deprimida._[^41]

[^41]: entrevista realizada el 3 de diciembre del 2012.

El sistema de alojamiento de la fábrica alberga una cantidad inmensa de fuerza de
trabajo, formado por migrantes internos que no disponen de redes de apoyo familiar
ni en vida en común.[^42]

[^42]: Pun Ngai y Chris Smith «Poner el proceso laboral transnacional en su lugar:
El régimen de trabajo dormitorio en la China possocialista», _Trabajo, empleo y sociedad_
vol. 21 n.° 1, Londres, 2007, pp. 27-45; J. Chan y N. Pun, 2010, _op.cit._

Lo que es sorprendente es que todas estas «Medidas de seguridad administrativas para
las residencias de Foxconn» fueron establecidas solo tras la publicidad negativa
que siguió a los suicidios. Algunas internautas cuelgan imágenes de jirafas enjauladas
del zoológico como metáfora de la experiencia de las trabajadoras confinadas en dormitorios
sellados por alambre de espino, y de la extrema deshumanización de sus vidas laborales.[^43]

[^43]: _Financial Times,_ 20 de enero del 2012.

## **¿Qué hace el sindicato de la empresa?**

Para entender por qué ningún miembro del sindicato oficial de la empresa visitó a
Yu en el hospital o se ofreció a ayudarla, es necesario analizar la naturaleza de
la Federación Nacional de Sindicatos de China (+++ACFTU+++). A partir de 2003, el
nuevo gobierno chino presidido por Hu Jintao y wen Jiabao promovió una campaña de
sindicación centrada en las empresas privadas y de capital extranjero.[^44]

[^44]: Mingwei Lui: «"Donde hay trabajadores, debería haber sindicatos", Sindicato
Sindicalización en la era del creciente empleo informal», en Sarosh Kuruvilla, Ching
Kwan Lee y Mary E Gallagher (eds.):_Del tazón de arroz de hierro a la informalización: mercados,
Workers, and the State in A Changing China,_ Cornell University Press, Ithaca (Nueva
York), 2011, págs. 157-172.

En enero de 2010, la federación tenía una afiliación total de 258 millones de personas,
de los cuales el 36% —que se suponían 94 millones— eran migrantes procedentes de
zonas rurales.[^45]

[^45]: Xinhua, 7 de enero de 2012.

Estas cifras superan con creces la afiliación en todo el planeta de la Confederación
Sindical Internacional (+++CSI+++) —con sus 175 millones de trabajadores distribuidos
en 156 países y territorios, excluyendo China—.[^46]

[^46]: Los datos son de 2012. La última lista mundial de afiliados a la +++CSI+++,
de junio de 2018 es de 207.796.315 personas, https://web.archive.org/web/20211011043339/https://www.ituc-csi.org/IMG/pdf/18_07_10_list_of_affiliates_ac.pdf (última consulta: octubre
de 2018).

En junio de 2010, Existían sindicatos en el 87,7% de las compañías no estatales,
incluyendo diferentes tipos de entidades empresariales y contratistas.[^47]

[^47]: +++ACFTU+++(Federación Nacional de Sindicatos de China):«Terminación de 2012
Targets on Union Construction in the Non-State Enterprises», n.° 19, Pekín, julio
de 2012. Versión impresa (en chino).

Estos establecen un notable contraste con Estados Unidos donde, en las últimas décadas,
la presencia de sindicatos en el sector privado se ha hundido hasta ocupar un porcentaje
muy pequeño tanto de la mano de obra industrial como en el sector de servicios.[^48]

[^48]: Jennifer Jihye Chun: _Organización en los márgenes: la política simbólica de
Labor in South Korea and the United States,_ Cornell University Press, Ithaca (Nueva
York), 2009.

Pese a las impresionantes cifras de afiliación del sindicato chino, su dependencia
operativa y financiera respecto a la administracion mina profundamente sus capacidades
de representar a las trabajadoras,[^49]

[^49]: Rudolf Traub-Merz y Kinglun Ngok (eds.): _Democracia industrial en China: Con
Estudios adicionales sobre Alemania, Corea del Sur y Vietnam,_ China Social Sciences Press,
Pekín, 2012, https://web.archive.org/web/20211011045224/http://library.fes.de/pdf-files/bueros/china/09128/09128-english%20version.pdf (última consulta: octubre del 2018).

y el sindicato fabril de Foxconn no es ninguna excepción.

«La comunicación abierta y directa entre empleados y gerentes es el método más eficaz
para identificar y resolver los problemas en el trabajo y construir un negocio armónico»,
afirmaba un informe de empresa de Foxconn publicado poco después de aprobarse la
Ley de contratos laborales de China.[^50]

[^50]: Ley de Contratación laboral china. El texto mencionado aparece en Foxconn
Grupo de Tecnología, _op. cit., p. 16. Acerca de los requisitos de esta nueva ley,
véase Mary E. Gallagher y Baohua Dong: «Legislating Harmony: Labor Law Reform in
China contemporánea», en _Del tazón de arroz de hierro a la informalización..., op. cit., pp.
36-60; Haiyan Wang, Richard P. Appelbaum, Francesca Degiuli y Nelson Lichtenstein:
«Nueva Ley de Contratos Laborales de China: ¿Se está moviendo China hacia un mayor poder para los trabajadores?»,
_Tercera palabra trimestral,_ vol. 30, n° 3, Routledge, Londres, 2009, pp. 485-501; Virginia
E. Harper Ho: «¿De los contratos al cumplimiento? Una mirada temprana a la implementación bajo
La nueva legislación laboral de China», _Columbia Journal of Asian Law,_ vol. 23, n° 1,
Nueva York, 2009, pág. 34; Jenny Chan: «¿Progreso significativo o reforma ilusoria? analizando
China's Labor Contact Law», _Nuevo Foro Laboral, vol. 18, n.° 2, Nueva York, 2009, págs.
43-51.

Aunque desconocido para el público en general, desde 1988 hasta el 2006, Foxconn,
como muchas otras compañías inversoras de capital extranjero, evadió sus responsabilidades
elementales y fracasó en la tarea de organizar un sindicato que reforzase la comunicación
entre las trabajadoras y la direccion. En el momento en que el Gobierno chino se
vio forzado a intervenir de manera directa en la movilización, fue cuando la fábrica
de shenzhen Longhua terminó sindicándose, el último día de 2006.[^51]

[^51]: +++IHLO+++, 2 de enero de 2007.«»

Acto seguido, el +++CEO+++ de Foxconn , Terry Gou, tomó el control sobre el recién
creado sindicato, designando a Chen Peng, su asistente personal, como la representante
sindicato.[^52]

[^52]: Grupo de tecnología Foxconn, _op. cit., p. 17

Como era de esperar, a partir de aquel momento, la organización sindical no investigó
los factores ambientales responsables de los altos niveles de estrés y depresión
de las obreras en el centro de trabajo. En su lugar, la señora Peng pronunció en
público comentarios insensibles como que «el suicidio es ridículo, irresponsable,
supone un sinsentido y es algo que debería ser evitado».[^53]

[^53]: _China Daily, 19 de agosto de 2010._

El sindicato de Foxconn tiene la apariencia de un sindicato y actúa como si lo fuera,
pero ha fracasado en proteger la salud, los derechos fundamentales y la dignidad
de las trabajadoras. Así, en una de las empresas de mayor tamaño y mayoríndice de
sindicalización del mundo —el 90% de sus 1.4 millones de empleadas están afiliadas—,[^54]

[^54]: Foxconn Technology Group, 7 de julio de 2012.

Las trabajadoras carecen de canales de comunicación fiables en sus centros de trabajo,
mediante los cuales pueden alcanzar la voz, proteger sus derechos u organizarse para
la negociación colectiva. eso mismo les sucede a los más de 270 millones de migrantes
originario del campo, empleados de dura condiciones en pequeñas y grandes instalaciones
a lo largo de toda China.[^55]

[^55]: _Xinhua, 22 de febrero de 2013.

## **¿Qué hace el Gobierno?**

A raíz de la creciente preocupación internacional por los suicidios, todos los niveles
del Gobierno chino acabaron comunicando su preocupación. El 26 de mayo de 2010, tras
el «duodécimo salto»[^56]

[^56]: Se refiere al duodécimo intento de suicidio, llevado a cabo el 26 de mayo
de 2010 en la planta de Shenzhen. Se contaban entonces doce intentos de suicidio,
con el resultado de diez muertos y dos supervivientes con secuelas graves; ver
https://web.archive.org/web/20211011045515/https://actrav-courses.itcilo.org/a3-55204/a3-55204-presentations/labour-market-zeng/at_download/file (última consulta : octubre de 2018).

Un portavoz del gobierno municipal de Shenzhen anunció que el gobierno mejoraría
«las condiciones de las trabajadoras y la gestión de la compañía» y, poco después
de otro intento de suicidio el 27 de mayo, el secretario provincial del Partido Comunista
de Cantón, Wang Yang, afirmó que

> _...el partido, las organizaciones gubernamentales y Foxconn deben trabajar de manera conjunta y adoptar medidas efectivas para prevenir que puedan volver a suceder tragedias similares._[^57]

[^57]: Beijin Review, 12 de julio de 2010.

Sin embargo, las medidas conjuntas específicas jamás se explicarán. Desde el Gobierno
central, el primer ministro Wen Jiabao urgió a los encargados ya las empresas a
tratar a las obreras migrantes como «nuestras trabajadoras, nuestros hijos».[^58]

[^58]: Xinhua, 15 de julio de 2010.

Aun así, en lugar de investigar y prevenir las razones estructurales de los suicidios,
los delegados locales del Gobierno chino se dedicaron a prohibir los informes «negativos»
Acerca de Foxconn:

> _28 de mayo de 2010: Acerca del incidente de Foxconn, en Internet, más allá del informe interno genérico, no debe haber ninguna otra información. Todo el contenido relacionado anterior al duodécimo salto debe ser bloqueado. Todas las páginas webs deben completar la tarea de limpieza esta noche. No debe haber chapuzas[^59]_
_29 de mayo de 2010: En la página principal de las páginas web de noticias y centros de información, blogs y microblogs,no debería haber noticias relacionadas con Foxconn excepto las de las fuentes oficiales[^60]_

[^59]: China Digital Times, 30 de mayo de 2010.

[^60]: _Ibíd._

A primera hora del 30 de mayo de 2010, fue bloqueado el blog de estudiantes del Grupo
Universitario de Investigaciones, cuya música de fondo estaba compuesta por la canción
_Grief_, dedicado a las víctimas y familias de las trabajadoras de Foxconn. La preocupacion
estatal por el bienestar de las obreras seria sacrificada una vez mas en el altar
de la conveniencia, evidenciando que las prioridades eran controlar a la plantilla
ya los medios de masas para proteger así a Foxconn.

Los gobiernos locales, con los sindicatos como parte integral de su estructura, facilitan
las mismas actividades empresariales que intensifican los malestares laborales [^61]

[^61]: Eli Friedman, Ching Kwan Lee: «Observando el mundo del trabajo chino: 30 años
Retrospectiva», _British Journal of Industrial Relations,_ vol. 48, n° 3, Londres,
2010, págs. 507-533.

Pese a las reformas legales, las leyes estatales y las afectaron para
proteger a las trabajadoras, a menudo implementadas con un sesgo claramente favorable
a los intereses de la empresa. Amparados en las políticas de descentralización fiscal
y administrativa de china, los gobiernos provinciales y los niveles administrativos
compiten entre abajo si para impulsar el crecimiento economico, animando a las
corporación a invertir en sus territorios,[^62]

[^62]: Chin Kwan Lee:_Contra la ley: Protesta laboral en Rustbelt y Sunbelt de China,_
Prensa de la Universidad de California, Berkeley (California), 2007, págs. 16-21.

provocando un conflicto institucional entre la legitimidad legal y la acumulación
local.

## **El «teléfono de emergencia» de la empresa**

Hasta que se produjo la oleada de suicidios, Foxconn nunca había elevado el salario
base de las trabajadoras recién contratadas por encima del mínimo legalmente establecido.
El gigante de la electronica, al igual que muchos otros patronos privados en vez
de revertir sus beneficios en mejoras salariales o de las condiciones de trabajo,
ha convertido la mayor parte de aquellos en fondos empresariales, inversiones y reinversiones.
Sin embargo, para seguir siendo competitivos en el mercado de trabajo, Foxconn se
vio obligada eventualmente a ofrecer a las obreras sueldos más elevados. Las trabajadoras
rurales recién contratadas siguen oscilando entre los 16 y los 25 años, y encajan
llegando desde los «cinco lagos y cuatro mares», los pueblos y ciudades chinos más
remotos. «Unimos todo el talento del país, trazamos perspectivas espléndidas», puede
leerse en uno de los carteles de contratación de Foxconn.

Sin embargo, tras el incremento salarial de 1200 yuanes al mes[^63]

[^63]: El equivalente a 153 euros.

o del 9% respecto al salario mínimo local de la ciudad de Shenzhen,[^64]

[^64]: _Shenzhen Daily,_ 10 de junio de 2010. Foxconn también elevó las cuotas de
producción, exigiendo una mayor intensidad laboral y, en algunos casos, horarios más
largos. Una trabajadora responsable de procesar las carcasas de los telefonos moviles
informar:

  > _El objetivo diario productivo estaba fijado anteriormente en 5210 piezas al día. En julio de 2010, fue elevado un 25% hasta 6400 piezas diarias. Estoy totalmente agotada._[^65]

[^65]: Entrevista realizada el 30 de marzo de 2011.

durante las temporadas de mayor demanda, las jornadas laborales duraban doce horas,
lo que incluye cuatro horas de trabajo extra obligatorios e ilegales. una chica
de 17 años decía:

  > _Cada día me veo presionada para ir más y más rápido, nos fuerzan a alcanzar todos y cada uno de los aumentos de cuotas. Es una vida laboral muy dura. Cuando mi cuerpo consigue acostumbrarse a la jornada diurna, entonces tengo que cambiar al turno de noche. Me encuentro realmente mal cuando tengo la menstruación.[^66]_

[^66]: Entrevista realizada el 11 de noviembre de 2010

En el momento culmen de los suicidios, Foxconn instaló un «teléfono de emergencia»,primero
en las plantas de Shenzhen y posteriormente en todas las fábricas en territorio chino.
Durante la fase piloto, en mayo de 2010, la empresa informó de que se habían atendido
710 llamadas, «incluyendo dieciséis que afirmaban plantearse cometerse suicidio».[^67]

[^67]: _Xinhua,_ 25 de mayo de 2010

Las trabajadoras pueden llamar al 78585 —la transcripción fonética de la numeración
en mandarín es: «por favor, ayúdame, ayúdame»—; sin embargo, es muy cuestionable
que las obreras bajo coacción aceptan informar confidencialmente y sin miedo, a través
de una línea directa situada en el mismo Centro de Atención al Empleado y gestionado
por la empresa y su servicio de asesoramiento. Cuando algunos llamaron para quejarse
de las excesivas horas extra, sus identidades fueron trasladadas directamente a
la administración de la empresa. Desde el momento en que las trabajadoras fueron
conscientes de esa ruptura de la confidencialidad, comenzaron a vacilar a la hora
de pedir ayuda. En otro caso, cuando una empleada anunció conflictos con sus encargados
de línea, en lugar de proporcionarle ayuda y apoyo, se le consiguió que renunciase
un puesto. La mayor parte de las trabajadoras entrevistadas se mofan del «centro
de cuidados» y lo denominan «centro de supervisión». No tienen confianza alguna en
la «línea interna de emergencia de la administración»

## **Vida y trabajo en una nueva generación**

Desde 2008 hasta 2012, los niveles mínimos salariales han registrado una media de
crecimiento anual del 12,6%,[^68]

[^68]: _Informe sobre China,_ 4 de enero de 2013.

en parte debido a las crecientes protestas de las trabajadoras durante un período
de escasez laboral, y también como un intento de estimular el consumo local. La actual
cohorte de obreras jóvenes, comparada con la generación de trabajadoras migrantes
más mayor, posee más acceso a noticias e información a través de las tecnologías
móviles y tiene mayores expectativas de protección de sus derechos e intereses. Exigen
condiciones laborales decentes y separadas elevadas aspiraciones de vivir «el sueño
chino» en la ciudad. Vista desde el pueblo, la ciudad parece ser el lugar donde todo
puede suceder.[^69]

[^69]: Jenny Chan: «¿Quién habla por los trabajadores de China?», _Labor Notes,_ 29 de mayo de
2013, https://web.archive.org/web/20211011045701/https://www.labornotes.org/blogs/2013/05/who-speaks-china%E2%80%99s-workers (última consulta: octubre de 2018).

Frente a estas altas expectativas de la segunda generación de migrantes rurales,
cuyo nivel educativo es más elevado que el de sus predecesores, la realidad del trabajo
en la cadena de montaje de Foxconn y en otras fábricas choca y se convierte en sorpresa.

Estas jóvenes obreras —muchos de ellos con educación técnica y habilidades vocacionales
básicos— no solo se encuentran con ejemplos de bajo salario, sino que pronto descubren
que no existe una prometedora ruta de ascenso, vía educación superior, mientras estemos
en el seno de la empresa. En la ciudad de Foxconn, las trabajadoras sufren una profunda
Ansiedad respecto a su futuro. Conscientes de los altos niveles de desgaste y abandono,
algunos intentan ahorrar lo suficiente de sus magros salarios para abrir un pequeño
negocio, ya sea en la ciudad o en algun otro lugar. La mayor parte fracasa rápidamente.
Las falladas expectativas de adquisición de habilidades y conocimientos y de ascenso
dentro del sistema de la fábrica, la ausencia de derechos laborales y ciudadanos
básicos, y la frustración que acompaña al ir y venir entre el campo y la ciudad han
insuflado en muchos de ellos rabia y un sentimiento de impotencia.[^70]

[^70]: Pun Ngai y Huilin Lu: «Proletarización inconclusa: yo, ira y clase
Acción entre la Segunda Generación de Trabajadores Campesinos en la China Actual»,_Moderno China, _ vol. 36, n° 5, Thousand Oaks (california), 2010, pp. 493-519; Jieh Min Wu: «Trabajadores Migrantes Rurales y Ciudadanía Diferencial de China: Una Comparación Institucional Análisis», en Martin King Whyte (ed.): _One Country, Two Societies: Rural-Urban Inequality en Contemporary China,_ Harvard University Press, Cambridge, 2010 pp. 55-81; Marca Selden y Jieh-Min Wu: «El Estado chino, proletarización incompleta y estructuras de la desigualdad en dos épocas», _The Asia-Pacific Journal,_ vol. 9, n.° 5, 2011, págs. 1-35, https://web.archive.org/web/20211011050240/https://apjjf.org/2011/9/5/Mark-Selden/3480/article.html

Mientras tanto, la posición de la clase obrera está en constante cambio como resultados
de los movimientos demográficos. Como consecuencia de la politica nacional del hijo
único —implementada desde finales de la década de 1970—, pero también de la hipermovilidad de las trabajadoras, Foxconn y otras compañías han comenzado a experimentar incipientes señales de escasez de mano de obra en las ciudades costeras y del interior.[^71]

[^71]: Baochang Gu y Yong Cai: «Fertility Prospects in China», _Expert Paper_ n.°
2011/14, División de Población de las Naciones Unidas, Nueva York, 2011, https://web.archive.org/web/20120526153616/https://www.un.org/esa/population/publications/expertpapers/2011-14_Gu&Cai_Expert- paper.pdf (última
consulta: octubre de 2018); John Knight, Quheng Deng y Shi Li: «El rompecabezas de los inmigrantes Escasez de mano de obra y excedente de mano de obra rural en China», _China Economic Review,_ n.°22, Nueva York, 2011, págs. 585-600.

Frente a la creciente exigencia de aumento de los salarios, Foxconn se ve sujeta
a las presiones implicables de Apple y otros gigantes electrónicos para mantener
el toyotismo _just-in-time_ y la competición con otros fabricantes en la lucha por
los contratos. La crítica es el desarrollo y el alcance del poder de las
obreras tanto bajo las condiciones globales como de China.

## **¿Un momento para el cambio?**

El auge de Foxconn, hasta convertirse en líder de la fabricación electrónica, ha
sido encumbrado como el exito de la industria exportadora china. Sin embargo, la
experiencia vivida por Yu ilustra como la obsesion de la compania con los objetivos
productivos, el crecimiento empresarial y los beneficios que habitualmente conlleva el
sacrificio de las necesidades humanas mas elementales de sus trabajadores.

Los sociólogos chinos han alertado de que las tragedias de Foxconn deben ser vistas
como «claros gritos de ayuda por parte de la nueva generación de trabajadoras migrantes»,[^72]

[^72]: _Sociólogo chino,_ 18 de mayo de 2010.

mientras que los académicos taiwaneses han hecho un llamamiento a acabar con la «disciplina
militar» en las fábricas y en las residencias fabriles.[^73]

[^73]: _Académicos taiwaneses, 13 de junio de 2010_

En el plano internacional, los preocupados académicos hicieron un llamamiento al
Gobierno chino ya los líderes industriales para «establecer estándares laborales,
niveles de salubridad ambiental y ocupacional, además de trabajar por la dignidad
de las trabajadoras» en las fábricas proveedoras[^74]

[^74]: Concerned International Scholars, 8 de junio de 2010.

La respuesta de la dirección de Foxconn y del sindicato ha sino un incremento de
la presión para aumentar la productividad —instalando brazos robóticos en algunos
cadenas de montaje— y un endurecimiento del control laboral. Aun así, las trabajadoras
están cada vez más envalentonadas y dispuestas a llevar a cabo acciones contra la
dirección, pese a los riesgos de despido. Como respuesta, el Estado ha permitido
a menudo huelgas y protestas a pequeña escala,[^75]

[^75]: El derecho a huelga fue garantizado por la constitución china en 1978, solo
para ser eliminado de nuevo en el redactado de la Constitución de 1982. Las huelgas
laborales quedan en un limbo; no están permitidos, pero tampoco prohibidos.

pero reaccionando vigorosamente para evitar que la acción organizativa de las trabajadoras
se extenderá en el tejido industrial, alcanzará los alrededores urbanos o se materializará
en la formación de sindicatos independientes.[^76]

[^76]: Elizabeth J. Perry: _Desafiando el Mandato del Cielo: Protesta Social y
State Power in china,_ ME Sharpe, Armonk (Nueva York), 2002; Ching Kwan Lee: «Caminos
of Labor Activism», en Elizabeth J. Perry y Mark Selden (eds.): _Chinese Society:
Change, Conflict and Resistance,_ Routledge, Londres, 2010, pp. 57-59: Elizabeth
J. Perry y Mark Selden: «Introducción: Conflicto reformista y resistencia en la
China», en _ibíd._

Existen señales del surgimiento de un momento de cambio, alentado por la colaboración
y la union de esfuerzos para reforzar el poder de las trabajadoras, llevadas a cabo
por las propias empleadas, junto a estudiantes y académicos preocupados por la materia
y activistas pertenecientes a movimientos de obreras trasnacionales. Para lograr
transformaciones duraderas, no es suficiente —nunca lo será— con limitar el discurso
a los propietarios de las corporaciones y el Estado chino, quienes han colaborado
mano a mano para proteger el sistema vigente. En definitiva, es esencial que las
trabajadores cuentan con su propia voz y participan directamente en la monitorización
y la negociación de sus condiciones laborales y de vida. Con las nuevas fabricas
comenzando a funcionar en las pujantes ciudades del interior, una proporción sustancial
de las trabajadoras rurales está siendo reclutada dentro de su propia provincia de
residencia e incluso en su misma ciudad o distrito. Las formas de resistencia de
las trabajadoras migrantes cambiarán conforme trabajarán cada vez más cerca de su lugar
de origen y pueden apoyarse en las redes sociales de ámbito local. Con un mayor sentimiento
de legitimidad asociada a la pertenencia a lo local, y tal vez con más recursos e
intereses, surgirán nuevas formas de poder de la clase trabajadora tanto en
las fábricas globales como en las comunidades de trabajadoras.

### _Jenny Chan_

## **«Me tragué una luna de hierro»**

> Xu Lizhi, trabajador migrante y poeta

En 2010 Xu Lizhi se marchó —desde su hogar en la rural Jienyang (Cantón)— a trabajar
en Foxconn —en la fábrica de componentes electrónicos de Shenzhen— comenzando así
una vida en la cadena de montaje. Desde 2012 hasta febrero de 2014 más de treinta
de sus escritos fueron publicados por el diario interno _Foxconn People_, entre los
que se produjeron poemas, ensayos, críticas cinematográficas y artículos sobre noticias.
Xu publicó estos escritos en una entrada de blog personal titulada «The Maduration
Given to Me by a Newspaper», indicando su gratitud por la cobertura de esta plataforma
para el desarrollo de sus aspiraciones literarias. La primera vez que su amigo Zheng[^77] leyó las poesías de Xu se quedó anonadado de que el joven poseyera tanto talento.
A partir de entonces Zheng siempre buscaba los escritos de Xu en el periódico. Zheng
tenía la impresión de que Xu era un chico tímido, «de pocas palabras, pero no silencioso»; «Xu afirmaba sus convicciones, pero parecía bastante solitario, tenía bastante aire de poeta». Cuando Zheng se enteró del suicidio de Xu todas sus vacaciones —de una semana de duracion— en la Fiesta Nacional china se cubrieron de dolor y no pudo salir de su casa durante los dias.

[^77]: Citado aquí con pseudónimo.


La mayor parte de los poemas de Xu eran descripciones de la vida en la cadena de montaje.
En «Workshop, My Youth Was Stranded Here» describía sus condiciones de vida en aquel momento:

> _Junto a la cadena de montaje, decenas_
_de miles de trabajadores (dagongzhe)[^78]_
_se alinean como palabras de una_
_página._
_«Más rápido, daos prisa», entre ellos,_
_escucho ladrar al supervisor._


[^78]: Desde la década de 1990 hasta los 2000 el término «dagongzhe» se refería principalmente a los asalariados migrantes llegados desde el campo para trabajar en empleos precarios. Este término se contrapone al que se usa para denominar a los empleados urbanos con puestos de trabajos estables —sobre todo en empresas estatales— a quienes se denominan como «gongren», expresión heredada de la era socialista y referida a las «obreras» urbanas con empleos fijos en empresas públicas o cooperativas. En los últimos años, sin embargo,ambos términos se han convertido en algo casi intercambiable — como reflejo entre el acercamiento de las condiciones de los diferentes tipos de trabajadores—, por lo que aquí traducimos, _dagongzhe_ simplemente como «trabajadores» —aunque irá acompañado de «migrante» cuando se considere necesario para su clasificación—. En general, el término refleja la ambigüedad actual del status de las migrantes obreras en China —diferenciados del resto, pero no reconocidos como urbanos ni tampoco como campesinos—, similar al ambiguo status que tiene la mano de obra migrante _internacional_ en otros países; por ejemplo, quienes trabajan en Estados Unidos, provenientes de áreas rurales de México.
Para un análisis de estos dos términos, vease Prolposition: «China's Migrant Workers»,
12 de enero de 2010, https://web.archive.org/web/20200612140130/https://libcom.org/history/chinas-migrant-workers (última consulta: octubre de 2018), una traducción aparecida en el suplemento en alemán dela revista _Wildcat_, nº 80, invierno 2007- 2008; y la introducción de Pun Ngaia _Hecho en China: Trabajadoras de fábrica en un lugar de trabajo global_, Duke University Press,Durhan y Londres, 2005. _(N. de la T.)

Él sintió que...

> _Una vez que ha entrado en el taller la_
_única elección es la sumisión_

... y que su juventud se escapó fríamente, por lo que solo podía...

> _Mirar cómo era triturada día y noche,_
_presionada, pulida, moldeada en unos cuantos_
_miserables recibos, llamados «salarios»._

Al principio, Xu Lizhi tuvo dificultades para adaptarse al cambio constante entre
turnos diurnos y nocturnos. En otro poema, se describe a sí mismo en la cadena de
Montaje:

> _Aquí, clavado junto a la línea de montaje,_
_manos al vuelo, cuantos dias claros,_
_cuántas noches negras, así tal cual, de pie_
_me duermo._[^79]

[^79]: Este extracto corresponde al poema «Así es como de pie me duermo», de la p.85.
Para él hemos utilizado la traducción directa del chino hecha por Tyra Díez. el resto
son traducciones de la versión francesa.

Describió su vida laboral como agotada

> _Fluyendo por mis venas, alcanzando_
_finalmente la punta de mi bolígrafo,_
_enraizándose en la piel, estas palabras_
_solo son legibles por los corazones de los_
_trabajadores migrantes._

Aunque solo vivió en Shenzhen durante unos pocos años se identificó profundamente
con la urbe. «Todo el mundo desea echar raíces en la ciudad», explicaba, pero la mayor
parte de los trabajadores migrantes —dagong— poetas escriben durante algunos años
y después regresan al campo, se casan y tienen hijos. Xu quería escapar a ese destino.
Intentó instalar un puesto callejero con un amigo, pero fracasó. También trató de
que le transfiriesen de la cadena de montaje a un puesto en logística, en el que
disfrutaría de una mayor libertad. Sabía que muy pocos serían los poetas que lograron
salir de allí (siendo poetas):

> _Tenemos que luchar constantemente por_
_nuestras vidas; es dificil ir mas alla de eso._

En febrero de ese año Xu se despidió de su trabajo en Foxconn y se mudó a Suzhou,
en Jiangsu. Su amigo explicaba que la novia de Xu trabajaba allí, pero aparentemente
las cosas no les fueron bien. Le dijo a Zheng que habia tenido problemas para encontrar
trabajo pero no dio detalles de lo que había sucedido. Medio año después volvió a
mudarse a Shenzhen. En una entrevista anterior Xu había dicho que amaba esta ciudad,
que disfrutó de la Central Book Mall[^80]

[^80]: La Central Book Mall o Shenzhen Book City es la librería más grande del mundo.
_(N.de la T.)_

y las bibliotecas publicas. Si regresaba a su hogar —en la rutal Jieyang— no encontraría
más que algunas pequeñas librerías «e incluso si intentase pedir libros por Internet,
no podrían ser entregados» (por lo remoto de su dirección).


Debido a su amor por los libros la primera oferta de trabajo a la que se postuló
tras su regreso a Shenzhen, a principios de septiembre, fue en la Central Book Mall.
Zheng recuerda que Xu le había dicho mientras trabajaba en Foxconn que su sueño era
convertirse en bibliotecario. Desafortunadamente, no obtuve el empleo, y Zheng piensa
que eso supuso una gran decepción para él. Dos años antes, cuando Foxconn había abierto
una bolsa de empleo para la biblioteca de los trabajadores de la empresa, Xu habia
presentó una solicitud para el puesto de bibliotecario, pero también allí fue rechazado.
Xu se estaba quedando sin dinero así que tras estas decepciones volvió a Foxconn
y comenzó a trabajar, el 29 de septiembre, en el mismo taller que anteriormente lo
había hecho. Eso debería haber supuesto una segunda oportunidad, pero no lo fue.
Aquella noche le comentó a Zheng, durante una conversación electrónica, que alguien
le había encontrado otro empleo, por lo que quizás dejaría Foxconn de nuevo, pero
Zheng no le dio mayor importancia, ya que acaba de retomar su puesto y supuso que
no se marcharía de inmediato. Lo siguiente que Zheng supo de Xu fue dos días más
tarde, cuando alguien publicó la noticia del suicidio de Xu en WeChat. Zheng no podía
creérselo: «¿No acabábamos de hablar dos noches antes?». Más tarde Zheng supo que
Xu se había suicidado a la mañana siguiente de su conversación, no dos días después
como habian informado los medios.

## **Desmintiendo los rumores en la red de que Xu era huérfano**

Aunque han pasado diez días desde la muerte de Xu cuando esta se menciona Zheng no
puede sobreponerse a la pena. Piensa que su suicidio fue consecuencia tanto de factores
internas como externas: no solo debido a las decepciones que había sufrido sino también
debido a su alma de poeta[^81].

[^81]: Esta explicación desdibuja el profundo odio a la vida en la cadena de montaje
que cristalinamente se refleja en muchos de los poemas de Xu y se suma a la desesperación por haber fallado repetidamente en sus intentos de encontrar un modo satisfactorio para escapar de esa vida, incluyendo la posibilidad de regresar a su pobre y vacío pueblo natal, donde se había visto privado del acceso de libros —su principal fue de placer y sentido— ya la posibilidad de estar junto a su novia y casarse con ella —lo que hubiera requerido más dinero del que Xu hubiera sido capaz de lograr en el campo—. Esta visión tampoco puede explicar por qué tanto trabajadores, sin ser poetas, han elegido suicidarse.

Tras la muerte de Xu algunos obituarios colgados en la red afirmaban que se había
quedó huérfano siendo un niño pequeño, que había sufrido abandono y vejaciones
hasta que una pobre señora mayor lo convirtió y lo crió, y que su abuela adoptiva había
muerto hacia unos años dejando a Xu solo en el mundo. Zheng negó estos rumores señalando que los escritos de Xu mencionaban a menudo a su madre y hablaban de la añoranza que sufria por su tierra. Por ejemplo, su segundo poema publicado en _Foxconn People_ se titulaba «Summertime Homesickness» (Añoranza de verano en casa). La poesía de Xu es fría y meditabunda en confrontación directa con una vida de miseria. sus poemas trazan una trayectoria en la que el perfume de la muerte se vuelve cada vez mas penetrante. Ya antes habia ensayado la muerte cientos de veces en sus escritos, por lo que el acto final no le requirió más que dar el pequeño paso que conducía al abismo.

**Li Fei y Zhang Xiaoquio**

## **«Aquí clavado junto a la línea de montaje, así tal cual, de pie me duermo»**

> Poemas de Xu Lizhi

******

**Antes de morirme**

Desearía echarle otro vistazo al mar, atestiguar cuánto fue el llanto de mi corta
[vida.

Quisiera una vez más subir hasta la cumbre, ordenar a gritos regresar a mi alma perdida.

Me gustaría también palpar el cielo, encontrarme con un azul gentil.

Ya que nada de esto puedo, me quiero ya morir.

Así que, ¡gente!,

no quisiera alarmaros ni mucho menos haceros suspirar o sufrir.

Si estuvo bien cuando vine, bien estuvo cuando partí.

******

**Conflicto*

Todos dicen que soy un chico callado y no lo niego. Realmente, tanto hablar como
no hablar provocar conflictos en esta sociedad.

> _7 de junio de 2013_

******

**Asi es como de pie duermo**

Al papel que tengo delante, amarillento, con la pluma le estampo descuidados trazos
[negros rebosantes de léxico de empleo. Taller, línea de montaje, maquinaria, cartilla,
hora extra sueldo...

Yo nada más obedezco, no puedo luchar, ni resistir no puedo denunciar, ni maldecir
solo extenuarme sin hacerme oir.

Desde el día en que llegué, ansío cada día 10 el check gris con mi estipendio tardío
solaz que me otorga mi dueño.

Por esa paga limé asperezas, discursos limé, decliné la huelga, olvidé la baja, permisos
rechacé, ni irme un poco antes ni llegar tarde juré.

Aqui clavado junto a la linea, manos al vuelo, cuantos dias claros, juntos noches
negras, así tal cual de pie me duermo.

> _20 de agosto de 2011_

******

**Un tornillo tirado en el suelo**

Hay un tornillo tirado en el suelo en este turno de la noche. Ha aterrizado de pie,
con un leve tintineo, sin llamar la atencion de nadie. En el mismo sitio donde alguien
pasó cierta noche allí tirado en el suelo.

> _9 de enero de 2014_

******

**Agüjero**

Dicen los viejos del pueblo que soy igual que mi abuelo y aunque me costó verlo al
principio luego no he podido más que verlo. Y ya no albergo ninguna duda de que me
parezco mucho a mi abuelo y no solo por fuera, sino también por dentro, uno mismo
temperamento, un mismo gusto, como criados por el mismo busto. A mí me apodan «el
percha» ya él le llamaban «perchero». El solía tragarse su rabia y yo soy servil
y zalamero. A él le gustaban los augurios ya mí los agüeros. En el otoño de 1943,
demonios extranjeros llegaron y lo asaron vivo[^82] Tenía 23 años, los mismos 23
que yo ahora vivo.

[^82]: Se refiere a una de las incursiones japonesas en zonas rurales chinas en la
segunda guerra sino-japonesa (1939-1945). Los invasores eran percibidos como «Demonios extranjeros», ya fueran japoneses, ingleses o alemanes _(N. de la T.)

> _18 de junio de 2013_

******

**Última morada**

Hasta el _bip bip_ de la máquina suena soñoliento y el taller cerrado acumula hierro
enfermo y tras los papeles se atesoran salarios ciertos al igual que su amor entierran
las obreras que no tienen tiempo de hablar, ni más que polvo [en el pecho. tienen,
eso sí, un estómago de hierro a rebosar de ácido sulfúrico y nítrico. la industria
les quita las lágrimas que no pueden [llorar, a fuerza de turno vacía su despertar.
La producción oprime su lozanía, horas extras [noche y día, sin ser aún viejos una
niebla incuban en su vida. Ya el sistema la piel les arranco y con una fina capa
de aluminio la revisión. Unos aguantan, otros caen enfermos, yo entre ellos dormito
a ratos, mientras superviso de nuestra juventud esta última morada.

> _21 de diciembre de 2011_

******

**Aún no ha terminado el camino de mi vida**

A nadie extrañaría que al camino de mi vida le quedase un largo trecho.

Y, sin embargo, con la adversidad tan pronto me he topado. No es que antes no hubiera
pero no era como esta. Así irrumpiendo tan repentina tan cruenta. No quiero parar
de luchar, pero todo es en vano. Desearía levantarme más que ningún otro, pero mis
piernas no responden, mi estómago no responde, ninguno de mis huesos responde, ninguno
de mis huesos responde y sigo aquí tumbado, lanzando en la noche oscura gritos de
auxilio callados que me devuelven una y otra vez ecos desesperados.

> _13 de julio 2014_

******

**Me tragué una luna de hierro**

Me tragué una luna de hierro, tuerca la llamaban. Me tragué las aguas negras de la
industria, cartillas de desempleo. Primaveras se afanan encorvadas sobre máquinas
marchitándose a destiempo. Me tragué la extenuación, el finiquito, el [vagabundeo.
Pasos de peatón elevados, oxidados tiempos. Ya no puedo tragar más. Todo lo que tragué
regurgita ahora a raudales y desde la tierra de mis antepasados se transforma en
poemas humillantes.

> _19 de diciembre de 2013_

******

**Habitación alquilada**

En un espacio de apenas diez metros, abarrotado, húmedo, sin ápice de sol, como,
duermo, defeco, pienso. Tengo tos, mareos, senilidad, enfermo. Bajo esta luz amarillenta
parezco idiota, me rio [nervioso, ando aquí allá, tarareo, escribo, leo parezco un
muerto.

******

** Roto por la muerte de Xu Lizhi, joven poeta y obrero nacido en los 90**

> Por Zhou Quizao, compañero en la fábrica de Foxconn

Cada vida perdida es otra despedida mía. Un clavo más desatornillado, un obrero más
suicidado. Tú has muerto en mi lugar y yo por ti seguiré escribiendo mientras desganado
atornillo un clavo y otro [clavo. Cumple hoy setenta y cinco nuestra querida [república
y el país entero lo celebra. Tú a tus veinticuatro en la foto gris sonríes. Hay lluvia
otoñal y viento cuando tu padre de pelo cano lleva la caja negra de tus cenizas de
vuelta al pueblo.

> _1 de octubre de 2014_

Posfacio

******

## **La clase creativa de los campos y el zoo de las manufacturas**

Unos diez gigantes de la subcontratación se reparten el mercado de la producción
electrónica mundial. La mayoría son taiwaneses —Foxconn, Pegatron, Quanta Computer,
Compal Electronics— o americanos —Flextronics, Jabil—, pero todos poseen fábricas
en China.[^83]

[^83]: «Global Top-10 EMS [European Monetary System] Providers See Revenues Grow
2,5% in 2014», _Digitimes_, Teipéi, 9 de abril de 2015, https://web.archive.org/web/20211011052949/https://www.digitimes.com/news/a20150409PD212.html

Fundada en 1974, Foxconn —también conocida como Hon Hai Precision Industry—, con
más de un millón de asalariados y el tercer empleador privado más importante del
mundo, fabrica por sí sola más de la mitad de la producción electrónica mundial.
Sus principales clientes son Apple, Amazon, Cisco, Dell, Google, Hewlett-Packard,
Microsoft, Motorola, Nintendo, Nokia y Sony. Desde las consolas de videojuegos Atari,
de la década de 1980, hasta los iPads, iPhones, Kindles o Blackberries de última
generación, incluyendo ordenadores, escáneres, impresoras, etc., casi todos los productos
electrónicos de consumo masivo provienen de fábricas chinas y, en particular, de
las que pertenecen a Foxconn. En Shenzhen Longhua, Foxconn City —la histórica planta
de producción del grupo— reúne a más de 350.000 obreras en un espacio de tres kilómetros
cuadrados. A cambio de unas 60 horas de trabajo semanales se gana, como mucho, lo
equivalente a 500 euros mensuales. La mayoría de trabajadores son migrantes de campo,
que viven hacinados en dormitorios de unas diez personas, sin intimidad alguna. A
partir de 2010, tras el impacto mediático de una oleada de suicidios[^84],

[^84]: _South China Morning Post_, 16 de junio de 2010, citado por Isabelle Thireau:
«Les cahiers de doléances du peuple chinois», _Le Monde diplomatique_ París, septiembre
de 2010, https://web.archive.org/web/20211011053223/https://www.monde-diplomatique.fr/2010/09/THIREAU/19638 (última consulta: noviembre 2018).

Se instalaron redes en las ventanas de esos edificios de doce pisos.

Desde entonces, la dirección  ha aumentado las remuneraciones[^85],pero ha
trasladado una parte de la producción a ciudades - fábrica situadas al interior
del país, haciendo trabajar a la mano de obra local con salarios inferiores[^86].

[^85]: No obstante, estas se han visto compensadas por el incremento de las cuotas
de productividad y de las retenciones realizadas en relación con el alojamiento.
De hecho, como precisan las investigaciones de Jenny Chan y sus colegas —gracias
al personal infiltrado a finales del año 2014—, el poder adquisitivo de las obreras
no ha aumentado, debido a la subida de precios. Nicki Lisa Cole y Jenny Chan: «Despite
Claims of Progress, Labor an Environmental Violations Continue to Plague Apple», _Truthout_,
Sacramento, febrero de 2015, https://web.archive.org/web/20211011053328/https://truthout.org/articles/despite-claims-of-progress-labor-violations-and-environmental-atrocities-continue-to-plague-apple-s-supply-chain/ (última consulta: noviembre de 2018)

[^86]: Jordan Puille: «En Chine, la vie selon Apple: un empire taïwanais», _Le Monde
diplomatique_, junio de 2012, https://web.archive.org/web/20211011054817/https://www.monde-diplomatique.fr/2012/06/POUILLE/47866 (última consulta: noviembre de 2018).

Cada detalle de la vida cotidiana de estas obreras de la industria electrónica recuerda
la extrema mezquindad sobre la que se sustenta el gran capital: los pequeños ahorros
generan grandes riquezas, sobre todo en el sector manufacturero. Las reuniones obligatorias
que se hacen al principio y final del día no se pagan; está prohibido laventar la
cabeza y hablar con los compañeros en la cadena; y la comida es ínsipa e insuficiente[^87].

[^87]: -iSlaves behind the iPhone-, SACOM, 24 DE SEPTIEMBRE DE 2011, https://web.archive.org/web/20211011055145/https://www.somo.nl/wp-content/uploads/2011/10/iSlave-behind-the-iPhone-Foxconn-Workers-in-Central-China.pdf
(última consulta: noviembre de 2018)

En la fábrica de la empresa Jabil situada en Wuxi los empleados deben pagar por todas
las etapas del proceso de contratación, incluida la visita médica, y no se suministra
agua potable en los dormitorios comunes[^88].

[^88]: China Labor Watch, informe del 4 de septiembre de 2013; -iExploitacion at
Jabil Wuxi-, SACOM, 25 de septiembre de 2014,
https://web.archive.org/web/20190907004234/http://www.chinalaborwatch.org/upfile/2014_09_252014.09.25%20iExploitation%20at%20Jabil%20Wuxi%20EN.pdf
(última consulta: noviembre de 2018). En todas las instalaciones de estas fábricas
proliferan los cánceres y las enfermedades respiratorias o neurológicas, debido a
la exposición al polvo de aluminio, los fluidos de corte y los disolventes [^89].

[^89]: Veánse las campañas de www.chinalabourwatch.org, www.goodelectronics.org y
www.greenamerica.org

El fundador y director general de Foxconn, un taiwanés multimillonario, encarna la
figura, totalmente obsoleta, de un empresario cruel salido directamente de las novelas
de Dickens. El señor Terry Tai-ming Gou es también el autor de un libro de máximas
que aparecen reproducidas en las paredes de sus talleres: «Un dirigente debe tener
el coraje de ser un dictador por el bien común». Aparece el puesto 184 de la lista
de la revista _Forbes_ de las principales fortunas mundiales; Gou no tiene gran estima
por sus empleadas. En junio de 2014, durante una conferencia de prensa, posaba en
los brazos de un robot y, tras asumir públicamente que quiere deshacerse lo antes
posible de los trabajadores humanos, lanzo una producción masiva de «foxbots». En
2012 cansado de «gestionar a un millón de animales» invitaba al director de Taipeí,
Chin Shih-chien, a dar un curso de gestión animal:

> _Durante su intervención en el estado, Chin explicó a la audiencia qué comportamiento adoptar frente a las diferentes especies animales en función de sus características. Después de haberlo escuchado atentamente, Gou le pidió a Chin que se pusiera en el lugar del director de Hon Hai, para regodeo de los doce directores ejecutivos presentes_[^90].

[^90]: «Foxconn Chairman Likens his Workforce to Animals», en _China Times_, 19 de
enero de 2012, https://web.archive.org/web/20160809110104/http://terminatorstudies.org/2012/02/10/foxconn-chairman-likens-his-workforce-to-animals/
última consulta: noviembre de 2018).

Diríase que estamos en el infierno y el paraíso. Bajo el sol de California, en el
campus de Mountain View, sede de Google, los empleados se reúnen en una piscina con
pelotas para favorecer los _brainstormings_ y tiene a su disposición gimnasios abiertos
día y noche, que les permiten ganar siete dólares por cada media hora que pasan en
ellos[^91].

[^91]:«Dans la Google du Loup» (En la boca del lobo; este título juega con el paralelismo
sonoro de la expresión francesa «Dans le gueule du loup»[N. de la T.], _Revue_ Z,
nº9, Montreunil-sous-Bois, 2015,https://web.archive.org/web/20200627205520/https://infokiosques.net/IMG/pdf/Dans_la_Google_du_loup_Z9-cahier.pdf
(última consulta: noviembre de 2018)).

Su salario medio ronda los 100.000 euros anuales y el lugar cuenta con unos treinta
restaurantes, todos completamente gratuitos, como expone un crítico gastronómico
de la bahía, que visita el establecimiento de la chef Hillary Bergh:

> _La colorizada ocupa un lugar de honor, es la base cromática de los buñuelos de maíz, de las nueces pecanas y de la calabaza de la granja ecológica Baia Nicchia. Su gusto es dulce y terroso, con un sorprendente toque de lavanda. El pescado, recién llegado de Half Moon Baby, es el más fresco que puede encontrarse localmente, a excepción de los bueyes de mar. A parte de hacer compost, cultivar huertos y fabricar ellos mismos productos básicos como el pan y la miel, Google y el grupo Bon Appetit siguen al pie de la letra las recomendaciones del Seafood  Watch Monterey Bay Aquarium[^92]. Aquí no veréis atún rojo ni salmón de criadero del Atlántico [...] De postre, barritas de nueces pecanas, ligeras y deliciosas, con un sútil toque de arce, y sin gluten, gracias a la harina de garbanzo. Para los momentos de descanso, la instalaciones disponen de numerosas «mini-cocinas», desbordantes de frutas, refrigerios de habas de soja japonesa, patatas de plátano y onzas de chocolate negro Tcho, elaborado por los pequeños chocolateros artesanos de San Francisco. Cuando se necesita un café, siempre hay un camarero profesional a la mano._[^93]

[^92]: La compañía norteamericana Monterey Bay Aquarium difunde y actualiza regularmente
su programa Seafood Watch en el que ofrece recomendaciones, con fundamento científico,
para que los consumidores y las empresas elijan las mejores opciones de pescado y
marisco con el fin de respetar los océanos (N. de la T.).

[^93]:«Luch at Google HQ is as Insanely Awesome as You Thought», en _Serious Eats_,
8 de enero de 2014, https://web.archive.org/web/20200601021147/https://www.seriouseats.com/2014/01/lunch-at-google-insanely-awesome-as-you-thought.html

Por su parte, el edénico parque que sirve de sede de Facebook es famoso por sus «bicis
comunitarias» de libre acceso y sus tiendas de caramelos gratuitos. En Apple, las
actividades filantrópicas de los empleados fuera del horario laboral son remuneradas
a veinticinco dólares la hora. Todo esto pone de relieve que el modelo hegemónico
que Sillicon Valley se ha forjado históricamente en torno a los antiguos hippies
de la cultura _hacker_ motivados por la esperanza de crear un mundo más justo, más
comprensible y pacificado por el hecho de haber puesto al alcance de todos las «herramientas
informáticas[^94].

[^94]: Es lo que explica Fred Turner en el libro _Aux sources de l'utopie numérique:
de la contre-culture à la cyberculture_, C&F, Caen, 2010.

¿No empezó Apple con dos amigos californianos que vendían un aparato para hacer llamadas
gratuitas, pirateando a la sociedad de telecomunicaciones AT&T?[^95].

[^95]: American Telephone & Telegraph es una compañía estadounidense de telecomunicaciones,
creada en 1885, que se ha erigido en varias ocasiones como la mayor empresa telefónica
del mundo y la más grande operadora de televisión por cable de Estados Unidos (N.
de la T.).

Así como el famoso eslogan de Google «Don't be evil»[^96]

[^96]: «Don't Evil» fue durante un tiempo la frase que enarbolaba Google para guiar
las decisiones y las acciones de sus empleados y usuarios, pero ya casi ha desaparecido
por completo de su código de conducta. El famoso eslogan se mantuvo muy visible hasta
abril de 2018, un mes antes de que Alphabet, la megaempresa que engloba a Google,
anunciara que se aliaba con el Departamento de Defensa estadounidense para desarrollar
el programa Project Maven, cuyo objetivo es el análisis de fotos y vídeos de campo
de batalla. Esta maniobra provocó dimisiones en masa de los empleados, por razones
éticas, políticas y por el empleo de inteligencia artificial en los drones de uso
bélico. (_N. de la T_)

pretendía evidenciar las ambiciones morales de la empresa, los asalariados de Apple
aún siguen recreándose en el reflejo angelical que les remite su actividad; su principal
motivo de satisfacción en el trabajo sería «el sentimiento de construir un mundo
mejor gracias a la tecnología»[^97].

[^97]: «Best things about working at Apple», en _Business Insider_, Nueva York, 5
de febrero de 2015.

Como en un cuento para niños, el sueño californiano de una tecnología liberadora
supone el reverso exacto de la vida cotidiana de las obreras chinos en las cadenas
de fabricación. En el universo liso y pulido de los parques tecnológicos mundiales
las condiciones de producción sobre las que se apoyan las «innovaciones» de la economía
de las grandes potencias son un tabú: las inmensas ciudades - fábrica pérdidas en
el humo de una China lejana se vuelven invisibles; los productos electrónicos de
consumo masivo que invaden nuestro día a día se producen en esas fábricas desde inicios
de los años ochenta y, sin embargo, se ha tenido que esperar hasta 2006 para que
los medios de comunicación publicasen una investigación sobre el trabajo de este
sector.[^98]

[^98]: En un reportaje del tabloide inglés _The Mail on Sunday_ sobre la fábrica
Foxconn Longhua, de Shenzhen.

Durante estos treinta años de negación no sólo se han invisibilizado las condiciones
de producción de los soportes digitales sino también su propia materialidad. A medida
que se iban multiplicando los campus y los laboratorios de I+D, y que la economía
de los países industrializados se situaba bajo el signo de «producción de conocimiento»
y del «intercambio de información» gracias a una operación ideológica de masas, se
fue «desmaterializando» la invasión de alta tecnología que posibilita todo esto.
La fábula platónica de una economía fundada en las «ideas» vehiculada gracias al
auge de la informática no solo ha contribuido a la configuración del «nuevo espíritu
del capitalismo»,[^99]

[^99]: Esta expresión da título al libro de referencia de los sociólogos Luc Boltanski
y Ève Chiapello que, en 1999, describía la rehabilitación de la empresa basándose
en los conceptos de «convivencialidad», «red», y «proyecto», en parte heredados de
la crítica surgida en 1968 (en castellano: _El nuevo espíritu del capitalismo_, trad.
de Alberto Riesco Sanz, Marisa Pérez Colina y Raúl Sánchez Cedillo, Akal, Tres Cantos,
2002).

sino que también ha acompañado el proceso de división mundial del trabajo que, en
los países ricos, se basa en la simple y pura evacuación de unos bienes materiales
que son, no obstante, cada vez más numerosos, más voraces en su consumo de energía
y materiales fósiles y más rápidamente obsoletos. Algo que, en el lapso de una sola
generación, ha conducido a la paradójica situación que vivimos: aunque el número
de fábricas y de trabajadores en cadena que existen en el planeta probablemente[^100].

[^100]: Desde 1955, el comercio mundial de productos manufacturados se ha multiplicado
por cien. Durante el período 1993 - 2013, la tasa media de crecimiento anual del
comercio mundial de mercancía fue de 5,3%. Desde la década de 1950, el número de
obreras no ha disminuido, ni siquiera en Francia (véanse las fuentes al respecto:
_Financial Times Lexicon_, «Globalisation», Lexicon.ft.com; Organización Mundial
de Comercio, comunicado del 14 de abril de 2014; «Les ouvries, ces travailleurs invisibles
des temps modernes», _L'Humanité_, Saint-Denis Cedex, 24 de febrero de 2014, https://www.humanite.fr/social-eco/les-ouvriers-ces-travailleurs-invisibles-des-temps-559763
[última consulta: noviembre de 2018]).

Una de las principales consecuencias de esta invisibilidad es la distorsión de nuestra
relación con la tecnología que nos impide pensar en sus defectos sociales globales.
Ingenieros, empresarios y editorialistas suelen hacer gala de una imaginación desbordante
al describir las ventajas que podría aportar a la sociedad tal o cual tecnología.
Del mismo modo que, a finales de los noventa, nos entusiasmamos con los teléfonos-móviles-salvadores-de-mujeres-que-sufrían
hoy pronosticamos las ventajas de los futuros drones como apoyo para las ambulancias,
del etiquetaje electrónico de los alimentos que permitirá a la nevera proponer recetas,
o de la conexión del cepillo de dientes que nos indicará cuándo finalizar el cepillado.
Pero, cuando les toca hacer el balance entre los beneficios esperados y el coste
humano y ecológico de la producción de nuevos objetos electrónicos, esos mismos actores
parecen estar desprovistos de toda imaginación. ¿Cómo es posible que se tomen tan
en serio los «servicios» que podrán ofrecernos los robots y los drones en la vida
cotidiana —que en el mejor de los casos no son más que artilugios electrónicos y
que, casi con toda seguridad, se revelarán como desastrosos para la sociedad— y que
se ignoren hasta tal punto los problemas, mucho más graves, que va a provocar su
difusión masiva?, ¿qué materiales se usan, de qué minas se extraen, en qué condiciones
y a qué precio?, ¿cuáles son los conflictos geopolíticos que hay que pagar por ello?,
¿cuál es la duración prevista de esos artilugios?, ¿qué sucede con los residuos y
el consumo de electricidad? Estas preguntas tal vez llegarán demasiado tarde, como
«dentro de cinco años, tener un robot de telepresencia sea tan banal como tener hoy
un _smartphone_», según vaticina Bruno Bonnell, director de Syrobo, sociedad piloto
del plan robótico, de la nueva Francia industrial[^101].

[^101]: «"Dentro de cinco años, todo el mundo tendrá un robot de telepresencia, pero
no un humanoide", según Bruno Bonell», www.unsine-digitale.fr, 20 de julio de 2014,
https://www.usine-digitale.fr/article/dans-5-ans-tout-le-monde-aura-un-robot-de-telepresence-mais-pas-un-humanoide-selon-bruno-bonnel.N270149
(última consulta: noviembre de 2018).

La compañía robótica emergente Aldebaran fundada en 2005 por un francés ha recibido
decenas de miles de euros de fondos públicos para desarrollar varias generaciones
de robots humanoides, como por ejemplo, Nao y Romero. Gracias a la «revolución»,
un programa de envergadura lanzado por los poderes públicos para robotizar el ámbito
de la asistencia personal,[^102]

[^102]: Impulsado por Arnaud Montebourg, ministro de Economía, Recuperación productiva
y Asuntos digitales del Gobierno de François Hollande, el programa detallado en l
informe «France Robots Initiatives», de marzo de 2013, también se contemplaba la
creación de un fondo de capital de riesgo, denominado «Robolution Capital», destinado
a financiar empresas incipientes en el campo de la robótica.

Aldebaran se beneficia considerablemente de los resultados de los mejores laboratorios
de robótica del país —como los del LASS-CNRS de Tolouse—,[^103]

[^103]: Laboratorio de Análisis y Arquitectura de Sistemas, asociado a CNRS (Centre
national de la recherche scieentifique, el organismo público de la investigación
más importante de Francia). (_N. de la T._)

mediante la colaboración entre el sector público y el privado. Absorbida a principios
de 2015 por el grupo japonés Softbank, Aldebaran es socia de Foxconn en el lanzamiento
de la producción masiva de robots semiandroides denominados «Pepper». Estas criaturas
de un metro veinte, dotadas de una pantalla plana en el tórax, son robots de compañía:

> _No limpia la casa, ni cocina sino que, partiendo de emociones universales (la alegría, la sorpresa, la cólera, la duda y la tristeza) y analizando las expresiones faciales, el lenguaje corporal y las palabras, Pepper adivina en qué estado se encuentra y se adapta a usted. Podrá levantarle la moral ¡poniendo su canción favorita!, por ejemplo[^104]_

[^104]: «Meet Preper, Aldebaran's New Personal Robot With an "Emotion Engine", _IEE
Spectrum_, enero de 2016 https://2016.robotix.in/blog/meet-pepper-aldebarans-new-personal-robot-with-an-emotion-engine/
(última consulta: noviembre de 2018).

¿Quién necesita un robot de compañía? A través de un artículo de _Le Monde_, sabemos
que la región de Ródano-Alpes ha comprado tres modelos Beam de la sociedad francesa
Awabot, mientras que la Academia de Versalles ha adquirido cinco robots Nao de Aldebaran.
La inversión provechosa, pues, basta con introducir uno en una clase para resolver
todos los problemas de educación nacional:

> _El responsable de tecnología digital educativa de la Academia, Franck Dubois, explica la escena, «¡nunca vista en veinte años de enseñanza!»: alumnos de 2º de ESO se olvidan del recreo. «Sí, sí», insiste jurándolo, sucedió recientemente en un instituto de Sèvres. Había llevado a Nao. «Al principio, los alumnos estaban sentados como de costumbre, luego se me acercaron y se pusieron de cuclillas. Se quedaron pegados a mí, patidifusos, durante una hora»._[^105]

[^105]: «Le visage humain des robots», _Le Monde_, París, 5 de diciembre de 2014,
lemde.fr/2CwBBxG.

Hace diez años, la idea de pasearse con un microordenador portátil para leer libros
parecía tan incongruente e innecesaria como hoy lo es confiar nuestros propios estados
de ánimo o el cuidado de los abuelos de un robot. Pero si la contratación pública
se sirve de este objeto, _a priori_ superfluo, para equiparar residencias de ancianos,
escuelas y hospitales, o si los ricos empiezan a utilizarlo como símbolo de alto
_standing_, entonces se integrará al equipamiento de electrodomésticos, ya considerable,
de las clases medias urbanas. ¿Cómo puede una sociedad ser tan materialista y, al
mismo tiempo, rehuir hasta tal punto la realidad de sus propias condiciones de posibilidad
materiales?, ¿cómo explicar que prácticamente no es cuestionado el consumismo inducido
por las nuevas tecnologías, tras haberse revelado al mundo entero las condiciones
en que se producen los objetos electrónicos y que provocaron una oleada de suicidios
en Foxconn?, ¿por qué no se alzan miles de voces para criticar la orientación dada
la investigación mundial en materias como la informática y la robótica, más todavía
cuando responden a la grotesca llamada de la «revolución»? La respuesta quizá se
encuentre en nuestra creencia en la omnipotencia tecnológica, pues pensamos que en
el moderno universo de los países ricos y las capitales mundiales esta puede solucionar
todos los problemas que debe afrontar la humanidad. Hasta principios de los años
2000, la desaparición de las fábricas de nuestro campo visual generó la ilusión de
que la alienación del trabajo en cadena «había sido superada». En cuanto a la producción
automatizada, habíamos superado el estadio del fordismo y el taylorismo para entrar
en la era de la información y la comunicación. Finalmente, el «progreso» nos había
liberado de la carga del trabajo físico y rutinario en pro de tareas intelectuales
y creativas. Debido a las proporciones alcanzadas por el desarrollo industrial en
China, pero también gracias al militantismo de las ONG, nos hemos visto obligados
a reconocer que la fábrica de antaño, con sus ritmos embrutecedores y sus capataces
acechantes, en realidad ha sido más _desplazada_ que _superada_. ¡Era evidente que
las máquinas que permitían la automatización de las fábricas europeas eran producidas
en otra parte! Pero esta mistificación solo se disparó en beneficio de otra fantasía:
los robots liberarán a los trabajadores del Tercer Mundo, quienes, a su vez ascenderán
hasta desempeñar tareas conceptuales. En el LAAS de Toulouse, el equipo de Gepetto[^106]

[^106]: Pueden verse sus líneas de investigación en projects.laas.fr/gepetto

está trabajando en un programa de investigación europeo titulado «Fábrica del futuro»,
cuyo objetivo es desarrollar robots que trabajarán en las cadenas de montaje, al
lado de las obreras, para aumentar la productividad. Este equipo lo dirige Jean-
Paul Laumond, el carismático fundador de la empresa emergente Kineó —extitular de
la Cátedra Liliane Bettencourt por la innovación del Còllegue de France—, y muy asiduo
a los medios de comunicación. Cuando se le pregunta por la supresión de empleos,
el enminente especialista en robótica levanta una foto tomada en 2006, donde se ve
el taller de una fábrica de Foxconn: miles de obreras chinas se alinean hasta el
infinito ante la cadena de montaje, en la misma posición, todos vestidos en el mismo
mono rosa: «¿Son estos los empleados que queréis salvar?», pregunta con algo de malevolencia[^107].

[^107]: Léase «Lettres aux roboticiens du LAAS», en _Revue Z_, n.º 9, Montreuil-
sous- Bois, 2015, https://web.archive.org/web/20211011055420/https://startuffenation.fail/high-tech-low-life/ (última consulta: noviembre de 2018).

Semejante respuesta muestra bien la confusa y abstracta noción que tenemos sobre
el impacto de la tecnología, producto de la falta de contacto directo con quienes
la padecen. Como muchos de sus colegas, este investigador parece no saber nada —¿es
posible?— de la violencia ejercida por la relación que se genera con la introducción
de máquinas en la industria. Sus predecesores no siempre fueron tan ingenuos. En
1949 el famoso matemático del MIT (Instituto Tecnológico de Massachusetts) y fundador
de la cibernética Norbert Wiener, en una carta dirigida al presidente del sindicato
de trabajadores del automóvil norteamericano, expresaba su preocupación por las consecuencias
en la vida de las obreras de la automatización de la cadena de montaje:

>_Toda mano de obra, en cuanto se pone a competir con un esclavo, sea este humano o mecánico, está condenada a sufrir las condiciones de trabajo del esclavo._

La introducción de robots en las cadenas de producción de Foxconn es una respuesta
a la recurrente penuria de mano de obra en China, agravando el impacto de hasta la
más modesta huelga, durante los procesos _just-in-time._[^108]

[^108]: «End of China's Migrant Miracle: Toil and Trouble», _Financial Times_, Londres,
7 de julio de 20015, www.ft.com/content/dc3db3e4-fef9-11e4-94c8-0014feabdc0.

Consecuencias inmediatas: más presión sobre las obreras y consolidación de una relación
de fuerzas favorables a la dirección. Además, lejos de ahorrarles empleos alienantes
a los trabajadores, la robotización provoca un aumento de la actividad del grupo
y, por lo tanto, de sus capacidades de explotación de la mano de obra mediante la
producción masiva de robots[^109].

[^109]: Foxconn anunció en 2015 la creación de un millón de empleos en India, en
gran parte para producir robots (_India Times_, 11 de julio de 2015). Al ofrecer
semejantes perspectivas de crecimiento a Foxconn, la investigación robótica multiplica
con creces la capacidad de explotación y el trabajo alienado.

En el sector manufacturero, la automatización total es un mito. Como recuerda Jenny
Chan, «las manos humanas son flexibles: las obreras siguen siendo fundamentales en
el crecimiento de Foxconn»[^110].

[^110]: Entrevista con Jenny Chan realizada por Internet.

En el contexto actual, no pueden compararse los recientemente introducidos «foxbots»
con la inteligencia y la motricidad humana[^111].

[^111]: «Les foxbots peniet toujours à remplacer les humains», en Génération Nouvelles
Technologies_, 5 de mayo de 2015, https://web.archive.org/web/20211011055420/https://startuffenation.fail/high-tech-low-life/ (última consulta: noviembre de 2018).

Resultaría muy costoso emplear sistemáticamente máquinas dotadas de una motricidad
tan eficaz. Aún sigue siendo más beneficioso explotar a mano de obra mal pagada,
por muy agitada e indisciplinada que esta sea. No hay, pues, posibilidad alguna de
que los humanos que sudan la gota gorda en las cadenas de ensamblaje sean «liberados»
pronto por los robots, sino que podemos apostar por lo contrario: que continuarán
sufriendo, antes que nadie y durante mucho tiempo, sus ritmos y disfuncionalidades.


Aunque los robots no pueden reemplazar por completo la mano de obra, a la larga amenazan con destruir, inevitablemente, una parte de los empleos. El mito de la automatización total cumple una función central en la gestión empresarial, pues las máquinas encarnan a un ejército de reserva susceptible de ocupar el lugar de los recalcitrantes: «si no trabajas lo suficiente, serás reemplazado por un robot», es una amenaza recurrente contra los asalariados de Foxconn[^112].

[^112]: «iSlaves behind de iPhone», _op. cit._, p.98.

Al transmitirles que se han convertido en algo superfluo, la robotización desempeña
un rol desmoralizador en el momento de organizarse y de hacer valer sus derechos:
la fuerza ideológica de la automatización radica en «deslegitimar la defensa del
oficio, incluso la idea de cómo se hace el trabajo, ya que este tiende a desaparecer
muy rápidamente», subraya el sociólogo David Gaborieau[^113].

[^113]: David Gaborieau: «La chimère de l'suine sans ouvriers occulte la réalité
du travail», en _Revue Z_, _op. cit._

¿Para qué luchar cuando ya no se tiene futuro alguno?

Los investigadores, los ejecutivos y los ingenieros que se calientan la cabeza en
los Silicon Valley de todo el planeta piensan que «están construyendo un mundo mejor
gracias a la tecnología». Esta creencia se basa en un inteligente juego de luces
y sombras, cuyo objetivo no es otro que evitar que las siniestras realidades de la
producción material salgan a la luz con toda su crudeza. ¿Soñarían tan fácilmente
las jóvenes generaciones con ser un Bill Gates o un Steve Jobs si vieran con claridad
que estas fortunas no se sustentan sobre una capacidad de invención visionaria sino
sobre la explotación de millones de trabajadores? Dar visibilidad al modelo de la
economía digital mundial, es decir, en su dimensión mundial y material, arruina dicho
mito, necesario para que las élites de los parques tecnológicos y quienes están a
su servicio se adhieran al mundo digital. Para que los diseñadores trabajen con entusiasmo
no basta con pagarles un salario generoso y mimarlos con políticas de gestión vanguardistas,
es preciso también que sientan que su actividad tiene un impacto positivo en el mundo;
en una palabra, que sean _buenos_[^114].

[^114]: Luc Boltanski y Ève Chiapello: _Le Nouvel Espirit du Capitalisme_, Gallimard,
París, 1999, pp. 102 y 139 (en castellano: véase la nota 17 de la p. 102).

Es por esto que Apple está transfiriendo su producción a subcontratistas menos visibles
que Foxconn,[^115]

[^115]: Este giro también está relacionado con las pequeñas mejoras concedidas por
Foxconn tras el escándalo de 2010: aumento salarial, reducción del número de obreras
por dormitorio, disminución de las horas extras, fin de las humillaciones y castigos,
y descenso de las sanciones disciplinarias. En 2014, Apple se ahorró 61 millones
de euros al confiar la producción del iPhone 6 a un rival empresarial menos expuesto
y, por ende, menos caro: Pegatron. Esta empresa recurre masivamente a los intermediarios
y, como señala Li Qiang, fundador de China Labour Watch, «todo subcontratista que
mejore las condiciones de trabajo queda automáticamente en desventaja». China Labour
Watch: «Analyzing Labor Conditions of Pegatron and Foxconn: Apple's Low Cost- Reality»,
febrero de 2015, pp. 6, https://web.archive.org/web/20150414060824/http://chinalaborwatch.org/upfile/2015_02_11/Analyzing%20Labor%20Conditions%20of%20Pegatron%20and%20Foxconn_vF.pdf (última consulta: noviembre de 2018).

cuyos dormitorios rodeados por redes de seguridad son ya famosos en el mundo entero.

¿Pueden los movimientos de trabajadoras cambiar las cosas? Como en otros sectores
industriales, las huelgas y las manifestaciones espontáneas son muy frecuentes en
las fábricas chinas de la industria electrónica. Exigen básicamente la aplicación
de la legislación laboral; a menudo, cosas tan elementales como el pago de los retrasos
salariales o el aumento de las remuneraciones. Si los gobiernos provinciales apoya
firmemente a las corporaciones, el Estado central —favorable a la creación de un
mercado interior a través del incremento de los sueldos y del nivel de vida— no reprime
sistemáticamente las movilizaciones, pero impide impide que devengan en un movimiento
político de fondo[^116].

[^116]: Friends of Goangchao: «The New Strikes in China», en gongchao.org, julio
de 2014; Chloé Froissart: «Chine, la marche des ouvries vers un syndicalisme autonome»,
en terrainsdeluttes.org, febrero de 2015. Leáse también los informes de la ONG China
Labour Watch y consúltese la herramienta, algo absurda, de _crowdmap_, en chinastrikes.crowdmap.com,
que permite acceder a una lista de las huelgas que se realizan en China en tiempo
real.

Si bien Jenny Chan apuesta por la competitividad de la «nueva generación de trabajadores,
más educada y menos resignada a la injusticia», todo indica que, pese a estos resurgimientos
militantes, el agotamiento y el aislamiento acabarán imponiéndose.

Los suicidios no cesan, constituyéndose a veces como una nueva herramienta de lucha
para las obreras: en 2012, en la fábrica de Foxconn en Wuhan, algunos asalariados
amenazaron en varias ocasiones con saltar esde el tejado de un edificio, para protestar
contra el traslado los empleados hacia una planta de producción en el interior del
país, donde los sueldos son inferiores. No cabe sino esperar un aumento de las protestas
obreras en China —y en otros lugares—. Pero antes, a las clases medias urbanas mundializadas
—infinitamente menos agobiadas por problemas de supervivencia e idiotizadas por
el caleidoscopio digital—, nos toca dejar de seguir sumándose a este modelo y repensar
la materialidad de nuestra existencia. Empezando por un ejercicio de imaginación:
¿y si el conjunto de infraestructuras necesarias para la producción de todos los
ordenadores, televisores, iPads, cámaras fotográficas y teléfonos que utilizamos
fueran relocalizadas en nuestros territorios? Veamos: minas de tierras escasas, de
oro, de cobre y de estaño; instalaciones petrolíferas, fábricas químicas, construcción
de nuevas centrales eléctricas, multiplicación de extracciones de agua, fábricas de
circuitos electrónicos y de ensamblaje, y tóxicos vertidos en cada etapa de la producción.
Mirar lo anterior de frente y no perderlo de vista, ¿no es acaso un requisito indispensable
en cualquier reflexión sobre «libertad», «autonomía», «solidaridad» y «creatividad»
que, supuestamente, debe verse incrementada gracias a todos esos objetos?

**Célia Izoard**, julio de 2015

**Fuente de los textos**

**Yang**

Estudiante y obrero de fabricación

**«La máquina es tu amo y tu señor»**

«Informe de la fábrica de Foxconn en Chongquing», en Pun Ngai _et al.: Wo Zai Fushikang_
(Trabajo en Foxconn), Pekín, 2012; traducción del inglés en el panfleto «China desembarca
en la Unión Europea» (agosto de 2013), en Échanges et Mouvement (BP 241, 75866 París
Cedex 18 - wwww.mondialisme.org).

**Pun Ngai**

Licenciada por la Universidad de Londres, doctora en sociología y profesora de Ciencias
Sociales Aplicadas en Hong Kong Polytechnic University. Es autora de los libros _Made
in China (2005)_; _Dying for an iPhone: Apple, Foxconn and lives of the Chinese workers_
(2015); _Labor in Post-Socialist China_ (2015).

**Tian Yu**

Obrera migrante en Foxconn

**«Crecimiento, tu nombre es sufrimiento»**

Texto de Jenny Chan publicado bajo el título «A suicide survivor: the life of a Chinese
migrant worker of Foxconn», _The Asia -Pacific Journal_, vol. 11, n.º 31, agosto
de 2013

**Jenny Chan**

Doctoranda laureada de Great Britain - China Educational Trust y titular de la beca
Reid Research en la Facultad de Historia y Ciencias Sociales de la Universidad de
Londres. Consejera en SACOM (Students and Scholars Against Corporate Misbehavoir),
organización defensora de los derechos de las obreras con sede en Hong Kong.

**Xu Lizhi**

Trabajador migrante y poeta

**«Me tragué una luna de hierro»**

Introducción de la redacción del blog Nao y necrológica de Li Fei y Zhang Xiaoquio
inicialmente publicada en chino con el título «Mi llegada ha ido bien, pero me iré»,
en _Shenzhen Evening News_ el 10 de octubre de 2010. La poesía de Xu Lizhi está publicada
en el blog de Nao en libcom.org

**Bibliografía**

ACFTU (Federación Nacional de Sindicatos de China)_ «Completion of 2012 Targets on
Union Construction in the Non-State Enterprise»m n.º 19, Pekín, julio de 2012. ANDORS,
Phyllis: «Women and Work in Shenzhen», _Bulletin of Concerned Asian Scholars_, vol.
20, n.º 3, Alexandria (Virginia), 1998. BERGH, Hillary: «Lunch at Google HQ is as
insanely awesome as you thought», en _Serious Eats_, 8 de enero de 2014.
