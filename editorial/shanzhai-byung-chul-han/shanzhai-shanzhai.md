# BYUNG-CHUL HAN SHANZHAI

## El arte de la falsificación y la deconstrucción en China

## SHANZHAI: FAKE

Shanzhai(山寨) es el neologismo chino que se emplea para
fake. Hoy en día existen expresiones como shanzhaiismus
(shanzhaizhuyi, 山寨主意;) cultura shanzhai (shanzhai wenhua,山寨文化)
o espíritu shanzhai (shanzhai jingshen, 山寨精神)
En China, el shanzhai abarca todos los terrenos
de la vida. Hay libros shanzhai, Premios Nobel shanzhai,
películas shanzhai, diputados shanzhai o estrellas del espectáculo
shanzhai. Al principio, el término shanzhai se refería
a los teléfonos, a falsificaciones de productos de marcas
como Nokia o Samsung que se comercializan bajo el nombre
de Nokir, Samsing o Anycat. En realidad son más que meras
falsificaciones baratas. Su diseño y funcionalidad no tienen
nada que envidiar al original. Las modificaciones técnicas
o estéticas les confieren una identidad propia. Son multifuncionales
y están a la moda. Los productos shanzhai se caracterizan ante todo
por una gran flexibilidad. Se pueden adaptar muy rápidamente
a las necesidades o las situaciones concretas, cosa que no está
al alcance de una gran empresa, pues sus procesos de producción
están fijados a largo plazo.
El shanzhai aprovecha el potencial de la situación. Este motivo
es suficiente para considerar que estamos ante un fenómeno
genuinamente chino.

![shanzhai](shanzhai-shanzhai-1.png "shanzhai")

La riqueza imaginativa de los productos shanzhai es
en muchas ocasiones superior a la del original. Por ejemplo,
existen teléfonos shanzhai con una función adicional
para reconocer dinero falso . Eso los convierte en un
original. Lo nuevo emerge a partir de variaciones y combinaciones
sorprendentes. El shanzhai visualiza un tipo singular de creatividad.
Sus productos van apartándose del original sucesivamente,
hasta mutar en originales. Las marcas establecidas se
modifican sin cesar. Adidas se convierte
en Adidos, Adadas, Adadis, Adis; Dasida, etcétera.
Juegan con las marcas a la manera dadaísta, lo cual no
solo se revela como una expresión de creatividad, sino que
también tiene un efecto paródico o subversivo frente al poder
económico y los monopolios.

![shanzhai](shanzhai-shanzhai-2.png "shanzhai")

La palabra shanzhai significa originalmente "fortaleza de montaña".
En la famosa novela El ladrón de Liang Schan-Moor (shui hu zhuan, 水滸傳)
se cuenta que durante la dinastía Song los rebeldes
(campesinos, funcionarios, comerciantes, pescadores, oficiales y monjes)
se atrincheraron en una fortaleza de montaña y combatieron
contra el corrupto régimen. Ya este contexto literario dota
al shanzhai de una dimensión subversiva. Las apariciones
shanzahi en Internet, que parodian a las de los medios
estatales controlados por el Partido, se interpretan como
actos subversivos contra el monopolio de opinión y representación.
Es expresión de la esperanza en que el movimiento
shanzhai deconstruya el poder de la autoridad
estatal a nivel político y libere las energías democráticas.
Pero si el shanzhai se reduce a su faz subversivo-anarquista,
entonces se pierde de vista su potencial creativo y lúdico.

![shanzhai](shanzhai-shanzhai-3.png "shanzhai")

La novela El ladrón de Liang-Schan-Moor se asemeja
al shanzhai sobre todo por su germen y forma de producción,
y no por su contenido subversivo. Para empezar,
su autoría no está clara. Se supone que las historias que
conforman el corazón de la narración fueron redactadas
por varios autores. Además, existen distintas versiones.
Una de ellas está formada por setenta capítulos, otra por
cien e incluso hay una de ciento veinte. En China, los
productos culturales no suelen estar ligados a un autor
individual. Con frecuencia tienen un origen colectivo y
no son una forma de expresión de un individuo inspirado
y genial. No pueden atribuirse claramente a un sujeto
artístico, que llegara a declararse propietario o incluso
creador. También hay otros clásicos, como Sueño en el
pabellón rojo (hong lou meng, 紅樓夢) o Romance de los
tres reinos (san guo yan yi, 三國演義),que no dejan de
reescribirse. Existen infinitas versiones de distintos
autores, con o sin happy end.
En el terreno de la literatura actual china se observa
un proceder parecido. Si una novela tiene éxito, no
tardan en aparecer Fakes. No siempre se trata de imitaciones
de nivel inferior que no disimulan su proximidad
con el original. Junto a las falsificaciones manifiestas,
también hay Fakes que transforman el original, ubicándolo
en un nuevo contexto o dotándolo de un giro sorprendente.
Su creatividad pasa por una transformación
y variación activas. También el éxito de Harry Potter
puso en marcha una dinámica de este tipo. Hoy existe
una gran cantidad de Fakes de Harry Potter, que dan
continuidad al original transformándolo. Harry Potter
y la muñeca de porcelana presenta una sinización de
la historia. En el monte sagrado Taishan, junto a sus
amigos chinos Long Long y Xing Xing, vence a su adversario
oriental Yandomort, la contrapartida china de
Voldemort. Harry Potter habla un chino fluido, aunque
no maneja muy bien los palillos, etcétera.


![shanzhai](shanzhai-shanzhai-4.png "shanzhai")

Los productos shanzhai no pretenden engañar a nadie. Su
atractivo consiste precisamente en que ellos mismos indican de
manera expresa que no son un original, sino que juegan con
este. El juego que habita el interior del shanzhai genera energías
deconstructivas. Las marcas shanzhai también muestran
rasgos humorísticos. La etiqueta del teléfono shanzhai iPncne
se parece a la del iPhone original aunque un poco usada.
Los productos shanzhai a menudo tienen su propio encanto. Su
creatividad, que es innegable, no se define por la discontinuidad
y la creación imprevista de lo nuevo, que rompe por completo
con lo antiguo, sino por un gusto juguetón por el cambio,
la variación, la combinación y la transformación.
La historia del arte china también se caracteriza por el
proceso y la transformación. Todas las recreaciones u obras
posteriores, que modifican incesantemente la obra de un
maestro y la adaptan a las nuevas circunstancias, no son
más que productos shanzhai magistrales. En China, la transformación
continuada está instaurada como método de creación y creatividad.[^1]

[^1]: La creatividad, que está en la base del movimiento shanzhai, presupone
una adaptación activa, una combinatoria juguetona. Esta forma de creatividad
no se refleja en los asiatismos trillados de la no acción o la contemplación.
En su tratado sobre la creatividad, Hans Lenk tampoco parte de tales asiatismos:
"En el taoísmo, por ejemplo, cuando uno piensa en Dao De Ching de Lao Tse, la
no acción, el 'wu wei', desempeña un papel decisivo. El pensamiento creativo
no tiene lugar como algo forzado o impuesto, no se da cuando se pretende
forzar o imponer, sino que hay que estar dispuesto a que acontezca. Wu chi'
significa 'ningún conocimiento'. Se refiere expresamente a que el saber no se
puede activar como coacción, sino que se presenta en un estado abierto, primitivo,
naif. Wu yú es la falta de anhelo, lo cual signfica: no mostrar ningún
deseo, ni interés, ni pasión, 'el gusto desinteresado' en el sentido de la estética
de Kant o el mantenerse abierto sin interés, el desentenderse. Este modo pasivo
de pensar o de actuar, sin conocimiento, sin pasión, es la idea que está en
la base de la reflexión taoísta sobre la creatividad. El desentendimiento es la
madre de la creatividad" (Hans Lenk, Kreative Aujstiege. Zur Philosophie und
Psychologie der Kreativitat [Ascenso creativo. Hacia una filosofía y psicología
de la creatividad], Frankfurt am Main, Suhrkamp, 2000, pág. 108).

El movimiento shanzhai desconstruye la creación como
creatio ex nihilo. Shanzhai es des-creación.
Frente a la identidad, reivindica la diferencia transformadora,
el diferir activo y activador; frente al ser, el camino. Así
es como manifiesta el shanzhai el genuino espíritu chino.
De hecho, la naturaleza, a pesar de que no está provista
de un genio creador, es más creativa que la más genial de las personas.
Los productos de alta tecnología a menudo son shanzhai de
los productos de la naturaleza. La propia creatividad de
la naturaleza responde a un proceso continuado de variación,
combinación y mutación. La evolución también sigue el modelo de la
transformación incesante y la adecuación. Occidente se
sustrae a la creatividad propia del shanzhai al considerarlo
meramente un fraude, un plagio y una ofensa a la propiedad intelectual.

El shanzhai se presenta como una forma híbrida intensiva.
El propio maoísmo chino era una forma de marxismo shanzhai.
Al no haber trabajadores ni proletariado industrial en China,
se transformaron las enseñanzas
marxistas originarias. Su capacidad de hibridación hace
que el comunismo chino se apropie del turbocapitalismo.
Los chinos no ven ninguna contradicción entre el
capitalismo y el comunismo. En realidad, la contradicción no
forma parte de las categorías del pensamiento chino.
Este se inclina más bien al "tanto esto como
lo otro" que al "esto o lo otro". El comunismo chino
muestra la misma capacidad de transformación que cualquier
obra de los grandes maestros, que se abre a la
transformación incesante. Se presenta como un cuerpo
híbrido. El antiesencialismo característico del proceso
de pensamiento chino no da lugar a ninguna fijación
ideológica. De ahí que también se pueda prever que en
China aparecerán formas shanzhai políticas híbridas y
sorprendentes. El sistema político en la China actual ya
ofrece rasgos marcados de hibridación. Con el tiempo,
el comunismo shanzhai chino probablemente mutará en
una forma política que podría denominarse democracia
shanzhai, sobre todo si el movimiento shanzhai libera
las energías antiautoritarias y subversivas.
