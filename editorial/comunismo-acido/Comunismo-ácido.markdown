# COMUNISMO ÁCIDO.
INTRODUCCIÓN INCONCLUSA?[^1]

[^1]: esta es la introducción inédita del proyecto de un futuro libro, escrita
en 2016. Es todo lo que queda de aquella obra previa a la muerte de Fisher
en enero de 2017. Las omisiones o partes inconclusas pertenecen al autor
y aparecen señaladas con este simbolo: //.

“EL FANTASMA DE UN MUNDO QUE PUEDE SER LIBRE”

> Mientras más cercana está la posibilidad de liberar al individuo de las
 restricciones justificadas en otra época por la escasez
y la falta de madurez, mayor es la necesidad de mantener y
extremar estas restricciones para que no se disuelva el orden de
dominación establecido. La civilización tiene que defenderse a sí
misma del fantasma de un mundo que puede ser libre.
[...] A cambio de las comodidades que enriquecen su vida, los
individuos venden no solo su trabajo, sino también su tiempo libre.
[...] La gente habita en edificios de apartamentos -y tiene
automóviles privados con los que ya no puede escapar a un
mundo diferente-. Tienen enormes refrigeradores llenos de comida
congelada. Tienen docenas de periódicos y revistas que exponen
los mismos ideales. Tienen innumerables oportunidades de elegir,
innumerables aparatos que son todos del mismo tipo y los
mantienen ocupadas y distraen su atención del verdadero
problema —que es la conciencia de que pueden trabajar menos
y además determinar sus propias necesidades y satisfacciones-.

Herbert Marcuse, _Eros y civilización_[^2]

[^2]: Herbert Marcuse, _Eros y civilización_, Madrid, Sarpe, 1983, pp. 95-100.

La idea central de este libro es que en los últimos cuarenta
años no se ha buscado otra cosa que exorcizar “el fantasma
de un mundo que puede ser libre”. Adoptar la perspectiva
de este mundo nos permitiría revertir algunos énfasis
en los que han insistido tantas de las batallas recientes
de la izquierda. En vez de intentar una superación del
capitalismo, deberíamos enfocarnos en lo que el capital
debe obstruir siempre: la capacidad colectiva de producir,
cuidarnos y disfrutar. En la izquierda lleyamos mucho
tiempo equivocándonos: no es que nosotras seamos
anticapitalistas, sino que el capitalismo, con sus policías
munidos de máscaras y gases lacrimógenos, con todas las
sutilezas teológicas de su economía, se propone bloquear
la emergencia de una Abundancia Roja.[^3]
//https://endefensadelsl.org/abundancia_roja.html
[^3]: Referencia al libro Abundancia roja. _Sueño y utopía en la URSS_, de Francis
Spufford (Madrid, Turner, 2011), que Fisher comenta en el volumen 2
de _K-punk_, (Buenos Aires, Caja Negra, 2020). El libro es un híbrido entre
la novela y el ensayo, en el que se relata ese instante en que la utopía del
comunismo soviético tomó por asalto la realidad. [N, del T.]

La superación del capital debe estar fundamentalmente basada en la
comprensión básica de que, lejos de “crear riqueza”, el capitalismo
siempre y necesariamente bloquea la producción de
una riqueza común.

El agente principal dedicado al exorcismo del espectro
de un mundo que puede ser libre es el proyecto que se
ha llamado neoliberalismo, pero no es el único. Y el verdadero
objetivo del neoliberalismo no fueron sus enemigos
oficiales, es decir, el monolito decadente del bloque
soviético, y las ruinas de la democracia social y el New Deal,
los cuales ya se desmoronaban por el peso de sus
propias contradicciones. Entenderemos mejor el neoliberalismo
si lo pensamos como un proyecto orientado a la
destrucción de los experimentos de socialismo democrático
y comunismo libertario que afloraban a finales de los
sesenta y principios de los setenta, al punto de volverlos
impensables.

La consecuencia más importante de la eliminación de
estas posibilidades fue la condición que yo llamé realismo
capitalista: la aceptación resignada de que no hay alternativa
al capitalismo. Si hubo un acontecimiento fundador
del realismo capitalista, se trató de la violenta demolición
del gobierno de Salvador Allende en Chile por parte
del golpe del General Pinochet, apoyado por los Estados
Unidos. Allende estaba experimentando con una forma de
socialismo democrático que ofrecía una alternativa real
tanto al capitalismo como al estalinismo. La destrucción
militar de la presidencia de Allende, y las encarcelaciones
y torturas masivas que vendrían después, son apenas el
ejemplo más violento y dramático de los esfuerzos del
capitalismo para aparecer como el único modo “realista de
organizar la sociedad. En Chile no solo se terminó con una
nueva forma de socialismo; el país, además, se transformó
en un laboratorio en el que se ensayaron las medidas
que luego se lanzarían en otros centros del neoliberalismo
(desregulación financiera, apertura de la economía
al capital extranjero, privatización). En países como los
Estados Unidos y el Reino Unido, la implementación del
realismo capitalista fue mucho más gradual y, además de
la represión, incluyó incentivos y tentaciones. Pero el
efecto final fue el mismo: extirpar la idea de un socialismo
democrático o un comunismo libertario.
Exorcizar “el fantasma de un mundo que puede ser
libre” no era una cuestión estrictamente política, sino
además cultural. Fue la cultura la que cultivó con mayor
impetu este fantasma, y la posibilidad de un mundo más
allá del trabajo duro; incluso, o quizás especialmente,
cultura que no se pensaba a sí misma como dotada de
orientación política.
Marcuse explica por qué esto es así, y el hecho de
que su influencia en los últimos años haya decaído es
en ese sentido, elocuente. _El hombre unidimensional_,
un libro que enfatiza el costado más pesimista de su trabajo
todavía es un punto de referencia, pero _Eros y civilización_,
al igual que muchos otros de sus textos, lleva mucho
tiempo fuera de circulación. La crítica que hace Marcuse
de la administración total de la vida y la subjetividad por
parte del capitalismo sigue teniendo resonancia, mietras
que su convicción de que el arte constituía un “Gran
Rechazo, la protesta contra aquello que es”[^4]
[^4] Herbert Marcuse, _El hombre unidimensional_, Barcelona, Planeta-De Agostini, 1993, p. 90.

terminó siendo considerado, en la época del realismo capitalista, como
un romanticismo pasado de moda, tan atractivo como irrelevante.
Pero Marcuse se había anticipado a estas detracciones, y la crítica de
_El hombre unidimensional_ mantiene su vigencia, porque proviene de un espacio secundario
una “dimensión estética” radicalmente incompatible con
la vida cotidiana bajo el capitalismo. Marcuse sos
que las “imágenes tradicionales de alienación artística"
asociadas con el Romanticismo no pertenecen al pasado.
En su lugar, decía él, en // una formulación, lo que ellas
"recogen y preservan en la memoria pertenece al futuro:
imágenes de una gratificación que disolvería la sociedad
que la suprime”.[^5]

[^5]: Ibid, p, 87.

El Gran Rechazo se oponía no solo al realismo capitalista,
sino al “realismo” como tal. Como escribe Marcuse,
hay “un conflicto inherente entre el arte y el realismo político”.[^6]

[^6]: Herbert Marcuse, _La dimensión estética. Crítica de la ortodoxia marxista_
Madrid, Biblioteca Nueva, 2007, p. 83.

El arte era una alienación positiva, una “negación racional”
del orden existente de las cosas, Theodor Adorno,
su predecesor de la Escuela de Frankfurt, también valoraba
la alteridad intrínseca del arte experimental. Pero en
la obra de Adorno, se nos invita a examinar infinitamente
las heridas de una vida dañada bajo el capitalismo; la idea
de un mundo más allá del capitalismo es arrojada a un
mas allá utópico. El arte solo marca nuestra distancia de
pra utopía. Por el contrario, Marcuse evoca con nitidez, y
como un proyecto inmediato, un mundo totalmente transformado. Fue
sin duda este aspecto de su trabajo lo que
hizo que Marcuse fuera aceptado con tanto entusiasmo
por algunos elementos de la contracultura de los sesenta.
El había anticipado la impugnación que implicaba la contracultura
a un mundo dominado por un trabajo sin sentido.
En _El hombre unidimensional_, Marcuse sostenía que las
liquras literarias más significativas en términos políticos
eran aquellas “que no se ganan la vida, o al menos no lo
hacen de un modo ordenado y normal”[^7].

[^7]: Herbert Marcuse, _El hombre unidimensional,_ op. tit., p. 89.

Estos personajes, y las formas de vida con las que se los asociaba, pasarían
a un primer plano con la contracultura.

De hecho, así como la obra de Marcuse presenta afinidades
con la contracultura, su análisis también da un
pronóstico de su eventual fracaso y asimilación finales.
Un tema central de _El hombre unidimensional_ era precisamente
la neutralización de las impugnaciones planteadas
por la estética. A Marcuse le preocupaba la popularización
de la vanguardia; no por un temor elitista de que la
democratización de la cultura corrompiera la pureza del
arte, sino porque la absorción del arte por parte de los
espacios administrados del comercio capitalista disimulaba su
incompatibilidad con la contracultura del capitalismo.
El ya había visto cómo la cultura capitalista habia hecho
sue el pandillero, el beatnik y la vampiresa pasaran de ser
"imágenes de otra forma de vida” a “rarezas o tipos de
la misma vida".[^8]

[^8]: Ibíd.

lo mismo pasaría con la contracultura, mucho de cuyos personajes
preferirían, de manera conmovedora, definirse como _freaks_.

En cualquier caso, Marcuse nos permite ver por qué los 60
siguen siendo un incordio para la época actual. En los últimos
años, los 60' parecen haberse transformado al mismo tiempo en un
pasado lejano, tan exótico y distante que no podríamos imaginarnos
vivir en él, y en un momento más vivído que el propio presente, una época
en la que la gente realmente vivía y las cosas realmente ocurrían. Pero
si la década nos ascecha no es por alguna confluencia de factores
irrecuperables e irrepetibles, sino porque los potenciales que se
habían materializado y empezado a democratizar
(el proyecto de una vida libre de la monotonía del trabajo )
deben ser sofocados constantemente. Para explicar por qué no nos
hemos dirigido hacia un mundo más allá del trabajo, debemos
considerar un proyecto social, político y cultural
cuyo _objetivo_ fue la produccion de escazes. Capitalismo: un
sistema que genera escazes artificial para producir escazes real; un sistema
que produce escazes real para generar escazes artificial. La escazes real
(la de recursos naturales) ascecha hoy al capitalismo, como lo Real que
su fantasía de expansión infinita debe reprimir a toda costa. La
escazes artificial (que es, fundamentalmente, una escazes de tiempo)
es necesaria, como plantea Marcuse, para distraernos de la posibilidades
inmanente de la libertad. (Por su puesto el triunfo del neoliberalismo
requirió de la cooptación del concepto de libertad. La libertad neoliberal,
evidentemente, no es libertad respecto del trabajo, sino libertad
_a través_ del trabajo).

Tal como predijo Marcuse la mayor disponibilidad de bienes y
aparatos de consumo en el Norte Global impidió ver como estos
mismos bienes y aparatos funcionaban para producir cada vez
más escasez de tiempo. Pero quizás incluso el propio Marcuse
no haya podido anticipar la capacidad del capitalismo del siglo XXI
para generar trabajo excesivo y administrar el tiempo por fuera del
trabajo remunerado. Acaso solo un futurólogo mordaz como Philip
K. Dick pudo predecir la ubicuidad banal de las comunicaciones
corporativas de hoy en día, su penetración en
practicamente todas las áreas de la conciencia y la vida
cotidiana.
“El pasado es mucho más seguro”, observa uno de los
narradores de _Por último, el corazón,_ la sátira distópica
de Margaret Atwood, “porque todo lo que tiene que ver
con él ya ha ocurrido. No se puede cambiar y por eso,
en cierto modo, no hay nada que temer”[^9]

[^9]: Margaret Atwood, _Por último, el corazón,_ Barcelona, Salamandra, 2015.

A pesar de lo que piensa el narrador de Atwood, el pasado todavía no
ha ocurrido. Constantemente hay que volver a narrar el
pasado, y el objetivo político de los relatos reaccionarios
es sofocar los potenciales que aún esperan en él, listos
para ser despertados otra vez. La contracultura de los sesenta
es hoy inseparable de su propia simulación, y la
reducción de la década a imágenes “icónicas”, “clásicos”
de la música y recuerdos nostálgicos ha neutralizado las
verdaderas promesas que habían explotado en esos años.
Los aspectos de la contracultura que podían reapropiarse
fuieron transformados en precursores del “nuevo espiritu
del capitalismo”, mientras que aquellos que eran incompatibles
con un mundo marcado por el exceso de trabajo
han sido desprestigiados como bocetos inservibles que,
según la contradictoria lógica reaccionaria, son peligrosos
o impotentes al mismo tiempo.

La contención de la contracultura parece haber confirmado la
validez del escepticismo y la hostilidad hacia
posiciones como las que proponía Marcuse. Si “la contracultura
condujo al neoliberalismo”, mejor habría sido que
Jamás hubiera existido. Pero el argumento contrario es
más convincente: el fracaso de la izquierda después de
los sesenta tuvo mucho que ver con su repudio hacia los
sueños desatados por la contracultura, y con su incapacidad
para implicarse en ellos. En la captura de estas nuevas
corrientes por parte de la nueva derecha, y en su implementación
para su propio proyecto de individualización
obligatoria y trabajo excesivo, no había nada inevitable.

¿Y si la contracultura era apenas un comienzo a los
tropezones, en lugar de lo mejor que podíamos esperar? ¿Y
si el éxito del neoliberalismo no fuera la demostración de
la inevitabilidad del capitalismo, sino un testamento de la
magnitud de la amenaza planteada por el fantasma de una
sociedad que podía ser libre?

Es con el espíritu de estas preguntas que este libro
regresa a los sesenta y los setenta. El ascenso del realismo
capitalista no podría haber ocurrido sin los relatos que
las fuerzas reaccionarias armaron acerca de esas décadas,
Volver a esos momentos nos permitirá continuar el proceso
de desmontar los relatos que el neoliberalismo tejió
sobre ellos. Y lo que es más importante, nos permitirá la
construcción de nuevos relatos.

En gran medida, repensar los setenta es más importante
que revisitar los sesenta. La década del setenta fue aquella
en la que el neoliberalismo empezó un ascenso que luego
narraría, retrospectivamente, como irresistible. Pero trabajos
recientes sobre los setenta (entre ellos _Stayin' Alive: The Last
Days of the Working Class_ [Manteniéndose viva. Los últimos
días de la clase trabajadora], de Jefferson Cowie; _When the
Lights Went Out_ [Cuando las luces se apagaron], de Andy
Beckett; y _That Option No Longer Exists_ [Esa opción ya no
existe], de John Medhurst) han subrayado que la década no
consistió únicamente en el drenaje de las posibilidades que
habían explotado en los sesenta. Los setenta fueron años
de lucha y transición, durante los cuales el significado
y el legado de la década anterior fueron campos de
batalla cruciales, Algunas de las tendencias emancipatorias
que habían emergido en los sesenta se intencificaron
y proliferaron en la década siguiente. “Para muchos británicos
politizados”, escribe Andy Beckett,
“la década _no_ fue la resaca después de los sesenta;
fue el momento en el que la gran fiesta de los sesenta
empezó de verdad”.[^10]

[^10]: Andy Beckett, _When the Lights Went Out:_ Britain in the Seventtes,
Londres, Faber and Faber, 2010, p. 209.

La exitosa Huelga de Mineros de 1972 implicó una alianza
entre mineros y estudiantes en huelga, que hacía eco
de una convergencia similar en París en 1968, cuando
los mineros usaron el campus de la Universidad
de Essex en Colchester como base en la zona de Anglia Oriental.

Al apuntar mucho más allá del relato facilista que
repite que “los sesenta condujeron al neoliberalismo”,
estas nuevas lecturas de los setenta nos permiten entender
mejor la habilidad, la feroz energía y la imaginación
improvisadora que debió desplegar la contrarrevolución
neoliberal. La instalación del realismo capitalista no
consistió de ninguna manera en la restauración de una
situación anterior: el individualismo obligatorio impuesto
por el neoliberalismo era una nueva forma de individualismo,
una definida en oposición a diferentes formas
de lo colectivo que se habían proclamado en los sesenta.
Este nuevo individualismo estaba diseñado para dejar
atrás aquellas formas colectivas, y hacer que las olvidávamos.
Por eso, recordarlas es menos un acto de rememoración
que un gesto de des-olvidar, un contra-exorcismo
del espectro de un mundo que puede ser libre.

_Comunismo ácido_ es el nombre que le dí a este espectro,
El concepto de comunismo ácido es una provocación y una promesa.
Es también una broma, pero una broma con
un propósito muy serio. Señala algo que en un momento
parecía inevitable, pero ahora aparece como imposible: la
convergencia de la conciencia de clase con la autoconciencia
feminista y la conciencia psicodélica, la fusión de nuevos
movimientos sociales con un proyecto comunista, una
estetización sin precedentes de la vida cotidiana.

El comunismo ácido refiere tanto a desarrollos históricos
reales como a una confluencia virtual que aún no ha convergido
en la realidad. Las potencialidades ejercen influencia
sin actualizarse. Las formaciones sociales reales son modeladas
por formaciones potenciales cuya actualización buscan
impedir. Se puede detectar la estampa de un “mundo que
puede ser libre” en las mismas estructuras de un mundo
del realismo capitalista que hace que la libertad imposible.

La fallecida crítica cultural Ellen Willis dijo que
transformaciones que imaginaba la contracultura
requerido “una revolución social y psíquica de una magnitud
casi inconcebible”.[^11]

[^11]: Ellen Willis, _Beginning the See the Light: Sex, Hope and Rock-and-Roll,_
Middletown, Wesleyan University Press, 1992, p. 158.

En nuestros tiempos, marcados por un desánimo mucho mayor,
es muy dificil recrear la confianza
de la contracultura de que tal “revolución social y psiquica
no solo podía ocurrir, sino que además ya estaba en vias de
revelarse. Pero necesitamos volver a una época en la que
proyecto de una liberación universal parecía inminente.



## NO MÁS LUNES POR LA MAÑANA DEPRIMENTES

Empecemos con un momento cuya aparente modestia la
vuelve aún más evocador:

> Era julio de 1966, y yo acababa de cumplir nueve años
estábamos de vacaciones en el Parque Nacional The Broads
y mi familia acababa de instalarse en el maravilloso
crucero de madera que sería nuestra casa flotante durante
la siguiente quincena. Se llamaba The Constellation y,
mientras mi hermano y yo explorábamos apurados las
camas y las claraboyas con cortinas en nuestra cabina,
construida en la proa del barco, la expectativa de lo que
nos esperaba hacía que una fuerza vital emanara de
nosotros como rayos en un sol de caricatura [...] Subí y
atravesé el barco para tomar posición en una zona pequeña
de la popa. En el camino, agarré la radio a transistores
Sanyo rosa y blanca de Sharon, mi hermana adolescente,
y la encendí. Miré hacia arriba, hacia el cielo despejado
de la tarde. Sonaba “River Deep, Mountain High”, de Ike
y Tina Turner, y una suerte de trance eufórico me
recorió el cuerpo. Bajé la mirada, y del celeste de ese cielo
ilimitado pasé a la estela cristalina y batida que creaba
nuestro barco a medida que avanzábamos. En ese
momento, “River Deep” cedió paso ante mi canción preferida del
momento: “Bus Stop”, de The Hollies. Mientras el falso
arreglo de guitarra de flamenco que marca el inicio se
mezclaba con el balbuceo profundo del motor del
Constellation, observé las aguas turbulentas y dije en voz alta,
pero para mi mismo: “Esto está pasando ahora. ESTO está
pasando ahora”[^12].

[^12]: Danny Baker _goin to sea in a Sieve_, Londres Phoenix, 2012, pp. 49-50


El relato pertenece a Going To Sea in a Sieve [Salir al
mar en un tamiz[^13]

[^13]: El titulo del libro de Danny Baker contiene una referencia  
_The jumblies_ de Edward Lear, cuyo primer verso dice “They went to sea in a
Sieve” [N. del T:] el libro de memorias del escritor y
presentador Danny Baker. No hace falta decir que esto no
es mas que una instantánea, una imagen saturada de sol
de un período que también contó con muchísima miseria
y horror. Los sesenta no fueron una utopía realizada, y las
oportunidades que esperaban a Baker no eran accesibles
para la mayor parte de la clase trabajadora. Del mismo
modo, seria fácil descartar la ensoñación de Baker como
mera nostalgia de una infancia perdida, el tipo de  
memorias doradas que podría tener prácticamente cualquiera
de cualquier período y de cualquier contexto social.
Pero hay algo muy específico en este momento,
que solo podría haber pasado ahí. Podemos enumerar
algunos de los factores que lo vuelven único:
una sensación de seguridad existencial y social que permitia
a las familias de clase trabajadora tomarse vacaciones.
El papel que jugaban las nuevas tecnologías, como la
la radio a transistores, para conectar a las personas con un afuera,
y para permitirles disfrutar el momento, un momento que, de algun modo, poseía
una _suficiencia exorbitante_; La manera en que la música genuinamente nueva
(música que meses antes habría sido inimaginable, y mucho mas años antes)
podía cristalizar esta escena e intencificarla
teñirla de una sensación de optimismo casual pero
no complaciente, una sensación de que el mundo estaba
mejorando.
Esta misma sensación de suficiencia exorbitante
palpable en “Sunny Afternoon”, de The Kinks, que Baker
tranquilamente podría haber escuchado en su radio a
transistores aquel día, o en “Im Only Sleeping”, de The
Beatles, que saldría un mes después; o en ocanciones posteriores
como “Lazy Sunday”, de Small Faces. Estas canciones contemplan
el cansancio y la pesadilla de la vida cotidiana desde
una perspectiva que flota a un margen, desde arriba o desde más allá: se trate de una calle
ajetreada vista desde la ventana de alguien que se quedó
dormido hasta tarde, y cuya cama se transforma en un bote
que se mece suavemente a la deriva; la escarcha y
la neblina de una mañana de lunes abjurada por la tarde
soleada de un domingo o que no tiene por qué terminar; o
las urgencias de la vida laboral desdeñadas con ligereza,
ocupada ahora por soñadoras de clase trabajadora que jamás
volveran a trabajar.


I'm Only Sleeping” (_“stay in bed, float upstream”_
[quedate en la cama, flota contra la corriente]) era el tema
gemelo de "Tomorrow Never Knows” (“_turn off your mind,
relax and float downstream”_ [apaga tu mente, relajate y
flota con la corriente]), la canción más autoconscientemente
psicodélica de _Revolver_. Si la letra de “Tomorrow
Never Knows”, adaptada del libro de Timothy Leary _The
Psychedelic Experience: A Manual Based on the Tibetan
Book of the Dead_ [La experiencia psicodélica. Un manual
basado en El libro tibetano de los muertos], parecía de
alugún modo trillada, la música y el diseño sonoro tenían
tada la capacidad de transportarte. “No sonaba como nada
gue jamás hubiéramos oído antes”, recuerda John Foxx
acerca de “Tomorrow Never Knows”,

pero, de alguna manera, parecia instantáneamente reco-
nocible. Sin dudas, la letra era un poco sospechosa, pero
la música, el sonido (electricidad orgánica, transmisiones
desintegradas, estaciones radiales perdidas, una misa ca-
tólica/budista acerca de un universo paralelo, como debe-
ría sentirse estar drogado) era una revelación ingrávida y
atemporal moviéndose sobre paisajes luminosos y nuevos
a una velocidad serena. La canción comunicaba, innova-
ha, infiltraba, fascinaba, elevaba: era un mapa de ruta
para el futuro. [^14]

Estos "paisajes luminosos y nuevos” eran mundos por
fuera del ámbito laboral, en los que la repetición monóto-
na del trabajo pesado cedía paso a exploraciones sin rumbo
de territorios extraños. Escuchados hoy, estos tracks
describen las condiciones necesarias para su propia pro-
ducción, es decir, el acceso a cierta modalidad de tiempo,
un tiempo que permitiera una absorción profunda.





[^14]: John Foxx, “The Golden Section: John Foxx's Favowrite Albums”, _The
Wnietus_, 3 de octubre de 2013, disponible en thequietus.com.



= 135 =














































-HoO5>o

FA

- 136 -

























El rechazo al trabajo era también un rechazo al
nalizar los sistemas de valoración que afirmaban a
existencia de uno se validaba a través del empleo
nerado. Era, en otras palabras, un rechazo a somete
una mirada burguesa que medía la vida en cérminos
éxito en los negocios. “Yo no venía de un contox
. que la gente tuviera “carreras”, escribe Danny |
Uno iba a trabajar, y solía tener distintos trabajos
momentos diferentes, todo en medio de un desor
Péro eso no te definía, ni marcaba el curso de tu
gracias a Dios.” Baker dejó la escuela, en el South
londinense, sin haberse graduado. Sin embargo, él (:
los tecaudos para que el recorrido picaresco qué lo 1
de ser asistente en una tienda de discos a productor
fanzines, periodista musical y luego presentador en
televisión y la radio, no se lea como una historia de de
venturas ni tampoco como una oda al trabajo duro
lo Rara como un relato de “mejoramiento” pequeñob
gués, sino como la historia de una desfachatez premiad

Esta “desfachatez” se basaba en saber que la satisfacción
no era algo que se esperaba que viniera del trabajo y
también en una inmensa confianza que le permitía de
preciar constantemente imperativos y ansiedades de la
burguesía. Los dos volúmenes de las mernorias de Bakor
dejan muy en claro los factores que le permitieron desa:
rrollar esta confianza: la relativa estabilidad del trabajo.
de su padre, en puertos que parecia que iban a ser el cy.
razón de la vida económica británica para siempre; el
lugar firme de su familia en una red de clase trabajadora
que complementaba los salarios con ganancias informales
Mesperadas; su adquisición de una vivienda social nueva
y con jardín. Su propia llegada a la escritura y el perio»
dismo estuvo facilitada no por algún impulso emprende»
dor, sino por una esfera pública emergente (constituida
a partir de la televisión, la radio y la prensa gráfica) en
la que se validaban y valoraban las perspectivas de lá





ase trabajadora. Pero esta era una clase trabajadora que
sa no se podía entender con los protocolos del realismo
incialista británico, y que no estaba limitada por la ca-
¡vatura de la clase dominante. Era una clase trabajadora
que ya no conocía su lugar, que había ido más allá de sí
miema. Incluso los viejos reductos de la burguesía ya no
eran más seguros. En los sesenta, Ted Hughes se había
iansformado en uno de los principales poetas británicos,
y Harold Pinter en uno de sus dramaturgos nuevos más
vatimulantes, y ambos producían obras que reflejaban la
experiencia de la clase trabajadora de maneras desafian-
les. difíciles y que, a través de la televisión, llegaban a
las salas de estar de un público masivo.
En cualquier caso, estamos lejos de la desaparición
del conflicto de clase que luego sería vitoreada poz los
ideólogos del neoliberalismo. Los acuerdos a los que ha-
bian llegado los trabajadores y el capital en sociedades
como las de los Estados Unidos y el Reino Unido acep-
laban que la clase era un rasgo permanente de la orga-
nización social. Asumían que había diferentes intereses
le clase que debían ser reconciliados, y que cualquier
gobierno de la sociedad que quisiera set efectivo, por no
decir justo, debía incluir a la clase trabajadora organiza-
da. Los sindicatos eran fuertes y la baja tasa de desem-
pleo envalentonaba sus demandas. Las expectativas de la
clase obrera eran altas; había habido victorias y habría
más en el futuro. Era fácil imaginar que la incómoda
Iregua entre el capital y la fuerza de trabajo acabaría, no
con un resurgimiento de la derecha, sino con la adopción
de políticas más socialistas, cuando no con el “comunis-
mo completo” que Nikita Kruschev pensaba que existiría
hacia 1980. Después de todo, en los Estados Unidos la
derecha estaba arrinconada, desacreditada y quizás heri-
la de muerte, o al menos eso parecía, luego del fracaso
extendido y horripilante de la Guerra de Vietnam. El “es-
tablisament” ya no infundía una deferencia automática;

COMUNISMO ÁCTOO.
INTRODUCCIÓN INCONCLUSA







187»








am»

Dm TARA.

- 138 -



































en su lugar, parecía incluso exhausto, desfasado, nercancias del sistema”.*” La cultura de masas, y la cul-
leto, a la espera de que se lo llevara alguna de las Mira dle la música en particular, era un terreno de lucha, y
vas olas culturales y políticas que erosionaban tod. so un cdominio exclusivo del capitalismo. La relación entre
viejas certezas. lomas estéticas y política era inestable y rudimentaria:

Y cuando los líderes de la cultura nueva no las lomas estéticas no “expresaban” simplemente alguna
clase obrera, parecian renegados de clase, como en iealidad capitalista ya existente, sino que anticipaban, y
de Pink Floyd, jóvenes de familias burguesas que hi de hecho producían, posibilidades nuevas. La mercantili-
rechazado sus destinos de clase y se habían identl! sou no era un punto en el que esta tensión se resolvía
“hacia abajo”, hacia afuera. Querían hacer lo que empre e inevitablemente a favor del capital: las mercan-
excepto trabajar en bancos o cualquier otro negocio, ¿las podian ser, en sí mismas, medios de propagación de

ras cuya posterior libidinización habría dejado atún sientes de rebeldía:
las mentes expandidas de los sesenta.

Las aspiraciones de la clase trabajadora no 11
una movilidad de clase cuyo dudoso premio sería la
tación gradual y reticente de los “mejores”, En «N
la nueva bohemia parecía apuntar a la eliminación
burquesía y de sus valores. De hecho, la com
que esto era algo inminente era una de las ph
de encuentro entre la contracultura y la izquie
lucionaria tradicional, las cuales en muchos 0
parecían divergir.

Desde luego, Ellen Willis sentía que las le
nantes de la política de izquierda eran incomyp

Los medios masivos ayudaban a difundir la rebelión, y el
«islema promocionaba amablemente los productos que la
Incentivaban, por la sencilla razón de que se podía ganar
dinero con los rebeldes, que también eran consumidores.
Fa monivel, la revuelta de los sesenta era una ilustración
inpertecta de la idea de Lenin de que el capitalista te
podia vender la soga con la que ahorcarlo.**

En el Reino Unido, Stuart Hall sentía una frustración
lar con gran parte de la izquierda existente, frustra-
Rue en su caso eran mucho más intensas porque se





los deseos y las ambiciones desencadenados por seideraba socialista. Pero el socialismo que quería Hall
Mientras la música que escuchaba ella hablaba soctalicmo que pudiera comprometerse con los deseos
el socialismo parecía enfocarse en la central sueños que él oía en la música de Miles Davis) debía
control estatal. La política de la contracult seso y su llegada estaba siendo bloqueada por figu-
al capitalismo, pensaba Willis, pero esto no bamto de la izquierda como de la derecha.

rechazo directo de todo aquello que se produr La polera figura obstaculizadora de la izquierda era
po capitalista. Willis sostenía una “polémik tensor complaciente de los sindicatos o de la social-
nociones estándares de la izquierda acerca pracla de la Guerra Fría: orientado hacia el pasado,
mo avanzado” y rechazaba solo parcialmente biálico. resignado a la “inevitabilidad” del capitalis-
que “la economía de consumo nos convierte e ske juleresado en preservar los ingresos y el estatus
de las mercancías, que la función de lor

es manipular nuestras fantasías, de mi Wills. Meginning to See the Light, op. cit.. p. avi.

una equivalencia entre satisfacción y la K

COMUNISMO ÁCIDO.
INTRODUCCIÓN INCONCLUSA

- 139 -




==

232 mMTmAii

- 140 -





















de los hombres blancos que en expandir la batalla para
incluir //, esta figura se define por sus concesiones y
eventual fracaso.

La otra figura, que quisiera llamar el Superyó Leninist
Estricto, se define por su rechazo absoluto de toda (0
cesión. Según Freud, el superyó se caracteriza por la 14!
turaleza excesiva, tanto en términos cuantitativos com
cualitativos, de sus demandas: hagamos lo que hagamo
Jamás es suficiente. El Superyó Leninista Estricto exigo un
ascetismo militante. El militante debe dedicarse exclusl
vamente al acontecimiento revolucionario, y COmpromo-
Lerse de manera obstinada con los medios necesarios pi
provocarlo, El Superyó Leninista Estricto es indiferente
al sufrimiento y hostil al placer. La fobia a la música q
tenía Lenin es, en este sentido, ilustrativa: “No puedo
escuchar música muy seguido. Te afecta los nervios, har
que quieras decir cosas bonitas y estúpidas, y acariciorle
la cabeza a alguien que fue capaz de crear algo tan he
so mientras vivia en este infierno vil”.

Si los complacientes líderes del trabajo organizado
se comprometen con el statu quo, el Superyó Leninisla
Estricto apuesta todo por un mundo absolutamente diles
rente. Era este mundo post-revolucionario lo que salvarl
al leninista, y era desde la perspectiva de este mundo q
él se juzgaba a sí mismo. Mientras tanto, es legítimo
incluso necesario cultivar una indiferencia hacia el sultk
miento actual: podemos y debemos caminar esquivando
a la gente sin techo, porque la caridad solo obstruye
llegada de la revolución.

Pero esta revolución tenía poco en común con
“revolución social y psíquica de una magnitud casi Im
concebible” que Ellen Willis creía encontrar sembrada
los sueños de la contracultura. La revolución tal como
concebía ella sería al mismo tiempo más inmediata (aloe
taría fundamentalmente cómo se organizaban el cuid do
y las tareas domésticas) y dotada de un alcance mayor!

COMUNISMO ÁCIDO.
TNTRODUCCIÓN INCONCLUSA

mundo transformado sería inimaginablemente más extra-
no que cualquier cosa que hubiera proyectado el marxis-
wo leninismo. La contracultura consideraba que ya estaba
produciendo espacios en los que esta revolución podía ser
experimentada.

Para tener una idea de cómo eran esos espacios, no
tay mejor manera que escuchar “Psychedelic Shack”, de
lhe Temptations, lanzado en diciembre de 1969, El grupo
iepresenta el papel de muchachos ingenuos y apasiona-
dos que acaban de regresar de una especie de País de las
Maravillas: “Strobe lights flashin' way till after sundown/
Mere ain't no such thing as time? Incense in the air
[Luces estroboscópicas que iluminan mucho más allá del
acaso/ El tiempo no existe/ Incienso en el aire].
